<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
      <title>Kutchina</title>
      <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
      <link rel="icon" href="img/favicon.ico" type="image/x-icon">
      <!--<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">
         <link href="https://fonts.googleapis.com/css?family=Montserrat:100i,400,500,600,700,900" rel="stylesheet"/>-->
      <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
      <link href="css/font-awesome.css" rel="stylesheet">
      <!--Style Plugin Start Here-->
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="css/owl.carousel.min.css" rel="stylesheet" type="text/css">
      <link href="css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
      <!--<link href="css/hover.css" rel="stylesheet" type="text/css"/>-->
      <link href="css/custom.css" rel="stylesheet" type="text/css"/>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async
         src="https://www.googletagmanager.com/gtag/js?id=UA-106489867-4"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         gtag('config', 'UA-106489867-4');
         gtag('config', 'AW-865401263');
      </script>
      <script>
         gtag('config', 'AW-865401263/OOgTCMy4xXoQr_PTnAM', {
         'phone_conversion_number': '1800 419 7333'
         });
      </script>
      
      <!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window, document,'script',

'https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '308963747007282');

fbq('track', 'PageView');

</script>

<noscript><img height="1" width="1" style="display:none"

src="https://www.facebook.com/tr?id=308963747007282&ev=PageView&noscript=1"

/></noscript>

<!-- End Facebook Pixel Code -->
   </head>
   <body>
      <div class="logo-section">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo"> <a href="https://kutchina.com/" target="_blank" style="display:inline-block;"><img src="img/logo.png" alt=""/></a> </div>
               <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 tollfree"> <h2> TOLL FREE NO:<a href="tel:18004197333" target="_blank"> 1800 419 7333</a> </h2></div>-->
            </div>
         </div>
      </div>
      <div class="banner"><img src="img/banner.jpg" alt="" /></div>
      <!--Section Start Here-->
      <div class="section1">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner-text">
                  <div class="enquire_now1">
                     <button type="button" class="enq_btn eb" data-toggle="modal" data-target="#myModal">Enquire Now <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                  </div>
                  <!-- Modal -->
                  <div id="myModal" class="modal fade" role="dialog">
                     <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Enquire Now</h4>
                           </div>
                           <div class="modal-body">
                              <form name="mod_frm" id="mod_frm" action="lead.php" method="POST">
                                 <div class="form-group_top">
                                    <input type="text" name="nme" class="form-control-top" id="nme" placeholder="Name*" onkeyup="nameValidate();"/>
                                    <p id="nm_err" style="color:red; display: none; text-align: left;">Enter your name</p>
                                 </div>
                                 <div class="form-group_top">
                                    <input type="text" name="phn" class="form-control-top" id="phn" placeholder="Phone*"/>
                                    <p id="ph_err" style="color:red; display: none; text-align: left;">Phone number is required</p>
                                    <p id="ph_err1" style="color:red; display: none; text-align: left;">Enter valid phone number</p>
                                 </div>
                                 <div class="form-group_top">
                                    <input type="text" name="pin" class="form-control-top" id="pin" placeholder="Pin Code*" maxlength="6" minlength="6" onkeyup="numValidate();"/>
                                    <p id="pin_err" style="color:red; display: none; text-align: left;">Pincode is required</p>
                                    <p id="pin_err1" style="color:red; display: none; text-align: left;">Invalid Pin Code. Please check again & add correct Pin Code</p>
                                 </div>
                                 <input type="hidden" name="utm_src" id="utm_src" value="<?php echo $_GET['utm_source']; ?>">
                                 <input type="hidden" name="utm_med" id="utm_med" value="<?php echo $_GET['utm_medium']; ?>">
                                 <input type="hidden" name="utm_camp" id="utm_camp" value="<?php echo $_GET['utm_campaign']; ?>">
                                 <input type="hidden" name="utm_refer" id="utm_refer" value="<?php echo $_SERVER['HTTP_REFERER']; ?>">
                                 <input type="hidden" name="ip_add" id="ip_add" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
                                 <div class="form-group_top">
                                    <input type="button" value="Submit" class="inner_inp_sbmt2" onclick="check();">
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="section2">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 kutchina kutchina_new">
                  <h2>Why Kuchina Water Healthifier?</h2>
                  <ul class="kutchina_list">
                     <li>
                        <span><img src="img/integrated-safe.png" /></span>
                        <h2>10 Stages Filtration Technology </h2>
                        <p>Protects from bacteria and chemicals </p>
                     </li>
                     <li>
                        <span><img src="img/antioxidant-technology.png" /></span>
                        <h2>Antioxidant Technology</h2>
                        <p>Increases your blood circulation &amp; reduces the ageing effects.</p>
                     </li>
                     <li>
                        <span><img src="img/mineral_enriched.png" /></span>
                        <h2>Mineral Enriched</h2>
                        <p>Adding essential minerals to help improve immunity</p>
                     </li>
                     <li>
                        <span><img src="img/alkaline_water.png" /></span>
                        <h2>Alkaline Water</h2>
                        <p>pH balance technology to ensure alkaline water</p>
                     </li>
                     <li>
                        <span><img src="img/backwash-technology.png" /></span>
                        <h2>Backwash Technology</h2>
                        <p>Easy and quick cleaning without any professional helping hand</p>
                     </li>
                  </ul>
               </div>
               <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="owl-carousel" id="owl-carousel_1">
                    <div class="item slide"> <img src="img/slider_img_1.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_2.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_3.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_4.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_6.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_7.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_8.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_9.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_10.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_11.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_12.jpg" alt="" /> </div>
                    <div class="item slide"> <img src="img/slider_img_13.jpg" alt="" /> </div>
                  </div>
                  </div>-->
            </div>
         </div>
      </div>
      <!--Section Ends Here-->
      <!-- section3 start-->
      <div class="section3">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 watch-video">
                  <h2>Watch Video</h2>
                  <div class="video-frame">
                     <iframe width="520" height="408" src="https://www.youtube.com/embed/5vnDTWT2Cr0?playlist=5vnDTWT2Cr0&amp;loop=1&amp;rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
                  <div class="video-frame-mobile">
                     <iframe width="100%" height="408" src="https://www.youtube.com/embed/5vnDTWT2Cr0?playlist=5vnDTWT2Cr0&amp;loop=1&amp;rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
                  <div class="video_box"><img src="img/video.png" alt="" /></div>
               </div>
            </div>
         </div>
      </div>
      <div class="section4">
         <div class="container">
            <div class="row">
               <div class="testimonial">
                  <h2>Customer Stories</h2>
                  <div class="owl-carousel" id="owl-carousel_2">
                     <div class="item slide">
                        <div class="slider_inn_text">
                           <div class="user_pic"><img src="img/user_t.png" alt=""></div>
                           <div class="user_text">
                              <h4>Anubhav Sarkar</h4>
                              <div class="quote"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                              <p>We have been using Kutchina's products for over a decade now. The quality of all their products and
                                 the quality of service, both are exceptionally great. Also, when it comes to modular kitchen, their designs and execution are at par with international standards.
                              </p>
                              <div class="quote2"><i class="fa fa-quote-right" aria-hidden="true"></i></div>
                           </div>
                        </div>
                     </div>
                     <div class="item slide">
                        <div class="slider_inn_text">
                           <div class="user_pic"><img src="img/user_tb.png" alt=""></div>
                           <div class="user_text">
                              <h4>Jaisurya Banerjea</h4>
                              <div class="quote"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                              <p>My family has been a loyal Kutchina fan for a long time now. We have two chimney units installed since several years with excellent after sales service being the hallmark of Kutchina's after sales commitment.<br><br>
                                 What my wife and grandmother really love is the unique one touch dry auto clean feature that cleans all the oil deposited on the inner housing, removing the need for us to clean the appliance manually. One more reason to be proud, it's an Indian homegrown brand that give us features and service that Foreign brands would find hard to beat. Keep up the great work folks!  
                              </p>
                              <div class="quote2"><i class="fa fa-quote-right" aria-hidden="true"></i></div>
                           </div>
                        </div>
                     </div>
                     <div class="item slide">
                        <div class="slider_inn_text">
                           <div class="user_pic"><img src="img/user_tp.png" alt=""></div>
                           <div class="user_text">
                              <h4>Priyanka Datta</h4>
                              <div class="quote"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                              <p>I am really impressed with the Product and it's after sale service..Few months back when I purchased this chimney i never thought it would keep my kitchen so clean & fresh.. Now being a homemaker i feel happy and proud to express my excitement that I have made the right decision to select the Brand name KUTCHINA</p>
                              <div class="quote2"><i class="fa fa-quote-right" aria-hidden="true"></i></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Section Start Here-->
      <!--Section Start Here-->
      <div class="section5">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 certificate">
                  <h2>Absolutely true!</h2>
                  <p>We make the best performing Water Purifiers with easiest cleaning solutions.</p>
               </div>
               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><img src="img/certificate.png" alt="" /></div>
            </div>
         </div>
      </div>
      <!--Section Ends Here-->
      <!--Section Start Here-->
      <div class="section6">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 services_text">
                  <h2>Other Services </h2>
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <ul class="services">
                     <li><span><i class="fa fa-check" aria-hidden="true"></i></span>After-sales Services</li>
                     <li><span><i class="fa fa-check" aria-hidden="true"></i></span>AMC</li>
                     <li><span><i class="fa fa-check" aria-hidden="true"></i></span>Widespread Branch Offices</li>
                     <li><span><i class="fa fa-check" aria-hidden="true"></i></span>Well-distributed Service Centers</li>
                     <li><span><i class="fa fa-check" aria-hidden="true"></i></span>Toll-free Assistance Contact</li>
                     <li><span><i class="fa fa-check" aria-hidden="true"></i></span>Widespread Exclusive Stores</li>
                  </ul>
                  <div class="enquire_now">
                     <button type="button" class="enq_btn eb" data-toggle="modal" data-target="#myModal">Enquire Now <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Section Ends Here-->
      <!--Section 7 Start Here-->
      <div class="section7">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="social_link"> <a href="https://www.facebook.com/kutchinaconnect/" target="_blank" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="https://twitter.com/kutchinaconnect" target="_blank" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a href="https://www.linkedin.com/company/kutchinaconnect/" target="_blank" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a> <a href="https://www.youtube.com/channel/UCAndsH2MCkpohS36lJafNGA" target="_blank" class="instagram"><i class="fa fa-youtube" aria-hidden="true"></i></a> </div>
                  <p class="copy_right">
                     Copyright @ 
                     <script>
                        var theDate=new Date()
                        document.write(theDate.getFullYear());
                     </script> Kutchina Modular Kitchen. All Rights Reserved
                  </p>
               </div>
            </div>
         </div>
      </div>
      <!--Section 7 Ends Here-->
      <div class="floating">
         <a class="enq_btn2 enq_btn" data-toggle="modal" data-target="#myModal"><img src="img/enquire_btn.jpg" alt="" /></a>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/owl.carousel.js" type="text/javascript"></script>
      <script type="text/javascript">
         $(document).ready(function() {
           $('#owl-carousel_1').owlCarousel({
         	loop: true,
         	margin: 4,
         	autoplay: true,
                autoPlaySpeed: 5000,
                autoPlayTimeout: 6000,
         	responsiveClass: true,
         	responsive: {
         	  0: {
         		items: 1,
         		nav: true
         	  },
         	  600: {
         		items: 1,
         		nav: false
         	  },
         	  1000: {
         		items: 1,
         		nav: true,
         		loop: true,
         		margin: 4
         	  }
         	}
           })
         })
      </script>
      <script type="text/javascript">
         $(document).ready(function() {
           $('#owl-carousel_2').owlCarousel({
         	loop: true,
         	margin: 4,
         	responsiveClass: true,
         	responsive: {
         	  0: {
         		items: 1,
         		nav: true
         	  },
         	  600: {
         		items: 2,
         		nav: false
         	  },
         	  1000: {
         		items: 1,
         		nav: true,
         		loop: true,
         		margin: 4
         	  }
         	}
           })
         })
      </script>
      <script>
         function nameValidate() {
           var element = document.getElementById('nme');
           element.value = element.value.replace(/[^a-zA-Z_ ]+/, '');
         };
         function numValidate() {
           var element = document.getElementById('pin');
           element.value = element.value.replace(/[^0-9]+/, '');
         };
         
           function check()
           {
             var name= jQuery("#nme").val();
             var ph= jQuery("#phn").val();
             var pin= jQuery("#pin").val();
             var phone_pattern = /^[\d\s+-]+$/;
             var zipRegex = /^[0-9]+$/;
            if(name == '' )
             {
                 jQuery("#nm_err").show();
                 jQuery("#nme").focus();
                 return false;        
             }
             else if(ph == '')
             {
                 jQuery("#nm_err").hide();
                 jQuery("#ph_err").show();
                 jQuery("#ph_err1").hide();
                 jQuery("#phn").focus();
                 return false;
                 
             }
             else if(!phone_pattern.test(ph))
             {
                 jQuery("#nm_err").hide();
                 jQuery("#ph_err").hide();
                 jQuery("#ph_err1").show();
                 jQuery("#phn").focus();
                 return false;
             
             }
             else if(ph.length < 10 || ph.length > 10 )
             {
                 jQuery("#nm_err").hide();
                 jQuery("#ph_err").hide();
                 jQuery("#ph_err1").show();
                 jQuery("#phn").focus();
                 return false;    
             }
             else if(pin == '' )
             {
                 jQuery("#nm_err").hide();
                 jQuery("#ph_err").hide();
                 jQuery("#ph_err1").hide();
                 jQuery("#pin_err").show();
                 jQuery("#pin").focus();
                 return false;        
             }
             else if ((pin.length)< 6 || (pin.length)> 6 )
             {    
                 jQuery("#nm_err").hide();
                 jQuery("#ph_err").hide();
                 jQuery("#ph_err1").hide();
                 jQuery("#pin_err").hide();
                 jQuery("#pin_err1").show();
                 jQuery("#pin").focus();
                 return false;
             }
             else
             {
                 jQuery("#nm_err").hide();
                 jQuery("#ph_err").hide();
                 jQuery("#ph_err1").hide();
                 jQuery("#pin_err").hide();
                 jQuery("#pin_err1").hide();
                 document.mod_frm.submit();        
             }
               
           }
         
         
         $('.enq_btn').click(function(){
           $('#mod_frm')[0].reset();
           $("#nm_err").hide();
           $("#ph_err").hide();
           $("#ph_err1").hide();
           $("#pin_err").hide();
           $("#pin_err1").hide();
         });
         
      </script>
   </body>
</html>