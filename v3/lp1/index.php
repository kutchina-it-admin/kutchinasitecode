<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kuchina</title>
<link href="css/style.css" rel="stylesheet" type="text/css" media="screen" />
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
<link rel="icon" href="images/favicon.png" type="image/x-icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    function checkValidation()
    {
          //alert("a");
          var name= jQuery("#name").val();
          var phone= jQuery("#phone").val();
		      var pincd= jQuery("#pincd").val();
		      var budget= jQuery("#budget").val();  
		      //var exp_time= jQuery("#exp_time").val();            
          var utm_src= jQuery("#utm_src").val();          
          var utm_med= jQuery("#utm_med").val();          
          var utm_camp= jQuery("#utm_camp").val();          
          var utm_refer= jQuery("#utm_refer").val();          
          var ip_add= jQuery("#ip_add").val();          
         
          //var email_pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
          var phone_pattern = /^[\d\s+-]+$/;
          var zipRegex = /^[0-9]+$/;
        
         if(name == '' )
          {          
          	alert("Enter your name");
            jQuery("#name").focus();
            return false;              
          }
          else if(phone == '')
          {
          	//alert("aa");
            alert("Enter your phone");
            jQuery("#phone").focus();
            return false;              
          }
          else if(!phone_pattern.test(phone))
          {
          	alert("Invalid phone");
            jQuery("#phone").focus();
            return false;          
          }
          else if(phone.length < 10 || phone.length > 10 )
          {   
          	alert("Invalid phone length");
            jQuery("#phone").focus();
            return false;          
          }
		      else if(pincd == '' )
          {         
          	alert("Enter your pin code");
          	jQuery("#pincd").focus();
          	return false;
          }
          else if ((pincd.length)< 6 || (pincd.length)> 6 )
          {
            alert("Invalid Pin Code. Please check again & add correct Pin Code");
            jQuery("#pincd").focus();
            return false;
          }
          else if (!zipRegex.test(pincd))
          {
            alert("Pin Code should be numbers only");
            jQuery("#pincd").focus();
            return false;
          }
          else if(budget == '')
          {
          	//alert("aa");
            alert("Select Budget");
            jQuery("#budget").focus();
            return false;              
          }
          /*else if(exp_time == '')
          {
          	//alert("aa");
            alert("Select Expected Time Of Purchase");
            jQuery("#exp_time").focus();
            return false;              
          }*/
          else
          {
        
            $.ajax({
                url: "https://docs.google.com/forms/d/e/1FAIpQLSe6Whet9V2I_UC-R0QrcWWq-z6Q-E42sTUrzyfO3EYz3n2LBA/formResponse",
                 data: {
                  "entry.1311384981": name,
                  "entry.572674096": phone,
			            "entry.457287131": pincd,
                  "entry.539924973": budget,
                  "entry.806141299": utm_src,
                  "entry.2128980076": utm_med,
                  "entry.664607976": utm_camp,
                  "entry.1606526349": utm_refer,
                  "entry.1915854911": ip_add,
                },
                type: "POST",
                crossDomain: true,
                dataType: "json",
                statusCode: {
                  0: function() {
                       console.log('Failure');
                       window.location.href='thank-you.html';
                  },
                  200: function() {
                       console.log('Success');
                       window.location.href='thank-you.html';
                  }
                },
                 // success: success
             });
              
          }
    }
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async
src="https://www.googletagmanager.com/gtag/js?id=UA-106489867-4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-106489867-4');
gtag('config', 'AW-865401263');
</script>

<script>
gtag('config', 'AW-865401263/OOgTCMy4xXoQr_PTnAM', {
'phone_conversion_number': '1800 419 7333'
});
</script>


<script>
(function(h,e,a,t,m,p) {
m=e.createElement(a);m.async=!0;m.src=t;
p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
})(window,document,'script','https://u.heatmap.it/log.js');
</script>

</head>
<body>

  <!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Choose the best chimney for me</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         <form name="hdr_frm" method="post" action="">
          <div class="form-group">
            <label for="name">Your Name:</label>
            <input type="text" class="form-control" id="name" name="name">
          </div>
          <div class="form-group">
            <label for="phone">Your Mobile Number:</label>
            <input type="text" class="form-control" id="phone" name="phone">
          </div>
            <div class="form-group">
            <label for="phone">Pin Code:</label>
            <input type="text" class="form-control" id="pincd" name="pincd">
          </div>
          <div class="form-group">
            <label for="budget">Budget:</label>
            <select class="form-control" id="budget" name="budget">
                <option value="">-Please Select-</option>
                <option value="9000 - 12000">9000 - 12000</option>
                <option value="13000 - 18000">13000 - 18000</option>
                <option value="19000 - 25000">19000 - 25000</option>
                <option value="Above 25,000">Above 25,000</option>
            </select>
          </div>
          <!--<div class="form-group">
            <label for="exp_time">Expected Time Of Purchase:</label>
            <select class="form-control" id="exp_time" name="exp_time">
                <option value="">-Please Select-</option>
                <option value="Immediately">Immediately</option>
                <option value="3 to 6 Months">3 to 6 Months</option>
                <option value="6 Months to 1 Year">6 Months to 1 Year</option>
            </select>
          </div>-->
          <input type="hidden" name="utm_src" id="utm_src" value="<?php echo $_GET['utm_source']; ?>">
          <input type="hidden" name="utm_med" id="utm_med" value="<?php echo $_GET['utm_medium']; ?>">
          <input type="hidden" name="utm_camp" id="utm_camp" value="<?php echo $_GET['utm_campaign']; ?>">
          <input type="hidden" name="utm_refer" id="utm_refer" value="<?php echo $_SERVER['HTTP_REFERER']; ?>">
          <input type="hidden" name="ip_add" id="ip_add" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">

          <button type="button" class="btn btn-primary"  onclick="return checkValidation();">Submit</button>
         </form>
        </div>
        
        <!-- Modal footer -->
      
        
      </div>
    </div>
  </div>
  
</div>

<div class="sec01 sec02">
<div class="wrapper">
  <div class="leftsec">
    <div class="logo"><a href="https://www.kutchina.com/" target="_blank"><img src="images/logo.png" alt="" /></a></div>
    
    <img src="images/main-t1.png" alt="" /> <h3 class="toll">TOLL FREE NO: <a href="tel:1800 419 7333">1800 419 7333</a></h3>
  </div>
  <div class="leftsec i-chimney"><img src="images/products1.png" alt="" /></div>
  
  <div><a href="#" class="btt" data-toggle="modal" data-target="#myModal"><img src="images/but.png"  alt="" /></a></div>
</div>
</div>
<div class="wrapper">
  <div class="mainwhy"><img src="images/whytt.png" alt="" /></div>
</div>
<div>
  <div class="header-tit">
    <h2> <img src="images/arr1.png" class="arrin" alt="" /><br />
      Pioneer in<br />
      <span>Intelligent Auto Clean Technology</span><br />
      <p>Hassle-free smart cleaning</p>
      <img src="images/icon01.png" width="80" alt="" class="iconin" /> </h2>
  </div>
  <div class="mainsd"><img src="images/autoclean2.png" alt="" /></div>
</div>
<div>
  <div class="header-tit">
    <h2> <img src="images/arr1.png" class="arrin" alt="" /><br />
      <span>Advanced Microprocessor</span><br />
      <p>Powering overall smart execution</p>
      <img src="images/icon02.png" width="80" alt="" class="iconin" /> </h2>
  </div>
  <div class="mainsd"><img src="images/autoclean6.png" alt="" /></div>
</div>
<div>
  <div class="header-tit">
    <h2> <img src="images/arr1.png" class="arrin" alt="" /><br />
      <span>Luxurious Design</span><br />
      <p>Making your kitchen look grand and elegant</p>
      <img src="images/icon04.png" width="80" alt="" class="iconin" /> </h2>
  </div>
  <div class="mainsd"><img src="images/autoclean1.png" alt="" /></div>
</div>
<!-- <div>
  <div class="header-tit">
    <h2> <img src="images/arr1.png" class="arrin" alt="" /><br />
      <span>Pricing</span><br />
      <p>Providing value for money, always</p>
      <img src="images/icon03.png" width="80" alt="" class="iconin" /> </h2>
  </div>
  <div class="mainsd"><img src="images/sal4.jpg" alt="" /></div>
</div> -->
<div>
  <div class="header-tit">
    <h2> <img src="images/arr1.png" class="arrin" alt="" /><br />
      <span>After-sales Services</span><br />
      <p>Complete assistance within 48 hours from enquiry</p>
      <img src="images/icon05.png" width="80" alt="" class="iconin" /> </h2>
  </div>
  <div class="mainsd"><img src="images/sal6.jpg" alt="" /></div>
</div>
<div>
  <div class="vid_sec">
    <div class="section3">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 watch-video">
            <h2>Watch Video</h2>
            <div class="video-frame">              
              <iframe src="https://www.youtube.com/embed/OE_2o_aqeDU?playlist=mElenk7dxfk&amp;loop=1&amp;rel=0" width="520" height="408" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
            </div>
            <div class="video-frame-mobile">              
              <iframe src="https://www.youtube.com/embed/OE_2o_aqeDU?playlist=mElenk7dxfk&amp;loop=1&amp;rel=0" width="100%" height="408" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
            </div>
            <div class="video_box"><img src="images/video.png" alt="" /></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="footersec">
  <div class="wrapper">
    <div class="leftsec"> <img src="images/foottext.png"alt="" /></div>
    <div class="leftsec fott"><a href="#" data-toggle="modal" data-target="#myModal"><img src="images/but-botm.png"  alt="" /></a></div>
  </div>
</div>
<div class="footerin">
  <div class="wrapper">
    <div class="leftsec">
      <p>© <?php echo(date('Y'));?> Kutchina. All rights reserved.</p>
    </div>
    <div class="leftsec footn">
      <div class="flow">Follow us on</div>
      <ul>
        <li><a href="https://www.facebook.com/kutchinaconnect/" target="_blank" ><img src="images/soc1.png" class="socl"  alt="" /></a></li>
        <li><a href="https://twitter.com/kutchinaconnect" target="_blank" ><img src="images/soc2.png" class="socl"  alt="" /></a></li>
        <li><a href="https://www.youtube.com/channel/UCAndsH2MCkpohS36lJafNGA" target="_blank"><img src="images/soc3.png" class="socl" alt="" /></a></li>
        <li><a href="https://plus.google.com/+KutchinaConnect" target="_blank"><img src="images/soc4.png" class="socl"  alt="" /></a></li>
      </ul>
    </div>
  </div>
</div>
</body>
</html>
