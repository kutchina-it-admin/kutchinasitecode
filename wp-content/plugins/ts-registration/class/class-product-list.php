<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}



class Product_Registration_List extends WP_List_Table
{

    private $status = array( 0 => "Pending", 1 => "Approved", 2 => "Rejected");

    /** Class constructor */
    public function __construct()
    {

        parent::__construct(['singular' => __('Registration', 'tsregistration') , //singular name of the listed records
        'plural' => __('Registrations', 'tsregistration') , //plural name of the listed records
        'ajax' => true
        //does this table support ajax?
        ]);

    }

    /**
     * Retrieve registrations data from the database
     *
     * @param int $per_page
     * @param int $page_number
     *
     * @return mixed
     */
    public static function get_registrations($per_page = 5, $page_number = 1)
    {

        global $wpdb;

        //$sql = "SELECT * FROM " . $wpdb->prefix . "portal_user_details";
        $sql = "SELECT  p.*,u.*,c.product_category, DATE_FORMAT(p.`created`, '%d/%m/%Y %H:%i:%S') as CreatedDate FROM `" . $wpdb->prefix . "portal_user_products` p 
        left join `" . $wpdb->prefix . "portal_user_details` u on p.`customer_phone` = u.`customer_phone`
        left join `" . $wpdb->prefix . "portal_product_catagory` c on p.`product_code` = c.`product_code`";

        if (! empty( $_GET['mishaDateFrom'] ) || ! empty( $_GET['mishaDateTo'] ) )
         {
            $sql .=  " WHERE DATE(p.`created`) BETWEEN '".sanitize_text_field( $_GET['mishaDateFrom'])."' AND '".sanitize_text_field( $_GET['mishaDateTo'])."'";
        }

        if (!empty($_REQUEST['orderby']))
        {
            $sql .= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql .= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
        }
        else
        {
            $sql .= ' ORDER BY `id`';
            $sql .= ' DESC';
        }

        $sql .= " LIMIT $per_page";
        $sql .= ' OFFSET ' . ($page_number - 1) * $per_page;
/*echo $sql;
exit();*/
        $result = $wpdb->get_results($sql, 'ARRAY_A');

        return $result;
    }

    public static function get_registration($id)
    {

        global $wpdb;

        $sql = "SELECT * FROM `" . $wpdb->prefix . "portal_user_products` p left join `" . $wpdb->prefix . "portal_user_details` u on p.`customer_phone` = u.`customer_phone` where `id` = $id";

        $result = $wpdb->get_row($sql, 'ARRAY_A');

        return $result;
    }

    /**
     * Delete a registration record.
     *
     * @param int $id registration ID
     */
    public static function delete_registration($id)
    {
        global $wpdb;

        $wpdb->delete($wpdb->prefix . "ts_registration", ['id' => $id], ['%d']);
    }

    /**
     * Returns the count of records in the database.
     *
     * @return null|string
     */
    public static function record_count()
    {
        global $wpdb;

        $sql = "SELECT COUNT(*) FROM " . $wpdb->prefix . "portal_user_details";

        return $wpdb->get_var($sql);
    }

    /** Text displayed when no registration data is available */
    public function no_items()
    {
        _e('No registrations avaliable.', 'tsregistration');
    }

    /**
     * Render a column when no column specific method exist.
     *
     * @param array $item
     * @param string $column_name
     *
     * @return mixed
     */
    public function column_default($item, $column_name)
    {
        switch ($column_name)
        {
            case 'cb':
            case 'customer_phone':
            case 'customer_name':
            case 'customer_email':
            case 'street':
            case 'city':
            case 'state':
            case 'zipcode':
            case 'product_serial_number':
            case 'product_name':
            case 'product_code':
            case 'product_category':
            case 'CreatedDate':
            case 'invoice_file':
            case 'created':
            case 'status':
            case 'category':
                return $item[$column_name];
            default:
                return print_r($item, true); //Show the whole array for troubleshooting purposes
                
        }
    }

    /**
     * Render the bulk edit checkbox
     *
     * @param array $item
     *
     * @return string
     */
    function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['id']);
    }

    /**
     * Method for name column
     *
     * @param array $item an array of DB data
     *
     * @return string
     */
    function column_name($item)
    {

        $delete_nonce = wp_create_nonce('sp_delete_registration');

        $title = $item['name'] . '<br/>' . $item['email'];

        return $title;
    }

    /**
     * Method for name column
     *
     * @param array $item an array of DB data
     *
     * @return string
     */
    function column_status($item)
    {

        return $this->status[$item['status']];
    }

    /**
     * Method for name column
     *
     * @param array $item an array of DB data
     *
     * @return string
     */
    function column_answer($item)
    {

        $delete_nonce = wp_create_nonce('sp_delete_registration');

        $title = '';

        if($item['status'] == 0)
        {
            
            $linktext = "Approve/Reject";

            $actions = ['reply' => sprintf('<a href="?page=%s&action=%s&id=%s&_wpnonce=%s">' . $linktext . '</a>', esc_attr($_REQUEST['page']) , 'approve', absint($item['id']) , $delete_nonce) ];

            return $title . $this->row_actions($actions);
        }
        return 'No Edit Allowed';
        
    }


    /**
     * Method for name column
     *
     * @param array $item an array of DB data
     *
     * @return string
     */
    function column_invoice_file($item)
    {

        $title = '<a target="blank" href="' . $item['invoice_file'] . '">View Invoice</a>';
        
        return $title;
    }

    /**
     *  Associative array of columns
     *
     * @return array
     */
    function get_columns()
    {
        $columns = ['cb' => '<input type="checkbox" />', 
         'CreatedDate' => __('Create date', 'tsregistration') ,
        'customer_phone' => __('Phone', 'tsregistration') ,
         'customer_name' => __('Name', 'tsregistration') ,
         'customer_email' => __('Email', 'tsregistration') ,
         'street' => __('street', 'tsregistration') ,
         'city' => __('city', 'tsregistration') ,
         'state' => __('state', 'tsregistration') ,
         'zipcode' => __('zipcode', 'tsregistration') ,
         'product_serial_number' => __('Product serial number', 'tsregistration') ,
         'product_name' => __('Product name', 'tsregistration') ,
         'product_code' => __('Product code', 'tsregistration') ,
         'product_category' => __('Product', 'tsregistration') ,
         'invoice_file' => __('Invoice file', 'tsregistration') ,
         'status' => __('Status', 'tsregistration') ,
         'answer' => __('Action', 'tsregistration') ,
         ];

        return $columns;
    }

    /**
     * Columns to make sortable.
     *
     * @return array
     */
    public function get_sortable_columns()
    {
        $sortable_columns = array(
            'customer_name' => array(
                'customer_name',
                true
            ) ,
            'status' => array(
                'status',
                false
            )
        );

        return $sortable_columns;
    }

    /**
     * Returns an associative array containing the bulk action
     *
     * @return array
     */
/*    public function get_bulk_actions()
    {
        $actions = ['bulk-delete' => 'Delete'];

        return $actions;
    }*/


    function extra_tablenav( $which ) {
        if ( $which == "top" ) : 
        $from = ( isset( $_GET['mishaDateFrom'] ) && $_GET['mishaDateFrom'] ) ? $_GET['mishaDateFrom'] : '';
        $to = ( isset( $_GET['mishaDateTo'] ) && $_GET['mishaDateTo'] ) ? $_GET['mishaDateTo'] : '';

        echo '<style>
        input[name="mishaDateFrom"], input[name="mishaDateTo"]{
            line-height: 28px;
            height: 28px;
            margin: 0;
            width:125px;
        }
        </style>
        <form id="posts-filter" name="datefilter" id="datefilter" action="" method="get" style="display:inline">
        <input type="hidden" name="page"  value="ts_registration" />
        <input type="hidden" name="action"  value="product" />
        Created Date
        <input type="text" name="mishaDateFrom" placeholder="Date From" value="' . esc_attr( $from ) . '" />
        <input type="text" name="mishaDateTo" placeholder="Date To" value="' . esc_attr( $to ) . '" />
        <input type="submit" name="filter_action" id="post-query-submit" class="button" value="Filter">
        <input type="button" name="reset" onclick="resetForm();" value="Reset">
        </form>
 
        <script>
        function resetForm() {
            jQuery(\'input[name="mishaDateFrom"]\').val(\'\');
            jQuery(\'input[name="mishaDateTo"]\').val(\'\');
        }
        jQuery( function($) {
            var from = $(\'input[name="mishaDateFrom"]\'),
                to = $(\'input[name="mishaDateTo"]\');
 
            $( \'input[name="mishaDateFrom"], input[name="mishaDateTo"]\' ).datepicker( {dateFormat : "yy-mm-dd"} );
            // by default, the dates look like this "April 3, 2017"
                // I decided to make it 2017-04-03 with this parameter datepicker({dateFormat : "yy-mm-dd"});
 
 
                // the rest part of the script prevents from choosing incorrect date interval
                from.on( \'change\', function() {
                to.datepicker( \'option\', \'minDate\', from.val() );
            });
 
            to.on( \'change\', function() {
                from.datepicker( \'option\', \'maxDate\', to.val() );
            });
 
        });
        </script>';
             endif;
    }

    /**
     * Handles data query and filter, sorting, and pagination.
     */
    public function prepare_items()
    {

        $this->_column_headers = $this->get_column_info();

        /** Process bulk action */
        $this->process_bulk_action();

        $per_page = $this->get_items_per_page('registrations_per_page', 5);
        $current_page = $this->get_pagenum();
        $total_items = self::record_count();

        $this->set_pagination_args(['total_items' => $total_items, //WE have to calculate the total number of items
        'per_page' => $per_page
        //WE have to determine how many items to show on a page
        ]);

        $this->items = self::get_registrations($per_page, $current_page);
    }

    public function process_bulk_action()
    {

        //Detect when a bulk action is being triggered...
        if ('delete' === $this->current_action())
        {

            // In our file that handles the request, verify the nonce.
            $nonce = esc_attr($_REQUEST['_wpnonce']);

            if (!wp_verify_nonce($nonce, 'sp_delete_registration'))
            {
                die('Go get a life script kiddies');
            }
            else
            {
                self::delete_registration(absint($_GET['registration']));

                // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
                // add_query_arg() return the current url
                wp_redirect(esc_url_raw(add_query_arg()));
                exit;
            }

        }

        // If the delete bulk action is triggered
        if ((isset($_POST['action']) && $_POST['action'] == 'bulk-delete') || (isset($_POST['action2']) && $_POST['action2'] == 'bulk-delete'))
        {

            $delete_ids = esc_sql($_POST['bulk-delete']);

            // loop over the array of record IDs and delete them
            foreach ($delete_ids as $id)
            {
                self::delete_registration($id);

            }

            // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
            // add_query_arg() return the current url
            wp_redirect(esc_url_raw(add_query_arg()));
            //exit;
            
        }
    }

}