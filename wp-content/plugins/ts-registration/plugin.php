<?php
/*
   Plugin Name: TS Registration
   Plugin URI: https://www.sitepoint.com/using-wp_list_table-to-create-wordpress-admin-tables/
   Description: Demo on how WP_List_Table Class works
   Version: 1.0
   Author: Collins Agbonghama
   Author URI:  https://w3guy.com
*/
define( 'WPTSR_PLUGIN', __FILE__ );

define( 'WPTSR_PLUGIN_BASENAME', plugin_basename( WPTSR_PLUGIN ) );

define( 'WPTSR_PLUGIN_NAME', trim( dirname( WPTSR_PLUGIN_BASENAME ), '/' ) );

define( 'WPTSR_PLUGIN_DIR', untrailingslashit( dirname( WPTSR_PLUGIN ) ) );

require_once WPTSR_PLUGIN_DIR . '/class/class-user-list.php';
require_once WPTSR_PLUGIN_DIR . '/class/class-product-list.php';



class SP_Plugin
{

    // class instance
    static $instance;

    // registration WP_List_Table object
    public $registrations_obj;
    public $product_registrations_obj;

    // class constructor
    public function __construct()
    {

    $this->processSubmit();

    if(isset($_GET['download_products']))
    {
        $csv = $this->generate_product_csv();

    }
    if(isset($_GET['download_users']))
    {
        $csv = $this->generate_user_csv();

    }
        add_filter('set-screen-option', [__CLASS__, 'set_screen'], 10, 3);
        add_action('admin_menu', [$this, 'plugin_menu']);
    }

    public static function send_sms($message, $phone)
    {
        if (!empty($message) && !empty($phone))
        {
            $argumentos['body'] = ['username' => 'kutchina@123', 'password' => 'Qq12345', 'sender' => 'KUTCHI', 'to' => $phone, 'message' => $message, 'reqid' => "1", 'format' => "text", ];

            $respuesta                    = wp_remote_get( "http://tra.smsmyntraa.com/API/WebSMS/Http/v1.0a/index.php", $argumentos );
            return $respuesta;
        }

        return '';
    }
    public static function processSubmit()
    {    	
     if (isset($_POST['reg_id']))
        {
            global $wpdb;  
            $wpdb->update($wpdb->prefix . "portal_user_products", array(
                'status' => isset($_POST['approve']) ? $_POST['approve'] : 0,
                'approval_date' => (isset($_POST['comment']) && $_POST['approve'] == 1) ? current_time('mysql', 1) : NULL,
                'comment' => isset($_POST['comment']) ? $_POST['comment'] : '',
                'purchase_point' => isset($_POST['purchase_point']) ? $_POST['purchase_point'] : '',
                'bill_date' => isset($_POST['bill_date']) ? $_POST['bill_date'] : '',
                'bill_no' => isset($_POST['bill_no']) ? $_POST['bill_no'] : ''
            ) , array(
                'id' => $_POST['reg_id']
            ) , array(
                '%d',
                '%s',
                '%s',
                '%s',
                '%s'
            ) , array(
                '%d'
            ));

            if (isset($_POST['notify']) && $_POST['notify'] == 1)
            {

                if($_POST['approve'] == 1) //approved
                {
                 $message = 'Welcome to KUTCHINA. Your Registered no. is '.$_POST['user_phone'].'. For any further service request contact our tollfree no 1800 419 7333 from your registered mobile number. Regards
';
                }

                if($_POST['approve'] == 2) //rejected
                {
                 $message = 'Dear Customer, Your details for registration through "www.kutchina.com" could not be processed. For further details please contact toll free no 1800 419 7333 from your registered mobile number. Regards';
                }

                wp_mail( $_POST['user_email'], "Product registration at Kutchina.com", $message);
                SP_Plugin::send_sms($message, $_POST['user_phone']);
               
            }            

            header('Location: '.get_site_url().'/wp-admin/admin.php?page=ts_registration&action=product');

        }
    }




    public static function generate_product_csv()
    {

        global $wpdb;
        $filename = 'product-' . date('d-M-Y') . '.csv';
        $where =  "";

        if (! empty( $_GET['mishaDateFrom'] ) || ! empty( $_GET['mishaDateTo'] ) )
         {
            $where =  " AND `purchase_date` BETWEEN '".sanitize_text_field( $_GET['mishaDateFrom'])."' AND '".sanitize_text_field( $_GET['mishaDateTo'])."'";
        }

        //$sql = "SELECT * FROM {$wpdb->prefix}portal_user_details WHERE `id` IN (" . implode(',', array_map('intval', $data)) . ")";
        $sql = "SELECT u.`customer_name`, u.`street`, NULL as Address2, u.`state`, u.`city`, u.`zipcode`,  NULL as PinCode,  NULL as Phone, u.`customer_phone` as Mobile, u.`customer_email`,  c.`product_category` as Product, p.product_name, p.product_serial_number, NULL as InstallBy, NULL as InstallDate, purchase_point, QUOTE(bill_date) as BillDate, bill_no FROM `" . $wpdb->prefix . "portal_user_products` p
         left join `" . $wpdb->prefix . "portal_user_details` u on p.`customer_phone` = u.`customer_phone` 
         left join `" . $wpdb->prefix . "portal_product_catagory` c on p.`product_code` = c.`product_code`
         WHERE `status` = 1 ".$where ;
        $results = $wpdb->get_results($sql);


        $csv_headers = array();
        $csv_headers[] = 'Name';
        $csv_headers[] = 'Address1';
        $csv_headers[] = 'Address2';
        $csv_headers[] = 'State';
        $csv_headers[] = 'City';
        $csv_headers[] = 'ZipCode';
        $csv_headers[] = 'PinCode';
        $csv_headers[] = 'Phone';
        $csv_headers[] = 'Mobile';
        $csv_headers[] = 'EmailId';
        $csv_headers[] = 'Product';
        $csv_headers[] = 'ModelName';
        $csv_headers[] = 'SerialNO';
        $csv_headers[] = 'InstallBy';
        $csv_headers[] = 'InstallDate';
        $csv_headers[] = 'PurchasePoint';
        $csv_headers[] = 'BillDate';
        $csv_headers[] = 'BillNo';

        $output_handle = fopen('php://output', 'w');

        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Description: File Transfer');
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Expires: 0');
        header('Pragma: public');

        fputcsv($output_handle, $csv_headers);

        foreach ($results as $result)
        {
            if($result->BillDate == "''")
            {
               $result->BillDate = '';
            }

            fputcsv($output_handle, (array)$result);
        }

        fclose($output_handle);
        die();
    }


    public static function generate_user_csv()
    {

        global $wpdb;
        $filename = 'user-' . date('d-M-Y') . '.csv';
        $where =  '';
        if (! empty( $_GET['mishaDateFrom'] ) || ! empty( $_GET['mishaDateTo'] ) )
         {
            $where =  " WHERE DATE(`created`) BETWEEN '".sanitize_text_field( $_GET['mishaDateFrom'])."' AND '".sanitize_text_field( $_GET['mishaDateTo'])."'";
        }

        //$sql = "SELECT * FROM {$wpdb->prefix}portal_user_details WHERE `id` IN (" . implode(',', array_map('intval', $data)) . ")";
        $sql = "SELECT `customer_name`, `street`, NULL as Address2, `state`, `city`, `zipcode`,  NULL as PinCode, `customer_phone`,  NULL as Mobile, `customer_email` FROM {$wpdb->prefix}portal_user_details ".$where ;

        $results = $wpdb->get_results($sql);

        $csv_headers = array();
        $csv_headers[] = 'Name';
        $csv_headers[] = 'Address1';
        $csv_headers[] = 'Address2';
        $csv_headers[] = 'State';
        $csv_headers[] = 'City';
        $csv_headers[] = 'ZipCode';
        $csv_headers[] = 'PinCode';
        $csv_headers[] = 'Phone';
        $csv_headers[] = 'Mobile';
        $csv_headers[] = 'EmailId';

        $output_handle = fopen('php://output', 'w');

        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Description: File Transfer');
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Expires: 0');
        header('Pragma: public');

        fputcsv($output_handle, $csv_headers);

        foreach ($results as $result)
        {
            fputcsv($output_handle, (array)$result);
        }

        fclose($output_handle);
        die();
    }



    public static function set_screen($status, $option, $value)
    {
        return $value;
    }

    public function plugin_menu()
    {

        $hook = add_menu_page('TS Registration', 'TS Registration', 'manage_portal', 'ts_registration', [$this, 'plugin_settings_page']);

        add_action("load-$hook", [$this, 'screen_option']);

    }

    public function plugin_settings_page()
    {
        $action = isset($_GET['action']) ? $_GET['action'] : '';
        switch ($action)
        {
            case 'approve':
                $this->plugin_product_registration_approval_page();
            break;
            case 'product':
                $this->plugin_product_registration_listing_page();
            break;
            default:
                $this->plugin_registration_listing_page();
        }
    }

    /**
     * Plugin settings page
     */
    public function plugin_product_registration_listing_page()
    {
        $query ='';
        if (! empty( $_GET['mishaDateFrom'] ) || ! empty( $_GET['mishaDateTo'] ) )
         {
            $query =  "&mishaDateFrom=".sanitize_text_field( trim($_GET['mishaDateFrom']))."&mishaDateTo=".sanitize_text_field( $_GET['mishaDateTo']);            
        }
		?>
		<div class="wrap">
		   <h2>TS Registration</h2>
		   		<?php self::displayHeader() ?>
				<p class="search-box">
					<a id="search-submit" class="button" href="admin.php?page=ts_registration&download_products<?php echo $query ?>" >Download CSV</a>
				</p>

		   <div id="poststuff">
		      <div class="metabox-holder columns-2">
		         <div>
		            <div class="meta-box-sortables ui-sortable">
		                  <?php
		        $this
		            ->product_registrations_obj
		            ->prepare_items();
		        $this
		            ->product_registrations_obj
		            ->display(); ?>
		            </div>
		         </div>
		      </div>
		      <br class="clear">
		   </div>
		</div>
		<?php
    }

    /**
     * Plugin settings page
     */
    public function plugin_product_registration_approval_page()
    {

        $registration = Product_Registration_List::get_registration($_GET['id']);
			?>
		<div class="wrap">
		   <h2>Approve Product Registration</h2>
		   <div id="poststuff">
		      <div id="post-body" class="metabox-holder columns-2">
		         <div id="post-body-content">
		            <div class="meta-box-sortables ui-sortable">
		               <form class="posttypesui" method="post" action="">
		                  <div class="postbox-container">
		                     <div id="poststuff">
		                        <div class="cptui-section postbox">
		                           <h2 class="hndle">
		                              <span>Product Registration Detail</span>
		                           </h2>
		                           <div class="inside">
		                              <div class="main">
		                                 <table class="form-table cptui-table">
		                                    <tbody>
		                                       <tr valign="top">
		                                          <th scope="row">
		                                             <label for="name">Phone</label> 
		                                          </th>
		                                          <td>
		                                             <?php echo $registration['customer_phone']; ?><br><span class="cptui-field-description"></span>
		                                             <p class="cptui-slug-details"></p>
		                                          </td>
		                                       </tr>
		                                       <tr valign="top">
		                                          <th scope="row"><label for="label">Product:</label> <span class="required">*</span></th>
		                                          <td><?php echo $registration['product_serial_number']; ?> (Serial Number)<br/>
		                                          	<?php echo $registration['product_name']; ?> (Product Name)<br/>
		                                          	<?php echo $registration['product_code']; ?> (Product Code)</td>
		                                       </tr>		                                       
		                                       <tr valign="top">
		                                          <th scope="row"><label for="label">Customer:</label> <span class="required">*</span></th>
		                                          <td><?php echo $registration['customer_name']; ?> (<?php echo $registration['customer_email']; ?>)</td>
		                                       </tr>		                                   
		                                       <tr valign="top">
		                                          <th scope="row"><label for="label">Address:</label> <span class="required">*</span></th>
		                                          <td><?php echo $registration['street']; ?><br/><?php echo $registration['city']; ?><br/><?php echo $registration['state']; ?><br/><?php echo $registration['zipcode']; ?></td>
		                                       </tr>		                                       
		                                       <tr valign="top">
		                                          <th scope="row"><label for="label">Invoice</label> <span class="required">*</span></th>
		                                          <td><a target="blank" href="<?php echo $registration['invoice_file']; ?>">View Invoice</a></td>
		                                       </tr>		                                       
		                                       <th scope="row">
		                                          <label for="name">Approved / Reject </label> 
		                                       </th>
		                                       <td>
		                                          <span class="cptui-field-description"></span>
		                                          <p class="cptui-slug-details"></p>
		                                          <div class="cptui-spacer">
                                                    <select name="approve">
                                                        <?php
                                                        $status = array( 0 => "Pending", 1 => "Approved", 2 => "Rejected");
                                                         foreach ( $status as $value => $label ) : ?>
                                                            <option value="<?php echo esc_attr( $value ); ?>"<?php selected( $value, $registration['status']); ?>><?php echo esc_html( $label ); ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    </div>
		                                       </td>
		                                       </tr>
					  	                       <tr valign="top">
		                                          <th scope="row"><label for="label">Comment</label> <span class="required">*</span></th>
		                                          <td><textarea type="text" id="label" name="comment" value="" aria-required="true" required="true" ><?php echo $registration['comment']; ?></textarea></td>
		                                       </tr>     
                                               <tr valign="top">
                                                  <th scope="row"><label for="label">PurchasePoint</label></th>
                                                  <td><input type="text" id="purchase_point" name="purchase_point" aria-required="true" value="<?php echo $registration['purchase_point']; ?>" /></td>
                                               </tr>                                        
                                               <tr valign="top">
                                                  <th scope="row"><label for="label">BillDate</label></th>
                                                  <td><input type="text" id="bill_date" name="bill_date" aria-required="true" value="<?php echo $registration['bill_date']; ?>" /></td>
                                               </tr>                                        
                                               <tr valign="top">
                                                  <th scope="row"><label for="label">BillNo</label></th>
                                                  <td><input type="text" id="bill_no" name="bill_no" aria-required="true" value="<?php echo $registration['bill_no']; ?>" /></td>
                                               </tr>
		                                       </tr>
		                                       <th scope="row">
		                                          <label for="name">Notify user through email/SMS </label> 
		                                       </th>
		                                       <td>
		                                          <span class="cptui-field-description"></span>
		                                          <p class="cptui-slug-details"></p>
		                                          <div class="cptui-spacer"><input type="checkbox" name="notify" value="1"><label for="update_post_types">Send Email/SMS ?</label><br></div>
		                                       </td>
		                                       </tr> 
		                                    </tbody>
		                                 </table>
		                                 <p class="submit">
		                                    <input type="submit" class="button-primary" name="ts_submit" value="Submit">
		                                    <input type="hidden" name="reg_id" value="<?php echo $_GET['id']; ?>">
                                            <input type="hidden" name="user_phone" value="<?php echo $registration['customer_phone']; ?>">
                                            <input type="hidden" name="user_email" value="<?php echo $registration['customer_email']; ?>">
		                                 </p>
                                        <script>
                                        jQuery( function($) {

                                            $( 'input[name="bill_date"]' ).datepicker( {dateFormat : "dd/mm/yy"} );
                                 
                                        });
                                        </script>
		                              </div>
		                           </div>
		                        </div>
		                     </div>
		                  </div>
		               </form>
		            </div>
		         </div>
		      </div>
		      <br class="clear">
		   </div>
		</div>
	<?php
    }
    /**
     * Plugin settings page
     */
    public function plugin_registration_listing_page()
    {
        $query ='';
        if (! empty( $_GET['mishaDateFrom'] ) || ! empty( $_GET['mishaDateTo'] ) )
         {
            $query =  "&mishaDateFrom=".sanitize_text_field( $_GET['mishaDateFrom'])."&mishaDateTo=".sanitize_text_field( $_GET['mishaDateTo']);
        }
		?>
		<div class="wrap">
		   <h2>TS Registration</h2>
		   		<?php self::displayHeader() ?>
				<p class="search-box">
					<a id="search-submit" class="button" href="admin.php?page=ts_registration&download_users<?php echo $query ?>" >Download CSV</a>
				</p>
		   <div id="poststuff">
		      <div class="metabox-holder columns-2">
		         <div>
		            <div class="meta-box-sortables ui-sortable">
		                
		                  <?php
		        $this
		            ->registrations_obj
		            ->prepare_items();
		        $this
		            ->registrations_obj
		            ->display(); ?>

		            </div>
		         </div>
		      </div>
		      <br class="clear">
		   </div>
		</div>
			<?php
	}

	public function displayHeader()
		    {
			?>
			<hr class="wp-header-end">
			<h2 class="screen-reader-text">Filter pages list</h2>
			<ul class="subsubsub">
				<li class="all"><a href="admin.php?page=ts_registration" aria-current="page" <?php if($_GET['action'] != 'product') echo 'class="current"'; ?>>User Registered<span class="count"></span></a> |</li>
				<li class="publish"><a href="admin.php?page=ts_registration&action=product" <?php if($_GET['action'] == 'product') echo 'class="current"'; ?> >Product Registered <span class="count"></span></a></li>

			</ul>



		<?php
    }

    /**
     * Screen options
     */
    public function screen_option()
    {
        $option = 'per_page';
        $args = ['label' => 'TS Registrations', 'default' => 5, 'option' => 'registrations_per_page'];
        add_screen_option($option, $args);
        $action = isset($_GET['action']) ? $_GET['action'] : '';
        switch ($action)
        {
            case 'product':
                $this->product_registrations_obj = new Product_Registration_List();
            break;
            default:
                $this->registrations_obj = new User_Registration_List();
        }
        
        
    }
    /** Singleton instance */
    public static function get_instance()
    {
        if (!isset(self::$instance))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
}

add_action('plugins_loaded', function ()
{
    SP_Plugin::get_instance();
});


class mishaDateRange{
 
    function __construct(){
 
        // include CSS/JS, in our case jQuery UI datepicker
        add_action( 'admin_enqueue_scripts', array( $this, 'jqueryui' ) );
 
    }
 
    /*
     * Add jQuery UI CSS and the datepicker script
     * Everything else should be already included in /wp-admin/ like jquery, jquery-ui-core etc
     * If you use WooCommerce, you can skip this function completely
     */
    function jqueryui(){
        wp_enqueue_style( 'jquery-ui', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css' );
        wp_enqueue_script( 'jquery-ui-datepicker' );
    }
 
}
new mishaDateRange();

