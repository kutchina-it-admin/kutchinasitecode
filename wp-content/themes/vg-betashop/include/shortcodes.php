<?php
//Shortcodes for Visual Composer

add_action('vc_before_init', 'betashop_vc_shortcodes');
function betashop_vc_shortcodes() {
	
	//Swallow Code Custom elements Visual Composer
	if ( is_plugin_active( 'vg-woocarousel/vg-woocarousel.php' ) ) {
		
		$vgwctitle = (isset($vgwctitle)) ? $vgwctitle : '';
		$args = array(
			'post_type' => 'vgwc',
			'posts_per_page' => -1,
		);

		$vgwc = new WP_Query($args);
		if ($vgwc->have_posts()) : 
			while ($vgwc->have_posts()) : $vgwc->the_post(); 
				$vgwctitle[get_the_title()]= get_the_ID();
			endwhile;
		endif;
	}	
	
	if ( is_plugin_active( 'vg-postcarousel/vg-postcarousel.php' ) ) {
		
		$vgwctitle = (isset($vgwctitle)) ? $vgwctitle : '';
		$args = array(
			'post_type' => 'vgpc',
			'posts_per_page' => -1,
		);

		$vgpc = new WP_Query($args);
		
		if ($vgpc->have_posts()) : 
			while ($vgpc->have_posts()) : $vgpc->the_post(); 
				$vgpctitle[get_the_title()]= get_the_ID();
			endwhile;
		endif;
	}
	if ( is_plugin_active( 'mega_main_menu/mega_main_menu.php' ) ) {
		foreach ( get_registered_nav_menus() as $key => $value ){
			$key = str_replace( ' ', '-', $key );
			$theme_menu_locations[ $key ] = $key;
		}
		foreach ( get_nav_menu_locations() as $key => $value ){
			$key = str_replace( ' ', '-', $key );
			$theme_menu_locations[ $key ] = $key;
		}
	}
	//End Swallow Code Custom elements Visual Composer
	
	if ( is_plugin_active( 'mega_main_menu/mega_main_menu.php' ) ) {
		//VG MegaMenu && Custom Menu
		vc_map(array(
			'name' => esc_html__('VG MegaMenu', 'vg-betashop'),
			'base' => 'vgmegamenu',
			'icon' => 'icon-wpb-vg icon-wpb-megamenu',
			'class' => '',
			'category' => esc_html__('VG Betashop', 'vg-betashop'),
			'description' => esc_html__('VG MegaMain Extensions Replace', 'vg-betashop'),
			'admin_label' => true,
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Widget title', 'vg-betashop' ),
					'param_name' => 'title',
					'description' =>  esc_html__( 'What text use as a widget title. Leave blank to use default widget title.', 'vg-betashop' ),
				),
				array(
					'type' => 'css_editor',
					'heading' =>  esc_html__( 'CSS box', 'vg-betashop' ),
					'param_name' => 'css',
					'group' =>  esc_html__( 'Design Options', 'vg-betashop' ),
				),
				array(
					'heading' =>  esc_html__( 'Select Mega Menu Desktop', 'vg-betashop' ),
					'description' =>  esc_html__( 'Select Mega menu in desktop.', 'vg-betashop' ),
					'param_name' => 'mega_menu',
					'type' => 'dropdown',
					'value' => $theme_menu_locations,
					'admin_label' => true,
					'save_always' => true,
				),	
				array(
					'heading' =>  esc_html__( 'Select Treeview Menu Mobile', 'vg-betashop' ),
					'description' => empty( $custom_menus ) ?  esc_html__( 'Custom menus not found. Please visit <b>Appearance > Menus</b> page to create new menu.', 'vg-betashop' ) :  esc_html__( 'Select menu to display.', 'vg-betashop' ),
					'param_name' => 'nav_menu',
					'type' => 'dropdown',
					'value' => $theme_menu_locations,
					'admin_label' => true,
					'save_always' => true,
				),	
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Extra class name', 'vg-betashop' ),
					'param_name' => 'el_class',
					'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vg-betashop' ),
				),
			)
		));
	}
	//Brand logos
	vc_map(array(
		'name' => esc_html__('Brand Logos', 'vg-betashop'),
		'base' => 'ourbrands',
		'icon' => 'icon-wpb-vg icon-wpb-ourbrands',
		'class' => '',
		'category' => esc_html__('VG Betashop', 'vg-betashop'),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' =>  esc_html__( 'Widget title', 'vg-betashop' ),
				'param_name' => 'title',
				'description' =>  esc_html__( 'Enter text used as widget title (Note: located above content element).', 'vg-betashop' ),
			),
			array(
				'type' => 'css_editor',
				'heading' =>  esc_html__( 'CSS box', 'vg-betashop' ),
				'param_name' => 'css',
				'group' =>  esc_html__( 'Design Options', 'vg-betashop' ),
			),
			array(
				'type' => 'dropdown',
				'holder' => 'div',
				'class' => '',
				'heading' => esc_html__('Number of rows', 'vg-betashop'),
				'param_name' => 'rowsnumber',
				'value' => array(
					'one'	=> 'one',
					'two'	=> 'two',
					'four'	=> 'four',
				),
			),
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'Items Visible:', 'vg-betashop' ),
				'param_name' => 'items_visible',
				'value'		 => '4',
			),		
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'Desktop - Column Number:', 'vg-betashop' ),
				'param_name' => 'desktop_number',
				'value' 	 =>	 esc_html__('1170,3', 'vg-betashop'),
			),		
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'S Desktop - Column Number:', 'vg-betashop' ),
				'param_name' => 'sdesktop_number',
				'value' 	 =>	 esc_html__('992,2', 'vg-betashop'),
			),		
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'Tablet - Column Number:', 'vg-betashop' ),
				'param_name' => 'tablet_number',
				'value' 	 =>	 esc_html__('800,2', 'vg-betashop'),
			),		
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'S Tablet - Column Number:', 'vg-betashop' ),
				'param_name' => 'stablet_number',
				'value' 	 =>	 esc_html__('650,2', 'vg-betashop'),
			),		
			array(
				'type' 		 => 'textfield',
				'heading' 	 =>  esc_html__( 'Mobile - Column Number:', 'vg-betashop' ),
				'param_name' => 'mobile_number',
				'value' 	 =>	 esc_html__('599,1', 'vg-betashop'),
			),
			array(
				'type' => 'checkbox',
				'heading' =>  esc_html__( 'Show Icon?', 'vg-betashop' ),
				'param_name' => 'add_show_icon',
				'description' =>  esc_html__( 'Show Icon.', 'vg-betashop' ),
				'value' => array(  esc_html__( 'Yes', 'vg-betashop' ) => 'Yes' ),
				'save_always' => true,
			),
			array(
				'type' => 'dropdown',
				'heading' =>  esc_html__( 'Icon library', 'vg-betashop' ),
				'value' => array(
					 esc_html__( 'Font Awesome', 'vg-betashop' ) => 'fontawesome',
					 esc_html__( 'Open Iconic', 'vg-betashop' ) => 'openiconic',
					 esc_html__( 'Typicons', 'vg-betashop' ) => 'typicons',
					 esc_html__( 'Entypo', 'vg-betashop' ) => 'entypo',
					 esc_html__( 'Linecons', 'vg-betashop' ) => 'linecons',
				),
				'save_always' => true,
				'admin_label' => true,
				'param_name' => 'type',
				'description' =>  esc_html__( 'Select icon library.', 'vg-betashop' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
				'param_name' => 'icon_fontawesome',
				'value' => 'fa fa-adjust', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => true,
					// default true, display an 'EMPTY' icon?
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'fontawesome',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
				'param_name' => 'icon_openiconic',
				'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'openiconic',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'openiconic',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
				'param_name' => 'icon_typicons',
				'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'typicons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'typicons',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
				'param_name' => 'icon_entypo',
				'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'entypo',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'entypo',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
				'param_name' => 'icon_linecons',
				'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'linecons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'linecons',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
			),
			array(
				'type' => 'textfield',
				'heading' =>  esc_html__( 'Extra class name', 'vg-betashop' ),
				'param_name' => 'el_class',
				'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vg-betashop' ),
			),
		)
	));
	
	//Testimonials
	vc_map( array(
		"name" => esc_html__( "Testimonials", "vg-betashop" ),
		"base" => "woothemes_testimonials",
		"class" => "",
		"category" => esc_html__( "VG Betashop", "vg-betashop"),
		"params" => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_html__( "Number of testimonial", "vg-betashop" ),
				"param_name" => "limit",
				"value" => esc_html__( "10", "vg-betashop" ),
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_html__( "Image size", "vg-betashop" ),
				"param_name" => "size",
				"value" => esc_html__( "120", "vg-betashop" ),
			),
		)
	) );
	
	//Swallow Code Custom elements Visual Composer
	if ( is_plugin_active( 'vg-woocarousel/vg-woocarousel.php' ) ) {
		//List VG WooCarousel
		vc_map(array(
			'name' => esc_html__('VG WooCarousel', 'vg-betashop'),
			'base' => 'listvgwccarousel',
			'icon' => 'icon-wpb-vg icon-wpb-woocarousel',
			'class' => '',
			'category' => esc_html__('VG Betashop', 'vg-betashop'),
			'description' => esc_html__('VG WooCarousel Extensions', 'vg-betashop'),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Widget title', 'vg-betashop' ),
					'param_name' => 'title',
					'description' =>  esc_html__( 'Enter text used as widget title (Note: located above content element).', 'vg-betashop' ),
				),
				array(
					'type' => 'css_editor',
					'heading' =>  esc_html__( 'CSS box', 'vg-betashop' ),
					'param_name' => 'css',
					'group' =>  esc_html__( 'Design Options', 'vg-betashop' ),
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('VG WooCarousel', 'vg-betashop'),
					'param_name' => 'alias',
					'admin_label' => true,
					'value' => $vgwctitle,
					'save_always' => true,
					'description' =>  esc_html__( 'Select your VG WooCarousel.', 'vg-betashop' ),
				),
				array(
					'type' => 'checkbox',
					'heading' =>  esc_html__( 'Show Icon?', 'vg-betashop' ),
					'param_name' => 'add_show_icon',
					'description' =>  esc_html__( 'Show Icon.', 'vg-betashop' ),
					'value' => array(  esc_html__( 'Yes', 'vg-betashop' ) => 'Yes' ),
					'save_always' => true,
				),
				array(
					'type' => 'dropdown',
					'heading' =>  esc_html__( 'Icon library', 'vg-betashop' ),
					'value' => array(
						 esc_html__( 'Font Awesome', 'vg-betashop' ) => 'fontawesome',
						 esc_html__( 'Open Iconic', 'vg-betashop' ) => 'openiconic',
						 esc_html__( 'Typicons', 'vg-betashop' ) => 'typicons',
						 esc_html__( 'Entypo', 'vg-betashop' ) => 'entypo',
						 esc_html__( 'Linecons', 'vg-betashop' ) => 'linecons',
					),
					'save_always' => true,
					'admin_label' => true,
					'param_name' => 'type',
					'description' =>  esc_html__( 'Select icon library.', 'vg-betashop' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
					'param_name' => 'icon_fontawesome',
					'value' => 'fa fa-adjust', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => true,
						// default true, display an 'EMPTY' icon?
						'iconsPerPage' => 4000,
						// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'fontawesome',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
					'param_name' => 'icon_openiconic',
					'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'openiconic',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'openiconic',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
					'param_name' => 'icon_typicons',
					'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'typicons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'typicons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
					'param_name' => 'icon_entypo',
					'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'entypo',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'entypo',
					),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
					'param_name' => 'icon_linecons',
					'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'linecons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'linecons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
				),
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Extra class name', 'vg-betashop' ),
					'param_name' => 'el_class',
					'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vg-betashop' ),
				),
			)
		));
	}
	
	if ( is_plugin_active( 'vg-postcarousel/vg-postcarousel.php' ) ) {
		//List VG PostCarousel
		vc_map(array(
			'name' => esc_html__('VG PostCarousel', 'vg-betashop'),
			'base' => 'listvgpccarousel',
			'icon' => 'icon-wpb-vg icon-wpb-postcarousel',
			'class' => '',
			'category' => esc_html__('VG Betashop', 'vg-betashop'),
			'description' => esc_html__('VG PostCarousel Extensions', 'vg-betashop'),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Widget title', 'vg-betashop' ),
					'param_name' => 'title',
					'description' =>  esc_html__( 'Enter text used as widget title (Note: located above content element).', 'vg-betashop' ),
				),
				array(
					'type' => 'css_editor',
					'heading' =>  esc_html__( 'CSS box', 'vg-betashop' ),
					'param_name' => 'css',
					'group' =>  esc_html__( 'Design Options', 'vg-betashop' ),
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('VG WooCarousel', 'vg-betashop'),
					'param_name' => 'alias',
					'admin_label' => true,
					'value' => $vgpctitle,
					'save_always' => true,
					'description' =>  esc_html__( 'Select your VG WooCarousel.', 'vg-betashop' ),
				),
				array(
					'type' => 'checkbox',
					'heading' =>  esc_html__( 'Show Icon?', 'vg-betashop' ),
					'param_name' => 'add_show_icon',
					'description' =>  esc_html__( 'Show Icon.', 'vg-betashop' ),
					'value' => array(  esc_html__( 'Yes', 'vg-betashop' ) => 'Yes' ),
					'save_always' => true,
				),
				array(
					'type' => 'dropdown',
					'heading' =>  esc_html__( 'Icon library', 'vg-betashop' ),
					'value' => array(
						 esc_html__( 'Font Awesome', 'vg-betashop' ) => 'fontawesome',
						 esc_html__( 'Open Iconic', 'vg-betashop' ) => 'openiconic',
						 esc_html__( 'Typicons', 'vg-betashop' ) => 'typicons',
						 esc_html__( 'Entypo', 'vg-betashop' ) => 'entypo',
						 esc_html__( 'Linecons', 'vg-betashop' ) => 'linecons',
					),
					'save_always' => true,
					'admin_label' => true,
					'param_name' => 'type',
					'description' =>  esc_html__( 'Select icon library.', 'vg-betashop' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
					'param_name' => 'icon_fontawesome',
					'value' => 'fa fa-adjust', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => true,
						// default true, display an 'EMPTY' icon?
						'iconsPerPage' => 4000,
						// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'fontawesome',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
					'param_name' => 'icon_openiconic',
					'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'openiconic',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'openiconic',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
					'param_name' => 'icon_typicons',
					'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'typicons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'typicons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
					'param_name' => 'icon_entypo',
					'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'entypo',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'entypo',
					),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'vg-betashop' ),
					'param_name' => 'icon_linecons',
					'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'linecons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'linecons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'vg-betashop' ),
				),
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Extra class name', 'vg-betashop' ),
					'param_name' => 'el_class',
					'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'vg-betashop' ),
				),
			)
		));
	}
	//End Swallow Code Custom elements Visual Composer
}
?>