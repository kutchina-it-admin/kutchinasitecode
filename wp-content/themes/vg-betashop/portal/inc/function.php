<?php
/* Register function to run at rest_api_init hook */
add_action('rest_api_init', function ()
{
    /* Setup siteurl/wp-json/menus/v2/header */
    register_rest_route('portal/v1', '/send-otp', array(
        'methods' => 'GET',
        'callback' => 'send_otp',
    ));

    /* Setup siteurl/wp-json/menus/v2/header */
    register_rest_route('portal/v1', '/verify-otp', array(
        'methods' => 'GET',
        'callback' => 'verify_otp',
    ));

    /* Setup siteurl/wp-json/menus/v2/header */
    register_rest_route('portal/v1', '/verify-serial-number', array(
        'methods' => 'GET',
        'callback' => 'verify_serial_number',
    ));

    /* Setup siteurl/wp-json/menus/v2/header */
    register_rest_route('portal/v1', '/update-product', array(
        'methods' => 'POST',
        'callback' => 'update_product',
    ));

    /* Setup siteurl/wp-json/menus/v2/header */
    register_rest_route('portal/v1', '/update-user', array(
        'methods' => 'POST',
        'callback' => 'update_user',
    ));
});

function update_product($data)
{

    $responce['status'] = '';
    $responce['message'] = '';

    $productDetailsFromDB = getProductDetail($_POST['sl_nmbr']);

    if (isset($productDetailsFromDB['product_serial_number']) && !empty($productDetailsFromDB['product_serial_number']))
    {
        $responce['status'] = 'error';
        $responce['message'] = 'Serial No already added.';
        return $responce;
        exit();
    }

    $productDetails = fetchProductData($_POST['sl_nmbr']);
    if (!(isset($productDetails['SerialNo']) && !empty($productDetails['SerialNo'])))
    {
        $responce['status'] = 'error';
        $responce['message'] = 'Serial No is invalid.';
        return $responce;
        exit();
    }

    $filePath = upload_invoice();

    if (empty($filePath))
    {
        $responce['status'] = 'error';
        $responce['message'] = 'File not uploaded, Please check the file.';
        return $responce;
        exit();
    }

    // $phone = $_COOKIE["portal_user_ph"];
    // echo $phone;
    global $wpdb;
    $parameters = $data->get_params();

    $sl_nmbr = $parameters['sl_nmbr'];
   // $purchs_date = $parameters['purchs_date'];
    $purchs_date = '';
    $prod_name = $productDetails['ProdName'];
    $prod_code = $productDetails['ProdCode'];
    $phone_number = $parameters['phone_number'];

    // echo $_COOKIE["portal_user"];
    if (!empty($phone_number))
    {

        $sql = "INSERT INTO `{$wpdb->prefix}portal_user_products` (`customer_phone`, `product_serial_number`, `product_name`, `product_code`, `purchase_date`, `invoice_file`) VALUES ('" . $phone_number . "', '" . $sl_nmbr . "', '" . $prod_name . "', '" . $prod_code . "', '" . $purchs_date . "', '" . $filePath . "')";

        $sql = $wpdb->prepare($sql, $data);
        $result = $wpdb->query($sql);

        if (isset($parameters['edt_optn']) && $parameters['edt_optn'] == "edit-address")
        {

            $phone_number = $parameters['phone_number'];
            $cust_name = $parameters['cust_name'];
            $email = $parameters['email'];
            $street = $parameters['street'];
            $state = $parameters['state'];
            $zipcode = $parameters['zipcode'];

            $sqlUSRDETL = "INSERT INTO `{$wpdb->prefix}portal_user_details` (`customer_phone`, `customer_name`, `customer_email`, `street`, `state`, `zipcode`) VALUES ('" . $phone_number . "', '" . $cust_name . "', '" . $email . "', '" . $street . "', '" . $state . "', '" . $zipcode . "') ON DUPLICATE KEY UPDATE customer_name='" . $cust_name . "', customer_email='" . $email . "', street='" . $street . "', state='" . $state . "', zipcode='" . $zipcode . "'";
            $sqlUSRDETL = $wpdb->prepare($sqlUSRDETL, $data);
            $resultUSRDETL = $wpdb->query($sqlUSRDETL);
        }

        $responce['status'] = 'success';
        $responce['message'] = 'Successfully added';
        return $responce;
        exit();
    }

    return $responce;
}

function upload_invoice()
{

    $filePath = '';

    $valid_extensions = array(
        'jpeg',
        'jpg',
        'png',
        'gif',
        'bmp',
        'pdf',
        'doc',
        'ppt'
    ); // valid extensions
    $upload_dir = wp_upload_dir();

    $invoices_dirname = $upload_dir['basedir'] . '/invoices/';
    $invoices_urlname = $upload_dir['baseurl'] . '/invoices/';
    if (!file_exists($invoices_dirname))
    {
        wp_mkdir_p($invoices_dirname);
    }

    $path = $invoices_dirname; // upload directory
    if ($_FILES['invoice'])
    {
        $img = $_FILES['invoice']['name'];
        $tmp = $_FILES['invoice']['tmp_name'];
        // get uploaded file's extension
        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
        // can upload same image using rand function
        $final_image = rand(1000, 1000000) . $img;
        // check's valid format
        if (in_array($ext, $valid_extensions))
        {
            $path = $path . strtolower($final_image);
            if (move_uploaded_file($tmp, $path))
            {
                return $invoices_urlname . strtolower($final_image);
            }
        }
        else
        {
            return '';
        }
    }

    return '';
}

function getProductDetail($sno)
{
    global $wpdb;

    if (!empty($sno))
    {
        $sql = "SELECT * FROM `" . $wpdb->prefix . "portal_user_products` WHERE `product_serial_number` = '{$sno}'";

        $result = $wpdb->get_row($sql, 'ARRAY_A');
        return $result;
    }
    return '';
}

function update_user($data)
{

    $responce['status'] = '';
    $responce['message'] = '';


    global $wpdb;
    $parameters = $data->get_params();


    $phone_number = $parameters['phone_number'];

    // echo $_COOKIE["portal_user"];
    if (!empty($phone_number))
    {

        $phone_number = $parameters['phone_number'];
        $cust_name = $parameters['cust_name'];
        $email = $parameters['email'];
        $street = $parameters['street'];
        $state = $parameters['state'];
        $zipcode = $parameters['zipcode'];

        $sqlUSRDETL = "INSERT INTO `{$wpdb->prefix}portal_user_details` (`customer_phone`, `customer_name`, `customer_email`, `street`, `state`, `zipcode`) VALUES ('" . $phone_number . "', '" . $cust_name . "', '" . $email . "', '" . $street . "', '" . $state . "', '" . $zipcode . "') ON DUPLICATE KEY UPDATE customer_name='" . $cust_name . "', customer_email='" . $email . "', street='" . $street . "', state='" . $state . "', zipcode='" . $zipcode . "'";
        $sqlUSRDETL = $wpdb->prepare($sqlUSRDETL, $data);
        $resultUSRDETL = $wpdb->query($sqlUSRDETL);

        $responce['status'] = 'success';
        $responce['message'] = 'Details successfully added/updated.';
        $responce['data'] = $parameters;
        return $responce;
    }

    return $responce;
}

function send_otp($data)
{
    $parameters = $data->get_params();
    generateNumericOTP(4);

    if (isset($parameters['phone']) && !empty($parameters['phone']))
    {
        $phone = $parameters['phone'];
        $OTP = generateNumericOTP(4);
        updateOTP($phone, $OTP);

        $message = "{$OTP} is your One Time Password to sign in to Kutchina Product Registration Portal";

        $argumentos['body'] = ['username' => 'u5095', 'msg_token' => 'JiOw6L', 'sender_id' => 'KUTCHN', 'message' => $message, 'mobile' => $phone];

        $respuesta                    = wp_remote_get( "http://tra1.smsmyntraa.com/api/send_transactional_sms.php", $argumentos );
        return $respuesta;
        
    }

    return '';
}

function generateNumericOTP($n)
{

    $generator = "1357902468";
    $result = "";

    for ($i = 1;$i <= $n;$i++)
    {
        $result .= substr($generator, (rand() % (strlen($generator))) , 1);
    }
    return $result;
    //return 1111;
}

function updateOTP($phone, $OTP)
{

    global $wpdb;
    $sql = "INSERT INTO {$wpdb->prefix}portal_login_otp (phone,otp) VALUES (%s,%s) ON DUPLICATE KEY UPDATE otp = %s";
    $sql = $wpdb->prepare($sql, $phone, $OTP, $OTP);
    $result = $wpdb->query($sql);
    return $result;
}

function verify_otp($request_data)
{
    global $wpdb;
    $parameters = $request_data->get_params();
    $phone = $parameters['phone'];
    $otp = $parameters['otp'];
    $data['status'] = 'false';

    if ($phone != '')
    {
        $sql = "SELECT * FROM " . $wpdb->prefix . "portal_login_otp WHERE `phone` = $phone";

        $result = $wpdb->get_row($sql, 'ARRAY_A');

        if (isset($result['otp']) && $result['otp'] == $otp)
        {

            $data['status'] = 'true';
        }
    }
    return $data;
}

function getPasswordHash($phone)
{
    global $wpdb;
    if ($phone != '')
    {
        $sql = "SELECT * FROM " . $wpdb->prefix . "portal_login_otp WHERE `phone` = $phone";

        $result = $wpdb->get_row($sql, 'ARRAY_A');

        return md5($phone . $result['otp']);
    }
    return '';
}

function getPasswordHashWithOTP($phone, $otp)
{
    global $wpdb;

    if ($phone != '')
    {
        $sql = "SELECT * FROM " . $wpdb->prefix . "portal_login_otp WHERE `phone` = $phone";

        $result = $wpdb->get_row($sql, 'ARRAY_A');

        if (isset($result['otp']) && $result['otp'] == $otp)
        {

            return md5($phone . $result['otp']);
        }
    }
    return '';
}

function fetchUserData($phone)
{

    if (!empty($phone))
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://kutchina.securnyx360.com:444/api/registered_number",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n    \"phoneNumber\": {$phone}\r\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ) ,
        ));

        $response = curl_exec($curl);
        $data = json_decode($response, true);
        if (isset($data['data']['register No'][0]['Registered Number']))
        {
            return $data['data']['register No'][0];
        }

    }

    return '';
}

function verify_serial_number($request_data)
{
    $parameters = $request_data->get_params();
    $serialNumber = $parameters['serialNumber'];
    if (!empty($serialNumber))
    {
        return fetchProductData($serialNumber);
    }

}

function fetchProductData($productSerial)
{

    if (!empty($productSerial))
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://kutchina.securnyx360.com:444/api/getSerialNo",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n    \"productSerialNo\": {$productSerial}\r\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ) ,
        ));

        $response = curl_exec($curl);
        $data = json_decode($response, true);

        if (isset($data['data']['productSerialNo'][0]['SerialNo']))
        {
            return $data['data']['productSerialNo'][0];
        }

    }

    return '';
}

function getUserData($phone)
{
    global $wpdb;
    $table = $wpdb->prefix . "portal_user_details";
    $result = '';

    if ($phone != '')
    {
        $sql = "SELECT * FROM {$table} WHERE `customer_phone` = $phone";

        $result = $wpdb->get_row($sql, 'ARRAY_A');
    }
    if (empty($result))
    {
        $user = fetchUserData($phone);

        if (!empty($user))
        {

            $insert = array(
                'customer_phone' => $user['Registered Number'],
                'customer_name' => $user['Company Name'],
                'street' => $user['Street'],
                'city' => $user['City'],
                'state' => $user['State'],
                'zipcode' => $user['Zip Code'],
                'created' => current_time('mysql', 1) ,
            );

            $wpdb->insert($table, $insert);

            $sql = "SELECT * FROM {$table} WHERE `customer_phone` = $phone";

            $result = $wpdb->get_row($sql, 'ARRAY_A');
        }
    }
    return $result;
}

