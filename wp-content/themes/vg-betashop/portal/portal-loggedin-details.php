<?php
/**
 * Template Name: Portal Logged-in Details Template
 *
 * Description: Request A Callback page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$site_url = get_site_url();

$portal_url = $site_url."/portal/";
$portal_login_url = $site_url."/portal-login/";


if((isset($_POST['user_hidden_phone']) && !empty($_POST['user_hidden_phone'])) && (isset($_POST['otp_number']) && !empty($_POST['otp_number'])))
{
   $key = getPasswordHashWithOTP($_POST['user_hidden_phone'],$_POST['otp_number']);
   setcookie("portal", $key, time()+3600);
   setcookie("portal_user", $_POST['user_hidden_phone'], time()+3600);
   header("location:{$portal_url}");
   exit();
}


if(!isset($_COOKIE["portal"]))
{
 header("location:{$portal_login_url}");
 exit();
}

if($_GET['action'] == "logout")
{
   setcookie("portal", "", time()-3600);
   setcookie("portal_user", "", time()-3600);
   header("location:{$portal_login_url}");
   exit();
}

if(isset($_COOKIE["portal"]) && isset($_COOKIE["portal_user"]) && $_COOKIE["portal"] == getPasswordHash($_COOKIE["portal_user"]))
{
}else{
 header("location:{$portal_login_url}");
 exit();
}
   
   $userData = getUserData($_COOKIE["portal_user"]);

   $checked = "";  

   $new = 0;

   $disabled = 'disabled="disabled"';

   if(empty($userData ))
   {
      $userData =array (
        'customer_phone' => $_COOKIE["portal_user"],
        'customer_name' => '',
        'customer_email' => '',
        'street' => '',
        'city' => '',
        'state' => '',
        'zipcode' => '',
        'created' => '',
      );

      $checked = "checked";

      $disabled = "";

      $new = 1;
   }

/*print_r(fetchProductData('190421815163'));
exit();*/
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/portal/css/easy-responsive-tabs.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/portal/css/style.css">

<style type="text/css">
  #message, #message-user , #message-user-top {
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
    display: none;
    width: 100%;
    height: auto;
    margin-top: 5px;
    line-height: 15px;
    padding: 8px 0px;
    font-size: 14px;
}
.error{
  background-color: #c24550;
}
.success{
  background-color: #dff0d8;
}
#
#productSubmit:disabled {
       background-color: #666;
}
.modal {
   z-index: 1501
}
.modal-dialog {
    margin: 180px auto;
}
</style>
<?php $phnn_numb = $_COOKIE["portal_user"]; ?>

    <div class="page_wrap">
      <section class="bnr_page-c-casea">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="profile">
                     <div id="verticalTab">
                        <ul class="resp-tabs-list">
                           <div class="top-profile">
                              <div class="pp profile_pic"><img src="<?php echo get_template_directory_uri() ?>/portal/images/profile_pic.jpg" alt=""/></div>
                              <div class="pp profile_text">
                                 <h2><?php echo $userData['customer_name']; ?></h2>
                                 <p><?php echo $userData['customer_phone']; ?></p>
                              </div>
                           </div>
                           <li id="register"><span><i class="fa fa-user" aria-hidden="true"></i></span> Register a Product</li>
<!--                            <li id="details"><span><i class="fa fa-info-circle" aria-hidden="true"></i>
</span> My Details</li> -->
<!--                            <li><a href="<?php echo $portal_url."?action=logout";?>" ><span><i class="fa fa-sign-out" aria-hidden="true"></i>
</span> LogOut</a></li> -->
                           <!--      <li><span><i class="fa fa-sticky-note-o" aria-hidden="true"></i></span> Addresses </li> -->
                           <!--         <li><span><i class="fa fa-heart" aria-hidden="true"></i></span> Wishlist </li> -->
                           <!--         <li><span><i class="fa fa-tag" aria-hidden="true"></i></span> My Tickets</li> -->
                        </ul>
                        <div class="resp-tabs-container">

                           <div>
                              <h2> Register a Product </h2>
                              <form class="" id="productForm" action="" method="POST"  enctype="multipart/form-data">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Sl. Number</label>
                                          <input type="text" class="form-control" id="sl_nmbr" name="sl_nmbr" placeholder="Sl. Number" required="required">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
<!--                                        <div class="form-group">
                                          <label class="control-label">Date of purchase</label>
                                          <input type="date" class="form-control" id="purchs_date" name="purchs_date" placeholder="Date of purchase" required="required">
                                       </div> -->
                                    </div>
                                          <input type="hidden" class="form-control" name="phone_number" value="<?php echo $phnn_numb; ?>">
                                 </div>                                 
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <label class="control-label">Invoice</label>
                                          <input name="invoice" id="invoice" class="form-control" type="file" required="
                                          required"  accept="image,.pdf" />
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <button type="button" id="sl_nmbr_check" class="btn_sub btn-default">Check</button>
                                          <span id="sl_nmbr_check_status"></span>
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="form-group" id="sl_nmbr_check_status1">

                                       </div>
                                    </div>
                                  	<div class="col-md-4">
                                       <div class="form-group" id="sl_nmbr_check_status2">

                                       </div>
                                    </div>
                                 </div>
                                <div class="row" <?php if($new == 1) echo "style='display:none;'";?>  >
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <ul class="edit_adrs">
                                             <li><input type="checkbox" value='edit-address' class="chk" id="edt_optn" name="edt_optn" <?php echo $checked; ?> /></li>
                                             <li><label class="control-label ed_text">Edit Address</label></li>
                                          </ul>
                                       </div>
                                    </div>
                                </div>

                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Customer name</label>
                                          <input type="text" name="cust_name" <?php echo $disabled; ?> class="address-field form-control" id="cust_name" placeholder="Customer name" value="<?php echo $userData['customer_name']; ?>" required="required">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Your Phone Number</label>
                                          <p class="form-control not-edit"><strong><?php echo $userData['customer_phone']; ?></strong></p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Your Email</label>
                                          <input type="email" <?php echo $disabled; ?> class="address-field form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Email ID should be in this format abc@xyz.com "id="user-email" name="email" value="<?php echo $userData['customer_email']; ?>" placeholder="Your Email" required="required">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Address</label>
                                          <textarea <?php echo $disabled; ?> class="address-field form-control" id="address" name="street" placeholder="Address" required="required"><?php echo $userData['street']; ?></textarea>
                                          <!-- <input type="email" <?php echo $disabled; ?> class="address-field form-control" placeholder="Address" required="required"> -->
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">State</label>
                                          <input type="text" <?php echo $disabled; ?> class="address-field form-control" id="state" name="state" placeholder="State" value="<?php echo $userData['state']; ?>" required="required">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Pincode</label>
                                          <input type="text" <?php echo $disabled; ?> class="address-field form-control" id="pncd" name="zipcode" placeholder="Pincode" pattern="[0-9]{6}" maxlength="6" value="<?php echo $userData['zipcode']; ?>" required="required">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <button type="submit" id="productSubmit" class="btn_sub btn-default">Submit</button>
                                         
                                    <!--       <button type="submit" class="btn_edit btn-default">Edit</button> -->
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                       </div>
                                    </div>
                                 </div>
                              </form>
                               <p id="message"></p><span id="submit-loader"></span>
                           </div>
                           <div>
                              <h2>  My Details </h2>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <a href="#" title="Edit" data-toggle="modal" data-target="#editModal">Edit Address <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                       </div>
                                       <p id="message-user-top"></p>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group fg2">
                                          <label class="control-label">Customer name</label>
                                          <p><div class="user-value" id="user-customer-name"><?php echo $userData['customer_name']; ?></div></p>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group fg2">
                                          <label class="control-label">Your Phone Number</label>
                                          <p><div class="user-value" id="user-customer-phone"><?php echo $userData['customer_phone']; ?></div></p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group fg2">
                                          <label class="control-label">Your Email</label>
                                          <p><div class="user-value" id="user-customer-email"><?php echo $userData['customer_email']; ?></div></p>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group fg2">
                                          <label class="control-label">Address</label>
                                          <p><div class="user-value" id="user-street"><?php echo $userData['street']; ?></div></p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group fg2">
                                          <label class="control-label">State</label>
                                          <p><div class="user-value" id="user-state"><?php echo $userData['state']; ?></div></p>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group fg2">
                                          <label class="control-label">Pincode</label>
                                          <p><div class="user-value" id="user-zipcode"><?php echo $userData['zipcode']; ?></div></p>
                                       </div>
                                    </div>
                                 </div>

                           </div>

                        </div>
                     </div>
                     <br />
                     <div style="height: 30px; clear: both"></div>
                  </div>
               </div>
            </div>
      </section>
</div>
                              <!-- Modal-2 -->
                              <div id="editModal" class="modal" role="dialog">
                                 <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"> Edit Address </h4>
                                       </div>
                                       <div class="modal-body">
                                          <form class="" id="edit-user" action="">

                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="form-group">
                                                      <label class="control-label">Customer name</label>
                                                      <input type="text" required name="cust_name" class="form-control" value="<?php echo $userData['customer_name']; ?>" id="cust_name" placeholder="Customer name">
                                                   </div>
                                                </div>
                                                <div class="col-md-12">
                                                   <div class="form-group">
                                                      <label class="control-label">Your Phone Number</label>
                                                      <p class="ypn_d"><strong><?php echo $userData['customer_phone']; ?></strong></p>
                                                      <input type="hidden" name="phone_number" class="form-control" value="<?php echo $phnn_numb; ?>">
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="form-group">
                                                      <label class="control-label">Your Email</label>
                                                      <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Email ID should be in this format abc@xyz.com "class="form-control" value="<?php echo $userData['customer_email']; ?>" id="mod-email" placeholder="Your Email">
                                                   </div>
                                                </div>
                                                <div class="col-md-12">
                                                   <div class="form-group">
                                                      <label class="control-label">Address</label>
                                                      <input type="text" required name="street"  class="form-control" value="<?php echo $userData['street']; ?>" placeholder="Address">
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <div class="form-group">
                                                      <label class="control-label">State</label>
                                                      <input type="text" required name="state"  class="form-control" value="<?php echo $userData['state']; ?>" placeholder="State">
                                                   </div>
                                                </div>
                                                <div class="col-md-12">
                                                   <div class="form-group">
                                                      <label class="control-label">Pincode</label>
                                                      <input type="text" required name="zipcode"  pattern="[0-9]{4}" maxlength="4" class="form-control" value="<?php echo $userData['zipcode']; ?>" placeholder="Pincode">
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                      <button type="submit" id="userSubmit" class="btn_sub btn-default">Submit</button>
                                                      <p id="message-user"></p><span id="submit-loader-user"></span>
                                                      <!--<button type="submit" class="btn_edit btn-default">Edit</button>-->
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group">
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div id="confirm" class="modal" role="dialog">
                                 <div class="modal-dialog" style="margin: 100px auto !important;">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" id="closeBtn" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"> Confirm </h4>
                                       </div>
                                       <div class="modal-body">
                                        Please confirm before final submittion


  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="confirmBtn">Confirm</button>
    <button type="button" data-dismiss="modal" id="cancelBtn" class="btn">Cancel</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>

      <script src="<?php echo get_template_directory_uri() ?>/portal/js/jquery.min.js"></script>
  <!--     <script src="<?php echo get_template_directory_uri() ?>/portal/js/bootstrap.min.js"></script> -->
      <script src="<?php echo get_template_directory_uri() ?>/portal/js/easy-responsive-tabs.js"></script>
      <script>
         $(document).ready(function () {



$("#productForm").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);
      $( "#submit-loader" ).html("<img style='width:25px' src='<?php echo get_template_directory_uri() ?>/portal/images/loading.gif' />");
      $("#productSubmit").prop('disabled', true); 

      $('#confirm').modal({
          backdrop: 'static',
          keyboard: false
      });
      $("#confirmBtn").on('click', function(e) {
                $.ajax({
                  url: "<?php echo get_site_url(); ?>/wp-json/portal/v1/update-product",
                  type: 'POST',
                  data: formData,
                  success: function (data) {
                   $("#productSubmit").prop('disabled', false); 
                   $( "#submit-loader" ).html("");
                   console.log(data);
                   if(data['status'] == 'error')
                   {
                      $( "#message" ).removeClass();
                      $( "#message" ).addClass('error');
                      $( "#message" ).html(data['message']).fadeIn( 300 ).delay( 5000 ).fadeOut( 2000 );
                   }
                   if(data['status'] == 'success')
                   {
                      $( "#message" ).removeClass();
                      $( "#message" ).addClass('success');
                      $( "#message" ).html('');
                      $( "#message" ).html(data['message']).fadeIn( 300 );
                      if(data['message'] == "Successfully added")
                      {
                        $('#sl_nmbr').val('');
                        $('#invoice').val('');
                        $('#productForm').hide();
                      }

                   }
                  },
                  cache: false,
                  contentType: false,
                  processData: false
              });
        });
      $("#cancelBtn,#closeBtn").on('click',function(e){
       e.preventDefault();  
       $("#productSubmit").prop('disabled', false); 
       $( "#submit-loader" ).html("");
       $('#confirm').modal.model('hide');
      });




         });

$("#edit-user").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);
      $( "#submit-loader-user" ).html("<img style='width:25px' src='<?php echo get_template_directory_uri() ?>/portal/images/loading.gif' />");
      $("#userSubmit").prop('disabled', true); 

    $.ajax({
        url: "<?php echo get_site_url(); ?>/wp-json/portal/v1/update-user",
        type: 'POST',
        data: formData,
        success: function (data) {
         $("#userSubmit").prop('disabled', false); 
         $( "#submit-loader-user" ).html("");
         console.log(data);
         if(data['status'] == 'error')
         {
            $( "#message-user" ).removeClass();
            $( "#message-user" ).addClass('error');
            $( "#message-user" ).html(data['message']).fadeIn( 300 ).delay( 5000 ).fadeOut( 2000 );
         }
         if(data['status'] == 'success')
         {
            $( "#message-user" ).removeClass();
            $( "#message-user" ).addClass('success');
            $( "#message-user" ).html(data['message']).fadeIn( 300 ).delay( 5000 ).fadeOut( 2000 );
            $( "#message-user-top" ).html(data['message']).fadeIn( 300 ).delay( 5000 ).fadeOut( 2000 );
            $( "#user-customer-name" ).html(data['data']['cust_name']);
            $( "#user-customer-phone" ).html(data['data']['phone_number']);
            $( "#user-customer-email" ).html(data['data']['email']);
            $( "#user-street" ).html(data['data']['street']);
            $( "#user-state" ).html(data['data']['state']);
            $( "#user-zipcode" ).html(data['data']['zipcode']);   
            $('#editModal').modal('toggle');         
         }
      
        },
        cache: false,
        contentType: false,
        processData: false
    });


});

    $('input[type="checkbox"]#edt_optn').click(function(){
        
        var atLeastOneIsChecked = $('input[type="checkbox"]#edt_optn:checked').length > 0;
        if(atLeastOneIsChecked)
        {
         jQuery(".address-field").prop("disabled", false);
        }
        else{
         jQuery(".address-field").prop("disabled", true);

        }
    });

    $('#sl_nmbr_check').click(function(){
       serialNumber = $('#sl_nmbr').val();

       $( "#sl_nmbr_check_status1" ).html("<img style='width:25px' src='<?php echo get_template_directory_uri() ?>/portal/images/loading.gif' />");
       $( "#sl_nmbr_check_status2" ).html("<img style='width:25px' src='<?php echo get_template_directory_uri() ?>/portal/images/loading.gif' />");
         jQuery.ajax({
           url: "<?php echo get_site_url(); ?>/wp-json/portal/v1/verify-serial-number?serialNumber=" + serialNumber ,
           type: "GET",
           success: function (data) {
            if (typeof(data['ProdName']) != "undefined")
            {
            $( "#sl_nmbr_check_status" ).html("<img style='width:25px' src='<?php echo get_template_directory_uri() ?>/portal/images/tick.png' />");
            $( "#sl_nmbr_check_status1" ).html('<label class="control-label">Product Name</label><br/>' + data['ProdName'] + '<input type="hidden" name="prod_name" id="prod_name" value="'+ data['ProdName'] +'">' );
            $( "#sl_nmbr_check_status2" ).html('<label class="control-label">Product Code</label><br/>' + data['ProdCode'] + '<input type="hidden" name="prod_code" id="prod_code" value="'+ data['ProdCode'] +'">' );
          }else{
            $( "#sl_nmbr_check_status1" ).html('Serial No Invalid' );
            $( "#sl_nmbr_check_status2" ).html('');
           }
          }
         });

       });

         
         $('#verticalTab').easyResponsiveTabs({
         type: 'vertical',
         width: 'auto',
         fit: true
         });

if (window.location.hash !== '') {
    $('#verticalTab').find(window.location.hash).click();
}
         });


      </script>


<?php betashop_get_footer(); 


?>