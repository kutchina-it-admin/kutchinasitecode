<?php
   /**
   * Template Name: Portal Login Template
   *
   * Description: Request A Callback page template
   *
   * @package    VG Amadea
   * @author     VinaGecko <support@vinagecko.com>
   * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
   * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
   *
   * Websites: http://vinagecko.com
   */
   $betashop_options  = betashop_get_global_variables();
   
   betashop_get_header();
   ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/portal/css/style.css">
<style type="text/css">
  #message {
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
    display: none;
    width: 100%;
    height: auto;
    margin-top: 5px;
    line-height: 15px;
    padding: 8px 0px;
    font-size: 14px;
}
</style>
<div class="container">
   <div class="formbox1" id="firstt_frm">
      <div class="p-text">
         <h3>Portal login</h3>
      </div>
      <form class="formsec" id="portal_det_frm" name="portal_det_frm" action="<?php echo get_site_url(); ?>/portal/" method="POST">
         <div class="verify_phone_otp_main">
            <div class="phone_send_otp" id="phone_send_otp">
               <span style="color:#fff;">+91</span><input type="text" class="forminp" placeholder="Enter Phone Number" value="" id="user_phone" name="user_phone" onkeyup="return numValidate();" >
              <!--  9435081382 -->
               <button class="frbt" id="loc_but" type="button" >Send OTP</button>
            </div>
            <div class="verify_otp" id="verify_otp" style="display: none;">
               <input type="hidden" class="forminp" placeholder="Enter OTP" id="user_hidden_phone" name="user_hidden_phone" />
               <input type="number"  value="" class="forminp" placeholder="Enter OTP" id="otp_number" name="otp_number" />
               <input type="button" class="frbt" id="vrf_otp_btn" value="Verify">
            </div>
            <p id="message"></p>
         </div>
      </form>
   </div>
   <div class="formbox1" id="scndd_frm">
      <form class="formsec" id="portal_otp_frm">
      </form>
   </div>
</div>
<?php betashop_get_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" ></script>
<script type="text/javascript">
function numValidate() {
  var element = document.getElementById('user_phone');
  element.value = element.value.replace(/[^0-9_]+/, '');
};

jQuery(document).ready(function () {


  jQuery("#otp_number").attr("disabled", true);
  jQuery("#vrf_otp_btn").attr("disabled", true);


  jQuery("#loc_but").click(function () {
    var user_phone = jQuery("#user_phone").val();

    var phone_pattern = /^[\d\s+-]+$/;
    if (user_phone == '') {
      //alert("aa");
      alert("Enter your 10 digit phone number");
      jQuery("#user_phone").focus();
      return false;
    } else if (!phone_pattern.test(user_phone)) {
      alert("Invalid phone number");
      jQuery("#user_phone").focus();
      return false;
    } else if (user_phone.length < 10 || user_phone.length > 11) {
      alert("Invalid phone number");
      jQuery("#user_phone").focus();
      return false;
    } else {
      jQuery("#user_phone").attr("disabled", true);
      jQuery("#loc_but").attr("disabled", true);

      jQuery.ajax({
        url: "<?php echo get_site_url(); ?>/wp-json/portal/v1/send-otp?phone=" + user_phone,
        type: "GET",
        success: function () {
          jQuery("#message" ).html("OTP Sent. OTP Valid For 2 mins").fadeIn( 300 ).delay( 5000 ).fadeOut( 2000 );
          return false;
        }
      });

      jQuery("#phone_send_otp").hide();
      jQuery("#verify_otp").fadeIn(2000);


      jQuery("#otp_number").attr("disabled", false);
      jQuery("#vrf_otp_btn").attr("disabled", false);

      jQuery('#user_hidden_phone').attr('value', user_phone);
    }
  });

  jQuery("#vrf_otp_btn").click(function () {
    var otp_number = jQuery("#otp_number").val();
    var user_phone = jQuery("#user_phone").val();

    var otp_pattern = /^[\d\s+-]+$/;
    if (otp_number == '') {
      jQuery("#message" ).html("Enter your OTP").fadeIn( 1000 ).delay( 5000 ).fadeOut( 1000 );
      jQuery("#otp_number").focus();
      return false;
    } else if (!otp_pattern.test(otp_number)) {
      jQuery("#message" ).html("Invalid phone number").fadeIn( 1000 ).delay( 5000 ).fadeOut( 1000 );
      jQuery("#otp_number").focus();
      return false;
    } else if (otp_number.length < 4 || otp_number.length > 4) {
      jQuery("#message" ).html("Invalid OTP").fadeIn( 1000 ).delay( 5000 ).fadeOut( 1000 );
      jQuery("#otp_number").focus();
      return false;
    } else {
      jQuery.ajax({
        url: "<?php echo get_site_url(); ?>/wp-json/portal/v1/verify-otp?phone=" + user_phone + "&otp=" + otp_number,
        type: "GET",
        success: function (data) {

          if (data['status'] == 'true') {
            jQuery("#message" ).html("Success").fadeIn( 1000 ).delay( 5000 ).fadeOut( 1000 );
            document.getElementById("portal_det_frm").submit();
          } else {
            jQuery("#message" ).html("Invalid OTP").fadeIn( 1000 ).delay( 5000 ).fadeOut( 1000 );
            jQuery("#otp_number").focus();
            return false;
          }
        }
      });


    }
  });


});
</script>