<?php
/**
 * @version    1.2
 * @package    VG Betashop
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>

<?php
	$betashop_options  = betashop_get_global_variables(); 
	$woocommerce 	= betashop_get_global_variables('woocommerce'); 
?>

<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />


<?php if (isset($betashop_options['share_head_code']) && $betashop_options['share_head_code']!='') {
	echo wp_kses($betashop_options['share_head_code'], array(
		'script' => array(
			'type' 	=> array(),
			'src' 	=> array(),
			'async' => array()
		),
	));
} ?>

<?php 
	// HieuJa get Preset Color Option
	$presetopt = betashop_get_preset();
	// HieuJa end block
?>

<?php wp_head(); ?>
</head>

<!-- Body Start Block -->
<body <?php body_class('bghome'); ?>>

<!-- Page Loader Block -->
<?php if (isset($betashop_options['betashop_loading']) && $betashop_options['betashop_loading']) : ?>
<div id="pageloader">
	<div id="loader"></div>
	<div class="loader-section left"></div>
	<div class="loader-section right"></div>
</div>
<?php endif; ?>

<div id="yith-wcwl-popup-message"><div id="yith-wcwl-message"></div></div>
<div class="wrapper box-layout">

	<!-- Top Header -->
	<div class="top-wrapper">
		<div class="header-container">
			<?php if (is_active_sidebar('sidebar-top-header')) : ?>
			<div class="top-bar">
				<div class="container">
					<div id="top">
						<div class="row">
								<?php dynamic_sidebar('sidebar-top-header'); ?>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
			
			<div class="header home1">
				<div class="container">
					<div class="row">
						
						<?php 
						$col_logo = '';
						if (is_active_sidebar('sidebar-text-welcome') && class_exists('Custom_Ajax_search_widget')) {
							$col_logo = '3' ;
						 
						}elseif(is_active_sidebar('sidebar-text-welcome') || class_exists('Custom_Ajax_search_widget')){
							$col_logo = '6';
						}else {
							$col_logo = '12 logo-center';
						}
						?>
						<div id="sp-logo" class="col-xs-12 col-md-<?php echo esc_attr($col_logo); ?>">
						<?php if(isset($betashop_options['logo_main']) && !empty($betashop_options['logo_main']['url'])){ ?>
							<div class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<img src="<?php echo esc_url($betashop_options['logo_main']['url']); ?>" alt="" />
								</a>
							</div>
						<?php } else if(isset($betashop_options['logo_text']) && !empty($betashop_options['logo_text'])) {?>
							<h1 class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<?php echo esc_html($betashop_options['logo_text']); ?>
								</a>
							</h1>
						<?php } else { ?>
							<div class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<img src="<?php echo esc_url(get_template_directory_uri() . '/images/presets/preset'. $presetopt .'/logo.gif' ); ?>" alt="" />
								</a>
							</div>
						<?php } ?>
						</div>
						
						<?php if (is_active_sidebar('sidebar-text-welcome')) : ?>
						<div class="text-top-bar col-xs-12 col-md-<?php echo (class_exists('Custom_Ajax_search_widget')) ? '4' : '6'; ?>">
							<div class="vg-welcome">
							<?php
								dynamic_sidebar( 'sidebar-text-welcome' );
							?>
							</div>
						</div>
						<?php endif; ?>
							
						<?php if(class_exists('Custom_Ajax_search_widget')) : ?>
						<div id="sp-search-cart" class="col-xs-12 col-md-<?php echo (is_active_sidebar('sidebar-text-welcome')) ? '5' : '6'; ?>">
							<div class="search-toggle">
									<i class="fa fa-search"></i>
							</div>
							<div id="sp-search" class="search-sticky">
								<div class="vg-search home1">
									  <?php ajax_autosuggest_form(); ?>
								</div>		
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<!-- Menu -->
			<div class="sp-main-menu-wrapper home1">
				<div class="container">
					<div class="row">
						<div class="menu-wrapper col-xs-12">
							<div id="menu-container">
								<div id="header-menu">
									<div class="header-menu visible-large">
										<?php echo wp_nav_menu(array("theme_location" => "primary","")); ?>
									</div>
									<div class="visible-small">
										<div class="mbmenu-toggler"><span class="title"><?php echo (isset($betashop_options['title_mobile_menu'])) ? esc_html($betashop_options['title_mobile_menu']) : esc_html__('Menu', 'vg-betashop'); ?></span><span class="mbmenu-icon"></span></div>
										<div class="nav-container">
											<?php wp_nav_menu(array('theme_location' => 'mobilemenu', 'container_class' => 'mobile-menu-container', 'menu_class' => 'nav-menu')); ?>
										</div>
									</div>
								</div>
								<?php if (class_exists('WC_Widget_Cart')) { ?>
								<div class="sp-cart home1">
									<div class="vg-cart">
										<?php the_widget('Custom_WC_Widget_Cart'); ?>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
							
					</div>
				</div>
			</div>
		</div>
	</div>
	
	