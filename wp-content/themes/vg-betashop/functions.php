<?php
/**
 * @version    1.2
 * @package    VG Betashop
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */

//Require plugins
require_once get_template_directory () . '/class-tgm-plugin-activation.php';

function betashop_register_required_plugins() {

    $plugins = array(
		array(
            'name'               => esc_html__('Mega Main Menu', 'vg-betashop'),
            'slug'               => 'mega_main_menu',
            'source'             => esc_url('http://wordpress.vinagecko.net/l/mega_main_menu.zip'),
            'required'           => true,
            'external_url'       => '',
        ),
		array(
            'name'               => esc_html__('Visual Composer', 'vg-betashop'),
            'slug'               => 'js_composer',
            'source'             => esc_url('http://wordpress.vinagecko.net/l/js_composer.zip'),
            'required'           => true,
            'external_url'       => '',
        ),
		array(
            'name'               => esc_html__('Redux Framework', 'vg-betashop'),
            'slug'               => 'redux-framework',
            'required'           => true,
        ),
		array(
            'name'               => esc_html__('VG WooCarousel', 'vg-betashop'),
            'slug'               => 'vg-woocarousel',
			'source'             => esc_url('http://wordpress.vinagecko.net/l/vg-woocarousel.zip'),
            'required'           => true,
            'external_url'       => '',
        ),
		array(
            'name'               => esc_html__('VG PostCarousel', 'vg-betashop'),
            'slug'               => 'vg-postcarousel',
			'source'             => esc_url('http://wordpress.vinagecko.net/l/vg-postcarousel.zip'),
            'required'           => true,
            'external_url'       => '',
        ),
		array(
            'name'               => esc_html__('Revolution Slider', 'vg-betashop'),
            'slug'               => 'revslider',
            'source'             => esc_url('http://wordpress.vinagecko.net/l/revslider.zip'),
            'required'           => true,
            'external_url'       => '',
        ),
        // Plugins from the WordPress Plugin Repository.

		array(
            'name'      => esc_html__('WooCommerce', 'vg-betashop'),
            'slug'      => 'woocommerce',
            'required'  => true,
        ),
		array(
            'name'      		 => esc_html__('WordPress AJAX Search & AutoSuggest Plugin', 'vg-betashop'),
            'slug'     			 => 'ajax-search-autosuggest',
            'required' 			 => true,
            'source'             => esc_url('http://wordpress.vinagecko.net/l/ajax-autosuggest-plugin.zip'),
            'external_url'       => '',
        ),		
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => esc_html__( 'Install Required Plugins', 'vg-betashop' ),
            'menu_title'                      => esc_html__( 'Install Plugins', 'vg-betashop' ),
            'installing'                      => esc_html__( 'Installing Plugin: %s', 'vg-betashop' ), // %s = plugin name.
            'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'vg-betashop' ),
            'notice_can_install_required'     => _n_noop( 'This g requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'vg-betashop' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'vg-betashop' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'vg-betashop' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'vg-betashop' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'vg-betashop' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'vg-betashop' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'vg-betashop' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'vg-betashop' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'vg-betashop' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'vg-betashop' ),
            'return'                          => esc_html__( 'Return to Required Plugins Installer', 'vg-betashop' ),
            'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'vg-betashop' ),
            'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'vg-betashop' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'betashop_register_required_plugins' ); 

//Init the Redux Framework
if ( class_exists( 'ReduxFramework' ) && !isset( $redux_demo ) && file_exists( get_template_directory().'/theme-config.php' ) ) {
    require_once( get_template_directory().'/theme-config.php' );
}


//Add Woocommerce support
add_theme_support( 'woocommerce' );
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

function betashop_start_wrapper_here() { // Start wrapper 
	
	if (is_shop()) { // Wrapper for the shop page

		echo '<div class="shop-wrapper">';

	} elseif (is_product_category() || is_product_tag()) {  // Wrapper for a product category page

		echo '<div class="product-category-wrapper">';

	} elseif (is_product()) { // Wrapper for a single product page

		echo '<div class="product-wrapper">';

	}
}

add_action(  'woocommerce_before_main_content', 'betashop_start_wrapper_here', 20  );


add_action(  'woocommerce_before_main_content', 'betashop_start_wrapper_here_end', 50  );
function betashop_start_wrapper_here_end() { // Start wrapper
	echo '</div>';
}

//Override woocommerce widgets
function betashop_override_woocommerce_widgets() {
	//Show mini cart on all pages
	if ( class_exists( 'WC_Widget_Cart' ) ) {
		unregister_widget( 'WC_Widget_Cart' ); 
		include_once( get_template_directory() . '/woocommerce/class-wc-widget-cart.php' );
		register_widget( 'Custom_WC_Widget_Cart' );
	}
}
add_action( 'widgets_init', 'betashop_override_woocommerce_widgets', 15 );

// Swallow code Override ajax search autosuggest widget
//Override woocommerce widgets
function betashop_override_ajaxsearch_autosuggest_widgets() {
	//Show mini cart on all pages
	if ( class_exists( 'Ajax_search_widget' ) ) {
		unregister_widget( 'Ajax_search_widget' ); 
		include_once( get_template_directory() . '/include/ajax-search-autosuggest.php' );
		register_widget( 'Custom_Ajax_search_widget' );
	}
}
add_action( 'widgets_init', 'betashop_override_ajaxsearch_autosuggest_widgets', 15 );

add_filter('woocommerce_product_get_rating_html', 'betashop_get_rating_html', 10, 2);

function betashop_get_rating_html($rating_html, $rating) {
  global $product;
  if ( $rating > 0 ) {
    $title = sprintf( esc_html__( 'Rated %s out of 5', 'vg-betashop' ), $rating );
  } else {
    $title = 'Not yet rated';
    $rating = 0;
  }

  $rating_html  = '<div class="vgwc-product-rating">';
	  $rating_html  .= '<div class="star-rating" title="' . esc_attr($title) . '">';
		$rating_html .= '<span style="width:' . esc_attr( ( $rating / 5 ) * 100 ) . '%"><strong class="rating">' . $rating . '</strong> ' . esc_html__( 'out of 5', 'vg-betashop' ) . '</span>';
	  $rating_html .= '</div>';
	  $rating_html .= $product->get_review_count(). esc_html__(" review(s)", "vg-betashop");
  $rating_html .= '</div>';

  return $rating_html;
}

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
function betashop_woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
	
	<span class="mcart-number"><?php echo WC()->cart->cart_contents_count; ?></span>
	
	<?php
	$fragments['span.mcart-number'] = ob_get_clean();
	
	return $fragments;
} 
add_filter( 'woocommerce_add_to_cart_fragments', 'betashop_woocommerce_header_add_to_cart_fragment' );

//Change price html
add_filter( 'woocommerce_get_price_html', 'betashop_woo_price_html', 100, 2 );
function betashop_woo_price_html( $price, $product ){
	if ( $product->is_type( 'variable' ) ) {
		return '<div class="vgwc-product-price price-variable">'. $price .'</div>';
	}
	else {
		return '<div class="vgwc-product-price">'. $price .'</div>';
	}
}

remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
// Add image to category description
// Code Changes by SUMAN for Custom Design ///////////////////////////////////////////////////////////////////////////////
function betashop_woocommerce_category_image() {
	global $wp_query, $betashop_options;
	if ( is_product_category() || is_shop() || is_product_tag()){
		$image = '';

		if(is_product_category() || is_product_tag()) {
			$cat = $wp_query->get_queried_object();
			$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
			$image = wp_get_attachment_url( $thumbnail_id );
		}
		if( empty($cat->description) && isset($betashop_options['cat_description']) && !empty($betashop_options['cat_description'])) {
			?><div class="term-description vg-description"><p><?php echo  $betashop_options['cat_description']; ?></p></div><?php
		}
		else if(!empty($cat->description)) {
			?><div class="term-description"><p><?php echo  $cat->description ?></p></div><?php	
		}
	    /*if ( ($image && !empty($image))) { ?>
		   <div class="category-image-desc"><img src="<?php echo esc_url($image); ?>" alt=""></div>
		<?php } else if(isset($betashop_options['cat_banner_link']) && !empty($betashop_options['cat_banner_link']) && isset($betashop_options['cat_banner_img']) && !empty($betashop_options['cat_banner_img'])) { ?>
			<div class="category-image-desc vg-cat-img"><a href="<?php echo esc_url($betashop_options['cat_banner_link']); ?>"><img src="<?php echo esc_url($betashop_options['cat_banner_img']['url']); ?>" alt=""></a></div>
		<?php } else if(isset($betashop_options['cat_banner_img']['url']) && !empty($betashop_options['cat_banner_img']['url'])) { ?>
			<div class="category-image-desc vg-cat-img"><img src="<?php echo esc_url($betashop_options['cat_banner_img']['url']); ?>" alt=""></div>
		<?php } else if(empty($image)){  ?>
			<div class="category-image-desc vg-cat-img"><img src="<?php echo esc_url(get_template_directory_uri() . '/images/category-image.jpg') ; ?>" alt=""></div>
		<?php 
		}*/
	}
}
add_action( 'woocommerce_archive_description', 'betashop_woocommerce_category_image', 20 );

// Change products per page
function betashop_woo_change_per_page() {
	global $betashop_options;
	
	$product_per_page = isset($betashop_options['product_per_page']) ? $betashop_options['product_per_page'] : 16 ;
	
	return $product_per_page;
}
add_filter( 'loop_shop_per_page', 'betashop_woo_change_per_page', 20 );

function betashop_copyright() {
	global $betashop_options;
	$copynotice = (isset($betashop_options['copyright-notice'])) ? $betashop_options['copyright-notice'] : '' ;
	$copylink 	= (isset($betashop_options['copyright-link'])) ? $betashop_options['copyright-link'] : '' ;
	if(strpos($copynotice,'{') && strpos($copynotice,'}')) {
		$copyright = str_ireplace('{','<a href="' . esc_url($copylink) .'">',$copynotice);
		$copyright = str_ireplace('}','</a>',$copyright);
	}
	else {
		$copyright = $copynotice;
	}
	return $copyright;
}

//Limit number of products by shortcode [products]
//add_filter( 'woocommerce_shortcode_products_query', 'betashop_woocommerce_shortcode_limit' );
function betashop_woocommerce_shortcode_limit( $args ) {
	global $betashop_options, $betashop_productsfound;
	
	if(isset($betashop_options['shortcode_limit']) && $args['posts_per_page']==-1) {
		$args['posts_per_page'] = $betashop_options['shortcode_limit'];
	}
	
	$betashop_productsfound = new WP_Query($args);
	$betashop_productsfound = $betashop_productsfound->post_count;
	
	return $args;
}

// Change number or products per row to 4
function betashop_loop_columns() {
	global $betashop_options;
	
	$product_per_row = isset($betashop_options['product_per_row']) ? $betashop_options['product_per_row'] : 3;
	
	return $product_per_row;
}
add_filter('loop_shop_columns', 'betashop_loop_columns', 999);

//Change number of related products on product page. Set your own value for 'posts_per_page'
function betashop_woo_related_products_limit( $args ) {
	global $product, $betashop_options;
	
	$related_amount = isset($betashop_options['related_amount']) ? $betashop_options['related_amount'] : 6 ;
	$args['posts_per_page'] = $related_amount;

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'betashop_woo_related_products_limit' );

//move message to top
remove_action( 'woocommerce_before_shop_loop', 'wc_print_notices', 10 );
add_action( 'woocommerce_show_message', 'wc_print_notices', 10 );


function betashop_view_mode_woocommerce_shop_loop() {
	global $betashop_options;
	
	$layout_product= isset($betashop_options['layout_product']) ? $betashop_options['layout_product'] : 'gridview';
	
	
	$customJSGrid = "
		jQuery(document).ready(function(){
			jQuery('.view-mode').each(function(){
				/* Grid View */					
				jQuery('#archive-product .view-mode').find('.grid').addClass('active');
				jQuery('#archive-product .view-mode').find('.list').removeClass('active');
				
				jQuery('#archive-product .shop-products').removeClass('list-view');
				jQuery('#archive-product .shop-products').addClass('grid-view');
				
				jQuery('#archive-product .list-col4').removeClass('col-xs-12 col-sm-6 col-lg-4');
				jQuery('#archive-product .list-col8').removeClass('col-xs-12 col-sm-6 col-lg-8');
			});
		});
	";
	
	$customJSList = "
		jQuery(document).ready(function(){
			jQuery('.view-mode').each(function(){
				/* List View */								
				jQuery('#archive-product .view-mode').find('.list').addClass('active');
				jQuery('#archive-product .view-mode').find('.grid').removeClass('active');
				
				jQuery('#archive-product .shop-products').addClass('list-view');
				jQuery('#archive-product .shop-products').removeClass('grid-view');
				
				jQuery('#archive-product .list-col4').addClass('col-xs-12 col-sm-6 col-lg-4');
				jQuery('#archive-product .list-col8').addClass('col-xs-12 col-sm-6 col-lg-8');
			});
		});
	";
	
	$customJS = ($layout_product == 'gridview') ? $customJSGrid : $customJSList;
	wp_add_inline_script('betashop-theme-js', $customJS);
}
add_filter( 'woocommerce_before_shop_loop', 'betashop_view_mode_woocommerce_shop_loop', 5 );

function betashop_view_mode_woocommerce_shop_loop_url() {
	$view = 'gridview';
	if(isset($_GET['view']) && $_GET['view']!=''){
		$view = $_GET['view'];
		$customJSGrid = "
			jQuery(document).ready(function(){
				jQuery('.view-mode').each(function(){
					/* Grid View */					
					jQuery('#archive-product .view-mode').find('.grid').addClass('active');
					jQuery('#archive-product .view-mode').find('.list').removeClass('active');
					
					jQuery('#archive-product .shop-products').removeClass('list-view');
					jQuery('#archive-product .shop-products').addClass('grid-view');
					
					jQuery('#archive-product .list-col4').removeClass('col-xs-12 col-sm-6 col-lg-4');
					jQuery('#archive-product .list-col8').removeClass('col-xs-12 col-sm-6 col-lg-8');
				});
			});
		";
		
		$customJSList = "
			jQuery(document).ready(function(){
				jQuery('.view-mode').each(function(){
					/* List View */								
					jQuery('#archive-product .view-mode').find('.list').addClass('active');
					jQuery('#archive-product .view-mode').find('.grid').removeClass('active');
					
					jQuery('#archive-product .shop-products').addClass('list-view');
					jQuery('#archive-product .shop-products').removeClass('grid-view');
					
					jQuery('#archive-product .list-col4').addClass('col-xs-12 col-sm-6 col-lg-4');
					jQuery('#archive-product .list-col8').addClass('col-xs-12 col-sm-6 col-lg-8');
				});
			});
		";
		
		$customJS = ($view == 'gridview') ? $customJSGrid : $customJSList;
		wp_add_inline_script('betashop-theme-js', $customJS);
	}
}
add_filter( 'woocommerce_after_girdview', 'betashop_view_mode_woocommerce_shop_loop_url', 30 );

/*Remove product link open*/
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );


/*Close div*/

//Single product organize
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 25 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
add_action( 'woocommerce_show_related_products', 'woocommerce_output_related_products', 20 );
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals'); 
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_short_description', 10 );


function betashop_single_meta(){
	global $betashop_options;
	//remove single meta
	if(isset($betashop_options['single_meta']) && $betashop_options['single_meta']) {
	  remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	}
}
add_action( 'init', 'betashop_single_meta' );

//Display social sharing on product page
function betashop_woocommerce_social_share(){
	global $betashop_options;
?>
	<div class="share_buttons">
		<?php if (isset($betashop_options['share_code']) && $betashop_options['share_code']!='') {
			echo wp_kses($betashop_options['share_code'], array(
				'div' => array(
					'class' => array()
				),
				'span' => array(
					'class' => array(),
					'displayText' => array()
				),
			));
		} ?>
	</div>
<?php
}
add_action( 'vg_social_share', 'betashop_woocommerce_social_share', 35 );

//Display stock status on product page
function betashop_product_stock_status(){
	global $product;
	?>
	<div class="in-stock">
		<?php esc_html_e('Availability:', 'vg-betashop');?> 
		<?php if($product->is_in_stock()){ ?>
			<span><?php echo $product->get_stock_quantity()." "; ?><?php esc_html_e('In stock', 'vg-betashop');?></span>
		<?php } else { ?>
			<span class="out-stock"><?php esc_html_e('Out of stock', 'vg-betashop');?></span>
		<?php } ?>	
	</div>
	<?php
}
add_action( 'woocommerce_single_product_summary', 'betashop_product_stock_status', 10 ); 
 
//Show countdown on product page
function betashop_product_countdown(){
	global $product;
	$product_countdown ='';
	if ($product->is_in_stock()) {
	?>
		<?php
		$countdown = false;
		$sale_end = get_post_meta( $product->get_id(), '_sale_price_dates_to', true );
		/* simple product */
		if($sale_end){
			$countdown = true;
			$sale_end = date('Y/m/d', (int)$sale_end);
			?>
			<?php $product_countdown .='<div class="box-timer"><div class="timer-grid" data-time="'.esc_attr($sale_end).'"></div></div>'; ?>
		<?php } ?>
		<?php /* variable product */
		if($product->get_children()){
			$vsale_end = array();
			
			foreach($product->get_children() as $pvariable){
				$vsale_end[] = (int)get_post_meta( $pvariable, '_sale_price_dates_to', true );
				
				if( get_post_meta( $pvariable, '_sale_price_dates_to', true ) ){
					$countdown = true;
				}
			}
			if($countdown){
				/* get the latest time */
					$vsale_end_date = max($vsale_end);
					$vsale_end_date = date('Y/m/d', $vsale_end_date);
					?>
					<?php $product_countdown .='<div class="box-timer"><div class="timer-grid" data-time="'.esc_attr($vsale_end_date).'"></div></div>'; ?>
				<?php
			}
		}
		?>
	<?php
	}
	echo $product_countdown;
}

add_action( 'woocommerce_single_product_summary', 'betashop_product_countdown', 35 ); 
add_action( 'get_betashop_product_countdown', 'betashop_product_countdown' ); 
 
//Show buttons wishlist, compare, email on product page
function betashop_product_buttons(){
	global $product;
	?>
	<?php if(class_exists('YITH_WCWL') || class_exists('YITH_Woocompare')){ ?>
	<div class="actions">
		<div class="action-buttons">
			<div class="add-to-links">
				<?php if(class_exists('YITH_Woocompare')){ ?>
					<?php echo do_shortcode('[yith_compare_button]') ?>
				<?php } ?>
				
				<?php if(class_exists('YITH_WCWL')) { ?> 
					<?php echo preg_replace("/<img[^>]+\>/i", " ", do_shortcode('[yith_wcwl_add_to_wishlist]')); ?>
				<?php } ?>
				
			</div>
			<?php echo '<div class="sharefriend"><a href="mailto: yourfriend@domain.com?Subject=Checkout this product: '.esc_attr($product->get_title()).'">'.esc_html__( 'Email your friend', 'vg-betashop' ).'</a></div>'; ?>
			
		</div>
	</div>
	<?php } ?>
	<?php
}
add_action( 'woocommerce_single_product_summary', 'betashop_product_buttons', 30 );

//Project organize
remove_action( 'projects_before_single_project_summary', 'projects_template_single_title', 10 );
add_action( 'projects_single_project_summary', 'projects_template_single_title', 5 );
remove_action( 'projects_before_single_project_summary', 'projects_template_single_short_description', 20 );
remove_action( 'projects_before_single_project_summary', 'projects_template_single_gallery', 40 );
add_action( 'projects_single_project_gallery', 'projects_template_single_gallery', 40 );
//projects list
remove_action( 'projects_loop_item', 'projects_template_loop_project_title', 20 ); 
 
//Change search form
function betashop_search_form( $form ) {
	if(get_search_query()!=''){
		$search_str = get_search_query();
	} else {
		$search_str = esc_html__( 'Search...', 'vg-betashop' );
	}
	
	$form = '<form role="search" method="get" id="blogsearchform" class="searchform" action="' . esc_url(home_url( '/' ) ). '" >
	<div class="form-input">
		<input class="input_text" type="text" value="'.esc_attr($search_str).'" name="s" id="search_input" />
		<button class="button" type="submit" id="blogsearchsubmit"><i class="fa fa-search"></i></button>
		<input type="hidden" name="post_type" value="post" />
		</div>
	</form>';
	$inlineJS = '
		jQuery(document).ready(function(){
			jQuery("#search_input").focus(function(){
				if(jQuery(this).val()=="'. esc_html__( 'Search...', 'vg-betashop' ).'"){
					jQuery(this).val("");
				}
			});
			jQuery("#search_input").focusout(function(){
				if(jQuery(this).val()==""){
					jQuery(this).val("'. esc_html__( 'Search...', 'vg-betashop' ).'");
				}
			});
			jQuery("#blogsearchsubmit").click(function(){
				if(jQuery("#search_input").val()=="'. esc_html__( 'Search...', 'vg-betashop' ).'" || jQuery("#search_input").val()==""){
					jQuery("#search_input").focus();
					return false;
				}
			});
		});
	';
	wp_add_inline_script('betashop-theme-js', $inlineJS);
	
	return $form;
}
add_filter( 'get_search_form', 'betashop_search_form' ); 

add_filter( 'woocommerce_breadcrumb_defaults', 'betashop_woocommerce_breadcrumbs', 20, 0  );
function betashop_woocommerce_breadcrumbs() {
    return array(
		'delimiter'   => '<li class="separator"> / </li>',
		'wrap_before' => '<ul id="breadcrumbs" class="breadcrumbs">',
		'wrap_after'  => '</ul>',
		'before'      => '<li class="item">',
		'after'       => '</li>',
		'home'        => _x( 'Home', 'breadcrumb', 'vg-betashop' ),
	);
}


function betashop_limitStringByWord ($string, $maxlength, $suffix = '') {

	if(function_exists( 'mb_strlen' )) {
		// use multibyte functions by Iysov
		if(mb_strlen( $string )<=$maxlength) return $string;
		$string = mb_substr( $string, 0, $maxlength );
		$index = mb_strrpos( $string, ' ' );
		if($index === FALSE) {
			return $string;
		} else {
			return mb_substr( $string, 0, $index ).$suffix;
		}
	} else { // original code here
		if(strlen( $string )<=$maxlength) return $string;
		$string = substr( $string, 0, $maxlength );
		$index = strrpos( $string, ' ' );
		if($index === FALSE) {
			return $string;
		} else {
			return substr( $string, 0, $index ).$suffix;
		}
	}
} 
// Set up the content width value based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ){
	$content_width = 625; 
}

if ( ! function_exists( 'betashop_setup' ) ) :
	function betashop_setup() {
		/*
		 * Makes betashop Themes available for translation.
		 *
		 * Translations can be added to the /languages/ directory.
		 * If you're building a theme based on VinaGecko, use a find and replace
		 * to change 'vg-betashop' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('vg-betashop', get_template_directory() . '/languages');

		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style();

		// Adds RSS feed links to <head> for posts and comments.
		add_theme_support('automatic-feed-links');

		// This theme supports a variety of post formats.
		add_theme_support('post-formats', array('image', 'gallery', 'video', 'audio'));

		// Register menus
		
		register_nav_menu('top-menu', esc_html__('Top Menu', 'vg-betashop'));
		register_nav_menu('primary', esc_html__('Primary Menu', 'vg-betashop'));
		register_nav_menu('mobilemenu', esc_html__('Mobile Menu', 'vg-betashop'));
		register_nav_menu('primary2', esc_html__('Primary Menu Layout 2', 'vg-betashop'));
		register_nav_menu('mobilemenu2', esc_html__('Mobile Menu Layout 2', 'vg-betashop'));
		register_nav_menu('menu-top-footer', esc_html__('Menu Top Footer', 'vg-betashop'));
		
		/*Category Menu*/
		register_nav_menu('category-product', esc_html__('Category Product Menu', 'vg-betashop'));
		register_nav_menu('mobilemenucategory', esc_html__('Mobile Menu Category Product', 'vg-betashop'));
		/*
		 * This theme supports custom background color and image,
		 * and here we also set up the default background color.
		 */
		add_theme_support('custom-background', array(
			'default-color' => 'e6e6e6',
		));
		add_theme_support( "custom-header", array(
			'default-color' => '',
		));
		
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');
		
		// This theme uses a custom image size for featured images, displayed on "standard" posts.
		add_theme_support('post-thumbnails');

		set_post_thumbnail_size(1170, 9999); // Unlimited height, soft crop
		add_image_size('betashop-category-thumb', 870, 475, true); // (cropped)
		add_image_size('betashop-post-thumb', 300, 165, true); // (cropped)
		add_image_size('betashop-post-thumbwide', 570, 415, true); // (cropped)
	}
endif;
add_action('after_setup_theme', 'betashop_setup'); 
 
function betashop_get_font_url() {
	$font_url = '';

	/* translators: If there are characters in your language that are not supported
	 * by Open Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'vg-betashop' ) ) {
		$subsets = 'latin,latin-ext';

		/* translators: To add an additional Open Sans character subset specific to your language,
		 * translate this to 'vg-betashop', 'cyrillic' or 'vietnamese'. Do not translate into your own language.
		 */
		$subset = _x( 'no-subset', 'Open Sans font: add new subset (betashop, cyrillic, vietnamese)', 'vg-betashop' );

		if ( 'cyrillic' == $subset )
			$subsets .= ',cyrillic,cyrillic-ext';
		elseif ( 'vg-betashop' == $subset )
			$subsets .= ',betashop,betashop-ext';
		elseif ( 'vietnamese' == $subset )
			$subsets .= ',vietnamese';

		$protocol = is_ssl() ? 'https' : 'http';
		$query_args = array(
			'family' => 'Open+Sans:400italic,700italic,400,700',
			'subset' => $subsets,
		);
		$font_url = add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" );
	}

	return $font_url;
}
 
function betashop_scripts_styles() {
	global $wp_styles, $wp_scripts, $betashop_options;
	
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	*/
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	
	if ( !is_admin()) {
		// Add Bootstrap JavaScript
		wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.2.0', true );
	
		// Google Map JS
		wp_enqueue_script( 'google-js', 'https://maps.google.com/maps/api/js', array('jquery'), '3.2.0', true );
		
		// Add jQuery Cookie
		wp_enqueue_script('jquery-cookie', get_template_directory_uri() . '/js/jquery.cookie.js', array('jquery'), '1.4.1', true);	
	
		// Add Fancybox
		wp_enqueue_script( 'jquery-fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.pack.js', array('jquery'), '2.1.5', true );
		wp_enqueue_style( 'jquery-fancybox-css', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.css', array(), '2.1.5' );
		wp_enqueue_script('jquery-fancybox-buttons', get_template_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-buttons.js', array('jquery'), '1.0.5', true);
		wp_enqueue_style('jquery-fancybox-buttons', get_template_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-buttons.css', array(), '1.0.5');
	
		//Superfish
		wp_enqueue_script( 'superfish-js', get_template_directory_uri() . '/js/superfish/superfish.min.js', array('jquery'), '1.3.15', true );
	
		//Add Shuffle js
		wp_enqueue_script( 'modernizr-custom-js', get_template_directory_uri() . '/js/modernizr.custom.min.js', array('jquery'), '2.6.2', true );
		wp_enqueue_script( 'shuffle-js', get_template_directory_uri() . '/js/jquery.shuffle.min.js', array('jquery'), '3.0.0', true );
	
		// Add owl.carousel files
		wp_enqueue_script('owl.carousel', 	get_template_directory_uri() . '/js/owl.carousel.js', array('jquery'));
		wp_enqueue_style('owl.carousel', 	get_template_directory_uri() . '/css/owl.carousel.css');
		wp_enqueue_style('owl.theme', 		get_template_directory_uri() . '/css/owl.theme.css');
	
		// Add jQuery countdown file
		wp_enqueue_script( 'countdown', get_template_directory_uri() . '/js/jquery.countdown.min.js', array('jquery'), '2.0.4', true );
	
		// Add Plugin JS
		wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '20160115', true );
	   
         // Add lazy-load
        wp_enqueue_script('betashop-lazy', get_template_directory_uri() . '/js/jquery.lazy.min.js', array(), '1.7.4', TRUE);
        wp_enqueue_script('betashop-lazy-plugin', get_template_directory_uri() . '/js/jquery.lazy.plugins.min.js', array(), '1.7.4', TRUE);

		// Add theme.js file
		wp_enqueue_script( 'betashop-theme-js', get_template_directory_uri() . '/js/theme.js', array('jquery'), '20140826', true );
	}
	
	$font_url = betashop_get_font_url();
	if ( ! empty( $font_url ) )
		wp_enqueue_style( 'betashop-fonts', esc_url_raw( $font_url ), array(), null );
	
	if ( !is_admin()) {
	
		//Swallow Code Custom elements Visual Composer
		// Load Icon picker fonts:
		wp_enqueue_style( 'typicons', get_template_directory_uri() . '/css/typicons.min.css', array(), '4.9.2' );
		wp_enqueue_style( 'openiconic', get_template_directory_uri() . '/css/vc_openiconic.min.css' , array(), '4.9.2' );
		wp_enqueue_style( 'linecons', get_template_directory_uri() . '/css/vc_linecons_icons.min.css' , array(), '4.9.2' );
		wp_enqueue_style( 'entypo', get_template_directory_uri() . '/css/vc_entypo.min.css' , array(), '4.9.2' );
		//End Swallow Code Custom elements Visual Composer
	
		// Loads our main stylesheet.
		wp_enqueue_style( 'betashop-style', get_stylesheet_uri() );

		// Load bootstrap css
		if ( !is_rtl() ) {
			wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.5' );
		}
		
		// Load elegant css
		wp_enqueue_style( 'elegant-css', get_template_directory_uri() . '/css/elegant-style.css', array(), '1.0');
		
		// Load elegant css
		wp_enqueue_style( 'themify-style', get_template_directory_uri() . '/css/themify-icons.css', array(), '1.0');
		
		// Load fontawesome css
		wp_enqueue_style( 'fontawesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.2.0' );
		
		if ( is_rtl() ) {
			wp_enqueue_style( 'betashop-rtl', get_template_directory_uri() . '/css/bootstrap-rtl.min.css', array(), '3.3.2-rc1' );
		}
	}
	// Compile Less to CSS
	
	// HieuJa get Preset Color Option
	$presetopt = betashop_get_preset();	
	// HieuJa end block
	
	if (isset($betashop_options['bodyfont']) && !empty($betashop_options['bodyfont']['font-family'])) {
		$bodyfont = $betashop_options['bodyfont']['font-family'];
	} else {
		$bodyfont = '"Raleway", Helvetica, Arial, sans-serif';
	}
	
	if(isset($betashop_options['enable_less']) && $betashop_options['enable_less']){
		$themevariables = array(
			'body_font'			=> $bodyfont,
			'text_color'		=> $betashop_options['bodyfont']['color'],
			'heading_color'		=> $betashop_options['heading_color'],
			'primary_color' 	=> $betashop_options['primary_color'],
			'primary_color2' 	=> $betashop_options['primary_color2'],
			'primary_color3' 	=> $betashop_options['primary_color3'],
			'cart_color' 	=> $betashop_options['cart_color'],
			'botom_color' 		=> $betashop_options['botom_color'],
			'footer_color' 		=> $betashop_options['footer_color'],
			'sale_color'		=> $betashop_options['sale_color'],
			'featured_color'	=> $betashop_options['featured_color'],
			'rate_color' 		=> $betashop_options['rate_color'],
			'presetopt' 		=> $presetopt,
		);
		switch ($presetopt) {
			case 2:
				$themevariables['primary_color'] 	= $betashop_options['primary2_color'];
				$themevariables['primary_color2'] 	= $betashop_options['primary2_color2'];
				$themevariables['primary_color3'] 	= $betashop_options['primary2_color3'];
				$themevariables['cart_color'] 		= $betashop_options['cart2_color'];
				$themevariables['botom_color']		= $betashop_options['botom2_color'];
				$themevariables['footer_color']		= $betashop_options['footer2_color'];
				$themevariables['sale_color']		= $betashop_options['sale2_color'];
				$themevariables['featured_color']	= $betashop_options['featured2_color'];
				$themevariables['rate_color']	 	= $betashop_options['rate2_color'];
			break;
			case 3:
				$themevariables['primary_color'] 	= $betashop_options['primary3_color'];
				$themevariables['primary_color2'] 	= $betashop_options['primary3_color2'];
				$themevariables['primary_color3'] 	= $betashop_options['primary3_color3'];
				$themevariables['cart_color'] 		= $betashop_options['cart3_color'];
				$themevariables['botom_color']		= $betashop_options['botom3_color'];
				$themevariables['footer_color']		= $betashop_options['footer3_color'];
				$themevariables['sale_color']		= $betashop_options['sale3_color'];
				$themevariables['featured_color']	= $betashop_options['featured3_color'];
				$themevariables['rate_color']	 	= $betashop_options['rate3_color'];
			break;
			case 4:
				$themevariables['primary_color'] 	= $betashop_options['primary4_color'];
				$themevariables['primary_color2'] 	= $betashop_options['primary4_color2'];
				$themevariables['primary_color3'] 	= $betashop_options['primary4_color3'];
				$themevariables['cart_color'] 		= $betashop_options['cart4_color'];
				$themevariables['botom_color']		= $betashop_options['botom4_color'];
				$themevariables['footer_color']		= $betashop_options['footer4_color'];
				$themevariables['sale_color']		= $betashop_options['sale4_color'];
				$themevariables['featured_color']	= $betashop_options['featured4_color'];
				$themevariables['rate_color']	 	= $betashop_options['rate4_color'];
			break;
		}
		if( function_exists('compileLessFile') ){
			compileLessFile('theme.less', 'theme'.$presetopt.'.css', $themevariables);
			compileLessFile('compare.less', 'compare'.$presetopt.'.css', $themevariables);
			compileLessFile('ie.less', 'ie'.$presetopt.'.css', $themevariables);
		}
	}
	
	if ( !is_admin()) {
		if( isset($presetopt) ){
			// Load main theme css style
			wp_enqueue_style( 'betashop-css', get_template_directory_uri() . '/css/theme'.$presetopt.'.css', array(), '1.0.0' );
			//Compare CSS
			wp_enqueue_style( 'betashop-css', get_template_directory_uri() . '/css/compare'.$presetopt.'.css', array(), '1.0.0' );
			// Loads the Internet Explorer specific stylesheet.
			wp_enqueue_style( 'betashop-ie', get_template_directory_uri() . '/css/ie'.$presetopt.'.css', array( 'betashop-style' ), '20152907' );
		} else {
			// Load main theme css style
			wp_enqueue_style( 'betashop-css', get_template_directory_uri() . '/css/theme1.css', array(), '1.0.0' );
			//Compare CSS
			wp_enqueue_style( 'betashop-css', get_template_directory_uri() . '/css/compare1.css', array(), '1.0.0' );
			// Loads the Internet Explorer specific stylesheet.
			wp_enqueue_style( 'betashop-ie', get_template_directory_uri() . '/css/ie1.css', array( 'betashop-style' ), '20152907' );
		}
		$wp_styles->add_data( 'betashop-ie', 'conditional', 'lte IE 9' );
	}
	
	if(isset($betashop_options['enable_sswitcher']) && $betashop_options['enable_sswitcher']){
		// Add styleswitcher.js file
		wp_enqueue_script( 'betashop-styleswitcher-js', get_template_directory_uri() . '/js/styleswitcher.js', array(), '20140826', true );
		// Load styleswitcher css style
		wp_enqueue_style( 'betashop-styleswitcher-css', get_template_directory_uri() . '/css/styleswitcher.css', array(), '1.0.0' );
	}
	if ( is_rtl() ) {
		wp_enqueue_style( 'betashop-rtl', get_template_directory_uri() . '/rtl.css', array(), '1.0.0' );
	}
}
add_action( 'wp_enqueue_scripts', 'betashop_scripts_styles' );

//Swallow Code Custom elements Visual Composer        
function betashop_load_custom_wp_admin_style() {
	wp_enqueue_style( 'vg_js_composer_icon', get_template_directory_uri() . '/css/vg_js_composer_icon.css', array(), '1.0.0' );
}
add_action( 'admin_enqueue_scripts', 'betashop_load_custom_wp_admin_style' );
//End Swallow Code Custom elements Visual Composer

//Include
if (!class_exists('betashop_widgets') && file_exists(get_template_directory().'/include/vinawidgets.php')) {
    require_once(get_template_directory().'/include/vinawidgets.php');
}

/* Widgets list */
$vg_widgets = array(
    'vg_megamenu.php',
    'vg_woocarousel.php',
    'vg_postcarousel.php',
);

foreach ( $vg_widgets as $widget ) {
	require get_template_directory().'/include/widgets/'. $widget;
}

if (file_exists(get_template_directory().'/include/styleswitcher.php')) {
    require_once(get_template_directory().'/include/styleswitcher.php');
}
if (file_exists(get_template_directory().'/include/breadcrumbs.php')) {
    require_once(get_template_directory().'/include/breadcrumbs.php');
}
if (file_exists(get_template_directory().'/include/wooajax.php')) {
    require_once(get_template_directory().'/include/wooajax.php');
}
if (file_exists(get_template_directory().'/include/shortcodes.php')) {
    require_once(get_template_directory().'/include/shortcodes.php');
}
 
function betashop_mce_css( $mce_css ) {
	$font_url = betashop_get_font_url();

	if ( empty( $font_url ) )
		return $mce_css;

	if ( ! empty( $mce_css ) )
		$mce_css .= ',';

	$mce_css .= esc_url_raw( str_replace( ',', '%2C', $font_url ) );

	return $mce_css;
}
add_filter( 'mce_css', 'betashop_mce_css' ); 

/**
 * Filter the page menu arguments.
 *
 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.
 *
 * @since VinaGecko 1.2
 */
function betashop_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'betashop_page_menu_args' );

/**
 * Register sidebars.
 *
 * Registers our main widget area and the front page widget areas.
 *
 * @since VinaGecko 1.2
 */
function betashop_widgets_init() {
	register_sidebar(array(
		'name' => esc_html__('VG Blog Sidebar', 'vg-betashop'),
		'id' => 'sidebar-1',
		'description' => esc_html__('Sidebar on blog page', 'vg-betashop'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));	
	register_sidebar(array(
		'name' => esc_html__('VG Top Header', 'vg-betashop'),
		'id' => 'sidebar-top-header',
		'description' => esc_html__('Sidebar on Top Header Layout 1', 'vg-betashop'),
		'before_widget' => '<div id="%1$s" class="widget col-md-6 col-xs-12 %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));
	register_sidebar(array(
		'name' => esc_html__('VG Welcome', 'vg-betashop'),
		'id' => 'sidebar-text-welcome',
		'description' => esc_html__('Only Work With Layout 1', 'vg-betashop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));
	register_sidebar(array(
		'name' => esc_html__('VG Layout 2 Left Main Menu', 'vg-betashop'),
		'id' => 'sidebar-vg-left-main-menu',
		'description' => esc_html__('Only work with Layout 2', 'vg-betashop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));	
	register_sidebar(array(
		'name' => esc_html__('VG Category Sidebar', 'vg-betashop'),
		'id' => 'sidebar-category',
		'description' => esc_html__('Sidebar on product category page', 'vg-betashop'),
		'before_widget' => '<aside id="%1$s" class="widget vg-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));
	register_sidebar(array(
		'name' => esc_html__('VG Product Sidebar', 'vg-betashop'),
		'id' => 'sidebar-product',
		'description' => esc_html__('VG Sidebar on product page', 'vg-betashop'),
		'before_widget' => '<aside id="%1$s" class="widget vg-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));
	register_sidebar(array(
		'name' => esc_html__('VG Pages Sidebar', 'vg-betashop'),
		'id' => 'sidebar-page',
		'description' => esc_html__('Sidebar on content pages', 'vg-betashop'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));	

	global $betashop_options;
	$col_bottom = '';
	$betashop_options['show_about_us'] = isset($betashop_options['show_about_us']) ? $betashop_options['show_about_us'] : false;
	$betashop_options['newsletter_show'] = isset($betashop_options['newsletter_show']) ? $betashop_options['newsletter_show'] : false;
	
	if ($betashop_options['show_about_us'] && $betashop_options['newsletter_show']) {
		$col_bottom = '3' ;
	 
	}elseif($betashop_options['show_about_us'] || $betashop_options['newsletter_show']){
		$col_bottom = '4';
	}else {
		$col_bottom = '3';
	} 
	register_sidebar(array(
		'name' => esc_html__('VG Widget Bottom', 'vg-betashop'),
		'id' => 'bottom',
		'class' => 'bottom',
		'description' => esc_html__('Widget on bottom', 'vg-betashop'),
		'before_widget' => '<div id="%1$s" class="widget vg-bottom-menu vg-bottom-static col-md-'. esc_attr($col_bottom) .' col-xs-12 %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="vg-title bottom-static-title"><h3>',
		'after_title' => '</h3></div>',
	));
	register_sidebar(array(
		'name' => esc_html__('Product Category Sidebar', 'vg-betashop'),
		'id' => 'sidebar-product-category',
		'description' => esc_html__('Sidebar on product all category page', 'vg-betashop'),
		'before_widget' => '<aside id="%1$s" class="widget vg-widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<div class="vg-title widget-title"><h3>',
		'after_title' => '</h3></div>',
	));
}
add_action('widgets_init', 'betashop_widgets_init');


if (! function_exists('betashop_content_nav')) :
/**
 * Displays navigation to next/previous pages when applicable.
 *
 * @since VinaGecko 1.2
 */
function betashop_content_nav( $html_id ) {
	global $wp_query;

	$html_id = esc_attr( $html_id );

	if ( $wp_query->max_num_pages > 1 ) : ?>
		<nav id="<?php echo esc_attr($html_id); ?>" class="navigation" role="navigation">
			<h3 class="assistive-text"><?php esc_html_e( 'Post navigation', 'vg-betashop' ); ?></h3>
			<div class="nav-previous"><?php next_posts_link(wp_kses(__('<span class="meta-nav">&larr;</span> Older posts', 'vg-betashop'), array('span' => array('class' => array())))); ?></div>
			<div class="nav-next"><?php previous_posts_link(wp_kses(__('Newer posts <span class="meta-nav">&rarr;</span>', 'vg-betashop'), array('span' => array('class' => array())))); ?></div>
		</nav><!-- #<?php echo esc_attr($html_id); ?> .navigation -->
	<?php endif;
}
endif;

if ( ! function_exists( 'betashop_pagination' ) ) :
/* Pagination */
function betashop_pagination() {
	global $wp_query;

	$big = 999999999; // need an unlikely integer
	
	echo paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'prev_text'    => wp_kses(__('<i class="fa fa-chevron-left"></i>', 'vg-betashop'), array('i' => array('class' => array()))),
		'next_text'    => wp_kses(__('<i class="fa fa-chevron-right"></i>', 'vg-betashop'), array('i' => array('class' => array()))),
	) );
}
endif;

if ( ! function_exists( 'betashop_entry_meta' ) ) :
	function betashop_entry_meta() {
		
		$date = sprintf( '<span class="posted-on">'.esc_html__('Date : ', 'vg-betashop').'<a href="'. get_permalink() .'" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span><div class="separator"> / </div>',
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);

		$author = sprintf( '<span class="author vcard">'.esc_html__('Post by : ', 'vg-betashop').'<a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span><div class="separator"> / </div>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( esc_html__( 'View all posts by %s', 'vg-betashop' ), get_the_author() ) ),
			get_the_author()
		);
		
		$num_comments = (int)get_comments_number();
		$write_comments = '';
		if ( comments_open() ) {
			if ( $num_comments == 0 ) {
				$comments = esc_html__('0', 'vg-betashop');
			} elseif ( $num_comments > 1 ) {
				$comments = $num_comments;
			} else {
				$comments = esc_html__('1', 'vg-betashop');
			}
			$write_comments = '<span class="comments-link">'.esc_html__('Comments : ', 'vg-betashop').'<a href="' . get_comments_link() .'" class="link-comment">'. $comments.'</a></span>';
		}

		// Translators: 1 is author's name, 2 is date, 3 is the tags and 4 is comments.
		
		$utility_text = wp_kses(__( '%1$s%2$s%3$s%', 'vg-betashop' ), array());
		printf( $utility_text, $author, $date, $write_comments );
	}
	
endif;

if ( ! function_exists( 'betashop_entry_meta_categories' ) ) :
	function betashop_entry_meta_categories() {
		// Translators: used between list items, there is a space after the comma.
		$categories_list = '<div class="entry-categories"><span class="categories-list">'. get_the_category_list(esc_html__( ', ', 'vg-betashop' ) ) .'</span></div>';
		
		printf( $categories_list );
	}
endif;

if ( ! function_exists( 'betashop_entry_meta_tags' ) ) :
	function betashop_entry_meta_tags() {
		// Translators: used between list items, there is a space after the comma.
		$tag_list = '<div class="entry-tags"><span> '. esc_html__( 'Tags: ', 'vg-betashop' ) .' </span>'. get_the_tag_list( '', esc_html__( ', ', 'vg-betashop' ) ) .'</div>';
		
		printf( $tag_list );
	}
endif;

function betashop_entry_meta_small() {
	
	$date = sprintf( '<span class="posted-on">'.esc_html__('Date : ', 'vg-betashop').'<a href="'. get_permalink() .'" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);

	$author = sprintf( '<span class="author vcard">'.esc_html__('Post by : ', 'vg-betashop').'<a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span><div class="separator"> / </div>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( esc_html__( 'View all posts by %s', 'vg-betashop' ), get_the_author() ) ),
		get_the_author()
	);
	
	$utility_text = wp_kses(__( '<div class="entry-meta">%1$s%2$s</div>', 'vg-betashop' ), array('div' => array('class' => array())));
	
	printf( $utility_text, $author, $date );
}

function betashop_add_meta_box() {

	$screens = array( 'post' );

	foreach ( $screens as $screen ) {

		add_meta_box(
			'betashop_post_intro_section',
			esc_html__( 'Post featured content', 'vg-betashop' ),
			'betashop_meta_box_callback',
			$screen
		);
	}
}
add_action( 'add_meta_boxes', 'betashop_add_meta_box' );

function betashop_meta_box_callback( $post ) {

	// Add an nonce field so we can check for it later.
	wp_nonce_field( 'betashop_meta_box', 'betashop_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$value = get_post_meta( $post->ID, '_betashop_meta_value_key', true );

	echo '<label for="betashop_post_intro">';
	esc_html_e( 'This content will be used to replace the featured image, use shortcode here', 'vg-betashop' );
	echo '</label><br />';
	//echo '<textarea id="betashop_post_intro" name="betashop_post_intro" rows="5" cols="50" />' . esc_attr( $value ) . '</textarea>';
	wp_editor( $value, 'betashop_post_intro', $settings = array() );
	
	
}

function betashop_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['betashop_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['betashop_meta_box_nonce'], 'betashop_meta_box' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */
	
	// Make sure that it is set.
	if ( ! isset( $_POST['betashop_post_intro'] ) ) {
		return;
	}

	// Sanitize user input.
	$my_data = sanitize_text_field( $_POST['betashop_post_intro'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, '_betashop_meta_value_key', $my_data );
}
add_action( 'save_post', 'betashop_save_meta_box_data' );

if ( ! function_exists( 'betashop_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own betashop_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since VinaGecko 1.2
 */
function betashop_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php esc_html_e( 'Pingback:', 'vg-betashop' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link(esc_html__( '(Edit)', 'vg-betashop' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<div class="comment-avatar">
				<?php echo get_avatar( $comment, 50 ); ?>
			</div>
			<div class="comment-info">
				<header class="comment-meta comment-author vcard">
					<?php
						
						printf( '<cite><b class="fn">%1$s</b> %2$s</cite>',
							get_comment_author_link(),
							// If current post author is also comment author, make it known visually.
							( $comment->user_id === $post->post_author ) ? '<span>' . esc_html__( 'Post author', 'vg-betashop' ) . '</span>' : ''
						);
						printf( '<time datetime="%1$s">%2$s</time>',
							get_comment_time( 'c' ),
							/* translators: 1: date, 2: time */
							sprintf( esc_html__( '%1$s at %2$s', 'vg-betashop' ), get_comment_date(), get_comment_time() )
						);
					?>
					<div class="reply">
						<?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'vg-betashop' ), 'after' => '', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					</div><!-- .reply -->
				</header><!-- .comment-meta -->
				<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'vg-betashop' ); ?></p>
				<?php endif; ?>

				<section class="comment-content comment">
					<?php comment_text(); ?>
					<?php edit_comment_link( esc_html__( 'Edit', 'vg-betashop' ), '<p class="edit-link">', '</p>' ); ?>
				</section><!-- .comment-content -->
			</div>
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
}
endif;
if ( ! function_exists( 'before_comment_fields' ) &&  ! function_exists( 'after_comment_fields' )) :
//Change comment form
function betashop_before_comment_fields() {
	echo '<div class="comment-input">';
}
add_action('comment_form_before_fields', 'betashop_before_comment_fields');

function betashop_after_comment_fields() {
	echo '</div>';
}
add_action('comment_form_after_fields', 'betashop_after_comment_fields');

endif; 
 
function betashop_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'betashop_customize_register' );

/**
 * Enqueue Javascript postMessage handlers for the Customizer.
 *
 * Binds JS handlers to make the Customizer preview reload changes asynchronously.
 *
 * @since VinaGecko 1.2
 */
 
add_action( 'wp_enqueue_scripts', 'betashop_wcqi_enqueue_polyfill' );
function betashop_wcqi_enqueue_polyfill() {
    wp_enqueue_script( 'wcqi-number-polyfill' );
}

/* Remove Redux Demo Link */
function betashop_removeDemoModeLink()
{
    if(class_exists('ReduxFrameworkPlugin')) {
        remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2);
    }
    if(class_exists('ReduxFrameworkPlugin')) {
        remove_action('admin_notices', array(ReduxFrameworkPlugin::get_instance(), 'admin_notices'));    
    }
}
add_action('init', 'betashop_removeDemoModeLink');


// HieuJa add specific class
function betashop_add_query_vars_filter($vars){
    $vars[] = "demo";
    return $vars;
}
add_filter('query_vars', 'betashop_add_query_vars_filter');

// Get value from URL and set Cookie, Class Subffix
function betashop_body_class($classes)
{	
	global $betashop_options;
	
	// Set Class Layout for Body Tag
	$classes[] = betashop_get_layout();
	
	// Set Class Preset Color for Body Tag
	$classes[] = "preset-" . betashop_get_preset();
	
	return $classes;
}
add_filter('body_class', 'betashop_body_class');

// Override get_header function
function betashop_get_header()
{
	// Get Header Name
	$header = betashop_get_layout();
	
	get_header($header);	
}

// Override get_footer function
function betashop_get_footer()
{
	// Get Footer Name
	$footer = betashop_get_layout();
	
	get_footer($footer);	
}

// HieuJa get Layout
function betashop_get_layout()
{
    global $betashop_options;
    
    $layout = isset($betashop_options['page_layout']) ? $betashop_options['page_layout'] : 'layout-1';
    $demo   = get_query_var('demo', '');
    
    if(!empty($demo)) 
    {
        $demo   = str_replace("niche-", "", $demo);
        $demo   = str_split($demo);
        $layout = 'layout-' . $demo[0];
    }
        
    return $layout;
}

// HieuJa get Preset Color
function betashop_get_preset()
{
    global $betashop_options;
    
    $presetColor = isset($betashop_options['preset_option']) ? $betashop_options['preset_option'] : '1';    
    $demo        = get_query_var('demo', '');
    
    if(!empty($demo)) 
    {
        $demo        = str_replace("niche-", "", $demo);
        $demo        = str_split($demo);
        $presetColor = $demo[1];
    }
    return $presetColor;
}

// HieuJa get global variables
function betashop_get_global_variables($variable = 'betashop_options')
{
	global $woocommerce, $project, $projects, $betashop_productrows, $betashop_options, $betashop_productsfound, $product, $woocommerce_loop, $projects_loop, $post, $betashop_projectrows, $betashop_projectsfound, $betashop_secondimage, $wpdb, $wp_query, $is_IE;
	
	switch($variable)
	{
		case "betashop_options":
			return $betashop_options;
		break;
		case "product":
			return $product;
		break;	
		case "woocommerce":
			return $woocommerce;
		break;		
		case "project":
			return $project;
		break;	
		case "projects":
			return $projects;
		break;		
		case "betashop_productrows":
			return $betashop_productrows;
		break;	
		case "betashop_productsfound":
			return $betashop_productsfound;
		break;	
		case "woocommerce_loop":
			return $woocommerce_loop;
		break;	
		case "projects_loop":
			return $projects_loop;
		break;	
		case "post":
			return $post;
		break;	
		case "betashop_projectrows":
			return $betashop_projectrows;
		break;	
		case "betashop_projectsfound":
			return $betashop_projectsfound;
		break;	
		case "betashop_secondimage":
			return $betashop_secondimage;
		break;	
		case "wpdb":
			return $wpdb;
		break;		
		case "wp_query":
			return $wp_query;
		break;	
		case "is_IE":
			return $is_IE;
		break;
	}
	
	return false;
}

//swallow code sale and featured %
add_filter('woocommerce_featured_flash', 'betashop_change_featured_flash');
function betashop_change_featured_flash() {
	global $betashop_options;
	
	$featured_label_custom = isset($betashop_options['featured_label_custom']) ? $betashop_options['featured_label_custom'] :esc_html__('Hot','vg-betashop');
	
	return '<div class="vgwc-label vgwc-featured"><span>' . esc_html($featured_label_custom) . '</span></div>';
}

//Testimonials
function betashop_woothemes_testimonials_html( $html, $query, $args ) {
	$html = str_replace( 'itemprop', 'data-itemprop', $html );
	return $html;
}
add_filter( 'woothemes_testimonials_html', 'betashop_woothemes_testimonials_html', 10, 3 );

//Sale Label
add_filter('woocommerce_sale_flash', 'betashop_change_on_sale_flash');
function betashop_change_on_sale_flash() {
	global $product,$post,$betashop_options;
	$sale = '';
	
	$betashop_options['featured_label_custom'] = isset($betashop_options['featured_label_custom']) ? $betashop_options['featured_label_custom'] :"";
	$betashop_options['sale_label'] = isset($betashop_options['sale_label']) ? $betashop_options['sale_label'] :"";
	
	$format = $betashop_options['sale_label'];

	if (!empty($format)) {
		if ($format == 'custom') {
			$format = $betashop_options['sale_label_custom'];
		}
		$priceDiff = 0;
		$percentDiff = 0;
		$regularPrice = '';
		$salePrice = '';
		if (!empty($product)) {
			$salePrice = get_post_meta($product->get_id(), '_price', true);
			$regularPrice = get_post_meta($product->get_id(), '_regular_price', true);
			
			if($product->get_children()) {
				foreach ( $product->get_children() as $child_id ) {
					$all_prices[] = get_post_meta( $child_id, '_price', true );
					$all_prices1[] = get_post_meta( $child_id, '_regular_price', true );
					$all_prices2[] = get_post_meta( $child_id, '_sale_price', true );
					if(get_post_meta( $child_id, '_price', true ) == $salePrice) {
						$regularPrice = get_post_meta( $child_id, '_regular_price', true );
					}
				}
			}
		}
		if (!empty($regularPrice) && !empty($salePrice ) && $regularPrice > $salePrice ) {
			$priceDiff = $regularPrice - $salePrice;
			$percentDiff = round($priceDiff / $regularPrice * 100);
			
			$parsed = str_replace('{price-diff}', number_format((float)$priceDiff, 2, '.', ''), $format);
			$parsed = str_replace('{percent-diff}', $percentDiff, $parsed);
			if($product->is_featured()) {
				$sale = '<div class="vgwc-label vgwc-onsale sale-featured"><span>'. $parsed .'</span></div>';
			}
			else {
				$sale = '<div class="vgwc-label vgwc-onsale"><span>'. $parsed .'</span></div>';
			}
		}
	}
	return $sale;
}


function betashop_custom_script_css () {
	global $betashop_options;
	
	$customCSS  = (isset($betashop_options['advanced_editor_css'])) ? $betashop_options['advanced_editor_css'] : "";
	$customJS = (isset($betashop_options['advanced_editor_js'])) ? $betashop_options['advanced_editor_js'] : "";	
	
	wp_add_inline_style('betashop-style', $customCSS);
	wp_add_inline_script('betashop-theme-js', $customJS);
}
add_action('wp_enqueue_scripts', 'betashop_custom_script_css');

 /* Sticky Menu */
function betashop_sticky_header () {
    global $betashop_options;
    
    $stickyJS = (isset($betashop_options['enable_stickyheader']) && $betashop_options['enable_stickyheader']) ? "jQuery(document).ready(function($){ $('.top-wrapper').addClass('sticky-me'); });" : "";
    
    wp_add_inline_script('betashop-theme-js', $stickyJS);
}
add_action('wp_head', 'betashop_sticky_header');

/******************************************************************************/
/******************************* Add Lazy Load ********************************/
/******************************************************************************/
if(!is_admin()){
    function betashop_alter_att_attributes($attr) {
        $attr['data-src'] = $attr['src'];
        $attr['class']    = $attr['class'] . ' lazy';
        $attr['srcset '] = $attr['data-src'];
        $attr['src']      = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
        return $attr;
    }
    add_filter('wp_get_attachment_image_attributes', 'betashop_alter_att_attributes');
}

// add_filter( 'woocommerce_get_price_html', function( $price ) {
// 	if ( is_admin() ) return $price;

// 	return '';
// } );

include_once (dirname(__FILE__).'/service-center-admin/index.php');
include_once (dirname(__FILE__).'/our-branches-admin/index.php');
include_once (dirname(__FILE__).'/exclusive-stores-admin/index.php');

function my_woocommerce_catalog_orderby($orderby)
{
	unset($orderby["price"]);
	unset($orderby["price-desc"]);
	unset($orderby["rating"]);
	return $orderby;
}
add_filter("woocommerce_catalog_orderby","my_woocommerce_catalog_orderby",20);



/* ///////////////////////////////////// Custom Changes by SUMAN Start ////////////////////////////////////////// */

/* Cart Page Redirection Start */
//add_filter('add_to_cart_redirect', 'custom_add_to_cart_redirect');
 
// function custom_add_to_cart_redirect() {
//  global $woocommerce;
//  $checkout_url = $woocommerce->cart->get_checkout_url();
//  return $checkout_url;
// }

/* Cart Page Redirection End */



/* Add to Cart button text change Start */
/*add_filter( 'add_to_cart_text', 'woo_custom_product_add_to_cart_text' );            // < 2.1
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_custom_product_add_to_cart_text' );  // 2.1 +
  
function woo_custom_product_add_to_cart_text() {  
    return __( ' &nbsp; ', 'woocommerce' );  
}*/
/* Add to Cart button text change End */



/* Checkout page company name field remove Start */
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
unset($fields['billing']['billing_company']);

return $fields;
}
/* Checkout page company name field remove End */




// Change the default Country on checkout page.
add_filter( 'default_checkout_country', 'xa_set_default_checkout_country' );
function xa_set_default_checkout_country() {
     return 'IN';
}


// Change the default State on checkout page.
add_filter( 'default_checkout_state', 'xa_set_default_checkout_state' );
function xa_set_default_checkout_state() {  
     return 'WB';
}



// Remove Cart Item and Max 1 Product @ Cart
add_filter( 'woocommerce_add_to_cart_validation', 'bbloomer_only_one_in_cart', 99, 2 );  
function bbloomer_only_one_in_cart( $passed, $added_product_id ) {
// empty cart first: new item will replace previous
$post_categories = wp_get_post_terms( $added_product_id,'product_cat',array('fields'=>'ids'));

if(in_array(15, $post_categories))
  {
  	wc_empty_cart();
  }
return $passed;
}



/* Checkout page Field Show by product type Start */

add_filter( 'woocommerce_checkout_fields' , 'bbloomer_simplify_checkout_virtual' );
function bbloomer_simplify_checkout_virtual( $fields ) {
	if( ! is_admin() ) {
   $only_virtual = true;
    
   foreach( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
      if ( ! $cart_item['data']->is_virtual() ) $only_virtual = false;   
   }
     
    if( $only_virtual ) {
       unset($fields['order']['order_comments']);
    ?>
    	<style type="text/css">
    		.woocommerce-additional-fields h3{
    			display: none;
    		}
    		.cstmm_end_dtt{
    			display: none !important;
    		}
    		.add_start_date{
    			display: none;
    		}
    		.prgggg{
    			display: none;
    		}
    		.page.page-id-2608 div#customer_details{
    			height: 724px;
    			overflow: hidden;
    		}
    		.page.page-id-2608 .woocommerce-info{
    			display: none;
    		}
    	</style>
    <?php
     }
	}
     
     return $fields;
}




/* Checkout page Extra Field About Existing Warranty Start */



add_action( 'woocommerce_checkout_update_order_meta', 'add_order_existing_warranty' , 10, 1);
function add_order_existing_warranty ( $order_id ) {
     if ( isset( $_POST ['existing_warranty'] ) &&  '' != $_POST ['existing_warranty'] ) {
         add_post_meta( $order_id, '_ex_warranty',  sanitize_text_field( $_POST ['existing_warranty'] ) );
     }
}

add_filter( 'woocommerce_email_order_meta_fields', 'add_existing_warranty_to_emails' , 10, 3 );
function add_existing_warranty_to_emails ( $fields, $sent_to_admin, $order ) {
    if( version_compare( get_option( 'woocommerce_version' ), '3.0.0', ">=" ) ) {
        $order_id = $order->get_id();
    } else {
        $order_id = $order->id;
    }
    $existing_warranty = get_post_meta( $order_id, '_ex_warranty', true );
    if ( '' != $existing_warranty ) {
        $fields[ 'Existing AMC / Warranty' ] = array(
	    'label' => __( 'Existing AMC / Warranty', 'add_extra_fields' ),
	    'value' => $existing_warranty,
        );
     }
    return $fields;
}

add_filter( 'woocommerce_order_details_after_order_table', 'add_existing_warranty_to_order_received_page', 10 , 1 );
function add_existing_warranty_to_order_received_page ( $order ) {
    if( version_compare( get_option( 'woocommerce_version' ), '3.0.0', ">=" ) ) {            
        $order_id = $order->get_id();
    } else {
        $order_id = $order->id;
    }
    $existing_warranty = get_post_meta( $order_id, '_ex_warranty', true );
    
    if ( '' != $existing_warranty ) {
    	echo '<p><strong>' . __( 'Existing AMC / Warranty', 'add_extra_fields' ) . ':</strong> ' . $existing_warranty;
    }
}


add_action( 'woocommerce_admin_order_data_after_order_details', 'display_order_data_in_admin_existing_warranty', 10, 1 );
function display_order_data_in_admin_existing_warranty( $order ){
    if( $value = $order->get_meta( '_ex_warranty' ) ) {
        echo '<div class="order_data_column">        
        <p><strong style="float:left;">' . __( 'Existing AMC / Warranty', "woocommerce" ) . ':</strong><span style="float:left; width:100px;">' . $value . '</span></p>
        </div>';
    }
}

/* Checkout page Extra Field About Existing Warranty End */





/* Checkout page Extra Date Field Add Start */


add_action( 'woocommerce_after_checkout_billing_form', 'display_extra_fields_after_billing_address' , 10, 1 );
function display_extra_fields_after_billing_address () {

		$cart_items_ids = array();
		foreach (  WC()->cart->get_cart() as $cart_item ) {
		    $first_product_id = $cart_item[ 'data' ]->id;
		    $first_product_name = $cart_item[ 'data' ]->name;
		    $first_product_cat = $cart_item[ 'data' ]->category_ids;
		    break;
		}
		if(in_array(15, $first_product_cat))
		{
			$firstCharacter = $first_product_name[0];


	     _e( "Do You have an Existing AMC / Warranty ", "add_extra_fields");
	     ?>
	     <br>     

	    <label class="radio-inline">
	      <input type="radio" name="existing_warranty" value="AMC" checked>AMC
	    </label>
	    <label class="radio-inline">
	      <input type="radio" name="existing_warranty" value="Warranty">Warranty
	    </label>
	    <label class="radio-inline">
	      <input type="radio" name="existing_warranty" value="I do not have any">I do not have any
	    </label>

	    <br><br>

	     <?php 
    _e( "Start Date ", "add_extra_fields");
    ?>
    <abbr class="required" title="required" style="color: red; border: 0 !important; text-decoration: none; font-weight: 700;">*</abbr>
    <br>
    <input type="text" id="check_dt" name="add_start_date" class="add_start_date" placeholder="Start Date">
    <p class="prgggg" style="font-size: smaller; padding: 15px 0 0 0; font-weight: 600; line-height: 16px;">It may be the next day after expiry of the previous contract or any date of your choice. We recommend you choose a date as close as possible to the expiry of previous contract or warranty</p>



    <!--//////////////////////////////// End Date Calculation START ///////////////////////////////////-->

	
				<label class="cstmm_end_dtt">End Date </label>
				<?php if ($firstCharacter == '1') { ?>


					<script>
			        	jQuery(document).ready(function($) {
					    $(".add_start_date").datepicker({
					          minDate: 0,
					          maxDate: 60,

					          dateFormat: 'yy-mm-dd',
					          onSelect: function(dateText, inst) {
					          				$currDate = dateText;					          				
									    	//alert($currDate);

									    	//var date = $(this).datepicker('getDate');
									    	//alert(date);

									    	var my_date = $currDate;
											my_date = my_date.replace(/-/g, "/"); 
											//alert(my_date);

									    	var d = new Date(my_date);
											var year = d.getFullYear();
											var month = d.getMonth();
											var day = d.getDate();
											//var yearEndDate = new Date(year + 1, month, day);
											var yearEndDate = [year + 1, month + 1, day].join('-');
											
										    //alert(yearEndDate);
										    jQuery("#end_date").html(yearEndDate);
										    jQuery('#end_date_hid').val(yearEndDate);
										    jQuery('#end_date_hid_final').val(yearEndDate);
									    }
					     	});
			          	});
			      	</script>

			      	<br>
					<input type="text" id="end_date_hid" name="end_date_hid" class="end_date_hid" readonly>
					<input type="hidden" id="end_date_hid_final" name="end_date_hid_final" class="end_date_hid_final">

     	
				<?php } elseif ($firstCharacter == '2') { ?>
					<script>
			        	jQuery(document).ready(function($) {
					    $(".add_start_date").datepicker({
					          minDate: 0,
					          maxDate: 60,

					          dateFormat: 'yy-mm-dd',
					          onSelect: function(dateText, inst) {
					          				$currDate = dateText;
									    	//alert(dateText);

									    	//var date = $(this).datepicker('getDate');
									    	//alert(date);

									    	var my_date = $currDate;
											my_date = my_date.replace(/-/g, "/"); 
											//alert(my_date);

									    	var d = new Date(my_date);
											var year = d.getFullYear();
											var month = d.getMonth();
											var day = d.getDate();
											//var yearEndDate = new Date(year + 2, month, day);
											var yearEndDate = [year + 2, month + 1, day].join('-');

											//alert(yearEndDate);
											jQuery("#end_date").html(yearEndDate);
											jQuery('#end_date_hid').val(yearEndDate);
											jQuery('#end_date_hid_final').val(yearEndDate);
									    }
					     	});
			          	});
			      	</script>

			      	<br>
					<input type="text" id="end_date_hid" name="end_date_hid" class="end_date_hid" readonly>
					<input type="hidden" id="end_date_hid_final" name="end_date_hid_final" class="end_date_hid_final">


				<?php } elseif ($firstCharacter == '3') { ?>					
					<script>
			        	jQuery(document).ready(function($) {
					    $(".add_start_date").datepicker({
					          minDate: 0,
					          maxDate: 60,

					          dateFormat: 'yy-mm-dd',
					          onSelect: function(dateText, inst) {
					          				$currDate = dateText;
									    	//alert(dateText);

									    	//var date = $(this).datepicker('getDate');
									    	//alert(date);

									    	var my_date = $currDate;
											my_date = my_date.replace(/-/g, "/"); 
											//alert(my_date);

									    	var d = new Date(my_date);
											var year = d.getFullYear();
											var month = d.getMonth();
											var day = d.getDate();
											//var yearEndDate = new Date(year + 3, month, day);
											var yearEndDate = [year + 3, month + 1, day].join('-');

											//alert(yearEndDate);
											jQuery("#end_date").html(yearEndDate);
											jQuery('#end_date_hid').val(yearEndDate);
											jQuery('#end_date_hid_final').val(yearEndDate);
									    }
					     	});
			          	});
			      	</script>

			      	<br>
					<input type="text" id="end_date_hid" name="end_date_hid" class="end_date_hid" readonly>
					<input type="hidden" id="end_date_hid_final" name="end_date_hid_final" class="end_date_hid_final">

				<?php } ?>


		<!--////////////////////////////////////// End Date Calculation END /////////////////////////////////-->




     <?php
 }
}



add_action( 'wp_enqueue_scripts', 'enqueue_datepicker' );
function enqueue_datepicker() {
    if ( is_checkout() ) {
        // Load the datepicker script (pre-registered in WordPress).
        wp_enqueue_script( 'jquery-ui-datepicker' );
        // You need styling for the datepicker. For simplicity I've linked to Google's hosted jQuery UI CSS.
        wp_register_style( 'jquery-ui', '//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
        wp_enqueue_style( 'jquery-ui' );  
    }
}


add_action( 'woocommerce_checkout_update_order_meta', 'add_order_start_date_to_order' , 10, 1);
function add_order_start_date_to_order ( $order_id ) {
     if ( isset( $_POST ['add_start_date'] ) &&  '' != $_POST ['add_start_date'] ) {
         add_post_meta( $order_id, '_start_date',  sanitize_text_field( $_POST ['add_start_date'] ) );
     } else {
     	wc_add_notice( __( 'Please select Start Date' ), 'error' );
     }
}


add_action( 'woocommerce_checkout_update_order_meta', 'add_order_end_date_to_order' , 10, 1);
function add_order_end_date_to_order ( $order_id ) {
     if ( isset( $_POST ['end_date_hid_final'] ) &&  '' != $_POST ['end_date_hid_final'] ) {
         add_post_meta( $order_id, '_end_date',  sanitize_text_field( $_POST ['end_date_hid_final'] ) );
     }
}


add_filter( 'woocommerce_email_order_meta_fields', 'add_start_date_to_emails' , 10, 3 );
function add_start_date_to_emails ( $fields, $sent_to_admin, $order ) {
    if( version_compare( get_option( 'woocommerce_version' ), '3.0.0', ">=" ) ) {            
        $order_id = $order->get_id();
    } else {
        $order_id = $order->id;
    }
    $start_date = get_post_meta( $order_id, '_start_date', true );
    if ( '' != $start_date ) {
        $fields[ 'Start Date' ] = array(
	    'label' => __( 'Start Date', 'add_extra_fields' ),
	    'value' => $start_date,
        );
     }
    return $fields;
}


add_filter( 'woocommerce_order_details_after_order_table', 'add_start_date_to_order_received_page', 10 , 1 );
function add_start_date_to_order_received_page ( $order ) {
    if( version_compare( get_option( 'woocommerce_version' ), '3.0.0', ">=" ) ) {            
        $order_id = $order->get_id();
    } else {
        $order_id = $order->id;
    }
    $start_date = get_post_meta( $order_id, '_start_date', true );
    
    if ( '' != $start_date ) {
    	echo '<p><strong>' . __( 'Start Date', 'add_extra_fields' ) . ':</strong> ' . $start_date;
    }
}


// Display the extra data in the order admin panel
add_action( 'woocommerce_admin_order_data_after_order_details', 'display_order_data_in_admin', 10, 1 );
function display_order_data_in_admin( $order ){
    if( $value = $order->get_meta( '_start_date' ) ) {
        echo '<div class="order_data_column">        
        <p><strong style="float:left;">' . __( 'Start Date', "woocommerce" ) . ':</strong><span style="float:left; width:100px;">' . $value . '</span></p>
        </div>';
    }
}

add_action( 'woocommerce_admin_order_data_after_order_details', 'display_order_data_in_admin_end_date', 10, 1 );
function display_order_data_in_admin_end_date( $order ){
    if( $value = $order->get_meta( '_end_date' ) ) {
        echo '<div class="order_data_column">        
        <p><strong style="float:left;">' . __( 'End Date', "woocommerce" ) . ':</strong><span style="float:left; width:100px;">' . $value . '</span></p>
        </div>';
    }
}


/* Checkout page Extra Date Field Add End */




/* Checkout Button Confirmation Alert START */

add_action( 'wp_footer', 'checkout_place_order_script' );
function checkout_place_order_script() {
    // Only checkout page
    if( is_checkout() && ! is_wc_endpoint_url() ):

    // jQuery code start below
    ?>
    <script src="https://unpkg.com/sweetalert2@7.20.1/dist/sweetalert2.all.js"></script>
    <script type="text/javascript">
    jQuery( function($){
        var fc = 'form.checkout',
            pl = 'button[type="submit"][name="woocommerce_checkout_place_order"]';

        $(fc).on( 'click', pl, clickHandler1);
        
    });

    function clickHandler1(e){
            e.preventDefault(); // Disable "Place Order" button
        var fc = 'form.checkout',
            pl = 'button[type="submit"][name="woocommerce_checkout_place_order"]';
            // Sweet alert 2
            swal({
                title:  'Would you like to review the detail before proceeding',
                //text:   "Would you like to review the detail before proceeding",
                type:   'success',
                showCancelButton:   true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor:  '#d33',
                confirmButtonText:  "No",
                cancelButtonText:  "Yes"
            }).then((result) => {
                if (result.value) {
                    //$(fc).off(); // Enable back "Place Order button
                    jQuery(fc).off( 'click', pl, clickHandler1); 
                    jQuery(pl).trigger('click'); // Trigger submit
                    jQuery(fc).on( 'click', pl, clickHandler1); 
                } else {
                    jQuery('body').trigger('update_checkout'); // Refresh "Checkout review"
                   // bbloomer_checkout_fields_in_label_error();
                }
            });
        };
    </script>
    <?php
    endif;
}

/* Checkout Button Confirmation Alert END */




/* Checkout page Required Validation Message show under the field START */

add_filter( 'woocommerce_form_field', 'bbloomer_checkout_fields_in_label_error', 10, 4 );
 
function bbloomer_checkout_fields_in_label_error( $field, $key, $args, $value ) {
    if ( strpos( $field, '</span>' ) !== false && $args['required'] ) {
        $error = '<span class="error" style="display:none">';
        $error .= sprintf( __( '%s is a required field.', 'woocommerce' ), $args['label'] );
        $error .= '</span>';
        $field = substr_replace( $field, $error, strpos( $field, '</span>' ), 0);
    }
    return $field;
}

/* Checkout page Required Validation Message show under the field END */




/**
 * @snippet       How to Apply a Coupon Programmatically - WooCommerce Cart START
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @compatible    WC 3.8
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
/*  
add_action( 'woocommerce_add_to_cart', 'apply_matched_coupons' );

function apply_matched_coupons() {
    // If the current user is a shop admin
    if ( current_user_can( 'manage_woocommerce' ) ) return;
    // If the user is on the cart or checkout page
    if ( is_cart() || is_checkout() ) return;



    $args = array(
	    'posts_per_page'   => -1,
	    'orderby'          => 'ID',
	    'order'            => 'asc',
	    'post_type'        => 'shop_coupon',
	    'post_status'      => 'publish',
	);

	$coupons = get_posts( $args );
	
	$coupon_names = array();
	foreach ( $coupons as $coupon ) {
	    // Get the name for each coupon post
	    $coupon_name = $coupon->post_title;
	    array_push( $coupon_names, $coupon_name );
	}
	$winner = array_rand($coupon_names, 1);
	$coupon_winner = $coupon_names[$winner];


	

    $coupon_code = $coupon_winner;

    if ( WC()->cart->has_discount( $coupon_code ) ) return;

    WC()->cart->add_discount( $coupon_code );
}*/

/* Apply a Coupon Programmatically - WooCommerce Cart END */




/**
 * Changes the coupon label output from Coupon: {code} to Coupon: {description} START
 *
 * @param string $label the cart / checkout label
 * @param \WC_Coupon $coupon coupon object
 * @return string updated label
 */
function swwp_change_coupon_preview( $label, $coupon ) {
	
	if ( is_callable( array( $coupon, 'get_description' ) ) ) {
		$description = $coupon->get_description();
	} else {
		$coupon_post = get_post( $coupon->id );
		$description = ! empty( $coupon_post->post_excerpt ) ? $coupon_post->post_excerpt : null;
	}

	return $description ? sprintf( esc_html__( 'Coupon: %s', 'woocommerce' ), $description ) : esc_html__( 'Coupon', 'woocommerce' );
}
add_filter( 'woocommerce_cart_totals_coupon_label', 'swwp_change_coupon_preview', 10, 2 );


/* Changes the coupon label output from Coupon: {code} to Coupon: {description} END */


/* ///////////////////////////////////// Custom Changes by SUMAN End ////////////////////////////////////////// */

add_filter("wpcf7_posted_data", function ($wpcf7_posted_data) {

    $wpcf7_posted_data["refererURL"] = $_SERVER['HTTP_REFERER'];


    return $wpcf7_posted_data;

});


/**
 * @snippet       Show Product Inquiry CF7 @ Single Product Page - WooCommerce
 * @how-to        Get CustomizeWoo.com FREE
 * @sourcecode    https://businessbloomer.com/?p=21605
 * @author        Rodolfo Melogli
 * @compatible    WC 3.5
 */
 
// --------------------------
// 1. Display Button and Echo CF7
 
add_action( 'woocommerce_single_product_summary', 'bbloomer_woocommerce_cf7_single_product', 30 );
 
function bbloomer_woocommerce_cf7_single_product() {
	if(get_field('request_a_quote_form') == 'yes') {
		// echo '<button type="submit" id="trigger_cf" class="single_add_to_cart_button button alt">Request A Quote</button>';
		echo '<button type="submit" id="trigger_cf" class="single_add_to_cart_button button alt" data-toggle="modal" data-target="#myModal">Request A Quote</button>';
		
		// echo '<div id="product_inq" style="display:none">';

		echo '<div class="modal fade" id="myModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Request A Quote</h4></div><div class="modal-body">';
		echo do_shortcode('[contact-form-7 id="15293" title="Request a quote"]');
		echo '</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div></div>';
		
		// echo '</div>';
	}
}
 
// --------------------------
// 2. Echo Javascript: 
// a) on click, display CF7
// b) and populate CF7 subject with Product Name
// c) and change CF7 button to "Close"
 
add_action( 'woocommerce_single_product_summary', 'bbloomer_on_click_show_cf7_and_populate', 40 );
 
function bbloomer_on_click_show_cf7_and_populate() {
   
  ?>
    <script type="text/javascript">
        jQuery('#trigger_cf').on('click', function(){
      if ( jQuery(this).text() == 'Request A Quote' ) {
                   jQuery('#product_inq').css("display","block");
                   jQuery('input[name="your-subject"]').val('<?php htmlspecialchars_decode(the_title(), ENT_NOQUOTES); ?>');
         // jQuery("#trigger_cf").html('Close'); 
      } else {
         jQuery('#product_inq').hide();
         jQuery("#trigger_cf").html('Request A Quote'); 
      }
        });
    </script>
   <?php
      
}

// Added by Prabhanjan Add Column 'Item' 


/**
 * Adds 'Item' column header to 'Orders' page immediately after 'Total' column.
 *
 * @param string[] $columns
 * @return string[] $new_columns
 */
function sv_wc_cogs_add_order_item_column_header( $columns ) {

    $new_columns = array();

    foreach ( $columns as $column_name => $column_info ) {

        $new_columns[ $column_name ] = $column_info;

        if ( 'order_total' === $column_name ) {
            $new_columns['order_item'] = __( 'Item', 'my-textdomain' );
        }
    }

    return $new_columns;
}
add_filter( 'manage_edit-shop_order_columns', 'sv_wc_cogs_add_order_item_column_header', 20 );

/**
 * Add data to the new 'Item' column.
 */
function sv_wc_cogs_add_order_item_column_content( $column ) {
    global $post;

    if ( 'order_item' === $column ) {

		$order    = wc_get_order( $post->ID );
		$items_count = count( $order->get_items() );
		if($items_count >= 1)
		{
		 echo array_shift(array_values($order->get_items()))->get_name();
		}

       // echo get_post_meta( $post->ID, '_ex_warranty', true );
    }
}
add_action( 'manage_shop_order_posts_custom_column', 'sv_wc_cogs_add_order_item_column_content' );

/**
 * Adjusts the styles for the new 'Item' column.
 */
function sv_wc_cogs_add_order_item_column_style() {

    $css = '.widefat .column-order_date, .widefat .column-order_item { width: 9%; }';
    wp_add_inline_style( 'woocommerce_admin_styles', $css );
}
add_action( 'admin_print_styles', 'sv_wc_cogs_add_order_item_column_style' );

// End - Added by Prabhanjan Add Column 'Item' 

add_filter( 'woocommerce_available_payment_gateways', 'conditionally_disable_cod_payment_method', 10, 1);
function conditionally_disable_cod_payment_method( $available_gateways ){
    // Not in backend (admin)
    if( is_admin() ) 
        return $available_gateways;

    // HERE define your Products IDs
    $category_id = 15;

    // Loop through cart items
    foreach ( WC()->cart->get_cart() as $cart_item ){
        // Compatibility with WC 3+

        $product_id = version_compare( WC_VERSION, '3.0', '<' ) ? $cart_item['data']->id : $cart_item['data']->get_id();
        if (in_array( $category_id , $cart_item['data']->category_ids )){
            unset($available_gateways['cod']);
            break; // As "COD" is removed we stop the loop
        }
    }
    return $available_gateways;
}

include "portal/inc/function.php";
