<?php
/**
 * Template Name: Payment Section
 *
 * Description: Request A Callback page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<div class="main-container front-page about-page">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div>
	<div class="full-wrapper about-us-wrapper">
    <div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-content">
					<?php while (have_posts()) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php the_content(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post -->
					<?php endwhile; // end of the loop. ?>


				
				<style>
					.nomar{margin: 0; }
					.bbhu{padding: 8px 16px; width: 100%;}
.formyu{max-width:820px; float: none; margin: 20px auto 40px auto; text-align:center; background: #ddd; padding:30px 20px;}
.ttl{font-size: 18px; font-weight: bold;; color: #000; margin: 0 0 20px 0;}
.sev{width:100%!important;}
.sev input{ width: 100%!important; }
.formyu label{ float: left; padding: 0 0 0 0!important; margin: 0!important; font-size:16px; color: #000; font-weight: bold!important;  }
.clear{ clear: both; }
.payy{ float: left; }
.linh{ padding: 0 0 25px 0; font-size:22px; color: #000; font-weight: bold!important; }
				</style>
<div class="formyu">


			<form class="form-horizontal" role="form">
			    <fieldset>
			      <!-- <legend class="linh">Payment</legend> -->
			      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			        <label class=" control-label" for="card-holder-name">Name on Card</label>
			        <div class="">
			          <input type="text" class="form-control" name="card-holder-name" id="card-holder-name" placeholder="Card Holder's Name">
			        </div>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			        <label class=" control-label" for="card-number">Card Number</label>
			        <div class="">
			          <input type="text" class="form-control" name="card-number" id="card-number" placeholder="Debit/Credit Card Number">
			        </div>
			      </div>
			      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			        <label class="control-label" for="expiry-month">Expiration Date</label>
			        <div class="clear"></div>
			          <div class="row">
			            <div class="col-xs-6">
			              <select class="form-control col-sm-2" name="expiry-month" id="expiry-month">
			                <option>Month</option>
			                <option value="01">Jan (01)</option>
			                <option value="02">Feb (02)</option>
			                <option value="03">Mar (03)</option>
			                <option value="04">Apr (04)</option>
			                <option value="05">May (05)</option>
			                <option value="06">June (06)</option>
			                <option value="07">July (07)</option>
			                <option value="08">Aug (08)</option>
			                <option value="09">Sep (09)</option>
			                <option value="10">Oct (10)</option>
			                <option value="11">Nov (11)</option>
			                <option value="12">Dec (12)</option>
			              </select>
			            </div>
			            <div class="col-xs-6">
			              <select class="form-control" name="expiry-year">
			                <option value="13">2013</option>
			                <option value="14">2014</option>
			                <option value="15">2015</option>
			                <option value="16">2016</option>
			                <option value="17">2017</option>
			                <option value="18">2018</option>
			                <option value="19">2019</option>
			                <option value="20">2020</option>
			                <option value="21">2021</option>
			                <option value="22">2022</option>
			                <option value="23">2023</option>
			              </select>
			            </div>
			        </div>
			          </div>
			      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			        <label class=" control-label" for="cvv">Card CVV</label>
			        <div class="clear"></div>
			        <div class="">
			          <input type="text" class="form-control" name="cvv" id="cvv" placeholder="Security Code">
			        </div>
			      </div>
			      
			       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">&nbsp</div>
			       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			       
			          <button type="button" class="btn btn-primary payy">Pay Now</button>
			        
			      </div>
			    </fieldset>
			</form>




</div>







				

            	</div>


			</div>
            
            
		</div>
	</div>
</div>
</div>
<?php betashop_get_footer(); ?>