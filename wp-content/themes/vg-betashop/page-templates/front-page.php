<?php
/**
 * Template Name: Home Shop Template
 *
 * Description: Home Shop template
 *
 * @package    VG Betashop
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */

$betashop_options  = betashop_get_global_variables(); 

betashop_get_header();
?>


<style type="text/css">
#container {margin-bottom:-76px; position: relative; z-index: 999;}	
#container canvas {width: 100% !important; height:550px !important;}	
#container + .main-container.front-page .page-content .catlist-group {transform: none;-webkit-transform:none;}
.desc_txt_wrp {width: 100%; float: left; position: relative;}
.desc_txt_le_wrp {width: 350px; right: 103%; top: 0; position: absolute;}
.desc_txt_ri_wrp {width: 350px; left: 103%; top: 0; position: absolute;}
.desc_txt_li_wrp {width: 100%; float: left; padding: 10px; margin:0px 0 10px; background: rgba(0,0,0,0.6);}
.desc_txt_li_wrp h2 {font-size:13px; color: #fff; margin: 0; padding: 7px 0 7px 37px; position: relative;}
.desc_txt_li_wrp p {font-size:13px; color: #eee; margin: 0; padding:5px 0; line-height: 20px;}
.main-container .page-content {overflow: inherit;}
.desc_txt_ic {width: 30px; margin: 0; position: absolute; left: 0; top: 0;}
.read_more_btn_ban {background: rgba(0,0,0,0.5); border:1px solid #aaa; height: 38px; padding: 0 15px; font-size: 14px; color: #ccc; line-height: 37px; display: inline-block; text-transform: uppercase; }
.read_more_btn_ban:hover {background: #000; border-color: #fff; color: #fff;}
.desc_txt_li_wrp.read_more_btn_ban_wrp {background: none; padding-left: 0;}
.catlist-group {
    -webkit-transform: translate(0,-10px);
    -moz-transform: translate(0,-10px);
    -ms-transform: translate(0,-10px);
    -o-transform: translate(0,-10px);
    transform: translate(0,-10px);
}
#home_banner_main {display: none;}
</style>

<div id="container"></div>
    <div id="desc-container" style="display:none">
    	<div class="desc_txt_wrp">
    		<div class="desc_txt_le_wrp">
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/aesthetic_designs_ic.png" class="desc_txt_ic"> Aesthetic designs</h2>
    				<!-- <p>Sleek designs, vibrant colours, and innovative solutions bring power and elegance to suit evolving customer tastes.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/brass_burners_and_metallic_knobs_ic.png" class="desc_txt_ic">Brass burners and metallic knobs</h2>
    				<!-- <p>The long lasting knobs have been ergonomically designed for a long comfortable experience.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/built_in_safety_measures_ic.png" class="desc_txt_ic">Built-in Safety measures</h2>
    				<!-- <p>flame failure Device automatically cuts off the gas supply if the flames go off.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/flexible_use_for_both_NG_and_LPG_ic.png" class="desc_txt_ic">Flexible use for both NG and LPG</h2>
    				<!-- <p>Kutchina Hob can work on both LPG and NG options. Thus, giving the end user the flexibility of use.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/CE_certification_ic.png" class="desc_txt_ic">CE Certification</h2>
    				<!-- <p>Aesthetic and innovative products that comply with international standards of health, safety, and energy efficiency to adding convenience and commitment to customer life.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp read_more_btn_ban_wrp">
    				<a href="https://www.kutchina.com/product-category/appliances/hobs/" target="_blank" class="read_more_btn_ban">Read more</a>
    			</div>
    		</div>

    		
        <video width="560" height="360" style="object-fit: initial;" controls autoplay>
            <source src="wp-content/360/Gas.mp4">
            Your browser does not support the video tag.
        </video>
        </div>
    </div>
    <div id="desc-container1" style="display:none">
    	<div class="desc_txt_wrp">
    	<div class="desc_txt_le_wrp">
    			<!-- <div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/Dishwasher.mp4" class="desc_txt_ic"> Intelligent Technology That Evolves to New Age Needs</h2>
    				<p>I-auto clean chimney powered by AI, understands new age customers’ needs just we do. Here is an intelligent system that not only understands the user’s cooking cycle but also time constraints. The I- auto clean chimney, therefore, sets self-cleaning schedules as per cooking frequency and duration. And cleans without a touch of a button.</p> 
    			</div> -->
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/touch_free_cleaning_ic.png" class="desc_txt_ic"> iAutioclean enabled chimneys, Powered by AI.</h2>
    				<!-- <p>An innovative solution that breaks away from the need for buttons to make cleaning easy and very convenient. </p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/lifetime_warranty.png" class="desc_txt_ic"> Lifetime Warranty</h2>
    				<!-- <p>Guaranteed power and convenience for a lifetime with a warranty on every product.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/filter_less_for_greater_suction.png" class="desc_txt_ic"> Filter Less for Greater Suction</h2>
    				<!-- <p>High suction capacity based on intelligent technology & strong suction power, Kutchina chimney improves the aesthetics of your kitchen.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/cotton_winding_motors.png" class="desc_txt_ic"> 100% Cotton Winding Motors</h2>
    				<!-- <p>Sturdy machines made with alloy and metal to deliver chimney that works longer.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp read_more_btn_ban_wrp">
    				<a href="https://www.kutchina.com/product-category/appliances/chimney/" target="_blank" class="read_more_btn_ban">Read more</a>
    			</div>
    		</div>

    	
        <video width="560" height="360" style="object-fit: initial;" controls autoplay>
            <source src="wp-content/360/Chimney.mp4">
            Your browser does not support the video tag.
        </video>
    </div>
    </div>
    <div id="desc-container2" style="display:none">
    	<div class="desc_txt_wrp">
    	<div class="desc_txt_le_wrp">
    		<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/pre_set_functions.png" class="desc_txt_ic">9 Pre-Set Functions</h2>
    				<!-- <p>Equipped with 9 Functions – Kutchina BI Oven comes with defrost, bottom heat, convectional cooking, convection with fan, radiant grilling, double grilling, double grilling with fan, lamp and convection functions.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/child_lock.png" class="desc_txt_ic">Child Lock</h2>
    				<!-- <p>Child/Safety Lock – The child lock function of your Kutchina BI Microwave Oven prevents accidental program changes and stops children playing with the controls while the appliance is running.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/electric_control_touch_door.png" class="desc_txt_ic">Electric Control Touch Door</h2>
    				<!-- <p>Electric Control Touch Opening Door – Kutchina BI Microwave Oven comes with Touch Opening Door unlike the orthodox push button doors available in the market.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/convenient_cooking.png" class="desc_txt_ic">Convenient Cooking Experience with Skewer & Universal Pan</h2>
    				<!-- <p>Rotisserie Skewer & Universal Plan for Convenient Cooking Experience -  This feature allows you to heat the food evenly and at the same time it conserves the nutrition of the food.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/auto_cooking_programs.png" class="desc_txt_ic"> 10 Auto Cooking Programs</h2>
    				<!-- <p>10-Auto cooking Programs Menu-Kutchina BI Microwave Oven comes with 10 Indian and European cooking menu options - such as soup, cake, pizza, pasta, etc.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp read_more_btn_ban_wrp">
    				<a href="https://www.kutchina.com/product-category/appliances/bi-oven/" target="_blank" class="read_more_btn_ban">Read more</a>
    			</div>
    		</div>

    	
        <video width="560" height="360" style="object-fit: initial;" controls autoplay>
            <source src="wp-content/360/Owen.mp4">
            Your browser does not support the video tag.
        </video>
    </div>
    </div>
    <div id="desc-container3" style="display:none">
    	<div class="desc_txt_wrp">
    	<div class="desc_txt_le_wrp">
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/place_setting.png" class="desc_txt_ic">14 Place setting</h2>
    				<!-- <p>Kutchina dishwashers come with 14 place settings which means now you will have that extra space. </p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/d_child_lock.png" class="desc_txt_ic">Child Lock</h2>
    				<!-- <p>Child/safety lock – The child lock function of kutchina dishwashers prevents accidental program changes and stop children playing with the controls while the appliance is running. </p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/digital_display_interface.png" class="desc_txt_ic">Digital Display interface </h2>
    				<!-- <p>Digital Display Interface- Kutchina Dishwasher comes equipped with digital display. Which indicates the program's time, error indication & mode. </p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/energy_rating.png" class="desc_txt_ic"> A+++ Energy Rating </h2>
    				<!-- <p>A+++ Energy Efficiency- Kutchina Dishwasher consumes less power, has more efficient washing and drying functions.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp">
    				<h2><img src="wp-content/360/delay_functions.png" class="desc_txt_ic"> Delay Functions</h2>
    				<!-- <p>Delay Functions- This function allows the user to set a time when the Dishwasher needs to start its operation.</p> -->
    			</div>
    			<div class="desc_txt_li_wrp read_more_btn_ban_wrp">
    				<a href="https://www.kutchina.com/product-category/appliances/dishwashers/" target="_blank" class="read_more_btn_ban">Read more</a>
    			</div>
    		</div>

        <video width="560" height="360" style="object-fit: initial;" controls autoplay>
            <source src="wp-content/360/Dishwasher.mp4">
            Your browser does not support the video tag.
        </video>
    </div>
    </div>
<div class="main-container front-page">
	<div class="page-content">
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>
		<?php endwhile; ?>
	</div>
</div>
<?php betashop_get_footer(); ?>