<?php
 

  global $wpdb;
  
  if(isset($_GET['supportState']) || isset($_GET['selectCity']))
  {
    $supportState = $_GET['supportState'];
    //echo $selectState; exit;
    $single_state_str = "SELECT * FROM `kut_state` WHERE LOWER(`state_name`) = '".strtolower($supportState)."' "; 
    $single_state_posts = $wpdb->get_row($single_state_str, OBJECT);
    //print_r($single_state_posts); exit;
    $state_id = $single_state_posts->id;

    $citystr = "SELECT * FROM `kut_city` WHERE `state_id`= ".$state_id; 
    $city_posts = $wpdb->get_results($citystr, OBJECT);

    $selectCity = $_GET['selectCity'];
  }
  else{

     $city_str = "SELECT * FROM `kut_city` WHERE 1 ";
     $city_posts = $wpdb->get_results($citystr, OBJECT);                                 
  }

  
  

   if($supportState != '' && $selectCity == '' )
   {
      $search_querystr = "SELECT * FROM `kut_support` WHERE LOWER(`state`) = '".strtolower($supportState)."' "; 
   }
   else if($selectState == '' && $selectCity != '')
   {
      $search_querystr = "SELECT * FROM `kut_support` WHERE LOWER(`city`) = '".strtolower($selectCity)."' "; 
   }
   else if($supportState != '' && $selectCity != '')
   {
      $search_querystr = "SELECT * FROM `kut_support` WHERE LOWER(`city`) = '".strtolower($selectCity)."' AND  LOWER(`state`) = '".strtolower($supportState)."' "; 
   }
   else
   {
       $search_querystr = "SELECT * FROM `kut_support` WHERE 1";
   }


  $support_posts = $wpdb->get_results($search_querystr, OBJECT);

  //print_r($support_posts); exit;
?>
<div class="main-container page-faq full-width page-title">
    

      <div class="container">
              <div class="row">
                      <div class="page-content">
                     
                          <div class="col-md-4 col-sm-4 col-xs-12">
                               <div>
                                  <select id="selectState1" name="selectState1" class="form-control" onchange="selectCityFromState();">
                                    <option value="">Select State</option>
                                    <?php
                                       $state_str = "SELECT * FROM `kut_state` WHERE 1 ";
                                       $state_posts = $wpdb->get_results($state_str, OBJECT);

                                       for($i=0;$i<sizeof($state_posts);$i++)
                                       {
                                    ?>

                                    <option value="<?php echo $state_posts[$i]->state_name ?>" <?php if( strtolower($supportState) == strtolower($state_posts[$i]->state_name) ) echo "selected"; ?>><?php echo $state_posts[$i]->state_name ?></option>

                                    <?php } ?>
                                  </select>             
                              </div>


                               <div>
                                  <select id="selectCity" name="selectCity" class="form-control" onchange="selectCityFromState();">
                                    <option value="">Select City</option>
                                    <?php
                                       

                                       for($i=0;$i<sizeof($city_posts);$i++)
                                       {
                                    ?>

                                    <option value="<?php echo $city_posts[$i]->city_name ?>" <?php if( strtolower($selectCity) == strtolower($city_posts[$i]->city_name) ) echo "selected"; ?>><?php echo $city_posts[$i]->city_name ?></option>

                                    <?php } ?>
                                  </select>             
                              </div>



<!-- /////////////////////////////////////////// Map Functionality START ////////////////////////////////////////////// -->

<?php echo do_shortcode("[put_wpgm id=3]"); ?>

<!-- /////////////////////////////////////////// Map Functionality END ////////////////////////////////////////////// -->



                        </div>
                          <div class="col-md-8 col-sm-8 col-xs-12">

                             <div class="support_det">

                              <p class="service_sup_text">Be aware of Unauthorized service center & choose  genuine convenience only it is important that you protect your grievances for Authorized service center only</p>
                              <p>FOR ANY QUERIES RELATED TO SERVICE PLEASE CALL 18004197333<br/>
                                  <b> TIMING : </b><br/>
                                  WORKING DAYS : 9:30 AM TO 6:00 PM<br/>
                                  SATURDAY : 9:30 AM TO 3:00 PM<br/>
                                  SUNDAY : CLOSED </p>
                               
                             </div> 


                      <?php
                        for($i=0;$i<sizeof($support_posts);$i++)
                        {
                      ?>
                            <ul class="address2">
                              <li><strong>State :</strong><?php echo $support_posts[$i]->state ?></li>
                              <li><strong>City  :</strong><?php echo $support_posts[$i]->city ?></li>
                              <!-- <li><strong>Contact Person  :</strong><?php echo $support_posts[$i]->incharges_names ?></li> -->
                              <li><strong>Contact Number  :</strong><?php echo $support_posts[$i]->contact_no ?></li>
                              <li><strong>Address :</strong><?php echo $support_posts[$i]->address ?></li>
							  </ul>

                      <?php } ?>
                          </div>
                    </div>
            </div>
    </div>
</div>

<script type="text/javascript">

  function selectCityFromState()
  {
      var selectState = jQuery("#selectState1").val();
      var selectCity = jQuery("#selectCity").val();
      

       window.location.href = "<?php echo site_url();?>/our-presence?supportState="+selectState+"&selectCity="+selectCity+'#menu1';
  }
</script>

