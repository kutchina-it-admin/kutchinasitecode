<?php


  global $wpdb;

  if(isset($_GET['StoreType'])  || isset($_GET['selectState']) )
  {
    
   $storetype = $_GET['StoreType'];
   //$presencetype = $_GET['presenceType']; 
   $selectState = $_GET['selectState'];
   //$allSearch = $_GET['allSearch'];
    //exit; 

  }

  $where = "";

  if($storetype != ''  && $selectState == ''  )
  {
    //echo "1"; exit; 
    $where .= "  `channel_type`= '".$storetype."' ";
  }
  /*else if($presencetype != '' && $storetype == '' && $selectState == ''   )
  {
    //echo "2"; exit; 
    $where .= "  `dealer_type`= '".$presencetype."' ";
  }*/
  else if($storetype == '' && $selectState != ''   )
  {
    //echo "3"; exit;
    $where .= "  LOWER(state) = '".strtolower($selectState)."' " ;

  }
  else if( $storetype != '' && $selectState != ''   )
  {
    //echo "4"; exit;
    $where .= "  LOWER(state) = '".$selectState."'  AND `channel_type`= '".$storetype."' " ;

  }
  else if( $storetype != '' && $selectState == ''   )
  {
    //echo "5"; exit;
    $where .= "  `channel_type`= '".$storetype."'  " ;

  }
  
  else if($storetype == '' && $selectState != ''   )
  {
    //echo "7"; exit;
    $where .= "  LOWER(state) = '".strtolower($selectState)."'  " ;

  }
  /*else if($allSearch != '')
  {
    //echo "8"; exit;
    $where .= "  `name` LIKE '%$allSearch%' OR `address` LIKE '%$allSearch%'  " ;
  }*/


  if($where != '')
  {
    $querystr = "SELECT * FROM `kut_our_presence` WHERE ".$where; 
  }
  else
  {
    $querystr = "SELECT * FROM `kut_our_presence` WHERE 1 ";
  }
  

  $presence_posts = $wpdb->get_results($querystr, OBJECT);
  //print_r($presence_posts); exit;
?>
<div class="main-container page-faq full-width page-title">
 
  
  <div class="container">
    <div class="row">
    <div class="page-content">
      

      <div class="col-md-4 col-sm-4 col-xs-12">

            <div>
              <select id="StoreType" class="form-control" name="StoreType" onchange="store_type();">
                  <option value="">select channel</option>
                  <option value="direct_channel" <?php if($storetype == 'direct_channel') echo "selected"; ?> >Direct Channel</option>
                  <option value="retail" <?php if($storetype == 'retail') echo "selected"; ?>>Retail</option>
              </select>               
            </div>
          
            <!--<div>
                <select id="presenceType" class="form-control" name="presenceType" onchange="store_type();">
                  <option value="">Select Presence Type</option>
                  <option value="distributor" <?php if($presencetype == 'distributor') echo "selected"; ?>>Distributor</option>
                  <option value="dealer" <?php if($presencetype == 'dealer') echo "selected"; ?>>Dealer</option>
                </select>             
            </div>-->

             <div>
                <select id="selectState" class="form-control" name="selectState" onchange="store_type();">
                  <option value="">Select State</option>
                  <?php
                     $state_str = "SELECT * FROM `kut_state` WHERE 1 ";
                     $state_posts = $wpdb->get_results($state_str, OBJECT);

                     for($i=0;$i<sizeof($state_posts);$i++)
                     {
                  ?>

                  <option value="<?php echo $state_posts[$i]->state_name ?>" <?php if( strtolower($selectState) == strtolower($state_posts[$i]->state_name) ) echo "selected"; ?>><?php echo $state_posts[$i]->state_name ?></option>

                  <?php } ?>
                </select>             
            </div>


           <!-- <div>
              <input type="text" name="allSearch" id="allSearch"  class="form-control2" value="" placeholder="Search by keyword"  onchange="store_type();">
            </div>-->



<!-- /////////////////////////////////////////// Map Functionality START ////////////////////////////////////////////// -->

<?php echo do_shortcode("[put_wpgm id=2]"); ?>

<!-- <style type="text/css">
  #map {
    border:1px solid black;
    width: 100%;
    height: 600px;
    border-radius: 4px;
  }
</style>

<div id="map"></div>

<script>
function initMap() {
       var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: {lat: -33.9, lng: 151.2}
       });

    setMarkers(map);
}
    var beaches = [
        ['Bondi Beach', -33.890542, 151.274856, 4],
        ['Coogee Beach', -33.923036, 151.259052, 5],
        ['Cronulla Beach', -34.028249, 151.157507, 3],
        ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
        ['Maroubra Beach', -33.950198, 151.259302, 1]

      ];
    function setMarkers(map) {
      for (var i = 0; i < beaches.length; i++) {
          var beach = beaches[i];
          var marker = new google.maps.Marker({
            position: {lat: beach[1], lng: beach[2]},
            map: map,
            title: beach[0],
            zIndex: beach[3]
          });
        }
      }
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKeNCoOyx1-4b02iXNUtkqiqA_MweWnkg&callback=initMap&sensor=false"></script> -->


<!-- /////////////////////////////////////////// Map Functionality END ////////////////////////////////////////////// -->


          
      </div>
      <div class="col-md-8 col-sm-8 col-xs-12">
<?php
  for($i=0;$i<sizeof($presence_posts);$i++)
  {
?>
       <ul class="address3">
        <li><strong>State :</strong><?php echo $presence_posts[$i]->state ?></li>
        <li><strong>City  :</strong><?php echo $presence_posts[$i]->city ?></li>
        <li><strong>Name  :</strong><?php echo $presence_posts[$i]->name ?></li>
        <li><strong>Pincode  :</strong><?php echo $presence_posts[$i]->pincode ?></li>
        <li><strong>Contact Person :</strong><?php echo $presence_posts[$i]->contact_person ?></li>
        <li><strong>Contact Number :</strong><?php echo $presence_posts[$i]->contact_number ?></li>
        <li><strong>Email :</strong><?php echo $presence_posts[$i]->email ?></li>
        <li><strong>Address :</strong><?php echo $presence_posts[$i]->address ?></li>
      </ul>
<?php } ?>
      </div>

    </div>
  </div>
</div>
</div>

<script type="text/javascript">
  function store_type(){
    //alert("aa");
    var StoreType = jQuery("#StoreType").val();
    //alert(StoreType);
    //var presenceType = jQuery("#presenceType").val();
    //alert(presenceType);
    var selectState = jQuery("#selectState").val();
    //var allSearch = jQuery("#allSearch").val();
    /*window.location.href = "<?php echo site_url();?>/our-presence/?StoreType="+StoreType+"&presenceType="+presenceType+"&selectState="+selectState+"&allSearch="+allSearch;*/
	   window.location.href = "<?php echo site_url();?>/our-presence/?StoreType="+StoreType+"&selectState="+selectState+"#home";
  }
</script>

