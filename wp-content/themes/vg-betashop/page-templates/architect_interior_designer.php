<?php
/**
 * Template Name: Architect & Interior Designer
 *
 * Description: Request A Callback page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>

<style type="text/css">
  .connect-withus{
    position: fixed;
    top: 300px;
    right: -85px;
    z-index: 999999;
    border-radius: 4px;
    transform: rotateZ(-90deg);
}

.connect-withus a{
   font-size: 22px; color: #fff; font-weight: 600; padding: 12px 15px;
display: block;background: #ec1738;border-radius: 5px;

  }
  
  .connect-withus a .fa{font-size: 35px; vertical-align: middle; margin: -2px 0px 0px 9px; line-height:35px;}
.connect-withus a:hover{background: #0060aa; color: #fff;}




@media only screen and (max-width: 980px) {
.connect-withus {
    top: 250px;
    right: -70px;
}

}

@media only screen and (max-width: 600px) {

.connect-withus {
   /* bottom: 131px;
    left: 0;
    right: 0;*/
    text-align: center;
}
  .connect-withus a {
    font-size: 20px;
    color: #fff;
    font-weight: 600;
    padding: 12px 15px;
    display: block;
    background: #0060aa;
    border-radius: 0;
}

}

</style>

 <!-- Modal -->
  <div class="modal fade" id="video_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <video controls id="video1">
            <source src="https://www.kutchina.com/wp-content/uploads/2015/10/Kutchina_Kitchen_v2_x264.mp4" type="video/mp4">
            Your browser doesn't support HTML5 video tag.
          </video>
        </div>
      </div>
    </div>
  </div>
 <!-- Modal -->

<div class="main-container page-aid full-width page-title">
  <div class="arc_inte_design_banner">
    <img src="https://www.kutchina.com/wp-content/uploads/2015/10/Kutchina-architect-interior-designer_Banner-1-1.png" alt="" style="width: 100%;">
   <div class="item_arcita">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <h2>
             Creating beautiful <br>kitchens, together.
           </h2>
         </div>
       </div>
     </div>
   </div>
  </div>

<div class="most_beauty">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Designing <br> India’s most beautiful <br> & versatile kitchens…  </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <p>A name synonymous to innovation and convenience; Kutchina takes pride in shaping convenient kitchens for our customers’ modern living needs. Right from being at the forefront of developing ideas to transforming them with our passion – we are delighted to have successfully wrapped a decade of delivering smiles across the country with our unmatched Modular Kitchen set-ups.</p>
      </div>
      <div class="col-md-8">
       <div class="you_ar-v">
         <a href="#" class="" data-toggle="modal" data-target="#video_modal"><img src="https://www.kutchina.com/wp-content/uploads/2015/10/youtube.jpg" alt="#"></a>
       </div>
      </div>
    </div>
  </div>
</div>


<div class="strong_brand" style="background: url(https://www.kutchina.com/wp-content/uploads/2015/10/Kutchina-architect-interior-designer_Banner-2-Stores-1.png);">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2>Building a strong brand <br>with ideas and intelligence! </h2>
        <p><span>Since 2010</span>Kutchina’s customisable Modular Kitchens have raised the bar across all parameters of form, design, functionality, quality, technology, product service and affordability. We believe that convenience comes in all shapes and thus our offerings span across straight line, L shaped, U shaped, parallel, island and peninsula kitchen set-ups. This continuous effort has seen us pioneering millions of such stylish kitchens to our valued customers, and still counting! </p>
        <p>When it comes to our ever-increasing outreach and brand presence; currently we are operating with 52 stores pan India. But our drive to make the best and most exciting use of kitchen spaces inspires us to look forward to 52 more stores in the financial year of 2020-21.</p>
      </div>
    </div>
      <div class="row">
      <div class="col-md-6">
        <h3>52 Stores!</h3>
        <h6>all over India</h6>
      </div>
    </div>
  </div>
</div>
    


    <div class="most_popular">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Most Popular</h2>        
        <div id="owl-demoz" class="owl-carousel owl-theme">
           <div class="item">
            <div class="banner_pic">
          <img src="https://www.kutchina.com/wp-content/uploads/2015/10/slier_c.jpg" alt="">
            </div>
           </div>
            <div class="item">
            <div class="banner_pic">
          <img src="https://www.kutchina.com/wp-content/uploads/2015/10/slier_a.jpg" alt="">
            </div>
           </div>
            <div class="item">
            <div class="banner_pic">
          <img src="https://www.kutchina.com/wp-content/uploads/2015/10/slier_b.jpg" alt="">
            </div>
           </div>
            <div class="item">
            <div class="banner_pic">
          <img src="https://www.kutchina.com/wp-content/uploads/2015/10/slier_d.jpg" alt="">
            </div>
           </div>
            <div class="item">
            <div class="banner_pic">
          <img src="https://www.kutchina.com/wp-content/uploads/2015/10/slier_a.jpg" alt="">
            </div>
           </div>
          </div>
      </div>
    </div>
  </div>
</div>



<div class="be-a_partner">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2><span>Be a partner</span> <br>in creating customer <br>value & growth</h2>
        <p>With the modular kitchen industry set to grow at an exponential rate in the next few years, there is also a rising need for expanding our horizons to every nook and corner of the country. This also brings along a booming business opportunity for those who wish to partner with us in delivering world-class products and services to our valued customers.</p>
        <p>Whether you are an architect, interior designer or builder – together our urge for providing excellence, innovation and convenience to end-users can empower Kutchina to become India’s biggest name in modular kitchen. Because while on one hand, as we keep our customers first to focus on R&D that meets their evolving demands; the emphasis on building a ‘winning team’ delivering such desires with intelligent and affordable range of products is just as high! Our robust support system guarantees extraordinary execution and after-sales service that delights customers with a world-class and cherishable experience.</p>
      </div>
      <div class="col-md-6">
        <img src="https://www.kutchina.com/wp-content/uploads/2015/10/ex_img.png">
      </div>
    </div>
  </div>
</div>

<div class="w_c_dwn">
 <img src="https://www.kutchina.com/wp-content/uploads/2015/10/Kutchina-architect-interior-designer_Banner3-Features1.png" width="100%">
</div>

<div class="most_beauty an_ass" style="background: url(https://www.kutchina.com/wp-content/uploads/2015/10/Kutchina-architect-interior-designer_Banner-4-association-.png); background-size: cover;background-position: center;">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2>An association that <br>inspires & evolves </h2>     
        <p>Geared up for the next phase of growth, we are looking to your unique association to welcome a massive transformation in India’s kitchen scenes. We are certain that our uncompromising perfection along with your keen interest in providing nothing but the best to your clients can result in a glorious collaboration of ideas, opportunities and brilliance.</p>
        <p>Different kitchens require different solutions and as a Kutchina Partner/Kutchina Growth Partner, you can be rest assured to be on the top of the game when it comes to client satisfaction and experience.  As a partner in our success story, you enjoy:</p>
      </div>
      </div>
      <div class="row">
      <div class="col-md-12">
       <div class="blue_rnd">
         <p>Unlimited access to our stunning variety of products</p>
         <p>Regular assistance and product guidance</p>
         <p>Delivery of specific needs of your projects as per the budget</p>
         <p>Designing every kitchen space with finesse and exclusivity  </p>
         <p>A lifelong association of good work and growth</p>
       </div>
      </div>
      </div>   
  </div>
</div>
    

<div class="most_beauty an_ass cnct_w_us" style="background: url(https://www.kutchina.com/wp-content/uploads/2015/10/Kutchina-architect-interior-designer_Banner-5-Customer-Care-.png); background-size: cover;background-position: center;">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2>Shaping a stronger <br>brand with strong bonds</h2>     
        <p>So if you’re thinking of becoming a Kutchina Partner/ Kutchina Growth Partner and working alongside the best to cater to your clients’ new-age modular kitchen needs, Kutchina is the one-stop solution.</p>
       <span>Get in touch with us and explore infinite business possibilities to deliver the best to your clients!</span>
       <a href="#">Connect with us</a>
      </div>
      </div>  
  </div>
</div>
    

<div class="connect-withus"><a href="https://partners.kutchina.com/" target="_blank">Connect with us<i class="fa fa-angle-right"></i></a></div>









<script>
jQuery(document).ready(function() {
 
var owl = jQuery("#owl-demoz");
 
  owl.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : true, // itemsMobile disabled - inherit from itemsTablet option
      autoPlay: 7000,
       navigation : true
  });
 
  // Custom Navigation Events
  jQuery(".next").click(function(){
    owl.trigger('owl.next');
  })
  jQuery(".prev").click(function(){
    owl.trigger('owl.prev');
  })

 
});
</script>

</div>
<?php betashop_get_footer(); ?>
