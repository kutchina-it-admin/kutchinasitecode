<?php
 /* Template Name: Blog Template */	
 get_header();

?>
<div class="main-container page-faq full-width page-title">
    <div class="row-breadcrumb">
        <div class="container">
          <?php betashop_breadcrumb(); ?>
        </div>
    </div>

      <div class="container">
              <div class="row">
                      <div class="page-content">

                        <div class="col-xs-12">
                            <header class="entry-header">
                              <h1 class="entry-title"><?php the_title(); ?></h1>
                            </header>
                          </div>

                          
                          <div class="col-xs-12">
                           
                            <?php
                                $args = array(
                                    'posts_per_page'   => -1,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'post_type'        => 'post',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'author'           => '',
                                    'author_name'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => true 
                                );
                                $posts_array = get_posts( $args ); 

                                //print_r($posts_array); exit;
                                for($i=0;$i<sizeof($posts_array);$i++){
                            ?>
                            <div class="blog-box">
                              <h3><?php echo $posts_array[$i]->post_title ?></h3>
                              <p><?php echo substr($posts_array[$i]->post_content,0,250).'...' ?></p> 
							  <a href="<?php the_permalink($posts_array[$i]->ID); ?>">READ MORE</a>
                            </div>

                            <?php } ?>
                             
                          </div>

                    </div>
            </div>
    </div>
</div>
<?php get_footer(); ?>


