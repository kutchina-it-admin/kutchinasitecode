<?php
 /* Template Name: Amazon Product Template */	
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
  
?>
<div class="main-container page-faq full-width page-title">
    <div class="row-breadcrumb">
        <div class="container">
          <?php betashop_breadcrumb(); ?>
        </div>
    </div>

      <div class="container">
              <div class="row">
                      <div class="page-content">

                        <div class="col-xs-12">
                            <header class="entry-header">
                              <h1 class="entry-title"><?php the_title(); ?></h1>
                            </header>
                          </div>







<!-- /////////////////////////////////// Custom Changes for Amazon Category by SUMAN START ///////////////////////////////// -->

<!-- <?php
   $args = array(
               'taxonomy' => 'amazon_categories',
               'orderby' => 'name',
               'order'   => 'ASC'
           );
   $cats = get_categories($args);
   foreach($cats as $cat) {
?>
	<a href="<?php echo get_category_link( $cat->term_id ) ?>">      	
		<?php echo $cat->name; ?>
	</a>
<?php
   }
?> -->

<!-- /////////////////////////////////// Custom Changes for Amazon Category by SUMAN END ///////////////////////////////// -->















                          
                          <div class="col-xs-12">
                            
                                <?php
                                  $product_details = array(
									'posts_per_page'   => -1,
                                    'offset'           => 0, 
                                    'category_name'    => '',
                                    'orderby'          => 'date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'post_type'        => 'amazon_products',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'author'     => '',
                                    'author_name'    => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => true 
                                  );
                                  $post_total_array = get_posts( $product_details );
                                  //print_r($post_total_array); exit;
                                 for($i=0;$i<sizeof($post_total_array);$i++)
                                {  
                              ?>

                            <div class="list-row">
                              <figure><a href="<?php echo get_field('amazon_product_url',$post_total_array[$i]->ID); ?>" target="_blank"><img src="<?php $feat_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_total_array[$i]->ID),'article-image'); echo $feat_image[0]; ?>" width="285" height="219" alt=""></a></figure>
                              <div>
                                <!-- <h3><a href="<?php echo get_field('amazon_product_url',$post_total_array[$i]->ID); ?>" target="_blank"><?php echo $post_total_array[$i]->post_title;?></a></h3> -->
                                <h3><a href="<?php echo get_field('amazon_product_url',$post_total_array[$i]->ID); ?>" target="_blank"><?php echo strip_tags(substr($post_total_array[$i]->post_title,0,55))."...";?></a></h3>                         
                                <p><?php echo strip_tags(substr($post_total_array[$i]->post_content,0,200))."...";?> </p>
                               <!-- <a href="<?php //echo the_permalink($post_total_array[$i]->ID);?>" class="view-btn">Read More</a>-->
                               <a href="<?php echo get_field('amazon_product_url',$post_total_array[$i]->ID); ?>" class="button product_type_simple" target="_blank">Buy Now</a>
                              </div>
                            </div>
                            
                            <?php } ?>
                             
                          </div>

                    </div>
            </div>
    </div>
</div>


<?php betashop_get_footer(); ?>
