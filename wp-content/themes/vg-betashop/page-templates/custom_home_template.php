<?php
/**
 * Template Name: Custom Home Page Template
 *
 * Description: Custom Home page template March 2020
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>


<div class="banner_section">
  <div id="owl-demo" class="owl-carousel owl-theme bnr_static">

    <div class="item">
      <div class="banner_pic"><a href="https://www.kutchina.com/product-category/appliances/chimney/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/kutchina-chimney.jpg" alt=""></a></div>
    </div>

<div class="item">
  <div class="banner_pic"><a href="https://www.kutchina.com/product-category/appliances/chimney/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/I-Auto-Clean.jpg" alt=""></a></div>
</div>


<div class="item">
  <div class="banner_pic"><a href="https://www.kutchina.com/product-category/appliances/hobs/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/kutchina-hob.jpg" alt=""></a></div>
</div>

<div class="item">
  <div class="banner_pic"><a href="https://www.kutchina.com/product-category/appliances/microwave-oven/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/kutchina-bii-oven.jpg" alt=""></a></div>
</div>

<div class="item">
  <div class="banner_pic"><a href="https://www.kutchina.com/product-category/small-appliances/mixer-grinder/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/mixer-grindera.jpg" alt=""></a></div>
</div>

<!--<div class="item">
  <div class="banner_pic"><a href="https://www.kutchina.com/product-category/small-appliances/juicer/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/juicer.jpg" alt=""></a></div>
</div>-->

<div class="item">
  <div class="banner_pic"><a href="#"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/small-appliances_b.jpg" alt=""></a></div>
</div>


<div class="item">
  <div class="banner_pic"><a href="https://www.kutchina.com/product-category/modular-kitchens/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/kutchina-modular-kitchen.jpg" alt=""></a></div>
</div>

<div class="item">
  <div class="banner_pic"><a href="https://www.kutchina.com/product-category/water-purifiers/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/kutchina-healthifier.jpg" alt=""></a></div>
</div>


<div class="item">
  <div class="banner_pic"><a href="#"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/last_banner2.jpg" alt=""></a></div>
</div>

  </div>
 <!-- <div class="customNavigation">
  	<a class="btn prev">Previous</a>
  	<a class="btn next">Next</a>
  </div>-->
</div>

<div class="section3 s3_bg ">
  <div class="container">
    <div class="row icon_fst_sec">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="gimg_box">
    <a>
    <img src="<?php bloginfo ( 'template_url' ); ?>/images/50M-Satisfied.png" 
    onmouseover="this.src='<?php bloginfo ( 'template_url' ); ?>/images/50M-Satisfied-r.png'"
    onmouseout="this.src='<?php bloginfo ( 'template_url' ); ?>/images/50M-Satisfied.png'"
    border="0" alt=""/>
    </a>


</div>

        <div class="g_text_box">
          <h2>50M+ Satisfied<br> Customers</h2>
        </div>

      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="gimg_box">
    <a>
    <img src="<?php bloginfo ( 'template_url' ); ?>/images/48-Stores.png" 
    onmouseover="this.src='<?php bloginfo ( 'template_url' ); ?>/images/48-Stores-r.png'"
    onmouseout="this.src='<?php bloginfo ( 'template_url' ); ?>/images/48-Stores.png'"
    border="0" alt=""/>
    </a>

             
        </div>

                     <div class="g_text_box">
          <h2>50+ Stores <br>all Across India</h2>
        </div>

      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="gimg_box">
    <a>
    <img src="<?php bloginfo ( 'template_url' ); ?>/images/Best-Modular-Kitchen.png" 
    onmouseover="this.src='<?php bloginfo ( 'template_url' ); ?>/images/Best-Modular-Kitchen-r.png'"
    onmouseout="this.src='<?php bloginfo ( 'template_url' ); ?>/images/Best-Modular-Kitchen.png'"
    border="0" alt=""/>
    </a>

        </div>

        <div class="g_text_box">
          <h2>Best Modular Kitchen<br> Brand TOI 2019</h2>
        </div>

      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="gimg_box">
    <a>
    <img src="<?php bloginfo ( 'template_url' ); ?>/images/Uniquely-Designed.png" 
    onmouseover="this.src='<?php bloginfo ( 'template_url' ); ?>/images/Uniquely-Designed-r.png'"
    onmouseout="this.src='<?php bloginfo ( 'template_url' ); ?>/images/Uniquely-Designed.png'"
    border="0" alt=""/>
    </a>

        </div>

                <div class="g_text_box">
          <h2>Uniquely Designed <br> for Convenience</h2>
        </div>

      </div>
      </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
        <div class="gimg_box"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/about_p.jpeg" alt=""></div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="about_text_box about_text_box_b">
          <h2>ABOUT US</h2>
          <p>At Kutchina, change is constant. Our diligent R&D team consistently studies market trends and customer needs to bring in new technologies across our product range. It is this eye on the future that helped us pioneer India's first auto clean chimney and it continues to inspire us to innovate every day.
</p>
          <div class="about_enquire_now"><a href="<?php bloginfo ( 'wpurl' ); ?>/our-journey/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Know More</a></div>
        </div>
      </div>
    </div>



      
    </div>
  </div>



</div>
<div class="section4 s4_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 best_product">
        <div><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/ticon.png" alt=""></div>
        <div class="best_product_text">
          <h2>Uniquely Designed <br>Kitchen Products</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="banner_bg4">
    <div class="banner_desktop"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/chimney.jpg" alt=""></div>
    <div class="banner_mobile"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/banner_bg4_mobile.jpg" alt=""></div>
    <div class="container2">
      <div class="banner4_text">
        <h2>KITCHEN CHIMNEY</h2>
        <p>From the makers of India's first-auto clean chimney to all new in Intelligent Autoclean  chimney, added convenience to the life of the customer at a new level.</p>
		<ul class="product_list_home">
		<li><span>I-Auto Clean </span></li>
		<li>Filter-less, great suction</li>
		<li>100% copper winding motors</li>
		<li>Lifetime warranty on Motor </li>
		</ul>
		
		
        <div class="g_enquire_nowc"><a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/appliances/chimney/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Know More</a></div>
      </div>
    </div>
  </div>
</div>
<div class="section5 s5_bg">
  <div class="banner_bg4">
    <div class="banner_desktop"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/hobs.png" alt=""></div>
    <div class="banner_mobile"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/banner_bg5_mobile.jpg" alt=""></div>
    <div class="container2">
      <div class="banner5_text">
        <h2>Hobs</h2>
        <p>While our kitchens hobs are designed to ensure convenient cooking, they also enhance the looks of your kitchen. Innovative features such as mechanical timers and automatic ignition guarantee safety and ease of control.</p>
		<h3>Unique features:</h3>
	    <ul class="product_list_home2">
		<li class="mk"><span>Brass burners with metallic knobs</span></li>
		<li>Built-in safety measures</li>
		<li>Flexible use for NG and LPG</li>
		<li>CE Certification</li>
		</ul>
		
		
        <div class="g_enquire_now5"><a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/appliances/hobs/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Know More</a></div>
      </div>
    </div>
  </div>
</div>
<div class="section6 s6_bg">
  <div class="banner_bg6">
    <div class="banner_desktop"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/large-appliances.jpg" alt=""></div>
    <div class="banner_mobile"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/banner_bg6_mobile.jpg" alt=""></div>
    <div class="container2">
      <div class="banner6_text padd_other_ad">
        <h2>OTHER LARGE APPLIANCES</h2>
        <p>Our oven, microwave, and dishwasher ranges are equipped with advanced features that offer energy efficiency, convenience, user-friendliness, luxury and safety.</p>
		
		<!--<h3>Unique features:</h3>
	    <ul class="product_list_home3">
		<li>One-touch door opening and auto-cook programs for oven and microwave</li>
		<li>Unique delay for dishwashers</li>
		</ul>-->

        <div class="g_enquire_nowmo btn_2_desn">
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/appliances/dishwashers/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Dishwashers</a>
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/appliances/bi-oven/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>BI Oven</a>
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/appliances/microwave-oven/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Microwave Oven</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="section7 s7_bg">
  <div class="banner_bg7">
    <div class="banner_desktop"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/small-appliances-v2.png" alt=""></div>
    <div class="banner_mobile"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/banner_bg7_mobile.jpg" alt=""></div>
    <div class="container2">
      <div class="banner7_text bnr_7_txt">
        <h2>SMALL APPLIANCES</h2>
        <p>Our small appliances make time spent in the kitchen a whole lot more enjoyable. They are aesthetically designed and boast of cutting-edge features to offer customers utmost convenience.</p>
        <div class="g_enquire_now7 sm_ap_btn">
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/small-appliances/electric-kettle/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Electric Kettle</a>
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/small-appliances/hand-blender/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Hand Blender</a>
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/small-appliances/induction-cooker/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Induction Cooker</a>
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/small-appliances/juicer/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Juicer</a>
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/small-appliances/mixer-grinder/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Mixer Grinder</a>
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/small-appliances/otg/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>OTG</a>
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/small-appliances/sandwich-maker/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Sandwich Maker</a>
          <a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/small-appliances/toaster/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Toaster</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="section8 s8_bg">
  <div class="banner_bg8">
    <div class="banner_desktop"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/modular-kitchen.jpg" alt=""></div>
   <div class="banner_mobile"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/modular-kitchen_mobile.jpg" alt=""></div>
    <div class="container2">
      <div class="banner8_text">
        <h2>Modular Kitchens</h2>
        <p>Our modular kitchens help you make the best use of kitchen space, while adding a touch of modernity and convenience to Indian or hybrid kitchen spaces.</p>
		
				<h3>Unique features:</h3>
	    <ul class="product_list_home">
		<li class="fec">Free Expert Consultation </li>
		<li>Affordable price range</li>
		<li>German Technology </li>
		</ul>
		
		
        <div class="g_enquire_nowmc"><a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/modular-kitchens/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Know More</a></div>
      </div>
    </div>
  </div>
</div>
<div class="section9 s9_bg">
  <div class="banner_bg9">
    <div class="banner_desktop"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/water-healthifier-v2.jpg" alt=""></div>
    <div class="banner_mobile"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/banner_bg8_mobile.jpg" alt=""></div>
    <div class="container2">
      <div class="banner9_text">
        <h2>WATER HEALTHIFIER </h2>
        <p>Our water healthifiers are equipped with a 10-step filtration technology to get rid of all kinds of bacteria and harmful chemicals. Pre-carbon candle in filters removes bad taste and neutralises bad smell and colour.</p>
		
						<h3>Unique features:</h3>
	    <ul class="product_list_home2">
		<li>10-Step Filtration Technology</li>
		<li>Alkaline Water</li>
		<li class="at"><span>Antioxidant Technology</span></li>
		<li>Mineral Enriched</li>
		</ul>
		
		
        <div class="g_enquire_now9"><a href="<?php bloginfo ( 'wpurl' ); ?>/product-category/water-purifiers/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Know More</a></div>
      </div>
    </div>
  </div>
</div>
<div class="section10 s10_bg">
  <div class="container">


    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="support_and_services">
          <div class="gimg_box2">
          		<img src="<?php bloginfo ( 'template_url' ); ?>/images_new/support_services.jpg" alt="">
          </div>
          <div class="g_text_box2">
            <h2>Support & Service</h2>
            <h3>Customer Support</h3>
            <p>We are glad to be of help to you. In case of any queries or complaints please reach out to us via WhatsApp or Call and we will take care of it the earliest.</p>
          
         <!--  <div class="support_btn">
            <a href="<?php bloginfo ( 'wpurl' ); ?>/our-presence/"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Service & Support</a></div> -->
            <div class="srvs_call_and_whats_app">
              <a href="https://wa.me/7596071320" class="wht_btn" target="_blank"><span class="whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></span>7596071320</a>
              <a href="tel:03340500300" class="call_wht_btn" target="_blank"><span class="call"><i class="fa fa-phone" aria-hidden="true"></i></span>033-40500300</a>

            </div>
        </div>
      </div>
      </div>
      
      <?php            
        $args = array('post_type' => 'post', 'posts_per_page' =>1, 'orderby' => 'date', 'order' => 'DESC');
        $loop = new WP_Query( $args );            
        while ( $loop->have_posts() ) : $loop->the_post();
        $blog_img_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
      ?>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="support_and_services rd_blog">
          <div class="gimg_box2">
          	<img src="<?php echo $blog_img_url; ?>" alt="">
          </div>
          <div class="g_text_box3">
            <h2><?php the_title(); ?></h2>
            <ul class="new_features">
              <!-- <li><a href="#"><span><i class="fa fa-user-o" aria-hidden="true"></i></span><?php the_author(); ?></a></li> -->
              <li><a href="#"><span><i class="fa fa-calendar" aria-hidden="true"></i></span><?php the_time('F jS, Y'); ?></a></li>
              <!-- <li><a href="#"><span><i class="fa fa-comments-o" aria-hidden="true"></i></span>Comments </a></li> -->
            </ul>
            <p><?php the_excerpt(); ?></p>
          
          <div class="new_features_btn"><a href="https://www.kutchina.com/blog"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Read Our Blog</a></div></div>
        </div>
      </div>
      <?php endwhile; ?>


    </div>
  </div>
</div>




<script>
jQuery(document).ready(function() {
 
var owl = jQuery("#owl-demo");
 
  owl.owlCarousel({
      items : 1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : true, // itemsMobile disabled - inherit from itemsTablet option
      autoPlay: 7000
  });
 
  // Custom Navigation Events
  jQuery(".next").click(function(){
    owl.trigger('owl.next');
  })
  jQuery(".prev").click(function(){
    owl.trigger('owl.prev');
  })

 
});
</script>












<?php betashop_get_footer(); ?>