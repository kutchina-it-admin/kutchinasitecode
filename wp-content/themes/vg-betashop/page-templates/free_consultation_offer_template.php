<?php
/**
 * Template Name: Free Consultation Offer Page Template
 *
 * Description: Free Consultation Offer Page Template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>

<link rel='stylesheet' href='https://www.kutchina.com/wp-content/themes/vg-betashop/css/khome_free_cons.css' type='text/css' media='all' />


<div class="main-container front-page about-page" style="display:none;">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<!--<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php //the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div>-->
	
</div>

<div class="inner_banner_section">
  <div class="product_banner"><img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/free_consultation/main_banner.png" alt="" style="width:100%;"></div>
  <div class="product_banner_text">
    <div class="container">
      <div class="row">
      	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
        	<div class="banner_text_bg">
                <h2>The right time to register for</h2>
                <h3>The right chimney and modular kitchen!</h3>
                <p>Get FREE consultation after lock-down ends!</p>
            </div>
        </div>
        
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
        	<div class="pre_frm_container_512">
                <form name="ban_frm" id="ban_frm" action="" method="POST">
                  <div class="form-group">
                    <input type="text" class="form-control" id="nme" name="nme" placeholder="Name" >
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" id="eml" name="eml" placeholder="Email address">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="phn" name="phn" placeholder="phone">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="pin" name="pin" placeholder="Pincode">
                  </div>
                  <div class="form-group">
                    <select class="form-control" id="select_ite" name="select_ite" disabled="disabled">
                    	<option value="">Select One</option>
                    	<option value="Modular Kitchen">Modular Kitchen</option>
                    	<option value="Kitchen Chimney">Kitchen Chimney</option>
                    </select>
                  </div>
                  <input type="hidden" name="utm_src" id="utm_src" value="<?php echo $_GET['utm_source']; ?>">
                  <input type="hidden" name="utm_med" id="utm_med" value="<?php echo $_GET['utm_medium']; ?>">
                  <input type="hidden" name="utm_camp" id="utm_camp" value="<?php echo $_GET['utm_campaign']; ?>">
                  <input type="hidden" name="utm_refer" id="utm_refer" value="<?php echo $_SERVER['HTTP_REFERER']; ?>">
                  <input type="hidden" name="ip_add" id="ip_add" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
                  
                  <button type="button" id="ban_sbmt" class="btn btn-default" onclick="checkValidation();">Submit</button>
                </form>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="section33">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="right_panel_text">
          <h2>Unlock a unique offer!</h2>
          
          <div class="pre_row_for_bg">
              <div class="row row_cust_togrt">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="inner_togrt_612">
                        <p>A unique offer would be unlocked for you once the lock-down period is over! </p>
                        <p>If you’re thinking about buying a kitchen chimney, or a modular kitchen, register NOW. </p>
                        <p>You’ll get extensive consultation on which product you should choose, and why, for absolutely FREE!</p>
                    </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="inner_togrt_612">
                      <h4>To get this offer:</h4>
                      <ul>
                        <li><img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/free_consultation/pointer_bullett.png" alt=""> Register your details through the form above</li>
                        <li><img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/free_consultation/pointer_bullett.png" alt="">Wait till the lock-down ends</li>
                        <li><img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/free_consultation/pointer_bullett.png" alt="">Welcome our experts to you home and get free consultation</li>
                        <li><img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/free_consultation/pointer_bullett.png" alt="">Get the right advice on the right kitchen chimney or modular kitchen</li>
                      </ul>
                    </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="section34 s3_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/free_consultation/mkkc_1.png" alt="" class="hero_image_main">
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="product_categ_list_box">
            <div class="product_text_box">
                <h2>Modular <br/>Kitchen </h2>
            </div>
            
            <div class="product_text_desc">
                <h4>Kutchina modular kitchens are No. 1 in the market, since they:</h4>
                <ul class="mb_ten_ul">
                    <li>Make the most efficient use of your kitchen space</li>
                    <li>Are highly customisable to suit your style and taste</li>
                  </ul>
                  
                  <h4>Offer includes:</h4>
                <ul>
                    <li>FREE expert assessment of kitchen shape and size</li>
                    <li>Learn which setup fits your kitchen shape, for FREE</li>
                    <li>Learn how to customise a setup from the catalogue to complement your aesthetics, for FREE</li>
                  </ul>
                <a href="#">Read more</a>
            </div>
        </div>
      </div>      
    </div>
    <div class="row row_scd">
    	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="product_categ_list_box">
            <div class="product_text_box">
                <h2>Kitchen <br/>Chimney</h2>
            </div>
            
            <div class="product_text_desc">
                <h4>Forget about grease, smoke or grime with Kutchina kitchen chimneys.</h4>
                <ul class="mb_ten_ul">
                    <li>High output electric motors</li>
                    <li>All cooking fumes are drawn out </li>
                  </ul>
                  
                  <h4>Offer includes:</h4>
                <ul>
                    <li>Get FREE expert consultation for the best chimney for your kitchen</li>
                    <li>Chosen according to size, space, shape and built of kitchen, for FREE</li>
                    <li>FREE assessment based on your cooking habits</li>
                  </ul>
                <a href="#">Read more</a>
            </div>
        </div>
      </div>
      
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/free_consultation/mkkc_2.png" alt="" class="hero_image_main">
      </div>
    </div>
  </div>
</div>
<div class="footer_section1">
  <div class="container">
    <div class="row get_touch">
      <h2>Connect with us</h2>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 quick_enquiry fbox">
        <h2>Kutchina Home Makers Private Limited.</h2>
        <p>20 Chinar Park, Bajoria Towers <br/>Club Town Enclave <br/>Chinar Park, Rajarhat <br/>Kolkata – 700157</p>
        <p class="footer_pre_soc"><a href=""><img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/free_consultation/phone_ico.png" alt="">1800 419 7333</a></p>
        <p class="footer_pre_soc"><a href=""><img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/free_consultation/email_ico.png" alt="">enquiry@kutchina.com</a></p>
      </div>
      
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 quick_enquiry fbox">
        <form name="foo_frm" id="foo_frm" action="" method="POST">
        	<div class="row">
            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <input type="text" class="form-control" id="nme1" name="nme" placeholder="Name" >
          </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <input type="email" class="form-control" id="eml1" name="eml" placeholder="Email address">
          </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <input type="text" class="form-control" id="phn1" name="phn" placeholder="phone">
          </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            <input type="text" class="form-control" id="pin1" name="pin" placeholder="Pincode">
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <select class="form-control" id="select_ite1" name="select_ite" disabled="disabled">
                <option value="">Select One</option>
                <option value="Modular Kitchen">Modular Kitchen</option>
                <option value="Kitchen Chimney">Kitchen Chimney</option>
            </select>
          </div>
          <input type="hidden" name="utm_src" id="utm_src1" value="<?php echo $_GET['utm_source']; ?>">
          <input type="hidden" name="utm_med" id="utm_med1" value="<?php echo $_GET['utm_medium']; ?>">
          <input type="hidden" name="utm_camp" id="utm_camp1" value="<?php echo $_GET['utm_campaign']; ?>">
          <input type="hidden" name="utm_refer" id="utm_refer1" value="<?php echo $_SERVER['HTTP_REFERER']; ?>">
          <input type="hidden" name="ip_add" id="ip_add1" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <button type="button" id="foo_sbmt" class="btn btn-default pre_footer_frm" onclick="checkValidationfoo();">Submit</button>
          </div>
          </div>
        </form>
      </div>
      
      
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  var $location = jQuery('#select_ite');
  $('#pin').on('change', function(){
    $location.find("option[value='Modular Kitchen']").hide();
    $location.find("option[value='Kitchen Chimney']").hide();
    $.ajax({
        url: "https://www.kutchina.com/lead/",
        data: { "pin": $("#pin").val() },
        dataType:"html",
        type: "post",
        crossDomain : true,
        success: function(data){

          // alert(data);
        
          if (data == "Both") { //check for condition
            $("#select_ite").prop('disabled', false);
            $('#select_ite').prop('selectedIndex',0);
            $location.find("option[value='Modular Kitchen']").show();
            $location.find("option[value='Kitchen Chimney']").show(); //remove unwanted option
          }
          else if (data == "Single") { //check for condition
            $("#select_ite").prop('disabled', false);
            $('#select_ite').prop('selectedIndex',0);
            $location.find("option[value='Kitchen Chimney']").show();
            $location.find("option[value='Modular Kitchen']").hide(); //remove unwanted option
          }
          else{
            $("#select_ite").prop('disabled', true);
            $('#select_ite').prop('selectedIndex',0);
            $location.find("option[value='Modular Kitchen']").hide();
            $location.find("option[value='Kitchen Chimney']").hide();
          }
        }
    });
  });
  var $locationfoo = jQuery('#select_ite1');
  $('#pin1').on('change', function(){
    $locationfoo.find("option[value='Modular Kitchen']").hide();
    $locationfoo.find("option[value='Kitchen Chimney']").hide();
    $.ajax({
        url: "https://www.kutchina.com/lead/",
        data: { "pin": $("#pin1").val() },
        dataType:"html",
        type: "post",
        crossDomain : true,
        success: function(data){

          // alert(data);
        
          if (data == "Both") { //check for condition
            $("#select_ite1").prop('disabled', false);
            $('#select_ite1').prop('selectedIndex',0);
            $locationfoo.find("option[value='Modular Kitchen']").show();
            $locationfoo.find("option[value='Kitchen Chimney']").show(); //remove unwanted option
          }
          else if (data == "Single") { //check for condition
            $("#select_ite1").prop('disabled', false);
            $('#select_ite1').prop('selectedIndex',0);
            $locationfoo.find("option[value='Kitchen Chimney']").show();
            $locationfoo.find("option[value='Modular Kitchen']").hide(); //remove unwanted option
          }
          else{
            $("#select_ite1").prop('disabled', true);
            $('#select_ite1').prop('selectedIndex',0);
            $locationfoo.find("option[value='Modular Kitchen']").hide();
            $locationfoo.find("option[value='Kitchen Chimney']").hide();
          }
        }
    });
  });
});



    // jQuery(function(){
    //   var $location = jQuery('#select_ite');
    //   // $location.find("option[value='Modular Kitchen']").hide();
    //   // $location.find("option[value='Kitchen Chimney']").hide();

    //   $location.data('options', $location.find('option')); //cache the options first up when DOM loads
    //   jQuery('#pin').on('change', function () { //on change event
    //       $location.html($location.data('options')); //populate the options


    //       if ($(this).val() == "123456" || $(this).val() == "654321") { //check for condition
    //           $location.find("option[value='Kitchen Chimney']").remove(); //remove unwanted option
    //       }
    //       else if ($(this).val() == "123465" || $(this).val() == "564321") { //check for condition
    //           $location.find("option[value='Modular Kitchen']").remove(); //remove unwanted option
    //       }
    //       else{
    //         $location.find("option[value='Modular Kitchen']").remove();
    //         $location.find("option[value='Kitchen Chimney']").remove();
    //       }

    //   });

    //   var $locationfoo = jQuery('#select_ite1');
    //   // $locationfoo.find("option[value='Modular Kitchen']").hide();
    //   // $locationfoo.find("option[value='Kitchen Chimney']").hide();

    //   $locationfoo.data('options', $locationfoo.find('option')); //cache the options first up when DOM loads
    //   jQuery('#pin1').on('change', function () { //on change event
    //       $locationfoo.html($locationfoo.data('options')); //populate the options
    //       if ($(this).val() == "123456" || $(this).val() == "654321") { //check for condition
    //           $locationfoo.find("option[value='Kitchen Chimney']").remove(); //remove unwanted option
    //       }
    //       else if ($(this).val() == "123465" || $(this).val() == "564321") { //check for condition
    //           $locationfoo.find("option[value='Modular Kitchen']").remove(); //remove unwanted option
    //       }
    //       else{
    //         $locationfoo.find("option[value='Modular Kitchen']").remove();
    //         $locationfoo.find("option[value='Kitchen Chimney']").remove();
    //       }

    //   });
    // });



    function checkValidation()
    {
          //alert("a");
          var name = jQuery("#nme").val();
          var email = jQuery("#eml").val();
          var phone = jQuery("#phn").val();
          var pincd = jQuery("#pin").val();    
          var select_ite = jQuery("#select_ite").val();    
          var utm_src = jQuery("#utm_src").val();          
          var utm_med = jQuery("#utm_med").val();          
          var utm_camp = jQuery("#utm_camp").val();          
          var utm_refer = jQuery("#utm_refer").val();          
          var ip_add = jQuery("#ip_add").val();          
         
          var email_pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
          var phone_pattern = /^[\d\s+-]+$/;
          var zipRegex = /^[0-9]+$/;
        
         if(name == '' )
          {          
            alert("Enter your name");
            jQuery("#nme").focus();
            return false;              
          }
          else if(email == '')
          {
            //alert("aa");
            alert("Enter your email id");
            jQuery("#eml").focus();
            return false;              
          }
          else if(!email_pattern.test(email))
          {
            alert("Invalid email id");
            jQuery("#eml").focus();
            return false;          
          }
          else if(phone == '')
          {
            //alert("aa");
            alert("Enter your 10 digit phone number");
            jQuery("#phn").focus();
            return false;              
          }
          else if(!phone_pattern.test(phone))
          {
            alert("Invalid phone number");
            jQuery("#phn").focus();
            return false;          
          }
          else if(phone.length < 10 || phone.length > 10 )
          {   
            alert("Invalid phone number");
            jQuery("#phn").focus();
            return false;          
          }
      else if(pincd == '' )
          {         
            alert("Enter your pin code");
            jQuery("#pin").focus();
            return false;
          }
          else if ((pincd.length)< 6 || (pincd.length)> 6 )
          {
          alert("Invalid Pin Code. Please check again & add correct Pin Code");
        jQuery("#pin").focus();
        return false;
        }
        else if (!zipRegex.test(pincd))
        {
          alert("Pin Code should be numbers only");
          jQuery("#pin").focus();
        return false;
        }
          else if(select_ite == '')
          {
            //alert("aa");
            alert("Select your item");
            jQuery("#select_ite").focus();
            return false;              
          }
          else
          {
            jQuery("#ban_sbmt").attr("disabled", true);
            $.ajax({
                url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLSdFBs_ASBG1PHs7iU4_WTW601Jad7lhwEsiMvGyLWdiaFs0mg/formResponse",
                 data: {
                  "entry.632585193": name,
                  "entry.1068330097": email,
                  "entry.687495901": phone,
                  "entry.1297119613": pincd,
                  "entry.29626405": select_ite,
                  "entry.2044408704": utm_src,
                  "entry.1974105577": utm_med,
                  "entry.1061684578": utm_camp,
                  "entry.1903640747": utm_refer,
                  "entry.925368794": ip_add,
                },
                type: "POST",
                crossDomain: true,
                dataType: "json",
                statusCode: {
                  0: function() {
                       console.log('Failure');
                       window.location.href='/thank-you-for-free-consultation/';
                  },
                  200: function() {
                       console.log('Success');
                       window.location.href='/thank-you-for-free-consultation/';
                  }
                },
                 // success: success
             });
              
          }
    }

    function checkValidationfoo()
    {
          //alert("a");
          var name_foo = jQuery("#nme1").val();
          var email_foo = jQuery("#eml1").val();
          var phone_foo = jQuery("#phn1").val();
          var pincd_foo = jQuery("#pin1").val();    
          var select_ite_foo = jQuery("#select_ite1").val();    
          var utm_src_foo = jQuery("#utm_src1").val();          
          var utm_med_foo = jQuery("#utm_med1").val();          
          var utm_camp_foo = jQuery("#utm_camp1").val();          
          var utm_refer_foo = jQuery("#utm_refer1").val();          
          var ip_add_foo = jQuery("#ip_add1").val();          
         
          var email_pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
          var phone_pattern = /^[\d\s+-]+$/;
          var zipRegex = /^[0-9]+$/;
        
         if(name_foo == '' )
          {          
            alert("Enter your name");
            jQuery("#nme1").focus();
            return false;              
          }
          else if(email_foo == '')
          {
            //alert("aa");
            alert("Enter your email id");
            jQuery("#eml1").focus();
            return false;              
          }
          else if(!email_pattern.test(email_foo))
          {
            alert("Invalid email id");
            jQuery("#eml1").focus();
            return false;          
          }
          else if(phone_foo == '')
          {
            //alert("aa");
            alert("Enter your 10 digit phone number");
            jQuery("#phn1").focus();
            return false;              
          }
          else if(!phone_pattern.test(phone_foo))
          {
            alert("Invalid phone number");
            jQuery("#phn1").focus();
            return false;          
          }
          else if(phone_foo.length < 10 || phone_foo.length > 10 )
          {   
            alert("Invalid phone number");
            jQuery("#phn1").focus();
            return false;          
          }
      else if(pincd_foo == '' )
          {         
            alert("Enter your pin code");
            jQuery("#pin1").focus();
            return false;
          }
          else if ((pincd_foo.length)< 6 || (pincd_foo.length)> 6 )
          {
          alert("Invalid Pin Code. Please check again & add correct Pin Code");
        jQuery("#pin1").focus();
        return false;
        }
        else if (!zipRegex.test(pincd_foo))
        {
          alert("Pin Code should be numbers only");
          jQuery("#pin1").focus();
        return false;
        }
          else if(select_ite_foo == '')
          {
            //alert("aa");
            alert("Select your item");
            jQuery("#select_ite1").focus();
            return false;              
          }
          else
          {
            jQuery("#foo_sbmt").attr("disabled", true);
            $.ajax({
                url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLSdFBs_ASBG1PHs7iU4_WTW601Jad7lhwEsiMvGyLWdiaFs0mg/formResponse",
                 data: {
                  "entry.632585193": name_foo,
                  "entry.1068330097": email_foo,
                  "entry.687495901": phone_foo,
                  "entry.1297119613": pincd_foo,
                  "entry.29626405": select_ite_foo,
                  "entry.2044408704": utm_src_foo,
                  "entry.1974105577": utm_med_foo,
                  "entry.1061684578": utm_camp_foo,
                  "entry.1903640747": utm_refer_foo,
                  "entry.925368794": ip_add_foo,
                },
                type: "POST",
                crossDomain: true,
                dataType: "json",
                statusCode: {
                  0: function() {
                       console.log('Failure');
                       window.location.href='/thank-you-for-free-consultation/';
                  },
                  200: function() {
                       console.log('Success');
                       window.location.href='/thank-you-for-free-consultation/';
                  }
                },
                 // success: success
             });
              
          }
    }
</script>


<?php betashop_get_footer(); ?>
