<?php

  global $wpdb;
  
  if(isset($_GET['exclusiveState']) || isset($_GET['exclusiveCity']))
  {
    $exclusiveState = $_GET['exclusiveState'];
    //echo $exclusiveState; exit;
    $single_state_str = "SELECT * FROM `kut_state` WHERE LOWER(`state_name`) = '".strtolower($exclusiveState)."' "; 
    $single_state_posts = $wpdb->get_row($single_state_str, OBJECT);
    //print_r($single_state_posts); exit;
    $state_id = $single_state_posts->id;

    $citystr = "SELECT * FROM `kut_city` WHERE `state_id`= ".$state_id; 
    $city_posts = $wpdb->get_results($citystr, OBJECT);

    $exclusiveCity = $_GET['exclusiveCity'];
  }
  else{

     $city_str = "SELECT * FROM `kut_city` WHERE 1 ";
     $city_posts = $wpdb->get_results($citystr, OBJECT);                                 
  }

  
  

   if($exclusiveState != '' && $exclusiveCity == '' )
   {
      $search_querystr = "SELECT * FROM `kut_exclusive` WHERE LOWER(`state`) = '".strtolower($exclusiveState)."' "; 
   }
   else if($exclusiveState == '' && $exclusiveCity != '')
   {
      $search_querystr = "SELECT * FROM `kut_exclusive` WHERE LOWER(`city`) = '".strtolower($exclusiveCity)."' "; 
   }
   else if($exclusiveState != '' && $exclusiveCity != '')
   {
      $search_querystr = "SELECT * FROM `kut_exclusive` WHERE LOWER(`city`) = '".strtolower($exclusiveCity)."' AND  LOWER(`state`) = '".strtolower($exclusiveState)."' "; 
   }
   else
   {
       $search_querystr = "SELECT * FROM `kut_exclusive` WHERE 1";
   }


  $support_posts = $wpdb->get_results($search_querystr, OBJECT);

  //print_r($support_posts); exit;
?>
<div class="main-container page-faq full-width page-title">
    

      <div class="container">
              <div class="row">
                      <div class="page-content">
                      <div class="col-md-4 col-sm-4 col-xs-12">
                               <div>
                                  <select id="exclusiveState" name="exclusiveState" class="form-control" onchange="exclusiveCityFromState();">
                                    <option value="">Select State</option>
                                    <?php
                                       $state_str = "SELECT * FROM `kut_state` WHERE 1 ";
                                       $state_posts = $wpdb->get_results($state_str, OBJECT);

                                       for($i=0;$i<sizeof($state_posts);$i++)
                                       {
                                    ?>

                                    <option value="<?php echo $state_posts[$i]->state_name ?>" <?php if( strtolower($exclusiveState) == strtolower($state_posts[$i]->state_name) ) echo "selected"; ?>><?php echo $state_posts[$i]->state_name ?></option>

                                    <?php } ?>
                                  </select>             
                              </div>


                               <div>
                                  <select id="exclusiveCity" name="exclusiveCity" class="form-control" onchange="exclusiveCityFromState();">
                                    <option value="">Select City</option>
                                    <?php
                                       

                                       for($i=0;$i<sizeof($city_posts);$i++)
                                       {
                                    ?>

                                    <option value="<?php echo $city_posts[$i]->city_name ?>" <?php if( strtolower($exclusiveCity) == strtolower($city_posts[$i]->city_name) ) echo "selected"; ?>><?php echo $city_posts[$i]->city_name ?></option>

                                    <?php } ?>
                                  </select>             
                              </div>



<!-- /////////////////////////////////////////// Map Functionality START ////////////////////////////////////////////// -->

<?php echo do_shortcode("[put_wpgm id=4]"); ?>

<!-- /////////////////////////////////////////// Map Functionality END ////////////////////////////////////////////// -->



                        </div>
                      <div class="col-md-8 col-sm-8 col-xs-12">

                      <?php
                        for($i=0;$i<sizeof($support_posts);$i++)
                        {
                      ?>
                            <ul class="address">
                             <li><strong>State :</strong><?php echo $support_posts[$i]->state ?></li>
                             <li><strong>City  :</strong><?php echo $support_posts[$i]->city ?></li>                             
                             <li><strong>Contact Number  :</strong><?php echo $support_posts[$i]->contact_no ?></li>
                             <li><strong>Email  :</strong><?php echo $support_posts[$i]->mail_id_incharges ?></li>
                             <li><strong>Address :</strong><?php echo $support_posts[$i]->address ?></li>
                            </ul>

                      <?php } ?>
                          </div>
                    </div>
            </div>
    </div>
</div>

<script type="text/javascript">
  function exclusiveCityFromState()
  {
      var exclusiveState = jQuery("#exclusiveState").val();
      var exclusiveCity = jQuery("#exclusiveCity").val();
      

       window.location.href = "<?php echo site_url();?>/our-presence/?exclusiveState="+exclusiveState+"&exclusiveCity="+exclusiveCity+'#menu2';
  }
</script>
