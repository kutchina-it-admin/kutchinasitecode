<?php
/**
 * Template Name: Offer Listing Template
 *
 * Description: Request A Callback page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<style>
.prodd {
	width: 30%;
	float: left;
}
.priceg span{ width:100%; text-align:center; float:left; margin:-38px 0 0 0;}
.add_to_cart_button{ opacity:0;}
.main-container .page-content .button:hover {

    outline: 0;
    opacity: 0;

}
.titll {
	font-size: 23px;
	margin: 0 0 20px 0;
	border-right: 0;
	text-align: center;
	padding: 0 0 0 20px;
	transition: all 0.5s ease; height:130px;
}
.cartbut {
	border: none;
	margin-bottom: 0;
	background: #fff;
	margin: 0 8px 6px 0;
	width:32%;
	float:left;
}
table td {
	padding:0;
}
.vgwc-product-price {
	margin: 0;
}
.nomar {
	margin: 0;
}
.bbhu {
	padding: 8px 16px;
	margin: 25px 0 0 0;
	float: left;
}
.formyu {
	max-width:660px;
	float: none;
	margin: 20px auto 40px auto;
	text-align:center;
	background: #ddd;
	padding:30px!important;
}
.ttl {
	font-size: 18px;
	font-weight: bold;
	color: #000;
	margin: 0 0 20px 0;
}
.sev {
	width:100%!important;
}
.sev input {
	width: 100%!important;
}
.formyu label {
	float: left;
	padding: 0 0 0 0!important;
	margin: 0!important;
	font-size:16px;
	color: #000;
	font-weight: bold!important;
}
.clear {
	clear: both;
}
.till {
	background:#f2f2f2;
	color:#666;
	font-weight:bold;
	font-size:15px;
	font-family:Arial, Helvetica, sans-serif;
	padding:15px 0;
	transition: all 0.5s ease;
}
.cartbut:hover .titll{	color:#333;
	background:#fff;}
.cartbut:hover td {
	color:#fff;
	background:#ed2443;
}
.cartbut:hover {
	 cursor:pointer; box-shadow:0 3px 3px #999;
}
.cartbut:hover .till {
	background:#ccc; color:#000;
}
.cartbut:hover .cho_pri {
	background:#8d0218;
}
.cho_pri {
	transition: all 0.5s ease;
}
.disccountt{
	color: #fff;
}
.mdsec{
	cursor: pointer;
}
 @media (max-width: 480px) {
	 .titll {
	font-size: 18px;
	}
	 .cartbut tr td {
	width: 100%;
	float: left;
	margin: 0 0 0 0;
	}
	.new_pbox{
		float: left !important;
		width: 100% !important;	
	}
}
@media (max-width: 767px) {
	.titll {
		padding: 9px 0 4px 20px;
		border: none;
	}
	.cartbut { width:100%; margin:0 0 20px 0;}
	.new_pbox{
		float: left !important;
		width: 100% !important;	
	}
}

/********** new_changes_15_10_2019 **********/

.newPbox_content{
	width: 100%;
	float: left;
}

.new_pbox {
    width: 98% !important;
    background: #fff !important;
    border: 0 !important;
    min-height: auto !important;
    border-radius: 0 !important;
    margin: 19px 0 19px 1% !important;
    height: auto;
    box-shadow: 1px 0px 7px rgba(0, 0, 0, 0.3) !important;
    padding: 13px 13px;
}

.heading_new_pbox{
	width: 100%;
    float: left;
    background: transparent !important;
    color: #000 !important;
    padding: 0 !important;
    border-left: none !important;
    border-bottom: none !important;
    moz-border-radius: 10px 0px 10px 0;
    -webkit-border-radius: 10px 0px 10px 0;
    border-radius: 0;
    margin: 10px 0 !important;
    font-size: 20px !important;
    font-weight: 500 !important;
}

.newPbox_content .amt{
	font-size: 15px;
    color: #696969;
    text-align: left;
    margin: 0 0 0 0;
    line-height: inherit;
    font-weight: 500;
}

.pricegBtn{
    font-size: 17px !important;
    color: #000 !important;
    text-shadow: none !important;
    padding: 0 !important;
}

.pricegBtn a {
    padding: 12px 23px !important;
    border: 1px solid #000;
    border-radius: 30px;
    margin: 24px 0 23px 0;
    float: left;
    color: #000;
}

.new_pbox_Image{
	padding: 0;
}

.new_pbox_Image span {
    overflow: hidden;
    width: 100%;
    float: left;
}

.new_pbox_Image span img{
	width:100%;
	transition:0.3s ease-in-out;
}

.new_pbox:hover span img{
	transform: scale(1.2);
}

.pricegBtn a:hover{	
    border: 1px solid #ed2443;
    color: #fff;
    background: #ed2443;
}


</style>

<script type="text/javascript">
	function keyPressed(e)
	{
	     var key;      
	     if(window.event)
	          key = window.event.keyCode; //IE
	     else
	          key = e.which; //firefox      

	     return (key != 13);
	}
</script>

<div class="main-container front-page about-page">
  <div class="row-breadcrumb">
    <div class="container">
      <?php betashop_breadcrumb(); ?>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <header class="entry-header">
          <h1 class="entry-title">
            <?php the_title(); ?>
          </h1>
        </header>
      </div>
    </div>
  </div>
  <div class="full-wrapper about-us-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="page-content">
            <?php while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
              <div class="entry-content">
                <?php the_content(); ?>
              </div>
              <!-- .entry-content -->
            </article>
            <!-- #post -->
            <?php endwhile; // end of the loop. ?>
            <div class="">
              <?php

	//print_r($_SERVER);


		if ($_SERVER["REQUEST_METHOD"] == "POST"){
			$user_pincode = $_POST['user_pincode'];
			$chng_userpin_id_hid = substr($user_pincode, 0, 2);
		}
	?>



	
<!-- For West Bengal Only -->
<?php if ( $chng_userpin_id_hid == '70' || $chng_userpin_id_hid == '71' || $chng_userpin_id_hid == '72' || $chng_userpin_id_hid == '73' || $chng_userpin_id_hid == '74' ) { ?>

	<?php
		$sqlwb = "SELECT DISTINCT post_id FROM `kut_postmeta` WHERE meta_key = 'region_state' AND meta_value = 'WB & North East' ";
		$resultbywb = $wpdb->get_results( $sqlwb );
		foreach ($resultbywb as $reswb) {
			$coupID = $reswb->post_id;			

			$sqlwbcoup = "SELECT * FROM `kut_posts` WHERE ID = '". $coupID ."' AND post_status = 'publish' ";
			$resultbywbcoup = $wpdb->get_results( $sqlwbcoup );
			foreach ($resultbywbcoup as $reswbcoup) {
				$coupon_name = $reswbcoup->post_title;
				$coupon_desc = $reswbcoup->post_excerpt;
	?>

			<div class="pbox new_pbox">				
            	<div class="col-xs-12 col-sm-12 col-md-4 new_pbox_Image">
                	<span style="text-align: center;">
                		<?php
	                    	$coupon_image = get_field('coupon_image', $coupID);
	                    	if( !empty($coupon_image) ):
	                  	?>
                    		<img style="width: auto; height: 200px;" src="<?php echo $coupon_image['url']; ?>" alt="" style="display: inline-block;">
                    	<?php endif; ?>
                    </span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8 new_pbox_Image">
                	<div class="newPbox_content">
                    	<div class="col-xs-12 col-sm-12 col-md-8">
                    		<div class="pttl heading_new_pbox"><?php echo $coupon_name; ?></div>				
							<h6 class="amt"><?php echo $coupon_desc; ?></h6>				
						</div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                        	<span class="priceg pricegBtn"><a href="tel:<?php the_field('call_number', $coupID); ?>">Call Now</a></span>				
						</div>
                    </div>
                </div>			
            </div>

		<?php
			}
		}
		?>



<!-- For North East Only -->
<?php } elseif ( $chng_userpin_id_hid == '79' ) { ?>

	<?php
		$sqlne = "SELECT DISTINCT post_id FROM `kut_postmeta` WHERE meta_key = 'region_state' AND (meta_value = 'WB & North East' OR meta_value = 'North East') ";		
		$resultbyne = $wpdb->get_results( $sqlne );
		foreach ($resultbyne as $resne) {
			$coupID = $resne->post_id;

			$sqlnecoup = "SELECT * FROM `kut_posts` WHERE ID = '". $coupID ."' AND post_status = 'publish' ";
			$resultbynecoup = $wpdb->get_results( $sqlnecoup );
			foreach ($resultbynecoup as $resnecoup) {
				$coupon_name = $resnecoup->post_title;
				$coupon_desc = $resnecoup->post_excerpt;
	?>
	

			<div class="pbox new_pbox">				
			<div class="col-xs-12 col-sm-12 col-md-4 new_pbox_Image">
				<span style="text-align: center;">
					<?php
			        	$coupon_image = get_field('coupon_image', $coupID);
			        	if( !empty($coupon_image) ):
			      	?>
			    		<img style="width: auto; height: 200px;" src="<?php echo $coupon_image['url']; ?>" alt="" style="display: inline-block;">
			    	<?php endif; ?>
			    </span>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 new_pbox_Image">
				<div class="newPbox_content">
			    	<div class="col-xs-12 col-sm-12 col-md-8">
			    		<div class="pttl heading_new_pbox"><?php echo $coupon_name; ?></div>				
						<h6 class="amt"><?php echo $coupon_desc; ?></h6>				
					</div>
			        <div class="col-xs-12 col-sm-12 col-md-4">
			        	<span class="priceg pricegBtn"><a href="tel:<?php the_field('call_number', $coupID); ?>">Call Now</a></span>				
					</div>
			    </div>
			</div>			
			</div>

		<?php
			}
		}
		?>



<!-- For Bihar | Jharkhand | UP Only -->
<?php } elseif ( $chng_userpin_id_hid == '20' || $chng_userpin_id_hid == '21' || $chng_userpin_id_hid == '22' || $chng_userpin_id_hid == '23' || $chng_userpin_id_hid == '24' || $chng_userpin_id_hid == '25' || $chng_userpin_id_hid == '26' || $chng_userpin_id_hid == '27' || $chng_userpin_id_hid == '28' || $chng_userpin_id_hid == '80' || $chng_userpin_id_hid == '81' || $chng_userpin_id_hid == '82' || $chng_userpin_id_hid == '83' || $chng_userpin_id_hid == '84' || $chng_userpin_id_hid == '85' ) { ?>

	<?php
		$sqlbju = "SELECT DISTINCT post_id FROM `kut_postmeta` WHERE meta_key = 'region_state' AND meta_value = 'Bihar & Jharkhand & UP' ";		
		$resultbybju = $wpdb->get_results( $sqlbju );
		foreach ($resultbybju as $resbju) {
			$coupID = $resbju->post_id;

			$sqlbjucoup = "SELECT * FROM `kut_posts` WHERE ID = '". $coupID ."' AND post_status = 'publish' ";
			$resultbybjucoup = $wpdb->get_results( $sqlbjucoup );
			foreach ($resultbybjucoup as $resbjucoup) {
				$coupon_name = $resbjucoup->post_title;
				$coupon_desc = $resbjucoup->post_excerpt;
	?>


			<div class="pbox new_pbox">				
			<div class="col-xs-12 col-sm-12 col-md-4 new_pbox_Image">
				<span style="text-align: center;">
					<?php
			        	$coupon_image = get_field('coupon_image', $coupID);
			        	if( !empty($coupon_image) ):
			      	?>
			    		<img style="width: auto; height: 200px;" src="<?php echo $coupon_image['url']; ?>" alt="" style="display: inline-block;">
			    	<?php endif; ?>
			    </span>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 new_pbox_Image">
				<div class="newPbox_content">
			    	<div class="col-xs-12 col-sm-12 col-md-8">
			    		<div class="pttl heading_new_pbox"><?php echo $coupon_name; ?></div>				
						<h6 class="amt"><?php echo $coupon_desc; ?></h6>				
					</div>
			        <div class="col-xs-12 col-sm-12 col-md-4">
			        	<span class="priceg pricegBtn"><a href="tel:<?php the_field('call_number', $coupID); ?>">Call Now</a></span>				
					</div>
			    </div>
			</div>			
			</div>

		<?php
			}
		}
		?>



<!-- For Odisha Only -->
<?php } elseif ( $chng_userpin_id_hid == '75' || $chng_userpin_id_hid == '76' || $chng_userpin_id_hid == '77' ) { ?>

	<?php
		$sqlodi = "SELECT DISTINCT post_id FROM `kut_postmeta` WHERE meta_key = 'region_state' AND meta_value = 'Odisha' ";		
		$resultbyodi = $wpdb->get_results( $sqlodi );
		foreach ($resultbyodi as $resbodi) {
			$coupID = $resbodi->post_id;

			$sqlodicoup = "SELECT * FROM `kut_posts` WHERE ID = '". $coupID ."' AND post_status = 'publish' ";
			$resultbyodicoup = $wpdb->get_results( $sqlodicoup );
			foreach ($resultbyodicoup as $resodicoup) {
				$coupon_name = $resodicoup->post_title;
				$coupon_desc = $resodicoup->post_excerpt;
	?>


			<div class="pbox new_pbox">				
			<div class="col-xs-12 col-sm-12 col-md-4 new_pbox_Image">
				<span style="text-align: center;">
					<?php
			        	$coupon_image = get_field('coupon_image', $coupID);
			        	if( !empty($coupon_image) ):
			      	?>
			    		<img style="width: auto; height: 200px;" src="<?php echo $coupon_image['url']; ?>" alt="" style="display: inline-block;">
			    	<?php endif; ?>
			    </span>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 new_pbox_Image">
				<div class="newPbox_content">
			    	<div class="col-xs-12 col-sm-12 col-md-8">
			    		<div class="pttl heading_new_pbox"><?php echo $coupon_name; ?></div>				
						<h6 class="amt"><?php echo $coupon_desc; ?></h6>				
					</div>
			        <div class="col-xs-12 col-sm-12 col-md-4">
			        	<span class="priceg pricegBtn"><a href="tel:<?php the_field('call_number', $coupID); ?>">Call Now</a></span>				
					</div>
			    </div>
			</div>			
			</div>

		<?php
			}
		}
		?>


<!-- For Nagpur Only -->
<?php } elseif ( $chng_userpin_id_hid == '44') { ?>

	<?php
		$sqlodi = "SELECT DISTINCT post_id FROM `kut_postmeta` WHERE meta_key = 'region_state' AND meta_value = 'Nagpur' ";		
		$resultbyodi = $wpdb->get_results( $sqlodi );
		foreach ($resultbyodi as $resbodi) {
			$coupID = $resbodi->post_id;

			$sqlodicoup = "SELECT * FROM `kut_posts` WHERE ID = '". $coupID ."' AND post_status = 'publish' ";
			$resultbyodicoup = $wpdb->get_results( $sqlodicoup );
			foreach ($resultbyodicoup as $resodicoup) {
				$coupon_name = $resodicoup->post_title;
				$coupon_desc = $resodicoup->post_excerpt;
	?>


			<div class="pbox new_pbox">				
			<div class="col-xs-12 col-sm-12 col-md-4 new_pbox_Image">
				<span style="text-align: center;">
					<?php
			        	$coupon_image = get_field('coupon_image', $coupID);
			        	if( !empty($coupon_image) ):
			      	?>
			    		<img style="width: auto; height: 200px;" src="<?php echo $coupon_image['url']; ?>" alt="" style="display: inline-block;">
			    	<?php endif; ?>
			    </span>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 new_pbox_Image">
				<div class="newPbox_content">
			    	<div class="col-xs-12 col-sm-12 col-md-8">
			    		<div class="pttl heading_new_pbox"><?php echo $coupon_name; ?></div>				
						<h6 class="amt"><?php echo $coupon_desc; ?></h6>				
					</div>
			        <div class="col-xs-12 col-sm-12 col-md-4">
			        	<span class="priceg pricegBtn"><a href="tel:<?php the_field('call_number', $coupID); ?>">Call Now</a></span>				
					</div>
			    </div>
			</div>			
			</div>

		<?php
			}
		}
		?>

<!-- For Jalgaon Only -->
<?php } elseif ( $chng_userpin_id_hid == '42') { ?>

<?php
	$sqlodi = "SELECT DISTINCT post_id FROM `kut_postmeta` WHERE meta_key = 'region_state' AND meta_value = 'Jalgaon' ";		
	$resultbyodi = $wpdb->get_results( $sqlodi );
	foreach ($resultbyodi as $resbodi) {
		$coupID = $resbodi->post_id;

		$sqlodicoup = "SELECT * FROM `kut_posts` WHERE ID = '". $coupID ."' AND post_status = 'publish' ";
		$resultbyodicoup = $wpdb->get_results( $sqlodicoup );
		foreach ($resultbyodicoup as $resodicoup) {
			$coupon_name = $resodicoup->post_title;
			$coupon_desc = $resodicoup->post_excerpt;
?>


		<div class="pbox new_pbox">				
		<div class="col-xs-12 col-sm-12 col-md-4 new_pbox_Image">
			<span style="text-align: center;">
				<?php
					$coupon_image = get_field('coupon_image', $coupID);
					if( !empty($coupon_image) ):
				  ?>
					<img style="width: auto; height: 200px;" src="<?php echo $coupon_image['url']; ?>" alt="" style="display: inline-block;">
				<?php endif; ?>
			</span>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-8 new_pbox_Image">
			<div class="newPbox_content">
				<div class="col-xs-12 col-sm-12 col-md-8">
					<div class="pttl heading_new_pbox"><?php echo $coupon_name; ?></div>				
					<h6 class="amt"><?php echo $coupon_desc; ?></h6>				
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<span class="priceg pricegBtn"><a href="tel:<?php the_field('call_number', $coupID); ?>">Call Now</a></span>				
				</div>
			</div>
		</div>			
		</div>

	<?php
		}
	}
	?>


<!-- For Akola Only -->
<?php } elseif ( $chng_userpin_id_hid == '44') { ?>

<?php
	$sqlodi = "SELECT DISTINCT post_id FROM `kut_postmeta` WHERE meta_key = 'region_state' AND meta_value = 'Akola' ";		
	$resultbyodi = $wpdb->get_results( $sqlodi );
	foreach ($resultbyodi as $resbodi) {
		$coupID = $resbodi->post_id;

		$sqlodicoup = "SELECT * FROM `kut_posts` WHERE ID = '". $coupID ."' AND post_status = 'publish' ";
		$resultbyodicoup = $wpdb->get_results( $sqlodicoup );
		foreach ($resultbyodicoup as $resodicoup) {
			$coupon_name = $resodicoup->post_title;
			$coupon_desc = $resodicoup->post_excerpt;
?>


		<div class="pbox new_pbox">				
		<div class="col-xs-12 col-sm-12 col-md-4 new_pbox_Image">
			<span style="text-align: center;">
				<?php
					$coupon_image = get_field('coupon_image', $coupID);
					if( !empty($coupon_image) ):
				  ?>
					<img style="width: auto; height: 200px;" src="<?php echo $coupon_image['url']; ?>" alt="" style="display: inline-block;">
				<?php endif; ?>
			</span>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-8 new_pbox_Image">
			<div class="newPbox_content">
				<div class="col-xs-12 col-sm-12 col-md-8">
					<div class="pttl heading_new_pbox"><?php echo $coupon_name; ?></div>				
					<h6 class="amt"><?php echo $coupon_desc; ?></h6>				
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<span class="priceg pricegBtn"><a href="tel:<?php the_field('call_number', $coupID); ?>">Call Now</a></span>				
				</div>
			</div>
		</div>			
		</div>

	<?php
		}
	}
	?>

<!-- For Aurangabad Only -->
<?php } elseif ( $chng_userpin_id_hid == '43') { ?>

<?php
	$sqlodi = "SELECT DISTINCT post_id FROM `kut_postmeta` WHERE meta_key = 'region_state' AND meta_value = 'Aurangabad' ";		
	$resultbyodi = $wpdb->get_results( $sqlodi );
	foreach ($resultbyodi as $resbodi) {
		$coupID = $resbodi->post_id;

		$sqlodicoup = "SELECT * FROM `kut_posts` WHERE ID = '". $coupID ."' AND post_status = 'publish' ";
		$resultbyodicoup = $wpdb->get_results( $sqlodicoup );
		foreach ($resultbyodicoup as $resodicoup) {
			$coupon_name = $resodicoup->post_title;
			$coupon_desc = $resodicoup->post_excerpt;
?>


		<div class="pbox new_pbox">				
		<div class="col-xs-12 col-sm-12 col-md-4 new_pbox_Image">
			<span style="text-align: center;">
				<?php
					$coupon_image = get_field('coupon_image', $coupID);
					if( !empty($coupon_image) ):
				  ?>
					<img style="width: auto; height: 200px;" src="<?php echo $coupon_image['url']; ?>" alt="" style="display: inline-block;">
				<?php endif; ?>
			</span>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-8 new_pbox_Image">
			<div class="newPbox_content">
				<div class="col-xs-12 col-sm-12 col-md-8">
					<div class="pttl heading_new_pbox"><?php echo $coupon_name; ?></div>				
					<h6 class="amt"><?php echo $coupon_desc; ?></h6>				
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<span class="priceg pricegBtn"><a href="tel:<?php the_field('call_number', $coupID); ?>">Call Now</a></span>				
				</div>
			</div>
		</div>			
		</div>

	<?php
		}
	}
	?>

<!-- For Chattisgarh Only -->
<?php } elseif ( $chng_userpin_id_hid == '49' ) { ?>

<?php
	$sqlodi = "SELECT DISTINCT post_id FROM `kut_postmeta` WHERE meta_key = 'region_state' AND meta_value = 'Chattisgarh' ";		
	$resultbyodi = $wpdb->get_results( $sqlodi );
	foreach ($resultbyodi as $resbodi) {
		$coupID = $resbodi->post_id;

		$sqlodicoup = "SELECT * FROM `kut_posts` WHERE ID = '". $coupID ."' AND post_status = 'publish' ";
		$resultbyodicoup = $wpdb->get_results( $sqlodicoup );
		foreach ($resultbyodicoup as $resodicoup) {
			$coupon_name = $resodicoup->post_title;
			$coupon_desc = $resodicoup->post_excerpt;
?>


		<div class="pbox new_pbox">				
		<div class="col-xs-12 col-sm-12 col-md-4 new_pbox_Image">
			<span style="text-align: center;">
				<?php
					$coupon_image = get_field('coupon_image', $coupID);
					if( !empty($coupon_image) ):
				  ?>
					<img style="width: auto; height: 200px;" src="<?php echo $coupon_image['url']; ?>" alt="" style="display: inline-block;">
				<?php endif; ?>
			</span>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-8 new_pbox_Image">
			<div class="newPbox_content">
				<div class="col-xs-12 col-sm-12 col-md-8">
					<div class="pttl heading_new_pbox"><?php echo $coupon_name; ?></div>				
					<h6 class="amt"><?php echo $coupon_desc; ?></h6>				
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<span class="priceg pricegBtn"><a href="tel:<?php the_field('call_number', $coupID); ?>">Call Now</a></span>				
				</div>
			</div>
		</div>			
		</div>

	<?php
		}
	}
	?>


<?php } else { ?>

	<p>No Coupon Offer Found</p>

<?php } ?>



              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<style>
	.formbox1{max-width:625px; background:#007dc6; border:3px solid #fff; box-shadow:0 5px 10px #ccc; margin:100px auto 0 auto; min-height:400px;moz-border-radius:10px; -webkit-border-radius:10px; border-radius:10px; padding:20px; }
.st-icon{ width:96px; height:96px; margin:-70px auto 20px auto;}
.formbox1 h3{ font-size:15px; text-align:center; color:#fff; margin:0 0 0 0; font-weight:normal; line-height:22px;}
.formbox1 h3 span{ width:100%; text-align:center; font-size:30px; font-weight:bold; line-height:40px;}
.linkd{ margin:30px auto; display:block; text-align:center; }
.formbox1 h5{ font-size:16px; font-weight:lighter; color:#a8d6f1; text-align:center; line-height:22px;}
.forminp{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/formbg1.png) no-repeat;width:361px; height:43px; border:none; float:left; padding:0 0 0 65px;box-sizing: border-box; font-size:15px; color:#fff; font-weight:lighter;}
.forminp2{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/formbg2.png) no-repeat;width:361px; height:43px; border:none; float:left; padding:0 0 0 55px;box-sizing: border-box; font-size:15px; color:#fff; font-weight:lighter;}

.frbt{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/subtt.png) no-repeat; width:94px; height:43px; float:left; font-size:20px; color:#fff;  border:none; margin:0 0 0 -2px; cursor:pointer;}
.frbt:hover{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/subtt2.png) no-repeat; width:94px; height:43px; float:left; font-size:20px; color:#fff; border:none; margin:0 0 0 -2px; cursor:pointer;}
.formsec{ max-width:455px; margin:0 auto;}
.shadoe{ margin:0 auto 0 auto; display:block; text-align:center; } 
.shadoe img{ margin:10px 0 0 0;}
.formbox1 h5 span{ line-height:38px; font-size:26px; color:#fff; }

.subse{ margin:0 0 0 0; float:left;}
.subsemain{max-width:350px; background:#138fd7; margin:0 auto 1px auto; display:table; padding:7px 12px;}
.slt{ width:180px; float:left; font-size:16px; color:#fff;}
.srt{ width:130px; float:left; color:#ffd52e; font-size:16px; font-weight:bold;}
.consec h4{ font-size:20px; font-weight:normal; color:#fff; float:left; width:100%; margin:10px 0 12px 0;}
.consec h5{ float:left; width:100%; margin:0 0 5px 0; text-align:left; font-size:15px;}
.consec{ padding:10px 20px; margin:20px 0 25px 0; float:left; border:1px solid #138fd7;}
.agrbt{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/bghu.png) no-repeat; width:194px; height:43px;font-size:20px; color:#fff; border:none; float:right; margin:0 20px 0 0; cursor:pointer}
.backin{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/back.png) no-repeat left 14px; font-size:18px; color:#fff; padding:8px 10px 8px 20px; border:none; margin:0px 20px 0 0; float:left; cursor:pointer;}
.fity{ margin:20px auto 1px auto;}
.clear{ clear:both;}

.pbox{ width:100%; background:#00629b; border:1px solid #00a1ff; box-shadow:0 0px 5px #fff; float:left; min-height:240px; moz-border-radius:10px 0px 10px 0;-webkit-border-radius:10px 0px 10px 0;border-radius:10px 0px 10px 0; margin:0 1.6% 30px;}
.pttl{ width:97%; float:left; background:#00a1ff; font-size:18px; color:#fff; padding:10px 10px 10px 20px; border-left:6px solid #ffcc00;border-bottom:1px solid #ffcc00;box-sizing: border-box;moz-border-radius:10px 0px 10px 0;-webkit-border-radius:10px 0px 10px 0;border-radius:10px 0px 10px 0; margin:5px 0 40px 5px;}

.linkg{ width:100%; margin:0 auto; display:block; height:1px;}
.amt{ font-size:23px; color:#ffcc00; text-align:center; margin: 0 0 0 0; line-height:50px;}
.priceg{ font-size:30px; text-align:center; color:#fff; font-weight:normal; text-shadow:0 2px 3px #333; text-align:center; width:100%; float:left; padding:20px 0 0 0;}
.bigsun{ max-width:100%; margin:100px auto 50px auto;} 
.gapf{ width:100%; float:left; height:50px;}
.dcd{ margin:0 auto; float:none;}

@media(max-width:767px){
.pbox{ margin:40px auto; float:none; min-height:270px;}
}
</style>


<?php betashop_get_footer(); ?>