<?php
/**
 * Template Name: Request A Callback
 *
 * Description: Request A Callback page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<div class="main-container front-page about-page">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div><br />
	<div class="full-wrapper about-us-wrapper">
    <div class="container">
		<div class="row">



			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/imageqwone.jpg" alt="" />
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="page-content">
					<?php while (have_posts()) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php the_content(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post -->
					<?php endwhile; // end of the loop. ?>


				<?php
				    if(isset($_POST['request_submit'])){
				        $kutchina_cat = $_POST["kutchina_cat"];
				        $user_name = $_POST["user_name"];
				        $user_contact = $_POST["user_contact"];
				        $user_pincode = $_POST["user_pincode"];


				        $data = array(
						    'entry.1463328914' => $kutchina_cat,
						    'entry.1951998374' => $user_name,
						    'entry.1668149790' => $user_contact,
						    'entry.516695445' => $user_pincode,
					 	);

				    $ch=curl_init();
				  	curl_setopt($ch, CURLOPT_URL, 'https://docs.google.com/forms/d/e/1FAIpQLSducY50_zC1RJRfk62WBkvGR7kQGXmrnqrvMTO6oOg-tgtyXg/formResponse');
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
					curl_setopt($ch, CURLOPT_TIMEOUT, 15);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
					$response = curl_exec($ch);

					header('Location: https://www.kutchina.com/thank-you');
				    }
			    ?>



					<div class="form_section_request_cb">
						<form class="form-horizontal1" method="post">
					    <div class="form-group">
					      <label for="sel1">Category:</label>
					      <select class="form-control" id="kutchina_cat" name="kutchina_cat" required>
					      	<option value="">Select</option>
					        <option value="Chimney">Chimney</option>
					        <option value="Hobs">Hobs</option>
					        <option value="Water Purifier">Water Purifier</option>
					        <option value="Modular Kitchen">Modular Kitchen</option>
					      </select>
					    </div>
					    <div class="form-group">
					      <label for="usr">Name:</label>
					      <input type="text" class="form-control" name="user_name" id="user_name" required>
					    </div>
					    <div class="form-group">
					      <label for="usr">Phone Number:</label>
						  <input type="text" class="form-control" name="user_contact" id="user_contact" required>
					    </div>
					    <div class="form-group">
					      <label for="usr">Pincode:</label>
					      <input type="text" class="form-control" name="user_pincode" id="user_pincode" required>
					    </div>
					    <div class="form-group">				      
					        <button type="submit" name="request_submit" class="btn btn-primary btn_callback">Submit</button>				      
					    </div>
					  </form>
					</div>

            	</div>
			</div>
            <div class="clearfix"></div>
            <br />
            <div class="col-xs-12">
	            <!-- <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3682.780822539359!2d88.43762841496053!3d22.624658435156014!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39f89e32af2d0a97%3A0xf0a7af2c956f2c72!2sBajoria+Appliances+Pvt.+Ltd!5e0!3m2!1sen!2sin!4v1540532973803" allowfullscreen="allowfullscreen" width="100%" height="393px" frameborder="0"></iframe> -->
	            <p style="text-align: justify; line-height:23.5px;">The winner of the Times Business Award for the Best Modular Kitchen and Appliances in Eastern India, Kutchina is a name you can trust when it comes to kitchen appliances. The brand is committed to making your culinary experience easy and enjoyable. Our products surpass customers’ expectations and market demands. With dedicated centralised customer-care centres with toll-free numbers, free maintenance, and assistance,  we ensure that our relationship with customer grows after the sales. Kutchina products are technologically advanced, energy efficient and require low maintenance. Kutchina is a one-stop solution for all your culinary needs. From modular kitchens to advanced kitchen appliances, you name the product, we have it!</p>
	            <p style="text-align: justify; line-height:23.5px;">Kutchina products including chimney, hob, modular kitchen, and water purifier are designed to make your experience in the kitchen more enjoyable. Advanced auto-clean technology in Kutchina chimneys makes them one of our most sought after products. Our inbuilt hobs that come with mechanical timers and automatic ignition will give your kitchen a futuristic feel. Our water purifiers are first in India to have antioxidant technology which boosts immunity and detoxify the body. With 10 steps filtration technology, our water purifiers ensure that you enjoy clean and safe drinking water. We also help you give your kitchen a trendy and modern look with the latest modular kitchen designs. Our designs are high on ergonomics as well as aesthetics. We have over 17 years of experience in providing well-designed kitchens that are equipped with top-class shutters, storage solutions, and hardware accessories.</p>
	            <p><strong>Request a call back to know more about our products.</strong></p>
			</div>
            
            
            
		</div>
        <br /><br /><br />
	</div>
</div>
</div>
<?php betashop_get_footer(); ?>