<?php
/**
 * Template Name: All Products
 *
 * Description: All Products page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<div class="main-container front-page about-page">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div>
	<div class="full-wrapper about-us-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-xs-12">
					<?php dynamic_sidebar( 'sidebar-product-category' ); ?>
				</div>
				<div class="col-md-9 col-xs-12">
					<div class="page-content">
						<?php //while (have_posts()) : the_post(); ?>
							<!-- <article id="post-<?php //the_ID(); ?>" <?php //post_class(); ?>>
								<div class="entry-content">
									<?php //the_content(); ?>
								</div>
							</article> -->
						<?php //endwhile; ?>

						<?php 
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					        $argsQuery = array(
							    'posts_per_page' => 9,
						        'post_type' => 'product',
						        'post_status' => 'publish',
						        'paged' => $paged,
						        'order' => 'DESC',
						        'orderby' => 'date',
						        'meta_query' => array(
									array(
										'key' => '_regular_price',
										'value' => 0,
										'type' => 'numeric', // specify it for numeric values
										'compare' => '>'
									)
								),
						        'tax_query' => array(
						            array(
						                'taxonomy' => 'product_cat',
						                'field'    => 'term_id', // Or 'name' or 'term_id'
						                'terms'    => array(15),
						                'operator' => 'NOT IN', // Excluded
						            )
						        )
						    );
							$get_all_prods = new WP_Query( $argsQuery );

							if ($get_all_prods->have_posts()) :

							while ( $get_all_prods->have_posts() ) : $get_all_prods->the_post(); 

								?>
								<?php 
								// $image = get_the_post_thumbnail($loop->post->ID);
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'woocommerce_thumbnail' );
	// echo $image[0];
								$price = get_post_meta( get_the_ID(), '_regular_price', true);
								
								//if ($price != '' && $price != 0) { ?>
									
								<div class="col-lg-4 col-md-4 col-sm-6">	
									<div class="vgwc-item style1">
										<div class="ma-box-content all-product-box" style="margin: 0 0 30px 0 !important;">
											<div class="list-col4">
												<div class="vgwc-image-block">
													<?php the_post_thumbnail('woocommerce_thumbnail'); ?>
												</div>
											</div>

											<div class="list-col8">
												<div class="gridview">
													<div class="vgwc-text-block">
														<h3 class="vgwc-product-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
														<!-- <p><?php //echo $price; ?></p> -->
														<p><?php echo $product->get_price_html(); ?></p>
														<div class="vgwc-button-group">
															<div class="vgwc-add-to-cart">
																<?php echo do_shortcode('[add_to_cart id="'.esc_attr($product->get_id()).'"  style="none" show_price="false"]') ?>
															</div>											
														</div>
														<?php //if ($product->get_price() != '' && $product->get_price() != 0) { 
											
															if($product->is_in_stock()){

														?>
															
														<div class="vgwc-button-group">
															<div style="margin-bottom:10px;">
								        						<a class="button custom-button quickview quick-view" href="<?php echo esc_attr( $product->get_permalink() ); ?>">View product</a>
								    						</div>
														</div>
														
														<?php } else { ?>	
															
														<div class="vgwc-button-group">
															<div style="margin-bottom:10px;">
								        						<a class="button custom-button" style="background: #bfc3c5; border: 1px solid #bfc3c5">Out of stock</a>
								    						</div>
														</div>

														<?php } //} ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<?php //} ?>
						<?php
							endwhile;
						//endif; ?>
					</div>
				</div>
			</div>
			
			<div class="row all-product-box-paginate">			
		        <?php
					$total_pages = $get_all_prods->max_num_pages;
					if ($total_pages > 1){

						$current_page = max(1, get_query_var('paged'));

						echo paginate_links(array(
							'base' => get_pagenum_link(1) . '%_%',
							'format' => '/page/%#%',
							'current' => $current_page,
							'total' => $total_pages,
							'prev_text'    => __('« prev'),
							'next_text'    => __('next »'),
							'add_args'  => array()
						));
					}
						?>    
					<?php else :?>
					<h3><?php echo '404 Error: Not Found'; ?></h3>						
					
				<?php endif; wp_reset_postdata();?>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	

</style>

<?php betashop_get_footer(); ?>