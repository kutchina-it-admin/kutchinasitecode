<?php
/**
 * Template Name: Buy AMC
 *
 * Description: Request A Callback page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<div class="main-container front-page about-page">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div>
	<div class="full-wrapper about-us-wrapper">
    <div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="page-content">
					<?php while (have_posts()) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php the_content(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post -->
					<?php endwhile; // end of the loop. ?>


	<style>
	.insl{ margin:15px 0 0 0; float:right;}
	.bbt33{ text-align:right; margin:15px 0 0 0;}
	.bbt34{ text-align:right; margin:15px 0 0 0;}
		.nomar{margin: 0; }
		.bbhu{padding: 8px 16px!important; width: auto; margin:0 0 0 10px; }
		/*.lsd{margin:20px 0 0 0;}
		.formyu{max-width:620px; float: none; margin: 20px auto 40px auto; text-align:center; background: #ddd; padding:30px 20px;}*/
		.ttl{font-size: 17px; font-weight: 500; color: #666; margin: 0 0 50px 0;}
		.sev{width:100%;}
		.sev input{ width: 100%!important; }
	
		.clear{ clear: both; }
		.numim a{font-family: Arial;}
		.fom{ width:auto; height:auto; padding:20px; background:#fff; border:1px solid #ccc; float:left;}

.form_box1{    max-width: 680px;
    float: none;
    margin: 20px auto 40px auto;
    text-align: center;
    background: #f2f2f2; border:6px solid #fff; box-shadow:0 0 6px #ccc;
    padding: 45px 50px; overflow: hidden;}
  
.no-padding{ padding: 0 !important;}   
.bhu{background: #fff; padding: 5px 0px; margin: 0 0 6px 0;}
.tth{font-family: Arial;}
.bbt1{padding:0 0 0 15px;}
.bbt2{padding:0 5px 0 5px;}
.bbt3{padding:0 15px 0 0px;}
#firstt_frm button{border-radius: 0!important;}
#prod_serform button{border-radius: 0!important;}
#mod_nam button{border-radius: 0!important;}s

.form_buy_text p{ text-align: center !important; }


.banner_descs.banner_descs_by_amc {
    text-align: center;
    margin-bottom: 15px;
    padding: 0px 15px;
}


}

@media only screen and (max-width: 640px) {

	
.bbhu {
    padding: 8px 22px;
    width: 64px;
}
.main-container .page-content .input-text, .main-container .page-content input[type="text"], .main-container .page-content .form-row .input-text {
    padding: 0 10px !important;
}
}
@media only screen and (max-width: 767px) {
	.form_box1{padding:20px!important;}
	#loc_pinc .col-lg-10{width:76%!important;}
	#loc_pinc .col-lg-2{width:24%!important;}
	.bbt2 input{
    width: 100% !important;
    padding: 0 0 0 10px !important;
}
.bbt1, .bbt3{width:18%;}
.bbt1{padding: 0;}
.bbt2{width:64%;}

.bbt11, .bbt33{width:18%;}
.bbt33{ padding: 0; }
.bbt11{padding: 0;}
.bbt22{width:64%; padding: 0;}
#loc_pinc .form-group.mb-2.sev #user_pincode + #uname_response {
    margin: -40px 8px 0 0;

}
.sev b{text-align: center!important; float: none!important;}
.titll {

    padding: 9px 0 4px 20px;
    border: none;

}
#uname_response_neg {
    line-height: 14px;

}
}
	</style>


<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script>
<script type="text/javascript">


function keyPressed(e)
{
     var key;      
     if(window.event)
          key = window.event.keyCode; //IE
     else
          key = e.which; //firefox      

     return (key != 13);
}






	jQuery(document).ready(function(){

        jQuery("#prod_serform").hide(); //Second Form Hide
        jQuery("#mod_nam").hide(); //Third Form Hide        



        jQuery("#loc_but").click(function(e) {

        	var user_pincode = jQuery("#user_pincode").val().trim();

		      if(user_pincode != ''){
		        jQuery("#uname_response").show();
		        jQuery("#uname_response_neg").show();		        

		        jQuery.post("https://www.kutchina.com/wp-content/themes/vg-betashop/page-templates/zipcode_check.php?user_pincode="+user_pincode, function(data){
				    //alert(data);
				    if(data == 1) {

				    		
				    		jQuery("#uname_response_neg").hide(); // Negative Message Hide

		                	jQuery("#uname_response").html("<span class='exists' style='color: green'><i class='fa fa-check' aria-hidden='true'></i></span>");
		                
		                
		                /*setTimeout(function() {
			                jQuery("#prod_serform").show('slide'); //Second Form Show            
	            			jQuery("#firstt_frm").hide('slide'); //First Form Hide	            			
            			}, 900);*/
						
						jQuery("#firstt_frm").toggle("slide", {direction: "left"}, 1000, function(){
							jQuery("#prod_serform").toggle("slide", {direction: "right"}, 1000); //Second Form Show
						}); //First Form Hide
						

		            } else {
		            	jQuery("#uname_response").hide();

		                jQuery("#uname_response_neg").html("<span class='not-exists' style='color: red'>We currently do not service this Pin Code. Any inconvenience is regrettable</span>");
		            }
				});
		         
		      } else {
		        jQuery("#uname_response").hide();
		        jQuery("#uname_response_neg").hide();
		        jQuery("#prod_serform").hide(); //Second Form Hide
        		jQuery("#mod_nam").hide(); //Third Form Hide
		      }
            
        });





        /* Go Back Pin Area Section START */
        jQuery("#go_back_pin_area").click(function(e) {
		                
		                jQuery("#prod_serform").hide(); //Second Form Show            
            			jQuery("#firstt_frm").show(); //First Form Hide
            
        });
        /* Go Back Pin Area Section END */



        jQuery("#proddu_but").click(function(e) {



        	var product_serial_no = jQuery("#product_serial_no").val().trim();

        	if(product_serial_no != ''){
		        jQuery("#productserial_response").show();
		        jQuery.post("https://www.kutchina.com/wp-content/themes/vg-betashop/page-templates/model_check.php?product_serial_no="+product_serial_no, function(data){
		        	//alert(data);
		        	var tot_data = data.split('@@');		        	

				    if(tot_data[0] == 1) {

				    	var modd_id_productt = tot_data[2];
				    	//alert(modd_id_productt);				    	
				    	
				    	jQuery("#kut_mod_name_text").html(tot_data[1]);
				    	jQuery("#kut_mod_name_prodserial").html(product_serial_no);

				    	jQuery('#kut_mod_name').val(jQuery('#kut_mod_name').val() + tot_data[1]);
				    	/*jQuery('#kut_mod_name_hid').val(jQuery('#kut_mod_name_hid').val() + tot_data[1]);
				    	jQuery('#kut_mod_idd_hid').val(jQuery('#kut_mod_idd_hid').val() + tot_data[2]);*/
				    	jQuery('#kut_mod_name_hid').val(tot_data[1]);
				    	jQuery("#kut_mod_idd_hid").val(tot_data[2]);
		                
		                /*jQuery("#mod_nam").show('slide'); //Third Form Show
            			jQuery("#prod_serform").hide('slide'); //Second Form Hide*/


						jQuery("#prod_serform").toggle("slide", {direction: "left"}, 1000, function(){
							jQuery("#mod_nam").toggle("slide", {direction: "right"}, 1000); //Third Form Show
						}); //Second Form Hide

		            } else {
		                jQuery("#productserial_response").html("<span class='not-exists' style='color: red'>This product serial no doesn't exist</span>");
		            }
				});
		         
		      } else {
		      	jQuery("#productserial_response").hide();
	            jQuery("#mod_nam").hide(); //Third Form Show
	            //jQuery("#prod_serform").hide(); //Second Form Hide        	
        	}


        });



        
        /* Go Back Product Serial Section START */
        jQuery("#go_back_prodser_area").click(function(e) {        	

            jQuery("#mod_nam").hide('slide'); //Third Form Hide
            jQuery("#prod_serform").show('slide'); //Second Form Show
            
        });
        /* Go Back Product Serial Section END */



    });
</script>


<div class="formyu">


					<div class="firstt_frm" id="firstt_frm">
						<div class="form_buy_text">
							<p class="banner_descs banner_descs_by_amc">You may now buy an Annual Maintenance Contract for Chimneys and Water Purifiers online. Once you have completed the purchase our <br>representative will visit and deliver the AMC card to you.</p>
						</div>

						<div class="form_box1">
							<p class="ttl">Please enter Pin code of the location where the product is installed. We would like to be sure that we are available at your location</p>
							<form class="form-inline" id="loc_pinc">	
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 no-padding">
								<div class="form-group mb-2 sev">
							    <label for="inputPassword2" class="sr-only">Pin Code</label>
							    <input type="text" class="form-control nomar" id="user_pincode" name="user_pincode" placeholder="Enter Pin Code" onKeyPress="return keyPressed(event)">
							    <div id="uname_response" class="response"></div>
							    <div id="uname_response_neg" class="response"></div>
							  </div>
							</div>	
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
								<button id="loc_but" type="button" class="btn btn-primary mb-2 bbhu">
									<i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
							</form>
						</div>
						<div class="clear"></div>
					</div>
					

					

					<!-- Product Serial No Section -->
					<form class="form-inline" id="prod_serform">
						<div class="form_buy_text">
							<p class="banner_descs banner_descs_by_amc">Please enter the 12 digits Product Serial Number. You may find it on the box in which the product has been shipped to you, on the warranty card & on the side panel of the product.</p>
						</div>

	                    <div class="form_box1">
							<p class="ttl numim">In case you are having trouble finding the serial number please call <a href="tel:1800 419 7333"><b>1800 419 7333</b></a></p>
							

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group mb-2 sev">
								    <label for="inputPassword2" class="sr-only">Product Serial No</label>
								    <input type="text" class="form-control nomar" id="product_serial_no" name="product_serial_no" placeholder="Product Serial No" onKeyPress="return keyPressed(event)">
								    <div id="productserial_response" class="response"></div>
							  	</div>
							</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 insl bbt34">
								
                                
                                <button id="go_back_pin_area" type="button" class="btn btn-primary mb-2 bbhu"><!--<i class="fa fa-angle-double-left" aria-hidden="true"></i>-->Back</button>
                                <button id="proddu_but" type="button" class="btn btn-primary mb-2 bbhu"><!--<i class="fa fa-angle-double-right" aria-hidden="true"></i>--> Proceed</button>
							</div>
                            
							<!--<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 bbt3">
								
							</div>-->
						</div>
					</form>
					<div class="clear"></div>



					<!-- Model Name Section -->
					<form class="form-inline form_box1" id="mod_nam" method="POST" action="https://www.kutchina.com/package/">
					<!-- <form class="form-inline form_box1" id="mod_nam" method="POST" action="https://www.kutchina.com/amc-agreement/"> -->
						<p class="ttl">Model Details</p>

						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 bbt11">
							
						</div>

						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 bbt22">
							<div class="form-group mb-2 sev">


								<div class="row bhu">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><b style="text-align: left;float: left;"> Name:</b></div>
									<div class="kut_mod_name_text col-lg-6 col-md-6 col-sm-6 col-xs-12 tth" id="kut_mod_name_text"></div>
								</div>

								<div class="row bhu">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><b style="text-align: left; float: left;">Product Serial No:</b></div>
									<div class="kut_mod_name_prodserial col-lg-6 col-md-6 col-sm-6 col-xs-12 tth" id="kut_mod_name_prodserial"></div>
								</div>
								
								

								<!-- <input type="text" class="form-control nomar" id="kut_mod_name" name="kut_mod_name" placeholder="Model Name" readonly="readonly" onKeyPress="return keyPressed(event)"> -->
							    <input type="hidden" class="form-control nomar" id="kut_mod_name_hid" name="kut_mod_name_hid">
							    <input type="hidden" class="form-control nomar prdsr_iddd" id="kut_mod_idd_hid" name="kut_mod_idd_hid"> 
						  	</div>
						</div>

<div class="clear"></div><br />
<div class="fom">
						<p style="text-align:left;"><strong>By clicking Agree & Continue I hereby</strong></p>
						<ul style="list-style: disc inside; text-align:left; font-size:13px;">
							<li>Agree to the AMC <a href="https://www.kutchina.com/amc-terms-conditions/" target="_blank"><strong>Terms & Conditions</strong></a></li>
							<li>Agree to the fact that, the AMC will start only after the realization of the payment</li>
							<li>Agree to the fact that, if there is any delay in payment Kutchina will not be held &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;responsible for the same</li>
						</ul>
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bbt33">	
                        					
							<button id="go_back_prodser_area" type="reset" class="btn btn-primary mb-2 bbhu"><!--<i class="fa fa-angle-double-left" aria-hidden="true"></i>-->Back</button>
                            <button type="submit" class="btn btn-primary mb-2 bbhu lsd">Agree & Continue</button>	
                            
						</div>
                        </div>
					</form>
					<div class="clear"></div>
					



</div>


            	</div>


			</div>
            
            
		</div>
	</div>
</div>
</div>
<?php betashop_get_footer(); ?>