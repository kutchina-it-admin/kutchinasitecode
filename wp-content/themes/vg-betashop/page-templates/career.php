<?php
/**
 * Template Name: Career Page Template
 *
 * Description: Custom Career template March 2020
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>

  <div class="banner_section">
    <div id="owl-demo" class="owl-carousel owl-theme bnr_static">
      <div class="item">
        <div class="banner_pic"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/c-banner_1.jpg" alt=""></div>
      </div>
      <div class="item">
        <div class="banner_pic"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/c-banner_2.jpg" alt=""></div>
      </div>
      <div class="item">
        <div class="banner_pic"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/c-banner_3.jpg" alt=""></div>
      </div>
      <div class="item">
        <div class="banner_pic"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/c-banner_4.jpg" alt=""></div>
      </div>
      <div class="item">
        <div class="banner_pic"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/c-banner_5.jpg" alt=""></div>
      </div>
      <div class="item">
        <div class="banner_pic"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/c-banner_6.jpg" alt=""></div>
      </div>
    </div>
    <!-- <div class="customNavigation">
    	<a class="btn prev">Previous</a>
    	<a class="btn next">Next</a>
    </div>-->
  </div>

  <div class="career-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <header class="entry-header">
              <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <?php the_content(); ?>
        </div>
      </div>
  </div>

<script>
jQuery(document).ready(function() {
 
var owl = jQuery("#owl-demo");
 
  owl.owlCarousel({
      items : 1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : true, // itemsMobile disabled - inherit from itemsTablet option
      autoPlay: 7000
  });
 
  // Custom Navigation Events
  jQuery(".next").click(function(){
    owl.trigger('owl.next');
  })
  jQuery(".prev").click(function(){
    owl.trigger('owl.prev');
  })

 
});
</script>
<?php betashop_get_footer(); ?>
