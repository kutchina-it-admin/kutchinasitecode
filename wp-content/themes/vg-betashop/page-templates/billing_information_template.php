<?php
/**
 * Template Name: Billing Information
 *
 * Description: Request A Callback page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<div class="main-container front-page about-page">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div>
		<style>
			.nomar{margin: 0; }
			.bbhu{padding: 8px 16px; margin: 25px 0 0 0; float: left;}
			.formyu{max-width:820px; float: none; margin: 20px auto 40px auto; text-align:center; background: #ddd; padding:30px!important;}
			.ttl{font-size: 18px; font-weight: bold;; color: #000; margin: 0 0 20px 0;}
			.sev{width:100%!important;}
			.sev input{ width: 100%!important; }
			.formyu label{ float: left; padding: 0 0 0 0!important; margin: 0!important; font-size:16px; color: #000; font-weight: bold!important;  }
			.clear{ clear: both; }

			.chkkk{ float: left; margin:6px 13px 0 0 !important; }
		</style>
	<div class="full-wrapper about-us-wrapper">
    <div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-content">
					<?php while (have_posts()) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php the_content(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post -->
					<?php endwhile; // end of the loop. ?>


				
<div class="formyu">

			<form method="post" action="https://www.kutchina.com/payment">

				<div class="col-lg-12">
				    <input type="checkbox" class="chkkk">
				    <label for="exampleInputEmail1">Billing info is same as shipping information</label>
				</div>

<br><br>


			  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			    <label for="exampleInputEmail1">Name</label>
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Name">
			  </div>
			  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			    <label for="exampleInputEmail1">Phone</label>
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Phone Number">
			  </div>
			  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			    <label for="exampleInputEmail1">Email</label>
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email Id">
			  </div>

			  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			    <label for="exampleInputEmail1">Address</label>
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter your Address">
			  </div>
			  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			    <label for="exampleInputEmail1">Pin Code</label>
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Pin Code">
			  </div>
		
			  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	  					  
			  <button type="submit" class="btn btn-primary bbhu">Submit</button>
			</div>
			<div class="clear"></div>
			</form>

</div>









				

            	</div>


			</div>
            
            
		</div>
	</div>
</div>
</div>
<?php betashop_get_footer(); ?>