<?php
/**
 * Template Name: AMC Agreement
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<style>
	.ac_btn{background: #0065b2;
	font-size: 16px;
	color: #fff;
	padding: 7px 15px;
	margin-bottom: 20px;
	display: block;
	width: 163px;
	margin: 20px auto 20px auto;}
	.ac_btn:hover{ color: #fff; background:#0c8aea;}

	.tc_contant{}
	.tc_contant p{}
	.tc_contant p span{ color:#000; margin: 0 5px 0 0; }

</style>
<div class="main-container front-page about-page">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<!-- <div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div> -->

		
	<div class="full-wrapper about-us-wrapper">
    <div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-content">
					<?php while (have_posts()) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php the_content(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post -->
					<?php endwhile; // end of the loop. ?>


				


<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST"){
		$get_amc_prod = $_POST['kut_mod_idd_hid'];		
	}
?>



	<p>By clicking Agree & Continue I hereby</p>
	<ul style="list-style: disc inside;">
		<li>Agree to the AMC <a href="https://www.kutchina.com/amc-terms-conditions/" target="_blank"><strong>Terms & Conditions</strong></a></li>
		<li>Agree to the fact that, the AMC will start only after the realization of the payment</li>
		<li>Agree to the fact that, if there is any delay in payment Kutchina will not be held responsible for the same</li>
	</ul>

	<div class="tc_contant">

		<form class="form-inline form_box1" id="" method="POST" action="https://www.kutchina.com/package/">
			<input type="hidden" class="form-control nomar" id="kut_mod_name_hid" name="kut_mod_name_hid" value="<?php echo $get_amc_prod; ?>">
			<button type="submit" class="ac_btn">Agree & Continue</button>
		</form>

	</div>








				

            	</div>


			</div>
            
            
		</div>
	</div>
</div>
</div>
<?php betashop_get_footer(); ?>