<?php
/**
 * Template Name: Shop Template
 *
 * Description: Shop Template
 *
* @package    VG Betashop
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
if (! defined('ABSPATH')) exit; // Exit if accessed directly

betashop_get_header(); ?>
<?php
	$betashop_options  = betashop_get_global_variables(); 
	$betashop_productrows  = betashop_get_global_variables('betashop_productrows'); 
?>
<div class="main-container">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="page-content row">
				<?php if($betashop_options['sidebar_pos']=='left' || !isset($betashop_options['sidebar_pos'])) :?>
					<?php get_sidebar('category'); ?>
				<?php endif; ?>
				<div id="archive-product" class="col-xs-12 <?php if (is_active_sidebar('sidebar-category')) : ?>col-md-9<?php endif; ?>">
					<div class="default-shop">
						<?php while (have_posts()) : the_post(); ?>
							<?php get_template_part('content', 'page'); ?>
						<?php endwhile; // end of the loop. ?>
					</div>
				</div>
				<?php if($betashop_options['sidebar_pos']=='right') :?>
					<?php get_sidebar('category'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer('shop'); ?>