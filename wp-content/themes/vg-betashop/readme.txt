=== VG BetaShop ===

Contributors: automattic
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.0
Tested up to: 4.2.2
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A starter theme called VG BetaShop, or underscores.

== Description ==

VG BetaShop – Kitchen Appliances WooCommerce Theme is a great choice for all those things. It has 6 Layouts and 4 Preset Color with an unbranded admin Theme Options panel that gives you full control of the customization of your website.

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

VG BetaShop includes support for Infinite Scroll in Jetpack.

== Changelog ==

VERSION 1.0 - Released on August 10, 2016
- First release.

= 1.1 - May 15 2017 =
* Fully compatible with WooCommerce 3.0.5


== Credits ==

* Based on Underscores http://underscores.me/,(C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/,(C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
