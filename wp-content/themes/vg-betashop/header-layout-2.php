<?php
/**
 * @version    1.2
 * @package    VG Betashop
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>

<?php
	$betashop_options  = betashop_get_global_variables(); 
	$woocommerce 	= betashop_get_global_variables('woocommerce'); 
?>

<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />


<?php if (isset($betashop_options['share_head_code']) && $betashop_options['share_head_code']!='') {
	echo wp_kses($betashop_options['share_head_code'], array(
		'script' => array(
			'type' 	=> array(),
			'src' 	=> array(),
			'async' => array()
		),
	));
} ?>

<?php 
	// HieuJa get Preset Color Option
	$presetopt = betashop_get_preset();
	// HieuJa end block
?>

<?php wp_head(); ?>
</head>

<!-- Body Start Block -->
<body <?php body_class(); ?>>

<!-- Page Loader Block -->
<?php if (isset($betashop_options['betashop_loading']) && $betashop_options['betashop_loading']) : ?>
<div id="pageloader">
	<div id="loader"></div>
	<div class="loader-section left"></div>
	<div class="loader-section right"></div>
</div>
<?php endif; ?>

<div id="yith-wcwl-popup-message"><div id="yith-wcwl-message"></div></div>
<div class="wrapper <?php if(isset($betashop_options['page_style']) && $betashop_options['page_style']=='box'){echo 'box-layout';}?>">

	<!-- Top Header -->
	<div class="top-wrapper">
		<div class="header-container">
			<?php if (is_active_sidebar('sidebar-top-header')) : ?>
			<div class="top-bar">
				<div class="container">
					<div id="top">
						<div class="row">
								<?php dynamic_sidebar('sidebar-top-header'); ?>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
			<div class="header">
				<div class="container">
					<div class="row">
						<div id="sp-logo" class="col-xs-12 col-md-<?php echo (class_exists('Custom_Ajax_search_widget') || class_exists('WC_Widget_Cart')) ?  '3' : '12'; ?>">
						<?php if(isset($betashop_options['logo_main']) && !empty($betashop_options['logo_main']['url'])){ ?>
							<div class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<img src="<?php echo esc_url($betashop_options['logo_main']['url']); ?>" alt="" />
								</a>
							</div>
						<?php } else if(isset($betashop_options['logo_text']) && !empty($betashop_options['logo_text'])) {?>
							<h1 class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<?php echo esc_html($betashop_options['logo_text']); ?>
								</a>
							</h1>
						<?php } else { ?>
							<div class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<img src="<?php echo esc_url(get_template_directory_uri() . '/images/presets/preset'. $presetopt .'/logo.gif' ); ?>" alt="" />
								</a>
							</div>
						<?php } ?>
						</div>
						
						<?php if(class_exists('Custom_Ajax_search_widget') || class_exists('WC_Widget_Cart')) : ?>
						<div id="sp-search-cart" class="col-xs-12 col-md-9 <?php echo (class_exists('Custom_Ajax_search_widget') && class_exists('WC_Widget_Cart')) ? 'media' : ''; ?>">
							
							<?php if (class_exists('Custom_Ajax_search_widget')) { ?>
							<div class="search-toggle">
									<i class="fa fa-search"></i>
							</div>
							<div id="sp-search" class="search-sticky <?php echo (class_exists('WC_Widget_Cart')) ? 'media-body-layout-3' : ''; ?>">
								<div class="vg-search">
									  <?php ajax_autosuggest_form(); ?>
								</div>		
							</div>		
							<?php } ?>
							
							
							<?php if (class_exists('WC_Widget_Cart')) { ?>
							<div class="sp-cart <?php echo (class_exists('Custom_Ajax_search_widget')) ? 'media-right' : ''; ?>">
								<?php the_widget('Custom_WC_Widget_Cart'); ?>
							</div>
							<?php } ?>
							
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<!-- Menu -->
			<div class="sp-main-menu-wrapper home2">
				<div class="container">
					<div class="row">
						<?php if (is_active_sidebar('sidebar-vg-left-main-menu')) : ?>
						<div class="siderbar-list-category col-md-3 col-sm-6 col-xs-12">
							<div class="cateproductmenu">
								<?php dynamic_sidebar('sidebar-vg-left-main-menu'); ?>
							</div>
						</div>
						<?php endif; ?>
							
						<div class="menu-wrapper col-md-<?php echo (is_active_sidebar('sidebar-vg-left-main-menu')) ? '9' : '12'; ?> col-sm-<?php echo (is_active_sidebar('sidebar-vg-left-main-menu')) ? '6' : '12'; ?> col-xs-12">
							<div id="menu-container">
								<div id="header-menu">
									<div class="header-menu visible-large">
										<?php echo wp_nav_menu(array("theme_location" => "primary2","")); ?>
									</div>
									<div class="visible-small">
										<div class="mbmenu-toggler"><span class="title"><?php echo (isset($betashop_options['title_mobile_menu'])) ? esc_html($betashop_options['title_mobile_menu']) : esc_html__('Menu', 'vg-betashop'); ?></span><span class="mbmenu-icon"></span></div>
										<div class="nav-container">
											<?php wp_nav_menu(array('theme_location' => 'mobilemenu2', 'container_class' => 'mobile-menu-container', 'menu_class' => 'nav-menu')); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	