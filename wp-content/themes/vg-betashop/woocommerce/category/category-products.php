		<div class="container">			
			<div class="row">
				<div class="cstmmm_designnn_prodiii cstm_prod_filter_dsgn">
					<?php 
						// $categories = get_the_category();
						// $cat = $categories[0]->term_id;
						// echo $cat;

						$catt_trm = get_queried_object();
						$catt = get_queried_object()->term_id;

						if ($catt != 147 ) {
							
					?>
				<?php if($catt == 629 ) { ?>

					<?php if($blogsidebar=='left') : ?>
						<?php dynamic_sidebar( 'sidebar-product-category' ); ?>
					<?php endif; ?>

					<?php if($blogsidebar=='right') : ?>
						<?php dynamic_sidebar( 'sidebar-product-category' ); ?>
					<?php endif; ?>

				<?php } else { ?>

					<?php if($blogsidebar=='left') : ?>
						<?php get_sidebar('category'); ?>
					<?php endif; ?>

					<?php if($blogsidebar=='right') : ?>
						<?php get_sidebar('category'); ?>
					<?php endif; ?>

				<?php } ?>

					<div class="prodt_cstm_lst col-xs-12 col-md-9">
					<?php if (have_posts()) : ?>						
						
						<?php do_action('woocommerce_after_shop_header'); ?>						
						
						<?php if((is_shop() && '' !== get_option('woocommerce_shop_page_display')) || (is_product_category() && '' !== get_option('woocommerce_category_archive_display'))) : ?>
						<div class="all-subcategories">
							<?php woocommerce_product_subcategories(); ?>
							<div class="clearfix"></div>
						</div>
						<?php endif; ?>
						
						<?php if((is_shop() && 'subcategories' !== get_option('woocommerce_shop_page_display')) || (is_product_category() && 'subcategories' !== get_option('woocommerce_category_archive_display')) || (empty($product_subcategories) && 'subcategories' == get_option('woocommerce_category_archive_display')) || is_product_tag()): ?>
						<div class="toolbar canging_tabbiii">
							<div class="view-mode">
								<a href="#" class="grid active" title="<?php echo esc_attr__('Grid', 'vg-betashop'); ?>"><i class="fa fa-th-large"></i> <strong><?php echo esc_html__('Grid', 'vg-betashop'); ?></strong></a>
								<a href="#" class="list" title="<?php echo esc_attr__('List', 'vg-betashop'); ?>"><i class="fa fa-th-list"></i> <strong><?php echo esc_html__('List', 'vg-betashop'); ?></strong></a>
							</div>
							<?php do_action('woocommerce_before_shop_loop'); ?>
							<div class="clearfix"></div>
						</div>
						<?php endif; ?>
						
						<?php woocommerce_product_loop_start(); ?>								
							<?php $woocommerce_loop['loop'] = 0; while (have_posts()) : the_post(); ?>
								<?php wc_get_template_part('content', 'product'); ?>
							<?php endwhile; // end of the loop. ?>
						<?php woocommerce_product_loop_end(); ?>
						
						<?php if((is_shop() && 'subcategories' !== get_option('woocommerce_shop_page_display')) || (is_product_category() && 'subcategories' !== get_option('woocommerce_category_archive_display')) || (empty($product_subcategories) && 'subcategories' == get_option('woocommerce_category_archive_display')) || is_product_tag()): ?>
							<div class="toolbar tb-bottom">
								<?php do_action('woocommerce_after_shop_loop'); ?>
								<div class="clearfix"></div>
							</div>
						<?php endif; ?>
						
					<?php elseif (! woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>
						<?php wc_get_template('loop/no-products-found.php'); ?>
					<?php endif; ?>

					</div>

				<?php } ?>
				</div>

			</div>
			
			<div class="row">
				<div class="cstmmm_designnn_prodiii">
					<?php echo get_field('custom_section', $catt_trm); ?>
				</div>
			</div>
			
			</div>
