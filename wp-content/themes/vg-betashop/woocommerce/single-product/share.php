<?php
/**
 * Single Product Share
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if (! defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>
<?php $betashop_options  = betashop_get_global_variables();  ?>
<?php if(isset($betashop_options['product_sharing_show'])&& $betashop_options['product_sharing_show']) { ?>
	<div class="single-product-sharing">
		<?php do_action('vg_social_share'); // Sharing plugins can hook into here ?>
	</div>




				<?php
					global $product;
					$product_cats_ids = wc_get_product_term_ids( $product->get_id(), 'product_cat' );

						foreach( $product_cats_ids as $cat_id ) {
			    		$term = get_term_by( 'id', $cat_id, 'product_cat' );

			    			if ($cat_id === reset($product_cats_ids))
			        		$get_parent_cat_name = $term->name;
						}
				?>

				<!-- Large Appliances -->
				<?php if ( $get_parent_cat_name == 'Appliances' ) { ?>
					<div class="cust_cta">
						<a href="https://www.kutchina.com/partner/large-appliances">Find Us</a>
					</div>
				<?php } ?>

				<!-- Small Appliances -->
				<?php if ( $get_parent_cat_name == 'Small Appliances' ) { ?>
					<div class="cust_cta">
						<a href="https://www.kutchina.com/partner/small-appliances">Find Us</a>
					</div>
				<?php } ?>

				<!-- Modular Kitchens -->
				<?php if ( $get_parent_cat_name == 'Modular Kitchens' ) { ?>
					<div class="cust_cta">
						<a href="https://www.kutchina.com/partner/modular-kitchens">Find Us</a>
					</div>
				<?php } ?>

				<!-- Water Purifiers -->
				<?php if ( $get_parent_cat_name == 'Water Purifiers' ) { ?>
					<div class="cust_cta">
						<a href="https://www.kutchina.com/partner/water-purifiers">Find Us</a>
					</div>
				<?php } ?>


<?php } ?>






