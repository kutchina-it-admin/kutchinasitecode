<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

// Find the category + category parent, if applicable 
$term = get_queried_object(); 
$parent_id = empty( $term->term_id ) ? 0 : $term->term_id; 

// NOTE: using child_of instead of parent - this is not ideal but due to a WP bug ( http://core.trac.wordpress.org/ticket/15626 ) pad_counts won't work
$args = array(
	'child_of'		=> $parent_id,
	'menu_order'	=> 'ASC',
	'hide_empty'	=> 0,
	'hierarchical'	=> 1,
	'taxonomy'		=> 'product_cat',
	'pad_counts'	=> 1
);
$product_subcategories = get_categories( $args  );

betashop_get_header(); ?>
<?php
$betashop_options  = betashop_get_global_variables(); 
?>
<?php 
$bloglayout = 'left';
$blogsidebar = 'left';
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$bloglayout = $_GET['sidebar'];
	
	switch($bloglayout) {
		case 'right':
			$blogclass = 'sidebar-right';
			$blogcolclass = 9;
			$blogsidebar = 'right';
			break;
		default:
			$blogcolclass = 9;
	}
}else {
	if(isset($betashop_options['sidebar_pos']) && $betashop_options['sidebar_pos']!=''){
		$blogsidebar = $betashop_options['sidebar_pos'];
	}	
	switch($blogsidebar) {
		case 'right':
			$blogclass = 'sidebar-right';
			$blogcolclass = 9;
			$blogsidebar = 'right';
			break;
		default:
			$blogcolclass = 9;
	}
}
?>
<div class="main-container page-shop sidebar-<?php echo (isset($betashop_options['sidebar_pos'])) ? $betashop_options['sidebar_pos'] : ''; ?>">
	<div class="page-content section33">


		<?php include (TEMPLATEPATH . '/woocommerce/category/category-top.php'); ?>

		<section class="key_feature">
	      <div class="container">
	         <div class="row">
	           <div class="col-md-3">
	             <div class="feature_box">
	               <figure>
		               	<a>
    <img src="<?php bloginfo ( 'template_url' ); ?>/img_new/Induction-base_pan.png" 
    onmouseover="this.src='<?php bloginfo ( 'template_url' ); ?>/img_new/Induction-base_pan-r.png'"
    onmouseout="this.src='<?php bloginfo ( 'template_url' ); ?>/img_new/Induction-base_pan.png'"
    border="0" alt=""/>
    </a>
	               </figure>
	               <figcaption>
	                 <p>Induction Base Pan</p>
	               </figcaption>
	             </div>
	           </div>

	            <div class="col-md-3">
	             <div class="feature_box">
	               <figure>
		               	<a>
    <img src="<?php bloginfo ( 'template_url' ); ?>/img_new/marble-finish.png" 
    onmouseover="this.src='<?php bloginfo ( 'template_url' ); ?>/img_new/marble-finish-r.png'"
    onmouseout="this.src='<?php bloginfo ( 'template_url' ); ?>/img_new/marble-finish.png'"
    border="0" alt=""/>
    </a>
	               </figure>
	               <figcaption>
	                 <p>Marble Finish</p>
	               </figcaption>
	             </div>
	           </div>
                
                <div class="col-md-3">
	             <div class="feature_box">
	               <figure>
		               	<a>
    <img src="<?php bloginfo ( 'template_url' ); ?>/img_new/stylish-soft-touch-handles.png" 
    onmouseover="this.src='<?php bloginfo ( 'template_url' ); ?>/img_new/stylish-soft-touch-handles-r.png'"
    onmouseout="this.src='<?php bloginfo ( 'template_url' ); ?>/img_new/stylish-soft-touch-handles.png'"
    border="0" alt=""/>
    </a>
	               </figure>
	               <figcaption>
	                 <p>Stylish Soft Touch Handles</p>
	               </figcaption>
	             </div>
	           </div>

	            <div class="col-md-3">
	             <div class="feature_box">
	               <figure>
		               	<a>
    <img src="<?php bloginfo ( 'template_url' ); ?>/img_new/4layer-non-stick-coating.png" 
    onmouseover="this.src='<?php bloginfo ( 'template_url' ); ?>/img_new/4layer-non-stick-coating-r.png'"
    onmouseout="this.src='<?php bloginfo ( 'template_url' ); ?>/img_new/4layer-non-stick-coating.png'"
    border="0" alt=""/>
    </a>
	               </figure>
	               <figcaption>
	                 <p> 4 Layer Nonstick Coating </p>
	               </figcaption>
	             </div>
	           </div>
	             <!--<div class="col-md-3">
	             <div class="feature_box">
	               <figure>
			               	<a>
    <img src="<?php bloginfo ( 'template_url' ); ?>/img_new/Touch-Free.png" 
    onmouseover="this.src='<?php bloginfo ( 'template_url' ); ?>/img_new/Icon-4-r.png'"
    onmouseout="this.src='<?php bloginfo ( 'template_url' ); ?>/img_new/Icon-4.png'"
    border="0" alt=""/>
    </a>
	               </figure>
	               <figcaption>
	                 <p>2 year warranty</p>
	               </figcaption>
	             </div>
	           </div>-->

	         </div>
	      </div>
	   	</section>


		<?php include (TEMPLATEPATH . '/woocommerce/category/category-body.php'); ?>
		<?php include (TEMPLATEPATH . '/woocommerce/category/category-products.php'); ?>


	</div>
</div>


<section class="gtl_frm_call">
 <div class="container">
   <div class="row">
     <div class="col-md-12">
       <div class="frm_cal_back">
         <h5>for more information</h5>
         <button type="button" class="btn gt_up_call">Get a call back</button>
       </div>
     </div>
   </div>
 </div>
</section>

   
<?php do_action('woocommerce_after_girdview'); ?>
<?php betashop_get_footer(); ?>