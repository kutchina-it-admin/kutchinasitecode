<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

betashop_get_header(); ?>
<?php $betashop_options  =  betashop_get_global_variables(); ?>
<div class="main-container page-shop sidebar-<?php echo (isset($betashop_options['sidebar_product'])) ? $betashop_options['sidebar_product'] : ''; ?>">
	<div class="page-content">
		<div class="row-breadcrumb">
			<div class="container">
			<?php
				do_action('woocommerce_before_main_content');
			?>
			</div>
		</div>
		<div class="container">
			<?php $betashop_options['sidebar_product'] = (isset($betashop_options['sidebar_product'])) ? $betashop_options['sidebar_product'] : ""; ?>
			<div class="row">
				
				<div id="product-content" class="col-xs-12 <?php if (is_active_sidebar('sidebar-product')) : ?>col-md-9<?php endif; ?>">
					<div class="product-view">
						<?php while (have_posts()) : the_post(); ?>

							<?php wc_get_template_part('content', 'single-product'); ?>

						<?php endwhile; // end of the loop. ?>

<!-- kutchina -->
						<?php
							/**
							 * woocommerce_after_main_content hook
							 *
							 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
							 */
							do_action('woocommerce_after_main_content');
						?>

						<?php
							/**
							 * woocommerce_sidebar hook
							 *
							 * @hooked woocommerce_get_sidebar - 10
							 */
							//do_action('woocommerce_sidebar');
						?>

						<!-- Accordian Starts -->
						<?php
						// check if the repeater field has rows of data
							if( have_rows('faqs') ): ?>

							<div class="accordion_container">
								<h2>FAQ</h2>

							<?php 	// loop through the rows of data
						
						    	while ( have_rows('faqs') ) : the_row(); 
						
						?>
							
						        <div class="accordion_head"><?php echo get_sub_field('faq_question'); ?><span class="plusminus">+</span></div>
								<div class="accordion_body" style="display: none;">
								  <p><?php echo get_sub_field('faq_answer'); ?></p>
								</div>

							<?php 
								endwhile; 

						// else :

						    // no rows found ?>
						    
							</div>  

						<?php	endif;  ?>


<style type="text/css">
   .accordion_container {
   width: 100%;
   /*max-width: 500px;*/
   }
   .accordion_head {
   background-color: #0065b2;
   color: white;
   cursor: pointer;
   font-family: arial;
   font-size: 17px;
   margin: 0 0 1px 0;
   padding: 7px 11px;
   font-weight: bold;
   }
   .accordion_body {
   background: #e5eff7;
   }
   .accordion_body p {
   padding: 18px 18px;
   margin: 0px;
   font-size: 16px;
   }
   .plusminus {
   float: right;
   }
</style>


						<!-- Accordian ENDs -->

					</div>
				</div>
				<?php if($betashop_options['sidebar_product']=='left' || !isset($betashop_options['sidebar_product'])) :?>
					<?php get_sidebar('product'); ?>
				<?php endif; ?>
				
				<?php if($betashop_options['sidebar_product']=='right' || !isset($betashop_options['sidebar_product'])) :?>
					<?php get_sidebar('product'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php betashop_get_footer(); ?>

<script>
   jQuery(document).ready(function() {
   //toggle the component with class accordion_body
   jQuery(".accordion_head").click(function() {
     if (jQuery('.accordion_body').is(':visible')) {
       jQuery(".accordion_body").slideUp(300);
       jQuery(".plusminus").text('+');
     }
     if (jQuery(this).next(".accordion_body").is(':visible')) {
       jQuery(this).next(".accordion_body").slideUp(300);
       jQuery(this).children(".plusminus").text('+');
     } else {
       jQuery(this).next(".accordion_body").slideDown(300);
       jQuery(this).children(".plusminus").text('-');
     }
   });
   });
   
</script>