<?php
/**
 * @version    1.2
 * @package    VG Betashop
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>

<?php
	$betashop_options  = betashop_get_global_variables(); 
	$woocommerce 	= betashop_get_global_variables('woocommerce');
?>

<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<script src="https://pchen66.github.io/js/three/three.min.js"></script>
<script src="https://rawgit.com/pchen66/panolens.js/dev/build/panolens.min.js"></script>




<!-- Custom Home Page CSS & JS Links START -->

<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:500,600,700,800,900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?php //bloginfo ( 'template_url' ); ?>/css_new/font-awesome.min.css">
<link href="<?php //bloginfo ( 'template_url' ); ?>/css_new/bootstrap.min.css" rel="stylesheet">
<link href="<?php //bloginfo ( 'template_url' ); ?>/css_new/owl.carousel.css" rel="stylesheet">
<link href="<?php //bloginfo ( 'template_url' ); ?>/css_new/owl.theme.css" rel="stylesheet"> -->
<link href="<?php bloginfo ( 'template_url' ); ?>/css_new/khome.css" rel="stylesheet">
<link href="<?php bloginfo ( 'template_url' ); ?>/css_new/alag.css" rel="stylesheet">

<!-- Custom Home Page CSS & JS Links END -->





<?php if (isset($betashop_options['share_head_code']) && $betashop_options['share_head_code']!='') {
	echo wp_kses($betashop_options['share_head_code'], array(
		'script' => array(
			'type' 	=> array(),
			'src' 	=> array(),
			'async' => array()
		),
	));
} ?>

<?php 
	// HieuJa get Preset Color Option
	$presetopt = betashop_get_preset();
	// HieuJa end block
?>

<?php wp_head(); ?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NRWDH2Q');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '308963747007282');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=308963747007282&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->





<?php if (is_wc_endpoint_url( 'order-received' )) { ?>

	<!-- Event snippet for Kutchina - AMC conversion page -->
	<script>
	  gtag('event', 'conversion', {
	  	'send_to': 'AW-865401263/gpx4CPvh3qIBEK_z05wD',
	  	'value': 0.0,
	  	'currency': 'INR',
	  	'transaction_id': ''
	  });
	</script>

<?php } ?>


</head>

<!-- Body Start Block -->
<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NRWDH2Q"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!-- Page Loader Block -->
<?php if (isset($betashop_options['betashop_loading']) && $betashop_options['betashop_loading']) : ?>
<div id="pageloader">
	<div id="loader"></div>
	<div class="loader-section left"></div>
	<div class="loader-section right"></div>
</div>
<?php endif; ?>

<div id="yith-wcwl-popup-message"><div id="yith-wcwl-message"></div></div>
<div class="wrapper <?php if(isset($betashop_options['page_style']) && $betashop_options['page_style']=='box'){echo 'box-layout';}?>">

	<!-- Top Header -->
	<div class="top-wrapper">
		<div class="header-container">
			<?php if (is_active_sidebar('sidebar-top-header')) : ?>
			<div class="top-bar home1">
				<div class="container">


					<div id="top">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<ul class="top-bar-list">
						<li><a href="https://wa.me/7596071320" onClick="gtag('event', 'click', {'send_to': 'UA-106489867-1', 'event_category': 'Whatsapp', 'event_action': 'click'});"><span class="whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></span>7596071320</a></li>
						<li><a href="tel:18004197333" onClick="gtag('event', 'click', {'send_to': 'UA-106489867-1', 'event_category': 'Phone', 'event_action': 'click'});"><span class="call"><i class="fa fa-phone" aria-hidden="true"></i></span>18004197333</a></li>
						<li class="bfa"><a href="/amazon-products/" class="bfamazone" onClick="gtag('event', 'click', {'send_to': 'UA-106489867-1', 'event_category': 'Header Amazon Button', 'event_action': 'click'});">Buy From Amazon</a></li>

						<li class="bfk"><a href="https://www.kutchina.com/product-category/special-offer/" class="bfamazone" onClick="gtag('event', 'click', {'send_to': 'UA-106489867-1', 'event_category': 'Header Amazon Button', 'event_action': 'click'});">Buy From Kutchina</a></li>


				<li class="m_a_b">
				<a href="https://www.kutchina.com/my-account/" class="my-account-btn" onClick="">
					<span><i class="fa fa-user" aria-hidden="true"></i></span> My Account</a>
			</li>					




						<li><?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
							    $count = WC()->cart->cart_contents_count;
							    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php 
							    if ( $count > 0 ) {
							        ?>
							        <i class="fa fa-shopping-cart" aria-hidden="true" style="color: #fff; font-size: 20px;"></i>
							        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
							        <?php
							    }
							        ?></a>
							 
							<?php } ?>
						</li>
						<!--<li><?php //dynamic_sidebar('sidebar-top-header'); ?></li>-->
					   </ul>
								<!--<a href="https://www.kutchina.com/contact-us/">Contact Us</a>-->
								
							</div>
						</div>
					</div>

				</div>
			</div>
			<?php endif; ?>
			
			<div class="header home1">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<?php 
						$col_logo = '';
						if (is_active_sidebar('sidebar-text-welcome') && class_exists('Custom_Ajax_search_widget')) {
							$col_logo = '3' ;
						 
						}elseif(is_active_sidebar('sidebar-text-welcome') || class_exists('Custom_Ajax_search_widget')){
							$col_logo = '6';
						}else {
							$col_logo = '12 logo-center';
						}
						?>
						<div id="sp-logo" class="col-xs-12 col-md-<?php echo esc_attr($col_logo); ?>">
						<?php if(isset($betashop_options['logo_main']) && !empty($betashop_options['logo_main']['url'])){ ?>
							<div class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<img src="<?php echo esc_url($betashop_options['logo_main']['url']); ?>" alt="" />
								</a>
							</div>
						<?php } else if(isset($betashop_options['logo_text']) && !empty($betashop_options['logo_text'])) {?>
							<h1 class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<?php echo esc_html($betashop_options['logo_text']); ?>
								</a>
							</h1>
						<?php } else { ?>
							<div class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
									<img src="<?php echo esc_url(get_template_directory_uri() . '/images/presets/preset'. $presetopt .'/logo.gif' ); ?>" alt="" />
								</a>
							</div>
						<?php } ?>
						</div>
						
						<?php if (is_active_sidebar('sidebar-text-welcome')) : ?>
						<div class="text-top-bar col-xs-12 col-md-<?php echo (class_exists('Custom_Ajax_search_widget')) ? '4' : '6'; ?>">
							<div class="vg-welcome">
							<?php
								dynamic_sidebar( 'sidebar-text-welcome' );
							?>
							</div>
						</div>
						<?php endif; ?>						
							
						<!-- <?php if(class_exists('Custom_Ajax_search_widget')) : ?>
						<div id="sp-search-cart" style="text-align: right;" class="col-xs-12 col-md-<?php //echo (is_active_sidebar('sidebar-text-welcome')) ? '5' : '6'; ?>">
							<div class="search-toggle">
									<i class="fa fa-search"></i>
							</div>
							<div id="sp-search" class="search-sticky" style="display: inline-block;">
								<div class="vg-search home1">
									  <?php //ajax_autosuggest_form(); ?>
								</div>		
							</div>
						</div>
						<?php endif; ?> -->
                        
                        </div>
                        
                        
                  <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">      
                       			<!-- Menu -->
			<div class="sp-main-menu-wrapper home1">
						<div class="menu-wrapper">
							<div id="menu-container">
								<div id="header-menu">
									<div class="header-menu home1 visible-large">
										<?php echo wp_nav_menu(array("theme_location" => "primary","")); ?>
									</div>
									<div class="visible-small">
										<div class="mbmenu-toggler"><span class="title"><?php echo (isset($betashop_options['title_mobile_menu'])) ? esc_html($betashop_options['title_mobile_menu']) : esc_html__('Menu', 'vg-betashop'); ?></span><span class="mbmenu-icon"></span></div>
										<div class="nav-container">
											<?php wp_nav_menu(array('theme_location' => 'mobilemenu', 'container_class' => 'mobile-menu-container', 'menu_class' => 'nav-menu')); ?>
										</div>
									</div>
								</div>
								<?php if (class_exists('WC_Widget_Cart')) { ?>
								<div class="sp-cart home1">
									<div class="vg-cart">
										<?php the_widget('Custom_WC_Widget_Cart'); ?>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					
				
			</div> 
                        
                        
                 </div>       
                        

					</div>
				</div>
			</div>

		</div>
	</div>