jQuery(document).ready(function()
{
   
    jQuery("#generic_contact_submit").click(function()
    {
        var generic_contact_name= jQuery("#generic_contact_name").val();
		var generic_contact_email= jQuery("#generic_contact_email").val();
		var generic_contact_phone= jQuery("#generic_contact_phone").val();
		var generic_contact_message= jQuery("#generic_contact_message").val();
        
        var email_pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        var phone_pattern = /^[\d\s+-]+$/;
		if(generic_contact_name == '' || generic_contact_name == 'Enter your name')
		{
			//alert("aa");
			jQuery("#generic_contact_name").val("Enter your name");
			jQuery("#generic_contact_name").css('color', 'red');
			jQuery("#generic_contact_name").focus();
            return false;
			
		}
		else if(generic_contact_email == '' || generic_contact_email == 'Enter your email' || generic_contact_email == 'Invalid email')
		{
			//alert("aa");
			jQuery("#generic_contact_email").val("Enter your email");
			jQuery("#generic_contact_email").css('color', 'red');
			jQuery("#generic_contact_email").focus();
            return false;
		}
		else if(!email_pattern.test(generic_contact_email))
		{
		  
		  jQuery("#generic_contact_email").val("Invalid email");
		  jQuery("#generic_contact_email").css('color', 'red');
		  jQuery("#generic_contact_email").focus();
          return false;
		
		}	
		else if(generic_contact_phone == '' || generic_contact_phone == 'Enter your phone' || generic_contact_phone == 'Invalid phone' || generic_contact_phone == 'Invalid phone length')
		{
			//alert("aa");
			jQuery("#generic_contact_phone").val("Enter your phone");
			jQuery("#generic_contact_phone").css('color', 'red');
			jQuery("#generic_contact_phone").focus();
            return false;
			
		}
		else if(!phone_pattern.test(generic_contact_phone))
		{
		  
			jQuery("#generic_contact_phone").val("Invalid phone");
			jQuery("#generic_contact_phone").css('color', 'red');
			jQuery("#generic_contact_phone").focus();
            return false;
		
		}
        else if(generic_contact_phone.length < 8 || generic_contact_phone.length > 16 )
		{
		  
			jQuery("#generic_contact_phone").val("Invalid phone length");
			jQuery("#generic_contact_phone").css('color', 'red');
			jQuery("#generic_contact_phone").focus();
            return false;
		
		}
        else
		{
			jQuery("#ajax-loader").css('display','block');
			jQuery.ajax({		
				type: 'POST',
                url: MyAjax.ajaxurl,
                data: {"action": "contact_post_form_value", "generic_contact_name":generic_contact_name,"generic_contact_email":generic_contact_email,"generic_contact_phone":generic_contact_phone,"generic_contact_message":generic_contact_message},
                success: function(data){
                //alert(data);
                    if (data == 1) {
                       //jQuery("#ajax-loader").css('display','none');
                       
                       //swal("Thank you for contacting us. We will get back to you shortly!","","success");
                        document.getElementById("generic-book-now").reset();
                    }
                    else if(data == -1) {
                        //jQuery("#ajax-loader").css('display','none');
                       
                        //swal("Sorry! Some error occurred.","","error");
                        document.getElementById("generic-book-now").reset();
                    }
                
                }
			});
			
			
		}
        
    });
});


function remove_error_class()
{

     if(jQuery("#generic_contact_name").css("color") == "rgb(255, 0, 0)") {
        //alert("aa");
        jQuery("#generic_contact_name").val('');
        jQuery("#generic_contact_name").css('color', 'black');
    }
    
    if(jQuery("#generic_contact_email").css("color") == "rgb(255, 0, 0)") {  
        jQuery("#generic_contact_email").val('');
        jQuery("#generic_contact_email").css('color', 'black');
    }
    
    if(jQuery("#generic_contact_phone").css("color") == "rgb(255, 0, 0)") {  
        jQuery("#generic_contact_phone").val('');
        jQuery("#generic_contact_phone").css('color', 'black');
    }
    
    
    if(jQuery("#generic_contact_captcha").css("color") == "rgb(255, 0, 0)") {  
        jQuery("#generic_contact_captcha").val('');
        jQuery("#generic_contact_captcha").css('color', 'black');
    }
  
}



