<?php
session_start();
wp_enqueue_script('branch_jackson',get_template_directory_uri() . '/our-branches-admin/js/validation.js' , array( 'jquery' ));
wp_localize_script('branch_jackson', 'MyAjax', array( 'ajaxurl' => admin_url('admin-ajax.php')));


function generic_our_branch_form(){
       $dir = get_template_directory_uri();
      
	add_menu_page( 'Our Branch', 'Our Branch', 'manage_options', 'our-branch', 'branch_form_dynamic_fn',  $dir.'/our-branches-admin/images/pen.png' , 25 ); 
}
add_action( 'admin_menu', 'generic_our_branch_form' );


function generic_our_branch_option_fn() {

	add_submenu_page( 'our-branch', 'Our Branch List', 'Our Branch List', 'manage_options', 'our-branch-list','list_our_branch_fn');

     
}
add_action('admin_menu', 'generic_our_branch_option_fn');



function branch_form_dynamic_fn(){

global $wpdb;
$state_list_str = "SELECT * FROM `kut_state` where 1 "; 
$state_list = $wpdb->get_results($state_list_str, OBJECT);
//print_r($state_list); 

if(isset($_GET['statename']))
{

	$statename = $_GET['statename'];
	//echo $statename; exit;
	$single_state_str = "SELECT * FROM `kut_state` WHERE LOWER(`state_name`) = '".strtolower($statename)."' "; 
    $single_state_posts = $wpdb->get_row($single_state_str, OBJECT);
    //print_r($single_state_posts); exit;
    $state_id = $single_state_posts->id;


	$city_list_str = "SELECT * FROM `kut_city` WHERE `state_id`= ".$state_id; 
	$city_list = $wpdb->get_results($city_list_str, OBJECT);
	//print_r($city_list); 
}
else
{
	$city_list_str = "SELECT * FROM `kut_city` WHERE 1"; 
	$city_list = $wpdb->get_results($city_list_str, OBJECT);
}


?>

 <script>
 	
	function get_city_by_state()
	{
		//alert("aa");
		var state_name = $("#state").val();
		//alert(state_name);
		var url = window.location.href;
		window.location.href = url + "&statename="+state_name
	}

	function add_our_branch()
	{
		//alert("aa");
		var state = $("#state").val();
		var city = $("#city").val();
		var name = $("#name").val();
		var pincode = $("#pincode").val();
		var contact_person = $("#contact_person").val();
		var contact_number = $("#contact_number").val();
		var email = $("#email").val();
		var address = $("#address").val();
		var store_type = $("#store_type").val();
		var presence_type = $("#presence_type").val();

		var branch_edit_id = $("#branch_edit_id").val();
		

		var site_url = '<?php echo site_url(); ?>/ajax-files/add_our_branch.php';
		jQuery.ajax({		
					type: 'POST',
					url: site_url,
					data: {"state":state,"city":city,"name":name,"address":address,"pincode":pincode,"contact_person":contact_person,"contact_number":contact_number,"email":email,"store_type":store_type,"presence_type":presence_type,"branch_edit_id":branch_edit_id},
					success: function(data){
				   		//alert(data);
				   		if(data == 1) 
				   		{
				   			alert("Branch added.");
				   			window.location.href="<?php echo admin_url();?>admin.php?page=our-branch";
				   		}
				   		else if(data == 2) 
				   		{
				   			alert("Branch updated.");
							window.location.href="<?php echo admin_url();?>admin.php?page=our-branch&branch_id="+branch_edit_id;
				   		}
				   		else if(data == -1) 
				   		{
				   			alert(" not added 1.");
				   		}
				  	
				 
					}
				});

	}

</script>
<?php 
	if($_GET['branch_id'] != '')
	{
?>
<h1>Edit Our Branches</h1>
<?php } else { ?>		

<h1>Add Our Branches</h1>
<?php } ?>
<?php 
	global $wpdb;
	if($_GET['branch_id'] != '')
	{
		 $branch_det_edit = $wpdb->get_results("SELECT * FROM `kut_our_presence`  WHERE `id`= ".$_GET['branch_id']) ;
	}

	//print_r($branch_det_edit);
	//echo $branch_det_edit[0]->city;
	//echo $branch_det_edit[0]->channel_type;
	//echo $branch_det_edit[0]->dealer_type;
?>

<form>

<input type="hidden" name="branch_edit_id" id="branch_edit_id" value="<?php echo $_GET['branch_id']; ?>">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">
<select name="state" class="form-control" id="state" onchange="get_city_by_state();">
<option value="">Select State</option>
<?php 
for($i=0;$i<sizeof($state_list);$i++){
if( $_GET['branch_id'] != '' ) {
?>
<option value="<?php echo $state_list[$i]->state_name;?>" <?php if(strtolower($branch_det_edit[0]->state) == strtolower($state_list[$i]->state_name) ) echo "selected"; ?>><?php echo $state_list[$i]->state_name;?></option>

<?php } else { ?>

<option value="<?php echo $state_list[$i]->state_name;?>" <?php if( strtolower($statename) == strtolower($state_list[$i]->state_name) ) echo "selected"; ?>><?php echo $state_list[$i]->state_name;?></option>	

<?php } } ?>
</select>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">

<select name="city" class="form-control" id="city">
<option value="">Select City</option>
<?php 
for($j=0;$j<sizeof($city_list);$j++){
if( $_GET['branch_id'] != '' ) { 
?>

<option value="<?php echo $city_list[$j]->city_name;?>" <?php if(strtolower($branch_det_edit[0]->city) == strtolower($city_list[$j]->city_name) ) echo "selected"; ?>><?php echo $city_list[$j]->city_name;?></option>

<?php } else { ?>

<option value="<?php echo $city_list[$j]->city_name;?>"><?php echo $city_list[$j]->city_name;?></option>
<?php } } ?>

</select>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">
<input type="text" name="name" class="form-control" id="name" class="form-control" placeholder="Name:"  value="<?php echo $branch_det_edit[0]->name;?>">
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">
<input type="text" name="pincode" class="form-control" id="pincode" class="form-control" placeholder="Pincode:" value="<?php echo $branch_det_edit[0]->pincode;?>" >
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">
<input type="text" name="contact_person" class="form-control" id="contact_person" class="form-control" placeholder="Contact Person:" value="<?php echo $branch_det_edit[0]->contact_person;?>" >
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">
<input type="text" name="contact_number" class="form-control" id="contact_number" class="form-control" placeholder="Contact Number:" value="<?php echo $branch_det_edit[0]->contact_number;?>" >
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">
<input type="text" name="email" class="form-control" id="email" class="form-control" placeholder="Email:"  value="<?php echo $branch_det_edit[0]->email;?>">
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">
<textarea class="form-control" class="form-control"  name="address" id="address" placeholder="Address:"><?php echo $branch_det_edit[0]->address;?></textarea>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">
<select name="presence_type" class="form-control" id="presence_type">
<option value="">Presence Type</option>
<option value="distributor" <?php if($branch_det_edit[0]->dealer_type == 'distributor') { echo 'selected'; } ?> >Distributor</option>
<option value="dealer"  <?php if($branch_det_edit[0]->dealer_type == 'dealer') { echo 'selected'; } ?>>Dealer</option>

</select>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">
<select name="store_type" class="form-control" id="store_type">
<option value="">Store Type</option>
<option value="direct_channel" <?php if($branch_det_edit[0]->channel_type == 'direct_channel') { echo 'selected'; } ?>>Direct Channel</option>
<option value="retail" <?php if($branch_det_edit[0]->channel_type == 'retail') { echo 'selected'; } ?>>Retail</option>	
</select>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="form-group">
<div class="col-sm-12">
<input type="button" name="submit" value="Submit" onclick="add_our_branch();">
</div>
</div>
</div>
</form>

<?php 
} 
function list_our_branch_fn()
{
	global $wpdb;
	$branch_list_str = "SELECT * FROM `kut_our_presence` where 1 "; 
	$branch_list = $wpdb->get_results($branch_list_str, OBJECT);
	//print_r($branch_list); 


?>
<h2>Service List</h2>
<table cellpadding="0" cellspacing="0" class="details_list" style="border:1px solid #999;">
	<tr>
		<th style="border-right:1px solid #999;padding: 10px">State</th>
		<th style="border-right:1px solid #999;padding: 10px">City</th>
		<th style="border-right:1px solid #999;padding: 10px">Address</th>
		<th style="border-right:1px solid #999;padding: 5px 10px">Name</th>
		<th style="border-right:1px solid #999;padding: 5px 10px">Pin Code</th>
		<th style="border-right:1px solid #999;padding: 5px 10px">Contact Person</th>
		<th style="border-right:1px solid #999;padding: 5px 10px">Contact Number</th>
		<th style="border-right:1px solid #999;padding: 5px 10px">Email</th>
		<th style="border-right:1px solid #999;padding: 5px 0px">Presence Type</th>
		<th style="border-right:1px solid #999;padding: 5px 10px">Store Type</th>
		<th style="padding: 10px">Action</th>
	</tr>
<?php 
	for($i=0;$i<sizeof($branch_list);$i++)
	{
?>
<tr>
	<td style=" border-top:1px solid #999;  border-right:1px solid #999; padding: 5px 10px"><?php echo $branch_list[$i]->state;?></td>
	<td style=" border-top:1px solid #999;  border-right:1px solid #999;padding: 5px 10px"><?php echo $branch_list[$i]->city;?></td>
	<td style=" border-top:1px solid #999; border-right:1px solid #999;padding: 5px 10px"><?php echo $branch_list[$i]->address;?></td>
	<td style=" border-top:1px solid #999;  border-right:1px solid #999;padding: 5px 10px"><?php echo $branch_list[$i]->name;?></td>
	<td style=" border-top:1px solid #999;  border-right:1px solid #999;padding: 5px 10px"><?php echo $branch_list[$i]->pin_code;?></td>
	<td style=" border-top:1px solid #999;  border-right:1px solid #999;padding: 5px 10px"><?php echo $branch_list[$i]->contact_person;?></td>
	<td style=" border-top:1px solid #999;  border-right:1px solid #999;padding: 5px 10px"><?php echo $branch_list[$i]->contact_number;?></td>
	<td style=" border-top:1px solid #999;  border-right:1px solid #999;padding: 5px 10px"><?php echo $branch_list[$i]->email;?></td>
	<td style=" border-top:1px solid #999;  border-right:1px solid #999;padding: 5px 10px"><?php echo $branch_list[$i]->dealer_type;?></td>
	<td style=" border-top:1px solid #999; border-right:1px solid #999;padding: 5px 10px"><?php echo $branch_list[$i]->channel_type;?></td>

	<td style=" border-top:1px solid #999;padding: 5px 10px"><a href="<?php echo admin_url();?>admin.php?page=our-branch&branch_id=<?php echo $branch_list[$i]->id; ?>" target="_blank">Edit</a> </td>
</tr>
<?php	
	}
?>
</table>
<?php } ?>