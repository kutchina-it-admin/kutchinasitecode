<?php
/**
 * @version    1.2
 * @package    VG Betashop
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables(); 

// HieuJa get Preset Color Option
$presetopt = betashop_get_preset();
// HieuJa end block

?>


		<div class="footer_section1">
		  <div class="container">
		    <!--<div class="row get_touch">
		      <h2>Get in touch</h2>
		    </div>-->
		    <div class="row">
		      <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 quick_enquiry fbox">
		        <h2>Quick Enquiry</h2>        
		        <?php echo do_shortcode("[contact-form-7 id='6563' title='Quick Enquiry']"); ?>
		      </div>-->
              
              	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fbox2">
                <div class="logo-footer"><a><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/logo-footer.png" alt=""></a></div>
                <div class="desk_topm">
		        <ul class="link_fp">
		        
		          	<?php $defaults = array(
				                'theme_location'  => '',
				                'menu'            => 'Menu Copyrights', 
				                'container'       => '', 
				                'container_class' => 'menu-{menu slug}-container', 
				                'container_id'    => '',
				                //'menu_class'      => 'menu', 
				                'menu_id'         => '',
				                'echo'            => true,
				                'fallback_cb'     => 'wp_page_menu',
				                'before'          => '',
				                'after'           => '',
				                'link_before'     => '',
				                'link_after'      => '',
				                'items_wrap'      => '<li id="%1$s">%3$s</li>',
				                //'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
				                'depth'           => 0,
				                'walker'          => ''
				            ); ?>
				   	<?php wp_nav_menu( $defaults ); ?>
                      <!-- <li><?php //echo betashop_copyright(); ?></li> -->
                      <li>© <script>
                     var theDate=new Date()
                     document.write(theDate.getFullYear());
                  </script>  Kutchina. All rights reserved.</li>
		        </ul>
		        </div>
                </div>
              
              
		      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 products fbox">
		        <h2>Products</h2>
		        <ul class="products_list">
		          <li>
		          	<a href="<?php echo home_url( '/' ); ?>product-category/appliances/">
		          		<span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Large Appliances
		          	</a>
		          </li>
		          <li>
		          	<a href="<?php echo home_url( '/' ); ?>product-category/small-appliances/">
		          		<span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Small Appliances
		          	</a>
		          </li>
		          <li>
		          	<a href="<?php echo home_url( '/' ); ?>product-category/modular-kitchens/">
		          		<span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Modular Kitchens
		          	</a>
		          </li>
		          <li>
		          	<a href="<?php echo home_url( '/' ); ?>product-category/water-purifiers/">
		          		<span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Water Purifiers
		          	</a>
		          </li>
		          <li>
		          	<a href="<?php echo home_url( '/' ); ?>product-category/corporate-products/">
		          		<span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Corporate Products
		          	</a>
		          </li>
		        </ul>
		      </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 products fbox">
              	<h2>Customer Support</h2>
		        <ul class="products_list">
		          <li>
		          	<a href="<?php echo home_url( '/' ); ?>our-presence/#menu1">
		          		<span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Service & Support
		          	</a>
		          </li>
		          <li>
		          	<a href="<?php echo home_url( '/' ); ?>faq/">
		          		<span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>FAQ
		          	</a>
		          </li>
		          <li>
		          	<a href="<?php echo home_url( '/' ); ?>refund-policy-for-kutchina/">
		          		<span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Return & Refund Policy
		          	</a>
		          </li>
		        </ul>
                </div>
                

                
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fbox2">
                 
		        <ul class="social_link">
                <p class="mail_footer">
                	<a> Email : customercare@kutchina.com </a></p>
		          <li><a href="https://www.facebook.com/kutchinaconnect/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
		          <li><a href="https://twitter.com/kutchinaconnect" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
		          <li><a href="https://www.instagram.com/kutchinaconnectofficial/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
		          <li><a href="https://www.linkedin.com/company/kutchinaconnect/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
		          <li><a href="https://www.youtube.com/channel/UCAndsH2MCkpohS36lJafNGA" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
		        </ul>
		      </div>
              
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 fbox2">
                <p class="decleration_pra">
Kutchina Home Makers Private Limited(formerly known as Bajoria Entertainment Private Limited)<br/> Corporate Identification Number  - <strong>U92190WB2008PTC130401</strong>, Registered Office Address - Bajoria Tower, 2nd Floor, 20, Chinar Park, Kolkata 700157, West Bengal, India<br/> Telephone/Fax – <a href="tel:913340500300">+91 33 4050 0300</a>, Email – <a href="mailto:info@kutchina.com">info@kutchina.com</a>, Website – <a href="https://www.kutchina.com/">www.kutchina.com</a></p>
		      </div>
                
		      <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fbox">
		        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7365.563489992473!2d88.439901!3d22.624624!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf0a7af2c956f2c72!2sBajoria%20Entertainment%20Pvt.%20Ltd!5e0!3m2!1sen!2sin!4v1584614154072!5m2!1sen!2sin" width="100%" height="470" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
		      </div>-->


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 link_fp_mobile">
		        <ul class="link_fp">
		        
		          	<?php $defaults = array(
				                'theme_location'  => '',
				                'menu'            => 'Menu Copyrights', 
				                'container'       => '', 
				                'container_class' => 'menu-{menu slug}-container', 
				                'container_id'    => '',
				                //'menu_class'      => 'menu', 
				                'menu_id'         => '',
				                'echo'            => true,
				                'fallback_cb'     => 'wp_page_menu',
				                'before'          => '',
				                'after'           => '',
				                'link_before'     => '',
				                'link_after'      => '',
				                'items_wrap'      => '<li id="%1$s">%3$s</li>',
				                //'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
				                'depth'           => 0,
				                'walker'          => ''
				            ); ?>
				   	<?php wp_nav_menu( $defaults ); ?>
                      <!-- <li><?php //echo betashop_copyright(); ?></li> -->
                      <li>© <script>
                     var theDate=new Date()
                     document.write(theDate.getFullYear());
                  </script> Kutchina. All rights reserved.</li>
		        </ul>

</div>






		    </div>
		  </div>
		</div>



		<?php //if(isset($betashop_options['copyright_show']) && $betashop_options['copyright_show']) : ?>
<!--		<div class="footer_section2">
		  <div class="container">
		    <div class="row">
		      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 fbox2">
		        <ul class="link_fp">
		          <li><?php echo betashop_copyright(); ?></li>
		          	<?php $defaults = array(
				                'theme_location'  => '',
				                'menu'            => 'Menu Copyrights', 
				                'container'       => '', 
				                'container_class' => 'menu-{menu slug}-container', 
				                'container_id'    => '',
				                //'menu_class'      => 'menu', 
				                'menu_id'         => '',
				                'echo'            => true,
				                'fallback_cb'     => 'wp_page_menu',
				                'before'          => '',
				                'after'           => '',
				                'link_before'     => '',
				                'link_after'      => '',
				                'items_wrap'      => '<li id="%1$s">%3$s</li>',
				                //'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
				                'depth'           => 0,
				                'walker'          => ''
				            ); ?>
				   	<?php wp_nav_menu( $defaults ); ?>
		        </ul>
		        <div class="logo-footer"><a><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/logo-footer.png" alt=""></a></div>
		      </div>
		      <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 fbox2">
		        <ul class="social_link">
		          <li><a href="https://www.facebook.com/kutchinaconnect/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
		          <li><a href="https://twitter.com/kutchinaconnect"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
		          <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
		        </ul>
		      </div>
		    </div>
		  </div>
		</div>-->
		<?php //endif; ?>



<!-- Category Page Google Sheet Module by Suman START -->

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Enquiry Form</h4>
      </div>
      <div class="modal-body">

        <form id="form" onSubmit="" method="post" name="form1" class="form1">

        <div class="form-group">		  
		  <label for="name">Name:</label>
		  <input type="text" name="user_name" id="user_name" class="form-control" required>
		</div>


		<div class="form-group">
		  <label for="phone">Phone:</label>
		  <input type="text" name="user_phone" id="user_phone" class="form-control" required>
		</div>

		<div class="form-group">
		  <label for="phone">Pin Code:</label>
		  <input type="text" name="user_pincode" id="user_pincode" class="form-control" required>
		</div>
		<input type="submit" value="SUBMIT" class="submit-contact btn btn-info" class="btn btn-default">

		<p style="color:red; clear: both; font-weight: bold; padding: 10px 0 0;" class="form-status"></p>

		</form>

		<script type="text/javascript">
		  jQuery('.form1').submit(function(event) {

		  jQuery('.form-status').html("");
		    var send_data= jQuery('.form1').serialize();
		    jQuery.ajax({
		      url: 'https://www.kutchina.com/wp-content/themes/vg-betashop/category-quote.php',
		      type: 'POST',
		      dataType: 'json',
		      data: send_data,
		    })
		    .done(function(xhr) {
		      console.log("success");
		      if (xhr.status==1) {
		      jQuery('.form-status').append('Thank you very much for your mail. We will get back to you soon.');
		      //window.location.assign("thank-you.html");
		      alert('Thank you for your submission. We will get back to you soon.');

		      jQuery("#form")[0].reset();
		      }
		      
		      else{
		      jQuery('.form-status').append('Thank you for your submission. We will get back to you soon.');
		      //window.location.assign("thank-you.html");
		      alert('Thank you for your submission. We will get back to you soon.');
		      jQuery("#form")[0].reset();
		      };
		      })
		    .fail(function() {
		      console.log("error");
		    })
		    .always(function() {
		      console.log("complete");
		    });
		    event.preventDefault();
		  });
		</script>



		<script>
var  viewer, container,progress;

var lookAtPositions = [
    new THREE.Vector3(4871.39, 0, 0),
    new THREE.Vector3(0, 0, 0),
    new THREE.Vector3(0, -0.5, 0),
    new THREE.Vector3(0, -0.5, 0),
    new THREE.Vector3(0, 0.8, 0),


];

var infospotPositions = [
    new THREE.Vector3(3136.06, 1216.30, -1390.14),
    new THREE.Vector3(3258.81, 600, 0),
    new THREE.Vector3(3136.06, -400, 2600),
    new THREE.Vector3(3136.06, -1500, 2600),
    new THREE.Vector3(3136.06, -500, -1390.14),

];

container = document.querySelector('#container');
viewer = new PANOLENS.Viewer({ output: 'console',cameraFov:75, container: container,autoHideInfospot: false});

panorama = new PANOLENS.ImagePanorama('wp-content/360/bloom_3601.jpg');
panorama.addEventListener('enter-fade-start', function () {
    document.getElementById("desc-container1").style.display = "none";
    viewer.tweenControlCenter(lookAtPositions[0], 0);
});

panorama2 = new PANOLENS.ImagePanorama('wp-content/360/chimney and stove1.jpg');

panorama2.addEventListener('enter-fade-start', function () {

    viewer.tweenControlCenter(lookAtPositions[4], 0);
});
panorama5 = new PANOLENS.ImagePanorama('wp-content/360/chimney and stove1.jpg');
panorama5.addEventListener('enter-fade-start', function () {
    viewer.tweenControlCenter(lookAtPositions[2], 0);
});
panorama3 = new PANOLENS.ImagePanorama('wp-content/360/microvave and dishwasher1.jpg');
panorama3.addEventListener('enter-fade-start', function () {
    viewer.tweenControlCenter(lookAtPositions[1], 0);
});
panorama4 = new PANOLENS.ImagePanorama('wp-content/360/microvave and dishwasher1.jpg');
panorama4.addEventListener('enter-fade-start', function () {
    viewer.tweenControlCenter(lookAtPositions[3], 0);
});

panorama.link(panorama2, infospotPositions[0]);
panorama.link(panorama3, infospotPositions[2]);
panorama.link(panorama4, infospotPositions[3]);
panorama.link(panorama5, infospotPositions[4]);

panorama2.link(panorama, new THREE.Vector3(4000, 800, 520));
panorama3.link(panorama, infospotPositions[1]);
panorama4.link(panorama, infospotPositions[1]);
panorama5.link(panorama, infospotPositions[1]);

infospot = new PANOLENS.Infospot( 350, PANOLENS.DataImage.Info );
infospot.position.set( 2000, -2500, 100 );
infospot.addHoverElement( document.getElementById( 'desc-container' ), 200 );
panorama5.add( infospot );

infospot1 = new PANOLENS.Infospot( 350, PANOLENS.DataImage.Info );
infospot1.position.set( 4000, 800, 0 );
infospot1.addHoverElement( document.getElementById( 'desc-container1' ), 200 );
panorama2.add( infospot1 );

infospot2 = new PANOLENS.Infospot( 350, PANOLENS.DataImage.Info );
infospot2.position.set( 4000, -2000, 0 );
infospot2.addHoverElement( document.getElementById( 'desc-container2' ), 200 );
panorama3.add( infospot2 );

infospot3 = new PANOLENS.Infospot( 350, PANOLENS.DataImage.Info );
infospot3.position.set( 2000, -2000, 0 );
infospot3.addHoverElement( document.getElementById( 'desc-container3' ), 200 );
panorama4.add( infospot3 );

viewer.add(panorama, panorama2,panorama3,panorama4,panorama5);
</script>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>


  </div>
</div>

<!-- Category Page Google Sheet Module by Suman END -->


<!-- Zoho Desk Chat -->
<script type="text/javascript">var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"e8e4f602aa5edc51dc202b69cc12c9c7bed8229a7d27b30788e824cd86073fc6", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.in/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");</script>


			
	</div><!-- .wrapper -->
	<div class="to-top"><i class="fa fa-chevron-up"></i></div>
	<?php wp_footer(); ?>

	<script type="text/javascript">
		 jQuery(".gt_up_call").click(function() {
          jQuery('html, body').animate({
            scrollTop: jQuery(".ask_sec").offset().top
          }, 500);
         });
	</script>
</body>
</html>