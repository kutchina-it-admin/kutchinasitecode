<?php
session_start();
wp_enqueue_script('exclusive_jackson',get_template_directory_uri() . '/exclusive-stores-admin/js/validation.js' , array( 'jquery' ));
wp_localize_script('exclusive_jackson', 'MyAjax', array( 'ajaxurl' => admin_url('admin-ajax.php')));


function generic_our_exclusive_form(){
       $dir = get_template_directory_uri();
      
	add_menu_page( 'Exclusive Store', 'Exclusive Store', 'manage_options', 'exclusive-store', 'exclusive_form_dynamic_fn',  $dir.'/exclusive-stores-admin/images/pen.png' , 25 ); 
}
add_action( 'admin_menu', 'generic_our_exclusive_form' );


function generic_our_exclusive_option_fn() {

	add_submenu_page( 'exclusive-store', 'Exclusive List', 'Exclusive List', 'manage_options', 'exclusive-list','list_our_exclusive_fn');

     
}
add_action('admin_menu', 'generic_our_exclusive_option_fn');



function exclusive_form_dynamic_fn(){

global $wpdb;
$state_list_str = "SELECT * FROM `kut_state` where 1 "; 
$state_list = $wpdb->get_results($state_list_str, OBJECT);
//print_r($state_list); 

if(isset($_GET['statename']))
{

	$statename = $_GET['statename'];
	//echo $statename; exit;
	$single_state_str = "SELECT * FROM `kut_state` WHERE LOWER(`state_name`) = '".strtolower($statename)."' "; 
    $single_state_posts = $wpdb->get_row($single_state_str, OBJECT);
    //print_r($single_state_posts); exit;
    $state_id = $single_state_posts->id;


	$city_list_str = "SELECT * FROM `kut_city` WHERE `state_id`= ".$state_id; 
	$city_list = $wpdb->get_results($city_list_str, OBJECT);
	//print_r($city_list); 
}
else
{
	$city_list_str = "SELECT * FROM `kut_city` WHERE 1"; 
	$city_list = $wpdb->get_results($city_list_str, OBJECT);
}


?>

 <script>
 	
	function get_city_by_state()
	{
		//alert("aa");
		var state_name = $("#state").val();
		//alert(state_name);
		var url = window.location.href;
		window.location.href = url + "&statename="+state_name
	}

	function add_our_exclusive()
	{
		//alert("aa");
		var state = $("#state").val();
		var city = $("#city").val();
		var address = $("#address").val();
		var incharges_names = $("#incharges_names").val();
		var contact_no = $("#contact_no").val();
		var mail_id_incharges = $("#mail_id_incharges").val();
		
		var exclusive_edit_id = $("#exclusive_edit_id").val();
		

		var site_url = '<?php echo site_url(); ?>/ajax-files/add_exclusive_store.php';
		jQuery.ajax({		
					type: 'POST',
					url: site_url,
					data: {"state":state,"city":city,"address":address,"incharges_names":incharges_names,"contact_no":contact_no,"mail_id_incharges":mail_id_incharges,"exclusive_edit_id":exclusive_edit_id},
					success: function(data){
				   		//alert(data);
				   		if(data == 1) 
				   		{
				   			alert("Exclusive added.");
				   			window.location.href="<?php echo admin_url();?>admin.php?page=exclusive-store";
				   		}
				   		else if(data == 2) 
				   		{
				   			alert("Exclusive updated.");
							window.location.href="<?php echo admin_url();?>admin.php?page=exclusive-store&exclusive_id="+exclusive_edit_id;
				   		}
				   		else if(data == -1) 
				   		{
				   			alert(" not added.");
				   		}
				  	
				 
					}
				});

	}

</script>
<?php 
	if($_GET['exclusive_id'] != '')
	{
?>
<h1>Edit Our Exclusive Stores</h1>
<?php } else { ?>		

<h1>Add Our Exclusive Stores</h1>
<?php } ?>
<?php 
	global $wpdb;
	if($_GET['exclusive_id'] != '')
	{
		 $exclusive_det_edit = $wpdb->get_results("SELECT * FROM `kut_exclusive`  WHERE `id`= ".$_GET['exclusive_id']) ;
	}

	//print_r($branch_det_edit);
	//echo $branch_det_edit[0]->city;
	//echo $branch_det_edit[0]->channel_type;
	//echo $branch_det_edit[0]->dealer_type;
?>
		<form>
			<input type="hidden" name="exclusive_edit_id" id="exclusive_edit_id" value="<?php echo $_GET['exclusive_id']; ?>">
						<div class="form-group">
                            <div class="col-sm-12">
                              <select name="state" id="state" onchange="get_city_by_state();">
                              	<option value="">Select State</option>
                              	<?php 
                              		for($i=0;$i<sizeof($state_list);$i++){
                              		if( $_GET['exclusive_id'] != '' ) {
                              	?>
                              	<option value="<?php echo $state_list[$i]->state_name;?>" <?php if(strtolower($exclusive_det_edit[0]->state) == strtolower($state_list[$i]->state_name) ) echo "selected"; ?>><?php echo $state_list[$i]->state_name;?></option>

                              	<?php } else { ?>

                              	<option value="<?php echo $state_list[$i]->state_name;?>" <?php if( strtolower($statename) == strtolower($state_list[$i]->state_name) ) echo "selected"; ?>><?php echo $state_list[$i]->state_name;?></option>	

                              	<?php } } ?>
                              </select>
                            </div>
                        </div>

				        <div class="form-group">
                            <div class="col-sm-12">
                            
                             <select name="city" id="city">
                              	<option value="">Select City</option>
                              	<?php 
                              		for($j=0;$j<sizeof($city_list);$j++){
                              		if( $_GET['exclusive_id'] != '' ) { 
                              	?>

                              	<option value="<?php echo $city_list[$j]->city_name;?>" <?php if(strtolower($exclusive_det_edit[0]->city) == strtolower($city_list[$j]->city_name) ) echo "selected"; ?>><?php echo $city_list[$j]->city_name;?></option>

                              	<?php } else { ?>

                              	<option value="<?php echo $city_list[$j]->city_name;?>"><?php echo $city_list[$j]->city_name;?></option>
                              	<?php } } ?>

                              </select>
                            </div>
                        </div>

                         


                         <div class="form-group">
                            <div class="col-sm-12">
                              <textarea class="form-control"  name="address" id="address" placeholder="Address:"><?php echo $exclusive_det_edit[0]->address;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                              <textarea class="form-control"  name="incharges_names" id="incharges_names" placeholder="Incharge Name:"><?php echo $exclusive_det_edit[0]->incharges_names;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                              <textarea class="form-control"  name="contact_no" id="contact_no" placeholder="Contact No:"><?php echo $exclusive_det_edit[0]->contact_no;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                              <textarea class="form-control"  name="mail_id_incharges" id="mail_id_incharges" placeholder="Incharge Mail Id:"><?php echo $exclusive_det_edit[0]->mail_id_incharges;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                             <input type="button" name="submit" value="Submit" onclick="add_our_exclusive();">
                            </div>
                        </div>

		</form>

<?php 
} 
function list_our_exclusive_fn()
{
	global $wpdb;
	$exclusive_list_str = "SELECT * FROM `kut_exclusive` where 1 "; 
	$exclusive_list = $wpdb->get_results($exclusive_list_str, OBJECT);
	//print_r($exclusive_list); 


?>
<h2>Service List</h2>
<table>
	<tr>
		<th>State</th>
		<th>City</th>
		<th>Address</th>
		<th>Incharge Name</th>
		<th>Contact No</th>
		<th>Mail ID of Incharge</th>
		<th>Action</th>
	</tr>
<?php 
	for($i=0;$i<sizeof($exclusive_list);$i++)
	{
?>
<tr>
	<td><?php echo $exclusive_list[$i]->state;?></td>
	<td><?php echo $exclusive_list[$i]->city;?></td>
	<td><?php echo $exclusive_list[$i]->address;?></td>
	<td><?php echo $exclusive_list[$i]->incharges_names;?></td>
	<td><?php echo $exclusive_list[$i]->contact_no;?></td>
	<td><?php echo $exclusive_list[$i]->mail_id_incharges;?></td>


	<td><a href="<?php echo admin_url();?>admin.php?page=exclusive-store&exclusive_id=<?php echo $exclusive_list[$i]->id; ?>" target="_blank">Edit</a> </td>
</tr>
<?php	
	}
?>
</table>
<?php } ?>