<?php
/**
 * Template Name: AMC Term & Condition
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<style>
	.ac_btn{background: #0065b2;
	font-size: 16px;
	color: #fff;
	padding: 7px 15px;
	margin-bottom: 20px;
	display: block;
	width: 163px;
	margin: 20px auto 20px auto;}
	.ac_btn:hover{ color: #fff; background:#0c8aea;}

	.tc_contant{}
	.tc_contant p{}
	.tc_contant p span{ color:#000; margin: 0 5px 0 0; }

</style>
<div class="main-container front-page about-page">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div>

		
	<div class="full-wrapper about-us-wrapper">
    <div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-content">
					<?php while (have_posts()) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php the_content(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post -->
					<?php endwhile; // end of the loop. ?>


				


<!-- <?php
	if ($_SERVER["REQUEST_METHOD"] == "POST"){
		$get_amc_prod = $_POST['kut_mod_idd_hid'];		
	}
?> -->


<div class="tc_contant">
<p><span>1.</span> This contract is not transferable and shall cover the Equipment here in only at the location mentioned herein, subject however to clause no.6 and this contract shall cease forthwith if the customer ceases to be the owner of the set.</p>

<p><span>2.</span> It is to be noted that the equipment is in both electrical and mechanical working condition on the date of this agreement. During the tenure of this agreement, the company undertakes to perform the following services as a part of its obligations under this agreement either by company or through any of its authorized franchise service centre, the Company will provide 3 free service 
per year for all Chimney models and will repair the system as and when required.</p>

<p><span>3.</span> In the event of contract rate being paid by cheque, the contract shall commence subject to realization of the cheque and from the date of realization, thereof.</p>

<p><span>4.</span> For 1st, 2nd & 3rd years AMC(with spare parts) of all Chimney models, it is inclusive of all spares excluding consumables parts like Incandescent lamp/bulb, Knobs, any charcoal filters, upper cabinet, lower cabinet, front panel, vane, vane guard, Glass, oil collectors, accessories & any other metal/rubber/plastic items not mentioned above. The AMC contract is not covered to damages caused by normal wear & tear. Also parts like water container, cooking vessels, electric cord and plug & fittings used for 
installation of the products are not covered by this contract.</p>

<p><span>5.</span> For any complain or service the customer has to call us at our customer care Toll Free No. 1800 4197333 within the validity of the AMC or mail us at customercare@kutchina.com.</p>

<p><span>6.</span> In the event on any changes in address, the customer must inform the company 2 weeks in advance giving full details of his/her new address. In such cases the company shall continue to provide maintenance services to the customer's equipment at his/her new address provided the changed location is in the city or town with in the territory of the company's service centrels. If the customer for any reason whatsoever fails to inform the company about his/her change in the address, the company shall not be responsible and/or liable for the consequences arising there from.</p>

<p><span>7.</span> If the company is of the opinion that the servicing of the equipment needs to be done at the service centre of the company, then the equipment shall be transported to the said service centre for servicing and cost of transportation shall be borne by the customer.</p>

<p><span>8.</span> This product is designed purely for domestic use by private individuals at home only.</p>

<p><span>9.</span> The customer shall not directly or indirectly, during the subsistence of this agreement, open, alter, tamper with or in any way do any act which would result in alterations, and or changes in the internal operations of the Equipment.</p>

<p><span>10.</span> During the tenure/period of this contract the customer shall not entrust the servicing of the said equipment to any outside agency or person other than the company without prior written permission from the company. If it is found that the set is tampered/mishandled then the contract will be terminated and the customer cannot make any claim from the company.</p>

<p><span>11.</span> The liability of the company under the contract is restricted to problem arising out of the normal functioning of the unit. The contract does not cover the malfunctioning of the equipment or damage caused due to external factor, electrically short circuit, accident, fire or burning, riots, acts of god, voltage fluctuation etc.</p>

<p><span>12.</span> All defective parts replaces shall be company's absolute property. The company shall be at liberty to deal with the said parts in any manner deemed fit without any obligation to return the said parts to the customer.</p>

<p><span>13.</span> If any time during the tenure of the contract it is found that the serial number appearing on the equipment is defaced in any manner and/or is not legible, the company shall have the right to discontinue the contract with immediate effect. In the event, the customer shall not be entitled for any claim against the company.</p>

<p><span>14.</span> The company shall not be liable or deemed to be under default or failure in performance when such failure or default arises direct or indirectly from causes beyond its responsible control including delay in servicing due to non-availability of any components parts, and/or accessories and/or if the company is preventing from performing its obligation under this contract. All disputes are subjected to Kolkata Jurisdiction only. I/We have read and understood the rules and regulations of the service(s) opted for and agree to abide by the terms and conditions relating to the conduct thereof as also any changes brought about therein from time to time. A copy of the rules and regulations has been made available to me by the mode of a tear off. The company is entitled to amend the "Terms and Conditions" from time to time. </p>

<p><strong>MANDATORY CHECKS DURING AMC PERIOD</strong></p> 
<p>1. CLEAN BODY (OUTSIDE/INSIDE) </p>
<p>2. CLEAN BLOWER 3. CLEAN OIL CHANNEL </p>
<p>4. CLEAN OIL COLLECTORS 5. CHECK ALL THE FUNCTIONS OF THE MACHINE </p>

<!-- <form class="form-inline form_box1" id="" method="POST" action="https://www.kutchina.com/package/">
<input type="hidden" class="form-control nomar" id="kut_mod_name_hid" name="kut_mod_name_hid" value="<?php echo $get_amc_prod; ?>">
<button type="submit" class="ac_btn">Agree & Continue</button>
</form> -->

</div>
				

            	</div>


			</div>
            
            
		</div>
	</div>
</div>
</div>
<?php betashop_get_footer(); ?>