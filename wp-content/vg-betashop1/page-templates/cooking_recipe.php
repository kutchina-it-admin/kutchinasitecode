<?php
/**
 * Template Name: Cooking Recipe Page Template
 *
 * Description: Cooking Recipe Page Template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>

<link rel='stylesheet' href='https://www.kutchina.com/wp-content/themes/vg-betashop/css/new_css.css' type='text/css' media='all' />


<div class="main-container front-page about-page" style="display:none;">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<!--<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php //the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div>-->
	
</div>

<div class="inner_banner_section">
  <div class="product_banner"><img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/inner_banner.png" alt=""></div>  
</div>

<div class="section33">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="right_panel_text">
          <h2>Cooking Recipe</h2>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="section34 s3_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="exTab1">	
            <ul  class="nav nav-pills">
                <li class="active"><a  href="#1a" data-toggle="tab">Recipes by Kutchina</a></li>
                <li><a href="#2a" data-toggle="tab">Recipes by Visitors</a></li>
            </ul>
        
            <div class="tab-content clearfix">
                <div class="tab-pane active" id="1a">
                	<div class="row">
                        <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="image_name_maker">
                                <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/recip_img.png" alt="" class="thumb_image_recip" data-toggle="modal" data-target="#myModal">
                                <div class="name_and_respname">
                                    <p class="recip_name">Jol Paan</p>
                                    <p class="recip_by_name">by Kutchina</p>
                                </div>
                            </div>
                        
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">
                        
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Jol Paan – Perfect For Busy Days</h4>
                                    </div>
                                    <div class="modal-body">
                                      <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/recip_img.png" alt="" class="thumb_image_recip">
                                      <p class="popup_recip_name">Jol Paan – Perfect For Busy Days</p>
                                      <p class="popup_resp_by_name">Recipe by Kutchina</p>
                                      <p class="popup_resp_desc"><strong>How To Prepare</strong></p>
                                      <p class="popup_resp_desc">Wash the flat rice and transfer it to a large bowl.</p>
                                      <p class="popup_resp_desc">Add the curd, mashed banana, and jaggery powder.</p>
                                      <p class="popup_resp_desc">Mix well, and your Assamese breakfast is ready!</p>
                                      <table class="popup_table">
                                        <tr>
                                            <th>Ingredients</th>
                                            <th>Quantity</th>
                                        </tr>
                                        <tr>
                                            <td>Flat rice</td>
                                            <td>2⁄3 cup</td>
                                        </tr>
                                        <tr>
                                            <td>Curd</td>
                                            <td>1 cup</td>
                                        </tr>
                                        <tr>
                                            <td>Mashed banana</td>
                                            <td>2 Pcs</td>
                                        </tr>
                                        <tr>
                                            <td>Jaggery powder</td>
                                            <td>3-4 tablespoons</td>
                                        </tr>
                                      </table>
                                    </div>
                                  </div>
                                  
                                </div>
                            </div>
                        </div>-->
                        
                        <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="image_name_maker">
                                <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/recip_img.png" alt="" class="thumb_image_recip" data-toggle="modal" data-target="#myModal1">
                                <div class="name_and_respname">
                                    <p class="recip_name">Fried Egg And Leftover Salad Open Sandwich</p>
                                    <p class="recip_by_name">by Kutchina</p>
                                </div>
                            </div>
                        
                            <div class="modal fade" id="myModal1" role="dialog">
                                <div class="modal-dialog">
                        
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Fried Egg And Leftover Salad Open Sandwich</h4>
                                    </div>
                                    <div class="modal-body">
                                      <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/recip_img.png" alt="" class="thumb_image_recip">
                                      <p class="popup_recip_name">Fried Egg And Leftover Salad Open Sandwich</p>
                                      <p class="popup_resp_by_name">Recipe by Kutchina</p>
                                      <p class="popup_resp_desc"><strong>How To Prepare</strong></p>
                                      <p class="popup_resp_desc">Toast the slice of bread in a toaster.</p>
                                      <p class="popup_resp_desc">Heat the olive oil in a pan and crack the egg open.</p>
                                      <p class="popup_resp_desc">Sprinkle a little salt on the egg.</p>
                                      <p class="popup_resp_desc">While the egg’s cooking, place a handful of the leftover</p>
                                      <p class="popup_resp_desc">salad on the toast.</p>
                                      <p class="popup_resp_desc">Season it with salt and pepper if required.</p>
                                      <p class="popup_resp_desc">Now, place the egg on top.</p>
                                      <table class="popup_table">
                                        <tr>
                                            <th>Ingredients</th>
                                            <th>Quantity</th>
                                        </tr>
                                        <tr>
                                            <td>Egg</td>
                                            <td>1 pc</td>
                                        </tr>
                                        <tr>
                                            <td>Wheat bread</td>
                                            <td>1 slice</td>
                                        </tr>
                                        <tr>
                                            <td>Salt</td>
                                            <td>1 pinch</td>
                                        </tr>
                                        <tr>
                                            <td>Pepper</td>
                                            <td>1 pinch</td>
                                        </tr>
                                        <tr>
                                            <td>Olive oil</td>
                                            <td>1 teaspoon</td>
                                        </tr>
                                      </table>
                                    </div>
                                  </div>
                                  
                                </div>
                            </div>
                        </div>-->
                        
                        <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="image_name_maker">
                                <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/recip_img.png" alt="" class="thumb_image_recip" data-toggle="modal" data-target="#myModal2">
                                <div class="name_and_respname">
                                    <p class="recip_name">Peanut Butter Oatmeal</p>
                                    <p class="recip_by_name">by Kutchina</p>
                                </div>
                            </div>
                        
                            <div class="modal fade" id="myModal2" role="dialog">
                                <div class="modal-dialog">
                        
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Peanut Butter Oatmeal</h4>
                                    </div>
                                    <div class="modal-body">
                                      <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/recip_img.png" alt="" class="thumb_image_recip">
                                      <p class="popup_recip_name">Peanut Butter Oatmeal</p>
                                      <p class="popup_resp_by_name">Recipe by Kutchina</p>
                                      <p class="popup_resp_desc"><strong>How To Prepare</strong></p>
                                      <p class="popup_resp_desc">In a small saucepan, bring water and salt to a boil. Stir
                        in oats; cook 5 minutes over medium heat, stirring occasionally. Transfer oatmeal to bowls; stir in peanut butter, honey, cinnamon and, if desired, apple.</p>
                                      <table class="popup_table">
                                        <tr>
                                            <th>Ingredients</th>
                                            <th>Quantity</th>
                                        </tr>
                                        <tr>
                                            <td>Water</td>
                                            <td>1-3/4 cups</td>
                                        </tr>
                                        <tr>
                                            <td>Salt</td>
                                            <td>1/8 teaspoon</td>
                                        </tr>
                                        <tr>
                                            <td>Oats</td>
                                            <td>1 cup old-fashioned</td>
                                        </tr>
                                        <tr>
                                            <td>Creamy Peanut Butter</td>
                                            <td>2 tablespoons</td>
                                        </tr>
                                        <tr>
                                            <td>Honey</td>
                                            <td>2 tablespoons</td>
                                        </tr>
                                        <tr>
                                            <td>Ground Cinnamon</td>
                                            <td>1/2 to 1 teaspoon</td>
                                        </tr>
                                        <tr>
                                            <td>Chopped Apple</td>
                                            <td>Optional</td>
                                        </tr>
                                      </table>
                                    </div>
                                  </div>
                                  
                                </div>
                        
                            </div>
                        </div>-->
                        
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="image_name_maker">
                                <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/besan_laddu.png" alt="" class="thumb_image_recip" data-toggle="modal" data-target="#myModal3">
                                <div class="name_and_respname">
                                    <p class="recip_name">Microwave Besan Ladoo</p>
                                    <p class="recip_by_name">by Kutchina</p>
                                </div>
                            </div>
                        
                            <div class="modal fade" id="myModal3" role="dialog">
                                <div class="modal-dialog">
                        
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Microwave Besan Ladoo</h4>
                                    </div>
                                    <div class="modal-body">
                                      <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/besan_laddu.png" alt="" class="thumb_image_recip">
                                      <p class="popup_recip_name">Microwave Besan Ladoo</p>
                                      <p class="popup_resp_by_name">Recipe by Kutchina</p>
                                      
                                      <table class="popup_table">
                                        <tr>
                                            <th>Ingredients</th>
                                            <th>Quantity</th>
                                        </tr>
                                        <tr>
                                            <td>Besan (Chickpea Flour)</td>
                                            <td>2 Cup</td>
                                        </tr>
                                        <tr>
                                            <td>Fine Sugar</td>
                                            <td>1/2 Cup</td>
                                        </tr>
                                        <tr>
                                            <td>Ghee</td>
                                            <td>8 Tbsp</td>
                                        </tr>
                                        <tr>
                                            <td>Cardamom Powder</td>
                                            <td>1/2 Tsp</td>
                                        </tr>
                                        <tr>
                                            <td>Almonds</td>
                                            <td>6 Pcs</td>
                                        </tr>
                                        <tr>
                                            <td>Oil </td>
                                            <td>1 Tbsp</td>
                                        </tr>
                                      </table>
                                      
                                      <p class="popup_resp_desc"><strong>Instructions</strong></p>
                                      <ul class="method_ul_resp">
                                          <li class="popup_resp_desc">In a microwavable bowl, add besan (chickpea flour) along with 3 tbsp of ghee, mix evenly and microwave on high for 2 minutes.</p>
                                          <li class="popup_resp_desc">Stir the besan mixture using a spatula and microwave on high for 2 minutes.</li>
                                          <li class="popup_resp_desc">By now, the mixture would have turned slightly brown or deeper in color and a rich aroma of ghee would be coming out of it.</li>
                                          <li class="popup_resp_desc">Add 2 tbsp of ghee to this mixture and mix it with hand (it would feel course in texture).</li>
                                          <li class="popup_resp_desc">Now microwave for 2 more minutes.</li>
                                          <li class="popup_resp_desc">Add the remaining ghee, cardamom powder, almond flour and mix evenly.</li>
                                          <li class="popup_resp_desc">Microwave for 1 minute.</li>
                                          <li class="popup_resp_desc">Now the besan mixture will look deeper in color and courser in texture.</li>
                                          <li class="popup_resp_desc">Cool this mixture till it’s not very hot to touch.</p>
                                          <li class="popup_resp_desc">Add sugar and mix evenly.</li>
                                          <li class="popup_resp_desc">Now while the mixture is still warm, put oil in your hand and form small round balls / ladoo. (This needs to be made when the mixture is warm or else the ladoo's will not form).</li>
                                          <li class="popup_resp_desc">Serve warm or cold as per taste.</li>
                                      </ul>
                                    </div>
                                  </div>
                                  
                                </div>
                            </div>
                        </div>
                        
                        <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="image_name_maker">
                                <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/recip_img.png" alt="" class="thumb_image_recip" data-toggle="modal" data-target="#myModal4">
                                <div class="name_and_respname">
                                    <p class="recip_name">Roasted Caramelize Makhana Snack</p>
                                    <p class="recip_by_name">by Kutchina</p>
                                </div>
                            </div>
                        
                            <div class="modal fade" id="myModal4" role="dialog">
                                <div class="modal-dialog">
                        
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Roasted Caramelize Makhana Snack</h4>
                                    </div>
                                    <div class="modal-body">
                                      <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/recip_img.png" alt="" class="thumb_image_recip">
                                      <p class="popup_recip_name">Roasted Caramelize Makhana Snack</p>
                                      <p class="popup_resp_by_name">Recipe by Kutchina</p>
                                      <p class="popup_resp_desc"><strong>Method</strong></p>
                                      <p class="popup_resp_desc">Use a larger frying pan then you think you need, because it makes it easy to roast otherwise when you are roasting makhana will be dropping all over. Use heavy frying pan.</p>
                                      <p class="popup_resp_desc">In a frying pan take the makhana and drizzle with the oil, (save 2 teaspoons of oil and set this oil aside we will use later) and sprinkle the salt. Using your fingers mix it well until makhanas are coated well with oil and salt.</p>
                                      <p class="popup_resp_desc">Open the heat to medium low keep stirring till they are very light gold this should take about 6-8 minutes, try one makhana to make sure they are crispy. As they cool of, they will become crunchier.</p>
                                      <p class="popup_resp_desc">Remove them from frying pan, you will notice most of the salt will remain in the frying pan, wipe the frying pan, we will use it again.</p>
                                      <p class="popup_resp_desc">Use the same pan, heat over low medium heat. Put the oil we have saved in the frying pan, and sprinkle all the sugar, wait till you see sugar has start melting, now stir the sugar and keep stirring till the sugar has melted turn off the heat quick otherwise sugar will burn.</p>
                                      <p class="popup_resp_desc">Add almonds and roasted makhana, keep stirring till all the makhanas are coated with caramelize sugar and almonds. Spread it over non-stick surface and with spatula keep separating or separate with your fingers. Carnalized Makhanas are ready.</p>
                                      <table class="popup_table">
                                        <tr>
                                            <th>Ingredients</th>
                                            <th>Quantity</th>
                                        </tr>
                                        <tr>
                                            <td>Makhana (fox nuts, lotus seeds)</td>
                                            <td>4 cup</td>
                                        </tr>
                                        <tr>
                                            <td>Oil</td>
                                            <td>3 Tbsp</td>
                                        </tr>
                                        <tr>
                                            <td>Salt</td>
                                            <td>1 tsp</td>
                                        </tr>
                                        <tr>
                                            <td>Sugar</td>
                                            <td>6 Tbsp</td>
                                        </tr>
                                        <tr>
                                            <td>Almonds (sliced)</td>
                                            <td>2 Tbsp</td>
                                        </tr>
                                      </table>
                                    </div>
                                  </div>
                                  
                                </div>
                            </div>
                        </div>-->
                        
                        <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="image_name_maker">
                                <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/recip_img.png" alt="" class="thumb_image_recip" data-toggle="modal" data-target="#myModal5">
                                <div class="name_and_respname">
                                    <p class="recip_name">Potato Onion Cheela Recipe</p>
                                    <p class="recip_by_name">by Kutchina</p>
                                </div>
                            </div>
                        
                            <div class="modal fade" id="myModal5" role="dialog">
                                <div class="modal-dialog">
                        
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Potato Onion Cheela Recipe</h4>
                                    </div>
                                    <div class="modal-body">
                                      <img src="https://www.kutchina.com/wp-content/themes/vg-betashop/images/cooking_recipe/recip_img.png" alt="" class="thumb_image_recip">
                                      <p class="popup_recip_name">Potato Onion Cheela Recipe</p>
                                      <p class="popup_resp_by_name">Recipe by Kutchina</p>
                                      <p class="popup_resp_desc"><strong>Method</strong></p>
                                      <p class="popup_resp_desc">Add finely grated potatoes for 5 minutes in cold water.</p>
                                      <p class="popup_resp_desc">Take a bowl, add gram flour and whole wheat flour. Add red chilli powder, cumin powder, black pepper powder and salt to taste. Add water and make a smooth batter.</p>
                                      <p class="popup_resp_desc">Take grated potato out from cold water and add it to the gram and whole wheat batter. Mix it well and keep it aside.</p>
                                      <p class="popup_resp_desc">Heat one tablespoon Nutralite Garlic &amp; Oregano Spread in a nonstick pan, spread one portion of potato and onion mixture on the pan thinly and cook, turning sides until crisp and properly cooked. Serve hot.</p>
                                      <p class="popup_resp_desc">Serve Potato Onion Cheela along with  Dhaniya Pudina Chutney , Tomato Chutney or any other chutney for your breakfast or with Masala Chai.</p>
                                      <table class="popup_table">
                                        <tr>
                                            <th>Ingredients</th>
                                            <th>Quantity</th>
                                        </tr>
                                        <tr>
                                            <td>Finely Grated Potato</td>
                                            <td>2 pcs</td>
                                        </tr>
                                        <tr>
                                            <td>Finely Chopped Onion</td>
                                            <td>1 Pc</td>
                                        </tr>
                                        <tr>
                                            <td>Gram flour (besan)</td>
                                            <td>3 tablespoons</td>
                                        </tr>
                                        <tr>
                                            <td>Whole Wheat Flour</td>
                                            <td>2 tablespoon</td>
                                        </tr>
                                        <tr>
                                            <td>Red Chilli powder</td>
                                            <td>1 teaspoon</td>
                                        </tr>
                                        <tr>
                                            <td>Salt</td>
                                            <td>As required</td>
                                        </tr>
                                        <tr>
                                            <td>Cumin powder (Jeera)</td>
                                            <td>1 teaspoon</td>
                                        </tr>
                                        <tr>
                                            <td>Black pepper powder</td>
                                            <td>1/2 teaspoon</td>
                                        </tr>
                                        <tr>
                                            <td>Water</td>
                                            <td>As required</td>
                                        </tr>
                                        <tr>
                                            <td>Nutralite Garlic &amp; Oregano Spread</td>
                                            <td>As required</td>
                                        </tr>
                                      </table>
                                    </div>
                                  </div>
                                  
                                </div>
                            </div>
                        </div>-->
                        </div>
                </div>
                <div class="tab-pane" id="2a">
                	<h3>Comming Soon</h3>
                </div>
            </div>
        </div>
      </div> 
    </div>
  </div>
</div>





<?php betashop_get_footer(); ?>