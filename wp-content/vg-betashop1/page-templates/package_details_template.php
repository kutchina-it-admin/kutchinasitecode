<?php
/**
 * Template Name: Package Details
 *
 * Description: Request A Callback page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<style>
.prodd {
	width: 30%;
	float: left;
}
.priceg span{ width:100%; text-align:center; float:left; margin:-38px 0 0 0;}
.add_to_cart_button{ opacity:0;}
.main-container .page-content .button:hover {

    outline: 0;
    opacity: 0;

}
.titll {
	font-size: 23px;
	margin: 0 0 20px 0;
	border-right: 0;
	text-align: center;
	padding: 0 0 0 20px;
	transition: all 0.5s ease; height:130px;
}
.cartbut {
	border: none;
	margin-bottom: 0;
	background: #fff;
	margin: 0 8px 6px 0;
	width:32%;
	float:left;
}
table td {
	padding:0;
}
.vgwc-product-price {
	margin: 0;
}
.nomar {
	margin: 0;
}
.bbhu {
	padding: 8px 16px;
	margin: 25px 0 0 0;
	float: left;
}
.formyu {
	max-width:660px;
	float: none;
	margin: 20px auto 40px auto;
	text-align:center;
	background: #ddd;
	padding:30px!important;
}
.ttl {
	font-size: 18px;
	font-weight: bold;
	color: #000;
	margin: 0 0 20px 0;
}
.sev {
	width:100%!important;
}
.sev input {
	width: 100%!important;
}
.formyu label {
	float: left;
	padding: 0 0 0 0!important;
	margin: 0!important;
	font-size:16px;
	color: #000;
	font-weight: bold!important;
}
.clear {
	clear: both;
}
.till {
	background:#f2f2f2;
	color:#666;
	font-weight:bold;
	font-size:15px;
	font-family:Arial, Helvetica, sans-serif;
	padding:15px 0;
	transition: all 0.5s ease;
}
.cartbut:hover .titll{	color:#333;
	background:#fff;}
.cartbut:hover td {
	color:#fff;
	background:#ed2443;
}
.cartbut:hover {
	 cursor:pointer; box-shadow:0 3px 3px #999;
}
.cartbut:hover .till {
	background:#ccc; color:#000;
}
.cartbut:hover .cho_pri {
	background:#8d0218;
}
.cho_pri {
	transition: all 0.5s ease;
}
.disccountt{
	color: #fff;
}
.mdsec{
	cursor: pointer;
}
 @media (max-width: 480px) {
 .titll {
font-size: 18px;
}
 .cartbut tr td {
width: 100%;
float: left;
margin: 0 0 0 0;
}
}
@media (max-width: 767px) {
.titll {
 padding: 9px 0 4px 20px;
 border: none;
}
.cartbut { width:100%; margin:0 0 20px 0;}
}
</style>

<script type="text/javascript">
	function keyPressed(e)
	{
	     var key;      
	     if(window.event)
	          key = window.event.keyCode; //IE
	     else
	          key = e.which; //firefox      

	     return (key != 13);
	}
</script>

<div class="main-container front-page about-page">
  <div class="row-breadcrumb">
    <div class="container">
      <?php betashop_breadcrumb(); ?>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <header class="entry-header">
          <h1 class="entry-title">
            <?php the_title(); ?>
          </h1>
        </header>
      </div>
    </div>
  </div>
  <div class="full-wrapper about-us-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="page-content">
            <?php while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
              <div class="entry-content">
                <?php the_content(); ?>
              </div>
              <!-- .entry-content -->
            </article>
            <!-- #post -->
            <?php endwhile; // end of the loop. ?>
            <div class="">
              <?php

	//print_r($_SERVER);


		if ($_SERVER["REQUEST_METHOD"] == "POST"){
			$get_amc_prod = $_POST['kut_mod_idd_hid'];
			$kut_userpin_id_hid = $_POST['kut_userpin_id_hid'];
			$chng_userpin_id_hid = substr($kut_userpin_id_hid, 0, 2);			
		}
	?>



<div class="formbox1 bigsun">
	<div class="st-icon"><img src="<?php bloginfo ( 'template_url' ); ?>/images/step4.png" alt="" /></div>
	
	<!-- <div class="gapf">

		<?php if ( $chng_userpin_id_hid == '70' || $chng_userpin_id_hid == '71' || $chng_userpin_id_hid == '72' || $chng_userpin_id_hid == '73' || $chng_userpin_id_hid == '74') {
			
		} else { ?>

			<p class="text-center disccountt">
				<strong>Independence Day Offer ( Discount upto 15% ).</strong>
				<span class="mdsec" data-toggle="modal" data-target="#zipModal">"Please Click & Check your Availability"</span>
			</p>

		<?php } ?>

	</div> -->
	

  <?php
	    $args = array( 'post_type' => 'product', 'posts_per_page' => -1 );

	    $loop = new WP_Query( $args );	    
	    while ( $loop->have_posts() ) : $loop->the_post(); 
	    global $product;

	    /*echo "<pre>";
		print_r($product);
		echo "</pre>";*/

	    $prod_machineidd = get_post_meta( $product->get_id(), 'machine_id', true );
	    $price = get_post_meta( get_the_ID(), '_regular_price', true);
	    //echo $prod_machineidd;

	?>
              <?php 
	if ($get_amc_prod == $prod_machineidd) { ?>								

		<div class="pbox">
			<div class="pttl"><?php echo $product->post->post_title; ?></div>
			<img src="<?php bloginfo ( 'template_url' ); ?>/images/line1.png" class="linkg" alt="" />
			<?php if ($price > 0) { ?>
				<h6 class="amt">Amount Payable</h6>
			<?php } ?>
			<img src="<?php bloginfo ( 'template_url' ); ?>/images/line1.png" class="linkg" alt="" />
			<?php if ($price == '0') { ?>
				<span class="priceg">
					<span style="margin-top: 0;">N/A</span>
				</span>
			<?php } else { ?>
				<span class="priceg">
					<?php echo do_shortcode('[add_to_cart id="'.$post->ID.'"]'); ?>
					<span><?php echo '&#2352; '.$price; ?></span>				
				</span>
			<?php } ?>
		</div>

        <!--<table class="cartbut">
            <tr>
              <td class="titll" style="font-family:Arial, Helvetica, sans-serif; font-size:18px; padding:12px 10px;"><?php echo $product->post->post_title; ?></td>
            </tr>
            <tr>
              <td class="till">Amount Payable</td>
            </tr>
            <tr>
              <td><div class="pprce">
                  <div class="cho_pri" style="font-family:Arial, Helvetica, sans-serif;"> <?php echo do_shortcode('[add_to_cart id="'.$post->ID.'"]'); ?> <span><?php echo '&#2352; '.$price; ?></span> </div>
                </div></td>
            </tr>
		</table>-->

              <?php } ?>
							<?php endwhile; wp_reset_query(); ?>
                            <div class="clear"></div>
							
	</div>


              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<!-- ////////////////////////////////////////// Coupon Code Availability by Zip Code START //////////////////////////////////// -->

<div class="modal fade" id="zipModal" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title text-center">Coupon Code Availability</h4>
    </div>
    <div class="modal-body">

      <p class="text-center">Now You can Avail Discount on Purchasing 1 Year, 2 Years & 3 Years AMC Package on Chimney & Water Purifier.</p>
      <p class="text-center"><strong><i>Not valid in West Bengal</i></strong></p>

      <?php
      	$args = array(
		    'posts_per_page'   => -1,
		    'orderby'          => 'ID',
		    'order'            => 'asc',
		    'post_type'        => 'shop_coupon',
		    'post_status'      => 'publish',
		);
		    
		$coupons = get_posts( $args );

		foreach ( $coupons as $coupon ) {
		    $coupon_name = $coupon->post_title;
		    $coupon_desc = $coupon->post_excerpt;

		    $coupon_metadata = new WC_Coupon( $coupon->post_title );
		    $coupon_usage_left = $coupon_metadata->usage_limit - $coupon_metadata->usage_count;
	  ?>

	  <p class="text-center"><?php echo $coupon_desc; ?> <!-- - <strong><?php echo $coupon_usage_left; ?> Coupons Available</strong> --> - Use this coupon code - <strong><?php echo $coupon_name; ?></strong></p>     

	  <?php } ?>

      

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
  
</div>
</div>

<!-- //////////////////////////////////////////// Coupon Code Availability by Zip Code END //////////////////////////////////// -->



<style>
	.formbox1{max-width:625px; background:#007dc6; border:3px solid #fff; box-shadow:0 5px 10px #ccc; margin:100px auto 0 auto; min-height:400px;moz-border-radius:10px; -webkit-border-radius:10px; border-radius:10px; padding:20px; }
.st-icon{ width:96px; height:96px; margin:-70px auto 20px auto;}
.formbox1 h3{ font-size:15px; text-align:center; color:#fff; margin:0 0 0 0; font-weight:normal; line-height:22px;}
.formbox1 h3 span{ width:100%; text-align:center; font-size:30px; font-weight:bold; line-height:40px;}
.linkd{ margin:30px auto; display:block; text-align:center; }
.formbox1 h5{ font-size:16px; font-weight:lighter; color:#a8d6f1; text-align:center; line-height:22px;}
.forminp{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/formbg1.png) no-repeat;width:361px; height:43px; border:none; float:left; padding:0 0 0 65px;box-sizing: border-box; font-size:15px; color:#fff; font-weight:lighter;}
.forminp2{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/formbg2.png) no-repeat;width:361px; height:43px; border:none; float:left; padding:0 0 0 55px;box-sizing: border-box; font-size:15px; color:#fff; font-weight:lighter;}

.frbt{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/subtt.png) no-repeat; width:94px; height:43px; float:left; font-size:20px; color:#fff;  border:none; margin:0 0 0 -2px; cursor:pointer;}
.frbt:hover{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/subtt2.png) no-repeat; width:94px; height:43px; float:left; font-size:20px; color:#fff; border:none; margin:0 0 0 -2px; cursor:pointer;}
.formsec{ max-width:455px; margin:0 auto;}
.shadoe{ margin:0 auto 0 auto; display:block; text-align:center; } 
.shadoe img{ margin:10px 0 0 0;}
.formbox1 h5 span{ line-height:38px; font-size:26px; color:#fff; }

.subse{ margin:0 0 0 0; float:left;}
.subsemain{max-width:350px; background:#138fd7; margin:0 auto 1px auto; display:table; padding:7px 12px;}
.slt{ width:180px; float:left; font-size:16px; color:#fff;}
.srt{ width:130px; float:left; color:#ffd52e; font-size:16px; font-weight:bold;}
.consec h4{ font-size:20px; font-weight:normal; color:#fff; float:left; width:100%; margin:10px 0 12px 0;}
.consec h5{ float:left; width:100%; margin:0 0 5px 0; text-align:left; font-size:15px;}
.consec{ padding:10px 20px; margin:20px 0 25px 0; float:left; border:1px solid #138fd7;}
.agrbt{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/bghu.png) no-repeat; width:194px; height:43px;font-size:20px; color:#fff; border:none; float:right; margin:0 20px 0 0; cursor:pointer}
.backin{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/back.png) no-repeat left 14px; font-size:18px; color:#fff; padding:8px 10px 8px 20px; border:none; margin:0px 20px 0 0; float:left; cursor:pointer;}
.fity{ margin:20px auto 1px auto;}
.clear{ clear:both;}

.pbox{ width:235px; background:#00629b; border:1px solid #00a1ff; box-shadow:0 0px 5px #fff; float:left; min-height:240px; moz-border-radius:10px 0px 10px 0;-webkit-border-radius:10px 0px 10px 0;border-radius:10px 0px 10px 0; margin:0 1.6% 30px;}
.pttl{ width:97%; float:left; background:#00a1ff; font-size:18px; color:#fff; padding:10px 10px 10px 20px; border-left:6px solid #ffcc00;border-bottom:1px solid #ffcc00;box-sizing: border-box;moz-border-radius:10px 0px 10px 0;-webkit-border-radius:10px 0px 10px 0;border-radius:10px 0px 10px 0; margin:5px 0 40px 5px;}

.linkg{ width:100%; margin:0 auto; display:block; height:1px;}
.amt{ font-size:23px; color:#ffcc00; text-align:center; margin: 0 0 0 0; line-height:50px;}
.priceg{ font-size:57px; text-align:center; color:#fff; font-weight:normal; text-shadow:0 2px 3px #333; text-align:center; width:100%; float:left; padding:20px 0 0 0;}
.bigsun{ max-width:831px; margin:100px auto 50px auto;} 
.gapf{ width:100%; float:left; height:50px;}
.dcd{ margin:0 auto; float:none;}

@media(max-width:767px){
.pbox{ margin:40px auto; float:none; min-height:270px;}
}
</style>

<?php betashop_get_footer(); ?>
