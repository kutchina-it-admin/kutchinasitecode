<?php
 /* Template Name: Enquiry Template */	
 get_header();

  
  
?>
<div class="main-container page-faq full-width page-title">
    <div class="row-breadcrumb">
        <div class="container">
          <?php betashop_breadcrumb(); ?>
        </div>
    </div>

      <div class="container">
              <div class="row">
                      <div class="page-content">

                         <div class="col-xs-12">
                            <header class="entry-header">
                              <h1 class="entry-title"><?php the_title(); ?></h1>
                            </header>
                          </div>

                          
                          <div class="col-xs-12">
                            <ul class="nav nav-tabs">
                              <li class="active"><a data-toggle="tab" href="#home">Corporate</a></li>
                              <li><a data-toggle="tab" href="#menu1">Distributor/Dealer/Franchise</a></li>
                              <li><a data-toggle="tab" href="#menu2">Service Enquiry</a></li>
                            
                            </ul>

                             <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                  <?php echo do_shortcode('[contact-form-7 id="6492" title="Corporate Form"]'); ?>
                                </div>

                                <div id="menu1" class="tab-pane fade">
                                  <?php echo do_shortcode('[contact-form-7 id="6493" title="Distributor Form"]'); ?>
                                </div>

                                <div id="menu2" class="tab-pane fade">
                                  <?php echo do_shortcode('[contact-form-7 id="6494" title="Service Form"]'); ?>
                                </div>
                               
                              </div>
                          </div>

                    </div>
            </div>
    </div>
</div>

<script type="text/javascript">
  function selectCityFromState()
  {
      var selectState = jQuery("#selectState").val();
      var selectCity = jQuery("#selectCity").val();
      

       window.location.href = "http://10.0.9.105/kutchina/support/?selectState="+selectState+"&selectCity="+selectCity;
  }
</script>
<?php 
get_footer(); 
?>
