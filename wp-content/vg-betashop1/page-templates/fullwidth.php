<?php
/**
 * Template Name: Full Width
 *
* Description: Full Width template
 *
 * @package    betashop
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 */
$betashop_options  = betashop_get_global_variables(); 

betashop_get_header();
?>
<div class="main-container full-width">

	<div class="page-content">

		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part('content', 'page'); ?>
		<?php endwhile; // end of the loop. ?>

	</div>
</div>
<?php betashop_get_footer(); ?>