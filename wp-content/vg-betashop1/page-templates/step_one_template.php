<?php
/**
 * Template Name: Step One
 *
 * Description: Request A Callback page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>
<div class="main-container front-page about-page">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div>
	<div class="full-wrapper about-us-wrapper">
    <div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-content">
					<?php while (have_posts()) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php the_content(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post -->
					<?php endwhile; // end of the loop. ?>

				<style>
					.nomar{margin: 0; }
					.bbhu{padding: 8px 16px; width: 100%;}
.formyu{max-width:820px; float: none; margin: 20px auto 40px auto; text-align:center; background: #ddd; padding:30px 20px;}
.ttl{font-size: 18px; font-weight: bold;; color: #000; margin: 0 0 20px 0;}
.sev{width:100%!important;}
.sev input{ width: 100%!important; }
.formyu label{ float: left; padding: 0 0 0 0!important; margin: 0!important; font-size:16px; color: #000; font-weight: bold!important;  }
.clear{ clear: both; }
				</style>
<div class="formyu">
					
					<form class="form-inline" method="post" action="https://www.kutchina.com/shipping-details">	
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			    <label for="exampleInputEmail1">Date of Purchase</label>
			    <input type="text" class="form-control sev" id="exampleInputEmail1" placeholder="Date of Purchase">
					</div>	
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			    <label for="exampleInputPassword1">Invoice Number</label>
			    <input type="text" class="form-control sev" id="exampleInputPassword1" placeholder="Invoice Number">	
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<label>&nbsp</label>
						<button type="submit" class="btn btn-primary mb-2 bbhu">Next</button></div>			  
					  
					  
					</form>
					<div class="clear"></div>
</div>











				

            	</div>


			</div>
            
            
		</div>
	</div>
</div>
</div>
<?php betashop_get_footer(); ?>