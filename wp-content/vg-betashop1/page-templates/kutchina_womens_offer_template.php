<?php
/**
 * Template Name: Kutchina Women's Coupon Offer
 *
 * Description: Request A Callback page template
 *
 * @package    VG Amadea
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$betashop_options  = betashop_get_global_variables();

betashop_get_header();
?>


<style>
	.main-container .page-content{overflow: inherit;}
	.insl{ margin:15px 0 0 0; float:right;}
	.bbt33{ text-align:right; margin:15px 0 0 0;}
	.bbt34{ text-align:right; margin:15px 0 0 0;}
		.nomar{margin: 0;}
		.bbhu{padding: 8px 16px!important; width: auto; margin:0 0 0 10px; }
		/*.lsd{margin:20px 0 0 0;}
		.formyu{max-width:620px; float: none; margin: 20px auto 40px auto; text-align:center; background: #ddd; padding:30px 20px;}*/
		.formyu{ width:auto; background:none!important; max-width:100%; padding:0 0 0 0;}
		.ttl{font-size: 17px; font-weight: 500; color: #666; margin: 0 0 50px 0;}
		.sev{width:100%;}
		.sev input{ width: 100%!important; }
	
		.clear{ clear: both; }
		.numim a{font-family: Arial;}
		.fom{ width:auto; height:auto; padding:20px; background:#fff; border:1px solid #ccc; float:left;}

.form_box1{max-width: 680px;float: none;margin: 20px auto 40px auto;text-align: center;background: #f2f2f2; border:6px solid #fff;box-shadow:0 0 6px #ccc;padding: 45px 50px; overflow: hidden;}
.no-padding{ padding: 0 !important;}   
.bhu{background: #fff; padding: 5px 0px; margin: 0 0 6px 0;}
.tth{font-family: Arial;}
.bbt1{padding:0 0 0 15px;}
.bbt2{padding:0 5px 0 5px;}
.bbt3{padding:0 15px 0 0px;}
#firstt_frm button{border-radius: 0!important;}
#prod_serform button{border-radius: 0!important;}
#mod_nam button{border-radius: 0!important;}
.form_buy_text p{ text-align: center !important; }
.banner_descs.banner_descs_by_amc {
    text-align: center;
    margin-bottom: 15px;
    padding: 0px 15px;
}
.fa .fa-check{ color:#fff; font-size:25px !important;}
.exists{color: #fff;text-align: center;float: none;width: 100%;margin: 0 auto;display: block;}

@media only screen and (max-width: 640px) {	
.bbhu {
    padding: 8px 22px;
    width: 64px;
}
.main-container .page-content .input-text, .main-container .page-content input[type="text"], .main-container .page-content .form-row .input-text {
    padding: 0 10px !important;
}

}
@media only screen and (max-width: 767px) {
	.form_box1{padding:20px!important;}
	#loc_pinc .col-lg-10{width:76%!important;}
	#loc_pinc .col-lg-2{width:24%!important;}
	.bbt2 input{
    width: 100% !important;
    padding: 0 0 0 10px !important;
}
.bbt1, .bbt3{width:18%;}
.bbt1{padding: 0;}
.bbt2{width:64%;}

.bbt11, .bbt33{width:18%;}
.bbt33{ padding: 0; }
.bbt11{padding: 0;}
.bbt22{width:64%; padding: 0;}
#loc_pinc .form-group.mb-2.sev #user_pincode + #uname_response {
    margin: -40px 8px 0 0;
}
.sev b{text-align: center!important; float: none!important;}
.titll {
    padding: 9px 0 4px 20px;
    border: none;
}
#uname_response_neg {
    line-height: 14px;
}
}

.newFormSec .forminp{
	width: 80% !important;
}

.newFormSec #loc_but{
	width: 20% !important;
	font-size: 14px !important;
}

@media screen and (min-width: 240px) and (max-width:452px){
	.newFormSec .forminp {
		width: 100% !important;
		border-radius: 20px !important;
		text-indent: 50px !important;
	}
	.newFormSec #loc_but {
		width: 25% !important;
		font-size: 12px !important;
		margin: 10px 0px 0 37% !important;
	}	
}

@media screen and (min-width: 452px) and (max-width:652px){
	.newFormSec .forminp {
		text-indent: 50px !important;
	}	
}


</style>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script>

<div class="main-container front-page about-page">
	<div class="row-breadcrumb">
		<div class="container">
			<?php betashop_breadcrumb(); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
		</div>
	</div>
	<div class="full-wrapper about-us-wrapper">
    <div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="page-content">
					<?php while (have_posts()) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php the_content(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post -->
					<?php endwhile; // end of the loop. ?>


<script type="text/javascript">

function keyPressed(e)
{
     var key;      
     if(window.event)
          key = window.event.keyCode; //IE
     else
          key = e.which; //firefox      

     return (key != 13);
}


function numValidate() {
  var user_pincode = document.getElementById('user_pincode');
  user_pincode.value = user_pincode.value.replace(/[^0-9]+/, '');
};

function check()
  {
   var user_pincode = jQuery("#user_pincode").val();
   
   if(user_pincode == '' )
    {
        jQuery("#zip_err").show();
        jQuery("#user_pincode").focus();
        return false;
    } else if(user_pincode.length < 6 || user_pincode.length > 6 ) {
        jQuery("#zip_err").hide();        
        jQuery("#zip_err1").show();
        jQuery("#user_pincode").focus();
        return false;    
    } else {
        jQuery("#zip_err").hide();
        jQuery("#zip_err1").hide();
        document.loc_pinc.submit();
    }
  }

</script>



<div class="formyu offer_sec_dv">
	<div class="formbox1 offer_frm_sec" id="firstt_frm">

		<div>
			<h3><span>Enter your pin-code to check the Offer</span></h3>
			<img src="<?php bloginfo ( 'template_url' ); ?>/images/linei.png" class="linkd" alt="" />
		</div>
		<h5>Please enter Pin code of the location for checking availability for the offer</h5>
		<form class="formsec off_frm newFormSec" name="loc_pinc" id="loc_pinc" method="POST" action="<?php site_url() ?>/womens-day-offer-list/">
			<input type="text" class="forminp" placeholder="Enter Pin Code" id="user_pincode" name="user_pincode" onkeyup="numValidate();" onKeyPress="return keyPressed(event)"/>			
			<button class="frbt" id="loc_but" type="button" onclick="check();">Get Offer</button>			
			<div id="zip_err" class="response" style="color: #d4e5ef; display: none;">Pin code is required</div>
			<div id="zip_err1" class="response" style="color: #d4e5ef; display: none;">Enter valid zip code</div>
		</form>
		<div class="shadoe"><img src="<?php bloginfo ( 'template_url' ); ?>/images/shadoe.png" alt="" /></div>

	</div>					
</div>




            	</div>


			</div>
            
            
		</div>
	</div>
</div>
</div>

<style>
.formbox1{background:#007dc6; border:3px solid #fff; box-shadow:0 5px 10px #ccc; margin:50px auto 50px auto; min-height:400px;moz-border-radius:10px; -webkit-border-radius:10px; border-radius:10px; padding:60px 20px 20px;}
.st-icon{ width:96px; height:96px; margin:-70px auto 20px auto; position:relative; z-index:100;}
.formbox1 h3{ font-size:15px; text-align:center; color:#fff; margin:0 0 0 0; font-weight:normal; line-height:22px;}
.formbox1 h3 span{ width:100%; text-align:center; font-size:30px; font-weight:bold; line-height:40px;}
.linkd{ margin:30px auto; display:block; text-align:center; }
.formbox1 h5{ font-size:16px; font-weight:normal; color:#a8d6f1; text-align:center; line-height:22px;}
.forminp{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/formbg1.png) no-repeat !important;width:361px; height:43px!important; border:none; float:left; padding:0 0 0 65px!important; border:none!important;box-sizing: border-box; font-size:15px; color:#fff; font-weight:lighter;}
.forminp2{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/formbg2.png) no-repeat !important;width:361px; height:43px!important; border:none; float:left; padding:0 0 0 55px!important; border:none!important;box-sizing: border-box; font-size:15px; color:#fff; font-weight:lighter;}
.consec strong { color:#fff;}

.frbt{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/subtt.png) no-repeat !important; width:94px; height:43px; float:left; font-size:20px; color:#fff;  border:none; margin:0 0 0 -2px; cursor:pointer;}
.frbt:hover{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/subtt2.png) no-repeat; width:94px; height:43px; float:left; font-size:20px; color:#fff; border:none; margin:0 0 0 -2px; cursor:pointer;}
.formsec{ max-width:455px; margin:0 auto;}
.shadoe{ margin:0 auto 0 auto; display:block; text-align:center; } 
.shadoe img{ margin:10px 0 0 0;}
.formbox1 h5 span{ line-height:38px; font-size:26px; color:#fff; }

.subse{ margin:0 0 0 0; float:left;}
.subsemain{max-width:350px; background:#138fd7; margin:0 auto 1px auto; display:table; padding:7px 12px;}
.slt{ width:180px; float:left; font-size:16px; color:#fff;}
.srt{ width:130px; float:left; color:#ffd52e; font-size:16px; font-weight:bold;}
.consec h4{ font-size:20px; font-weight:normal; color:#fff; float:left; width:100%; margin:10px 0 12px 0;}
.consec h5{ float:left; width:100%; margin:0 0 5px 0; text-align:left; font-size:15px;}
.consec{ padding:10px 20px; margin:20px 0 25px 0; float:left; border:1px solid #138fd7;}
.agrbt{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/bghu.png) no-repeat; width:194px; height:43px;font-size:20px; color:#fff; border:none; float:right; margin:0 20px 0 0; cursor:pointer}
.backin{ background:url(<?php bloginfo ( 'template_url' ); ?>/images/back.png) no-repeat left 14px; font-size:18px; color:#fff; padding:8px 10px 8px 20px; border:none; margin:0px 20px 0 0; float:left; cursor:pointer;}
.fity{ margin:20px auto 1px auto;}
.clear{ clear:both;}

.pbox{ width:235px; background:#00629b; border:1px solid #00a1ff; box-shadow:0 0px 5px #fff; float:left; min-height:250px; moz-border-radius:10px 0px 10px 0;-webkit-border-radius:10px 0px 10px 0;border-radius:10px 0px 10px 0; margin:0 1.6% 30px;}
.pttl{ width:97%; float:left; background:#00a1ff; font-size:18px; color:#fff; padding:10px 10px 10px 20px; border-left:6px solid #ffcc00;border-bottom:1px solid #ffcc00;box-sizing: border-box;moz-border-radius:10px 0px 10px 0;-webkit-border-radius:10px 0px 10px 0;border-radius:10px 0px 10px 0; margin:5px 0 40px 5px;}

.linkg{ width:100%; margin:0 auto; display:block; height:1px;}
.amt{ font-size:23px; color:#ffcc00; text-align:center; margin: 0 0 0 0; line-height:50px;}
.priceg{ font-size:57px; text-align:center; color:#fff; font-weight:normal; text-shadow:0 2px 3px #333; text-align:center; width:100%; float:left; padding:20px 0 0 0;}
.bigsun{ max-width:831px;}
.gapf{ width:100%; float:left; height:50px;}

.dcd{ margin:0 auto; float:none;}
.not-exists{ color:#d4e5ef !important; font-size: 12px !important;}

@media(max-width:767px){
.forminp{width:190px;}
.forminp2{width:190px;}
#go_back_pin_area {

    border-radius: 0 !important;
    text-align: left;
    padding: 7px 0 0 21px !important;

}
#go_back_prodser_area{
    border-radius: 0 !important;
    text-align: left;
    padding: 7px 0 0 21px !important;}
	.agrbt{margin: 0 auto 20px auto;
    float: none;
    text-align: center;
    display: block;

}
}
</style>

<?php betashop_get_footer(); ?>