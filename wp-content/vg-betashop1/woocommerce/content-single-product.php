<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

	$betashop_options  =  betashop_get_global_variables();
	$betashop_secondimage  =  betashop_get_global_variables('betashop_secondimage');
	
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action('woocommerce_before_single_product');

	 if (post_password_required()) {
	 	echo get_the_password_form();
	 	return;
	 }

	 $terms = get_the_terms( $post->ID, 'product_cat' );
	 //print_r($terms);
	 $category = $terms[1]->name;
	 $category_id = $terms[1]->term_id;
	 $category_id_open_checkout = $terms[0]->term_id;	 
?>

<?php if( $category_id == 145 ){  ?>

	<div id="product-<?php the_ID(); ?>" <?php post_class(); ?> >
	<div class="row">
		
		<div class="col-xs-12 col-md-12">
			
	
			<div class="summary entry-summary single-product-info">
				<?php
					/**
					 * woocommerce_single_product_summary hook
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 */
					do_action('woocommerce_single_product_summary');
				?>
			</div><!-- .summary -->
		</div>
	</div>
	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action('woocommerce_after_single_product_summary');
	?>
	
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->


<?php } elseif ( $category_id_open_checkout == 306 ) { ?>



<div id="product-<?php the_ID(); ?>" <?php post_class(); ?> >
	<div class="opennn_checkkk_outtt_prooo">
		<div class="row">

			<div class="cstmm_prodd_opnn_chkk">
				<h1 class="opnn_hhh"><?php the_title(); ?></h1>
			</div>

			<div class="col-xs-12 col-md-<?php echo (is_active_sidebar('sidebar-product')) ? '6' : '5' ; ?>">
				<div class="single-product-image">
					<?php
						/**
						 * woocommerce_before_single_product_summary hook
						 *
						 * @hooked woocommerce_show_product_sale_flash - 10
						 * @hooked woocommerce_show_product_images - 20
						 */
						//do_action('woocommerce_before_single_product_summary');
						the_content();
					?>
				</div>
			</div>
			<div class="col-xs-12 col-md-<?php echo (is_active_sidebar('sidebar-product')) ? '6' : '7 col-summary' ; ?>">
				<div class="summary entry-summary single-product-info">
					<?php
						/**
						 * woocommerce_single_product_summary hook
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 */
						do_action('woocommerce_single_product_summary');
					?>
				</div><!-- .summary -->
			</div>
		</div>
		<?php
			/**
			 * woocommerce_after_single_product_summary hook
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action('woocommerce_after_single_product_summary');
		?>
		
		<meta itemprop="url" content="<?php the_permalink(); ?>" />


	</div>
</div>


<?php } else { ?>



<div id="product-<?php the_ID(); ?>" <?php post_class(); ?> >
	<div class="row">
		<div class="col-xs-12 col-md-<?php echo (is_active_sidebar('sidebar-product')) ? '6' : '5' ; ?>">
			<div class="single-product-image">
				<?php
					/**
					 * woocommerce_before_single_product_summary hook
					 *
					 * @hooked woocommerce_show_product_sale_flash - 10
					 * @hooked woocommerce_show_product_images - 20
					 */
					do_action('woocommerce_before_single_product_summary');
				?>
			</div>
		</div>
		<div class="col-xs-12 col-md-<?php echo (is_active_sidebar('sidebar-product')) ? '6' : '7 col-summary' ; ?>">
			<div class="summary entry-summary single-product-info">
				<?php
					/**
					 * woocommerce_single_product_summary hook
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 */
					do_action('woocommerce_single_product_summary');
				?>
			</div><!-- .summary -->
		</div>
	</div>
	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action('woocommerce_after_single_product_summary');
	?>
	
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php } ?>

<?php $betashop_secondimage = true; ?>

<?php do_action('woocommerce_show_related_products');

//dynamic_sidebar('sidebar-product'); ?>

<?php do_action('woocommerce_after_single_product'); ?>

<?php $betashop_secondimage = false; ?>