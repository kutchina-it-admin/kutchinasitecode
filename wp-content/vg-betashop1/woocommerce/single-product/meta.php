<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

?>

<!-- <button type="submit" name="add-to-cart" value="<?php //echo esc_attr($product->get_id()); ?>" data-product-id="15386" class="single_add_to_cart_button button alt" id="buy_now_button">
<?php //echo esc_html('Buy Now'); ?>
</button>
<input type="hidden" name="is_buy_now" id="is_buy_now" value="0" /> -->

<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>

	<?php endif; ?>

	<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>

<?php $prod_terms = get_the_terms( $post->ID, 'product_cat' ); 
	// echo $prod_terms[0]->term_id;
	// print_r($prod_terms);
	$product_parent_categories_all_hierachy = get_ancestors( $prod_terms[0]->term_id, 'product_cat' ); 
	// print_r($product_parent_categories_all_hierachy);
	// echo $product_parent_categories_all_hierachy[0];

?>
<?php 
	// foreach ($prod_terms as $prod_term) {
	//     $product_cat_id = $prod_term->term_id;
	//     $product_parent_categories_all_hierachy = get_ancestors( $product_cat_id, 'product_cat' );  

	//     $last_parent_cat = array_slice($product_parent_categories_all_hierachy, -1, 1, true);
	//     foreach($last_parent_cat as $last_parent_cat_value){

	//         echo '<strong>' . $last_parent_cat_value . '</strong><br>';
	//     }
	// }

if ($product_parent_categories_all_hierachy[0] != 143 && $prod_terms[0]->term_id != 143) {

	if($product_parent_categories_all_hierachy[0] == 142 || $prod_terms[0]->term_id != 142){
?>

		<p style="padding-top: 5px; font-size: 15px; font-weight:600;">If you have any queries you can contact our advisors <a href="https://www.kutchina.com/product-category/appliances/#ask_our_advisors" style="color:#0065b2;" target="_blank">here</a>.</p>

		<hr>
	<?php }
	else if($product_parent_categories_all_hierachy[0] == 144 || $prod_terms[0]->term_id != 144){
	?>

		<p style="padding-top: 5px; font-size: 15px; font-weight:600;">If you have any queries you can contact our advisors <a href="https://www.kutchina.com/product-category/water-purifiers/#ask_our_advisors" style="color:#0065b2;" target="_blank">here</a>.</p>

		<hr>
	<?php }
	else if($product_parent_categories_all_hierachy[0] == 145 || $prod_terms[0]->term_id != 145){
	?>

		<p style="padding-top: 5px; font-size: 15px; font-weight:600;">If you have any queries you can contact our advisors <a href="https://www.kutchina.com/product-category/modular-kitchens/#ask_our_advisors" style="color:#0065b2;" target="_blank">here</a>.</p>

		<hr>
	<?php } ?>
<?php } ?>

<div class="ambf_btns" style="display: contents;">
<?php 

$amazon_url =  get_field('amazon_url');
	//if($amazon_url != '')
	//{
?>
		<!-- <a href="<?php //echo $amazon_url; ?>" class="amz_btn"><img src="<?php //bloginfo("template_url"); ?>/images/buy-now-amazon-btn.png" width="200"></a> -->
<?php
	//}

$amazon_text =  get_field('amazon_text');
	if($amazon_text != '')
	{
?>
		<p style="font-size: 15px; font-weight: 600;"><?php echo $amazon_text; ?> <a href="<?php echo $amazon_url; ?>" style="color:#0065b2;" target="_blank">Click Here</a></p>
<?php
	}

$store_url =  get_field('store_url');
	if($store_url != '')
	{
?>
		<a href="<?php echo $store_url; ?>" class="bfs_btn">Buy From Store</a>
<?php
	}
$store_text =  get_field('store_text');
	if($store_text == 'yes')
	{
?>
		<p style="font-size: 15px; font-weight: 600;">Also available in stores. To find the store nearest to you <a href="https://www.kutchina.com/our-presence/" style="color:#0065b2;" target="_blank">click here</a>.</a>
<?php
	}
?>

</div>

<?php
$image = get_field('banner_image');
?>
<img src="<?php echo $image['url']; ?>" alt="">