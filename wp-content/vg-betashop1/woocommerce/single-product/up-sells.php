<?php
/**
 * Single Product Up-Sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if(! defined('ABSPATH')) {
	exit;
}

global $product, $woocommerce_loop;
global $betashop_options;


if ( $upsells ) : ?>
<div class="widget upsells_products_widget">
	<div class="vg-front-page-title2 inner-title"><h3><span class="word1"><?php echo (isset($betashop_options['upsells_title'])) ? esc_html($betashop_options['upsells_title']) : esc_html__('Upsell Products','vg-betashop'); ?></span></h3></div>
	
	<div class="upsells products">

		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $upsells as $upsell ) : ?>

				<?php
				 	$post_object = get_post( $upsell->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object );

					wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>
	</div>
</div>
<?php endif;

wp_reset_postdata();
