<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if(! defined('ABSPATH')) {
	exit;
}

global $product, $woocommerce_loop;
global $betashop_options;

if ( $related_products ) : ?>

<div class="widget related_products_widget">
	<div class="vg-front-page-title2 inner-title"><h3><span class="word1"><?php echo (isset($betashop_options['related_title'])) ? esc_html($betashop_options['related_title']) : esc_html__('Related Products','vg-betashop'); ?></span></h3></div>
	
	<div class="related products">
	
		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $related_products as $related_product ) : ?>

				<?php
				 	$post_object = get_post( $related_product->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object );

					wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</div>
</div>

<?php endif;

wp_reset_postdata();
