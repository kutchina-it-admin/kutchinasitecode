		<!-- <div class="row-breadcrumb">
			<div class="container">
			<?php				
				//do_action('woocommerce_before_main_content');
			?>
			</div>
		</div> -->


		<div class="inner_banner_section">
		  <div class="product_banner">		  	
		  	<?php
				global $wp_query, $betashop_options;
				$catt = get_queried_object()->term_id;
				// echo $catt;
				if ( is_product_category() || is_shop() || is_product_tag()){
					$image = '';

					if(is_product_category() || is_product_tag()) {
						$cat = $wp_query->get_queried_object();
						$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
						$image = wp_get_attachment_url( $thumbnail_id );
					}					
				    if ( ($image && !empty($image))) { ?>
					   <div class="category-image-desc">
					   	<a href="#archive-product"><img class="mineee_imagee_cstmmee" src="<?php echo esc_url($image); ?>" alt=""></a>
					   </div>
					<?php } else if(isset($betashop_options['cat_banner_link']) && !empty($betashop_options['cat_banner_link']) && isset($betashop_options['cat_banner_img']) && !empty($betashop_options['cat_banner_img'])) { ?>
						<div class="category-image-desc vg-cat-img">
							<a href="<?php echo esc_url($betashop_options['cat_banner_link']); ?>">
								<img class="mineee_imagee_cstmmee" src="<?php echo esc_url($betashop_options['cat_banner_img']['url']); ?>" alt="">
							</a>
						</div>
					<?php } else if(isset($betashop_options['cat_banner_img']['url']) && !empty($betashop_options['cat_banner_img']['url'])) { ?>
						<div class="category-image-desc vg-cat-img">
							<img class="mineee_imagee_cstmmee" src="<?php echo esc_url($betashop_options['cat_banner_img']['url']); ?>" alt="">
						</div>
					<?php } else if(empty($image)) {  ?>
						<?php if($catt == 145) {  ?>
						
						
							<!-- Modular Kitchen Slider Start -->
	<!--						<div class="category-image-desc vg-cat-img slider">
								<img class="mineee_imagee_cstmmee" src="https://www.kutchina.com/wp-content/uploads/2015/10/kutchina-modular-kitchen.jpg" alt="">
							</div>-->
							
							
							<div class="banner_section">
								<div id="owl-demo" class="owl-carousel owl-theme bnr_static">
								    <div class="item">
								    	<div class="banner_pic"><a href="<?php echo site_url(); ?>/product/l-shaped-luxury-kitchen-designs-modular-kitchen/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/modular-kitchens_banner_1.jpeg" alt=""></a></div>
								    </div>
									<div class="item">
										<div class="banner_pic"><a href="<?php echo site_url(); ?>/product/island-shape-modular-kitchen/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/modular-kitchens_banner_2.jpeg" alt=""></a></div>
									</div>
									<div class="item">
										<div class="banner_pic"><a href="<?php echo site_url(); ?>/product/kitchen-island-pendant-lighting/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/modular-kitchens_banner_3.jpeg" alt=""></a></div>
									</div>

									<div class="item">
										<div class="banner_pic"><a href="<?php echo site_url(); ?>/product/u-shape-modular-kitchen/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/modular-kitchens_banner_4.jpeg" alt=""></a></div>
									</div>

									<div class="item">
										<div class="banner_pic"><a href="<?php echo site_url(); ?>/product/l-shaped-kitchen-with-corner-pantry-modular-kitchen/"><img src="<?php bloginfo ( 'template_url' ); ?>/images_new/modular-kitchens_banner_5.jpeg" alt=""></a></div>
									</div>

								</div>
								<!-- <div class="customNavigation">
								  	<a class="btn prev">Previous</a>
								  	<a class="btn next">Next</a>
								</div>-->
							</div>
							
							<!-- Modular Kitchen Slider End -->
						<?php } else { ?>
							<div class="category-image-desc vg-cat-img">
								<img class="mineee_imagee_cstmmee" src="<?php echo esc_url(get_template_directory_uri() . '/images/category-image.jpg') ; ?>" alt="">
							</div>
						<?php } ?>
					<?php 
					}
				}

			?>
		  </div>		  
		</div>
		
		
		
	<script>
jQuery(document).ready(function() {
 
var owl = jQuery("#owl-demo");
 
  owl.owlCarousel({
      items : 1, //10 items above 1000px browser width
      itemsDesktop : [1000,1], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : true, // itemsMobile disabled - inherit from itemsTablet option
      autoPlay: 7000
  });
 
  // Custom Navigation Events
  jQuery(".next").click(function(){
    owl.trigger('owl.next');
  })
  jQuery(".prev").click(function(){
    owl.trigger('owl.prev');
  })

 
});
</script>	
		
		
