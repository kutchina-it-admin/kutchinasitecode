<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

// Find the category + category parent, if applicable 
$term = get_queried_object(); 
$parent_id = empty( $term->term_id ) ? 0 : $term->term_id; 

// NOTE: using child_of instead of parent - this is not ideal but due to a WP bug ( http://core.trac.wordpress.org/ticket/15626 ) pad_counts won't work
$args = array(
	'child_of'		=> $parent_id,
	'menu_order'	=> 'ASC',
	'hide_empty'	=> 0,
	'hierarchical'	=> 1,
	'taxonomy'		=> 'product_cat',
	'pad_counts'	=> 1
);
$product_subcategories = get_categories( $args  );

betashop_get_header(); ?>
<?php
$betashop_options  = betashop_get_global_variables(); 
?>
<?php 
$bloglayout = 'left';
$blogsidebar = 'left';
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$bloglayout = $_GET['sidebar'];
	
	switch($bloglayout) {
		case 'right':
			$blogclass = 'sidebar-right';
			$blogcolclass = 9;
			$blogsidebar = 'right';
			break;
		default:
			$blogcolclass = 9;
	}
}else {
	if(isset($betashop_options['sidebar_pos']) && $betashop_options['sidebar_pos']!=''){
		$blogsidebar = $betashop_options['sidebar_pos'];
	}	
	switch($blogsidebar) {
		case 'right':
			$blogclass = 'sidebar-right';
			$blogcolclass = 9;
			$blogsidebar = 'right';
			break;
		default:
			$blogcolclass = 9;
	}
}
?>
<div class="main-container page-shop sidebar-<?php echo (isset($betashop_options['sidebar_pos'])) ? $betashop_options['sidebar_pos'] : ''; ?>">
	<div class="page-content section33">
		<!-- <div class="row-breadcrumb">
			<div class="container">
			<?php				
				//do_action('woocommerce_before_main_content');
			?>
			</div>
		</div> -->


		<div class="inner_banner_section">
		  <div class="product_banner">		  	
		  	<?php
				global $wp_query, $betashop_options;
				if ( is_product_category() || is_shop() || is_product_tag()){
					$image = '';

					if(is_product_category() || is_product_tag()) {
						$cat = $wp_query->get_queried_object();
						$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
						$image = wp_get_attachment_url( $thumbnail_id );
					}					
				    if ( ($image && !empty($image))) { ?>
					   <div class="category-image-desc">
					   	<img class="mineee_imagee_cstmmee" src="<?php echo esc_url($image); ?>" alt="">
					   </div>
					<?php } else if(isset($betashop_options['cat_banner_link']) && !empty($betashop_options['cat_banner_link']) && isset($betashop_options['cat_banner_img']) && !empty($betashop_options['cat_banner_img'])) { ?>
						<div class="category-image-desc vg-cat-img">
							<a href="<?php echo esc_url($betashop_options['cat_banner_link']); ?>">
								<img class="mineee_imagee_cstmmee" src="<?php echo esc_url($betashop_options['cat_banner_img']['url']); ?>" alt="">
							</a>
						</div>
					<?php } else if(isset($betashop_options['cat_banner_img']['url']) && !empty($betashop_options['cat_banner_img']['url'])) { ?>
						<div class="category-image-desc vg-cat-img">
							<img class="mineee_imagee_cstmmee" src="<?php echo esc_url($betashop_options['cat_banner_img']['url']); ?>" alt="">
						</div>
					<?php } else if(empty($image)) {  ?>
<!-- 						<div class="category-image-desc vg-cat-img">
	<img class="mineee_imagee_cstmmee" src="<?php echo esc_url(get_template_directory_uri() . '/images/category-image.jpg') ; ?>" alt="">
</div> -->
					<?php 
					}
				}

			?>
		  </div>		  
		</div>










		<div class="container">			
			<div class="row">




				
				<div id="archive-product" class="col-xs-12 col-md-<?php echo (is_active_sidebar( 'sidebar-category' )) ? 9 : 12 ; ?>">
					<div class="archive-border">
						<?php
							/**
							* remove message from 'woocommerce_before_shop_loop' and show here
							*/
							do_action('woocommerce_show_message');
						?>	
						<div class="catheader-wrapper">
							<div class="cate-des right_panel_text">
								<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
									<header class="entry-header">
										<h2 class="entry-title"><?php woocommerce_page_title(); ?></h2>
									</header>

								<?php endif; ?>

								<div class="total_divv">													
									<p><?php do_action('woocommerce_archive_description'); ?></p>

										<?php
											$current_category = single_cat_title("", false);
											if ($current_category == 'Small Appliances') {
										?>
									
											<div class="custom_cat_button">
												<a href="https://www.kutchina.com/partner" class="btn btn-info" role="button">Locate Us</a>
											</div>

										<?php } ?>

										<?php
											$current_category = single_cat_title("", false);
											if ($current_category == 'Water Purifiers') {
										?>
									
											<div class="custom_cat_button custom_cat_button_water">
												<a href="https://www.kutchina.com/partner" class="btn btn-info btn_water" role="button">Locate Us</a>
											</div>

										<?php } ?>
									
								</div>


							</div>
						</div>
	

<!-- ////////////////////////////////////////////// Custom changes by SUMAN START /////////////////////////////////////////// -->

					
					<?php
					    $parentid = get_queried_object_id();    
					    $args = array(
					        'parent' => $parentid
					    );
					    $categories = get_terms(
					        'product_cat', $args
					    );
					    //$counter = 0;
					    if ( $categories ) :
					        foreach ( $categories as $category ) :

					        	//$counter++;

					            $category_thumbnail = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);
					        	$image = wp_get_attachment_url($category_thumbnail);
					        	$link = get_term_link( $category->slug, $category->taxonomy );

					        	//if( $counter % 2 == 0 ) :

						        //echo '<div class="row">';
					        	/*echo '<ul class="proddiiii_subbb_cattii">';
					        	echo '<li>';

					        		echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-padding pc_box_main gbox_main1">';
						        		echo '<div class="pc_text_box">';
						        			echo esc_html($category->name);
						        		echo '</div>';

						        		echo '<div class="g_enquire_now_pc"><a href="#"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Enquire Now</a></div>';
						        	echo '</div>';

						    		echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-padding">';
						    			echo '<div class="gimg_box_p2">';
						        			echo '<a href="'. $link .'"><img src="'.$image.'"></a>';
						        		echo '</div>';
						        	echo '</div>';

						        echo '</ul>';
						        echo '</li>';

					        	else:*/

					        	echo '<ul class="proddiiii_subbb_cattii">';
						        	echo '<li>';

							    		echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-padding">';
							    			echo '<div class="gimg_box_p2">';
							        			echo '<a href="'. $link .'"><img src="'.$image.'"></a>';
							        		echo '</div>';
							        	echo '</div>';

							        	echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-padding pc_box_main gbox_main1">';
							        		echo '<div class="pc_text_box">';
							        			echo '<h2>';
							        				echo esc_html($category->name);
							        			echo '</h2>';
							        		echo '</div>';

							        		echo '<div class="g_enquire_now_pc"><a href="#"><span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>Enquire Now</a></div>';
							        	echo '</div>';

							        echo '</li>';
						        echo '</ul>';

					        	//endif;
					        	
					        	

					        endforeach;
					    endif;
					?>
					
					<div class="clearfix"></div>




					<!--<ul class="wsubcategs">
						<?php $curr_taxoo_id = get_queried_object()->term_id; ?>
						<?php
						$wsubargs = array(
						    'hierarchical' => 1,
						    'show_option_none' => '',
						    'hide_empty' => 0,
						    'parent' => $category->term_id,
						    'taxonomy' => 'product_cat'
						);
						$wsubcats = get_categories($wsubargs);
						foreach ($wsubcats as $wsc):
						?>
						    <li>        
						        <div class="products">
						            <?php echo do_shortcode('[product_category category="'.$wsc->slug.'"]');?>
						        </div>
						    </li>
						<?php endforeach;?>
					</ul> -->



<!-- ////////////////////////////////////////////// Custom changes by SUMAN END /////////////////////////////////////////// -->

						





					<?php
						/**
						 * woocommerce_after_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
						 */
						do_action('woocommerce_after_main_content');
					?>

					<?php
						/**
						 * woocommerce_sidebar hook
						 *
						 * @hooked woocommerce_get_sidebar - 10
						 */
						//do_action('woocommerce_sidebar');
					?>

					</div>

<div class="cstmmm_designnn_prodiii">
					<?php 
						// $categories = get_the_category();
						// $cat = $categories[0]->term_id;
						// echo $cat;
						$catt = get_queried_object()->term_id;

						if ($catt != 147 ) {
							
					?>

					
					<?php if (have_posts()) : ?>						
						
						<?php do_action('woocommerce_after_shop_header'); ?>						
						
						<?php if((is_shop() && '' !== get_option('woocommerce_shop_page_display')) || (is_product_category() && '' !== get_option('woocommerce_category_archive_display'))) : ?>
						<div class="all-subcategories">
							<?php woocommerce_product_subcategories(); ?>
							<div class="clearfix"></div>
						</div>
						<?php endif; ?>
						
						<?php if((is_shop() && 'subcategories' !== get_option('woocommerce_shop_page_display')) || (is_product_category() && 'subcategories' !== get_option('woocommerce_category_archive_display')) || (empty($product_subcategories) && 'subcategories' == get_option('woocommerce_category_archive_display')) || is_product_tag()): ?>
						<div class="toolbar canging_tabbiii">
							<div class="view-mode">
								<a href="#" class="grid active" title="<?php echo esc_attr__('Grid', 'vg-betashop'); ?>"><i class="fa fa-th-large"></i> <strong><?php echo esc_html__('Grid', 'vg-betashop'); ?></strong></a>
								<a href="#" class="list" title="<?php echo esc_attr__('List', 'vg-betashop'); ?>"><i class="fa fa-th-list"></i> <strong><?php echo esc_html__('List', 'vg-betashop'); ?></strong></a>
							</div>
							<?php do_action('woocommerce_before_shop_loop'); ?>
							<div class="clearfix"></div>
						</div>
						<?php endif; ?>
						
						<?php woocommerce_product_loop_start(); ?>								
							<?php $woocommerce_loop['loop'] = 0; while (have_posts()) : the_post(); ?>
								<?php wc_get_template_part('content', 'product'); ?>
							<?php endwhile; // end of the loop. ?>
						<?php woocommerce_product_loop_end(); ?>
						
						<?php if((is_shop() && 'subcategories' !== get_option('woocommerce_shop_page_display')) || (is_product_category() && 'subcategories' !== get_option('woocommerce_category_archive_display')) || (empty($product_subcategories) && 'subcategories' == get_option('woocommerce_category_archive_display')) || is_product_tag()): ?>
							<div class="toolbar tb-bottom">
								<?php do_action('woocommerce_after_shop_loop'); ?>
								<div class="clearfix"></div>
							</div>
						<?php endif; ?>
						
					<?php elseif (! woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>
						<?php wc_get_template('loop/no-products-found.php'); ?>
					<?php endif; ?>



				<?php } ?>
			</div>


					
				</div>

				<?php if($blogsidebar=='left') : ?>
					<?php get_sidebar('category'); ?>
				<?php endif; ?>
				
				<?php if($blogsidebar=='right') : ?>
					<?php get_sidebar('category'); ?>
				<?php endif; ?>





				





			</div>  <!-- Row Class End -->
		</div>
	</div>
</div>
<?php do_action('woocommerce_after_girdview'); ?>
<?php betashop_get_footer(); ?>