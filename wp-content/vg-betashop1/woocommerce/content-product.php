<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global  $woocommerce_loop;

$betashop_options  		= betashop_get_global_variables();
$product  				= betashop_get_global_variables('product');
$betashop_productsfound  	= betashop_get_global_variables('betashop_productsfound');
$post  					= betashop_get_global_variables('post');

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Extra post classes
if (0 == ($woocommerce_loop['loop']) % $woocommerce_loop['columns'] || 0 == $woocommerce_loop['columns']) {
 $classes[] = 'first';
}
if (0 == ($woocommerce_loop['loop'] + 1) % $woocommerce_loop['columns']) {
 $classes[] = 'last';
}

if(is_product_category()) {
	if (  1 === ( $woocommerce_loop['loop'] % ($woocommerce_loop['columns'] - 1))) {
		$classes[] = 'first-sm';
	}
}

if ($woocommerce_loop['columns'] == 3 ) {
	$colwidth = 4;
	$colwidth_sm = 6;
} else {
	$colwidth = 3;
	$colwidth_sm = 4;
}

$classes[] = ' item-col col-lg-'. esc_attr($colwidth) .' col-sm-'. esc_attr($colwidth_sm) .' col-xs-6';?>

<div <?php post_class($classes); ?>>
	<div class="vgwc-item style1">
		<div class="ma-box-content">
			<?php do_action('woocommerce_before_shop_loop_item'); ?>
			
			<div class="list-col4">
				<div class="vgwc-image-block">
					<a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
						<?php 
						echo $product->get_image('shop_catalog', array('class'=>'primary_image'));
						$image_second = '';
						if(isset($betashop_options['second_image']) && $betashop_options['second_image']){
							$attachment_ids = $product->get_gallery_image_ids();
							if($attachment_ids[0] && ($attachment_ids[0] != get_post_thumbnail_id(get_the_ID()))) {
								$image_second = wp_get_attachment_image( $attachment_ids[0], apply_filters( 'single_product_small_thumbnail_size', 'shop_catalog' ), false, array('class'=>'secondary_image') );
							}
							elseif(isset($attachment_ids[1])){
								$image_second = wp_get_attachment_image( $attachment_ids[1], apply_filters( 'single_product_small_thumbnail_size', 'shop_catalog' ), false, array('class'=>'secondary_image') );
							}	
						}
						echo $image_second;
						?>
					</a>
					
					<?php if ($product->is_featured()) : ?>
						<?php echo apply_filters('woocommerce_featured_flash', '<div class="vgwc-label vgwc-featured">' . esc_html__('Hot', 'vg-betashop') . '</div>', $post, $product); ?>
					<?php endif; ?>
					
					<?php if ($product->is_on_sale() && $product->is_featured() ) : ?>
						<?php echo apply_filters('woocommerce_sale_flash', '<div class="vgwc-label vgwc-onsale sale-featured">' . esc_html__('Sale', 'vg-betashop') . '</div>', $post, $product); ?>
					<?php elseif ($product->is_on_sale()) : ?>
						<?php echo apply_filters('woocommerce_sale_flash', '<div class="vgwc-label vgwc-onsale">' . esc_html__('Sale', 'vg-betashop') . '</div>', $post, $product); ?>
					<?php endif; ?>
					<?php do_action('get_betashop_product_countdown');?>
				</div>
			</div>
			<div class="list-col8">
				<div class="gridview">
					<div class="vgwc-text-block">
						<h3 class="vgwc-product-title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						
						<?php echo $product->get_price_html(); ?>
						
						<?php if (get_option('woocommerce_enable_review_rating') === 'yes') { ?>
							<?php echo wc_get_rating_html( $product->get_average_rating()); ?>
						<?php } ?>	
						
						<div class="vgwc-button-group">
							<div class="vgwc-add-to-cart">
								<?php echo do_shortcode('[add_to_cart id="'.esc_attr($product->get_id()).'"  style="none" show_price="false"]') ?>
							</div>
							<div class="add-to-links">
							
								<?php if (class_exists('YITH_WCWL')) {
									echo '<div class="vgwc-wishlist">'.preg_replace("/<img[^>]+\>/i", " ", do_shortcode('[yith_wcwl_add_to_wishlist]')). '</div>';
								} ?>
								<?php if(class_exists('YITH_Woocompare')) {
									echo '<div class="vgwc-compare">'. do_shortcode('[yith_compare_button]') . '</div>';
								} ?>
							</div>
							<?php if(isset($betashop_options['quick_view']) && $betashop_options['quick_view']){ ?>
								<div class="vgwc-quick">
									<a class="quickview quick-view" data-quick-id="<?php the_ID();?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php esc_html_e('Quick View', 'vg-betashop');?></a>
								</div>
							<?php } ?>
						</div>						
					</div>
				</div>
				<div class="listview">
					<div class="vgwc-text-block">
						<div class="vgwc-product-title">
							<h3>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>	
						</div>
						
						<?php if (get_option('woocommerce_enable_review_rating') === 'yes') { ?>
							<?php echo wc_get_rating_html( $product->get_average_rating()); ?>
						<?php } ?>
						
						<?php echo $product->get_price_html(); ?>
						
						<div class="product-desc"><?php the_excerpt(); ?></div>
						
						<div class="vgwc-button-group">
							<div class="vgwc-add-to-cart">
								<?php echo do_shortcode('[add_to_cart id="'.esc_attr($product->get_id()).'"  style="none" show_price="false"]') ?>
							</div>
							<div class="add-to-links">
								<?php if (class_exists('YITH_WCWL')) {
									echo '<div class="vgwc-wishlist">'.preg_replace("/<img[^>]+\>/i", " ", do_shortcode('[yith_wcwl_add_to_wishlist]')). '</div>';
								} ?>
								<?php if(class_exists('YITH_Woocompare')) {
									echo '<div class="vgwc-compare">'. do_shortcode('[yith_compare_button]') . '</div>';
								} ?>
							</div>
							<?php if(isset($betashop_options['quick_view']) && $betashop_options['quick_view']){ ?>
								<div class="vgwc-quick">
									<a class="quickview quick-view" data-quick-id="<?php the_ID();?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php esc_html_e('Quick View', 'vg-betashop');?></a>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<?php //do_action('woocommerce_after_shop_loop_item'); ?>
		</div>
	</div>
</div>