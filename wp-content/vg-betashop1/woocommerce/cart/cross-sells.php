<?php
/**
 * Cross-sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if(! defined('ABSPATH')) {
	exit;
}

global $product, $woocommerce_loop;
global $betashop_options;

if ( $cross_sells ) : ?>

	<div class="cross-sells">

		<div class="vg-front-page-title2 inner-title"><h3><span class="word1"><?php echo (isset($betashop_options['crosssells_title'])) ? esc_html($betashop_options['crosssells_title']) : esc_html__('You may be interested in','vg-betashop'); ?></span></h3></div>
		<div class="cross-carousel">
			<?php woocommerce_product_loop_start(); ?>

				<?php foreach ( $cross_sells as $cross_sell ) : ?>

					<?php
						$post_object = get_post( $cross_sell->get_id() );

						setup_postdata( $GLOBALS['post'] =& $post_object );

						wc_get_template_part( 'content', 'product' ); ?>

				<?php endforeach; ?>

			<?php woocommerce_product_loop_end(); ?>
		</div>
	</div>

<?php endif;

wp_reset_postdata();