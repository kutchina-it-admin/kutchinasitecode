<?php
session_start();
wp_enqueue_script('contact_jackson',get_template_directory_uri() . '/service-center-admin/js/validation.js' , array( 'jquery' ));
wp_localize_script('contact_jackson', 'MyAjax', array( 'ajaxurl' => admin_url('admin-ajax.php')));


function generic_admin_manage_contact_form(){
       $dir = get_template_directory_uri();
      
	add_menu_page( 'Service Center', 'Service Center', 'manage_options', 'service-center', 'contact_form_dynamic_fn',  $dir.'/service-center-admin/images/pen.png' , 25 ); 
}
add_action( 'admin_menu', 'generic_admin_manage_contact_form' );


function generic_home_page_option_fn() {

	add_submenu_page( 'service-center', 'Service Center List', 'Service Center List', 'manage_options', 'service-center-list','list_service_center_fn');
    
}
add_action('admin_menu', 'generic_home_page_option_fn');


function contact_form_dynamic_fn(){

global $wpdb;
$state_list_str = "SELECT * FROM `kut_state` where 1 "; 
$state_list = $wpdb->get_results($state_list_str, OBJECT);
//print_r($state_list); 

if(isset($_GET['statename']))
{

	$statename = $_GET['statename'];
	//echo $statename; exit;
	$single_state_str = "SELECT * FROM `kut_state` WHERE LOWER(`state_name`) = '".strtolower($statename)."' "; 
    $single_state_posts = $wpdb->get_row($single_state_str, OBJECT);
    //print_r($single_state_posts); exit;
    $state_id = $single_state_posts->id;


	$city_list_str = "SELECT * FROM `kut_city` WHERE `state_id`= ".$state_id; 
	$city_list = $wpdb->get_results($city_list_str, OBJECT);
	//print_r($city_list); 
}
else
{
	$city_list_str = "SELECT * FROM `kut_city` WHERE 1"; 
	$city_list = $wpdb->get_results($city_list_str, OBJECT);
}


?>

 <script>
 	
	function get_city_by_state()
	{
		//alert("aa");
		var state_name = $("#state").val();
		//alert(state_name);
		var url = window.location.href;
		window.location.href = url + "&statename="+state_name
	}

	function add_service_center()
	{
		//alert("aa");
		var state = $("#state").val();
		var city = $("#city").val();
		var incharge_name = $("#incharge_name").val();
		var address = $("#address").val();
		
		var contact_no = $("#contact_no").val();
		var mail_id = $("#mail_id").val();
		var fde = $("#fde").val();
		var office_land_no = $("#office_land_no").val();
		var office_mail_id = $("#office_mail_id").val();
        var service_edit_id = $("#service_edit_id").val();
		

		var site_url = '<?php echo site_url(); ?>/ajax-files/add_service_center.php';
		jQuery.ajax({		
					type: 'POST',
					url: site_url,
					data: {"state":state,"city":city,"incharge_name":incharge_name,"address":address,"contact_no":contact_no,"mail_id":mail_id,"fde":fde,"office_land_no":office_land_no,"office_mail_id":office_mail_id,"service_edit_id":service_edit_id},
					success: function(data){
				   		//alert(data);
				   		if(data == 1) 
				   		{
				   			alert("Service center added.");
				   			window.location.href="<?php echo admin_url();?>admin.php?page=service-center";
				   		}
						else if(data == 2) 
				   		{
				   			alert("Service center updated.");
							window.location.href="<?php echo admin_url();?>admin.php?page=service-center&service_id="+service_edit_id;
				   		}
				   		else if(data == -1) 
				   		{
				   			alert("Service center could not be added.");
				   		}
				  	
				 
					}
				});

	}

</script>
<h1>Add Service Centers</h1>
<?php 
	global $wpdb;
	if($_GET['service_id'] != '')
	{
		 $service_det_edit = $wpdb->get_results("SELECT * FROM `kut_support`  WHERE `id`= ".$_GET['service_id']) ;
	}
?>
		<form>
<input type="hidden" name="service_edit_id" id="service_edit_id" value="<?php echo $_GET['service_id']; ?>">
						<div class="form-group">
                            <div class="col-sm-12">
                              <select name="state" id="state" onchange="get_city_by_state();">
                              	<option value="">Select State</option>
                              	<?php 
                              		for($i=0;$i<sizeof($state_list);$i++){ 
                              	?>
                              	<option value="<?php echo $state_list[$i]->state_name;?>" <?php if( strtolower($statename) == strtolower($state_list[$i]->state_name) ) echo "selected"; ?>><?php echo $state_list[$i]->state_name;?></option>
                              	<?php } ?>
                              </select>
                            </div>
                        </div>

				        <div class="form-group">
                            <div class="col-sm-12">
                             <select name="city" id="city">
                              	<option value="">Select City</option>
                              	<?php 
                              		for($j=0;$j<sizeof($city_list);$j++){ 
                              	?>
                              	<option value="<?php echo $city_list[$j]->city_name;?>"><?php echo $city_list[$j]->city_name;?></option>
                              	<?php } ?>
                              </select>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="col-sm-12">
                              <input type="text" name="incharge_name" id="incharge_name" class="form-control" placeholder="Incharge Name:" value="<?php echo $service_det_edit[0]->incharges_name ;  ?>"  >
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="col-sm-12">
                              <textarea class="form-control"  name="address" id="address" placeholder="Address:"><?php echo $service_det_edit[0]->address ;  ?></textarea>
                            </div>
                        </div>
			
			
						 <div class="form-group">
                            <div class="col-sm-12">
                              <input type="text" name="contact_no" id="contact_no" class="form-control" placeholder="Incharge Contact No:" value="<?php echo $service_det_edit[0]->contact_no ;  ?>" >
                            </div>
                        </div>
			
						<div class="form-group">
                            <div class="col-sm-12">
                              <input type="text" name="mail_id" id="mail_id" class="form-control" placeholder="Incharge Mail Id:" value="<?php echo $service_det_edit[0]->mail_id_incharges ;  ?>" >
                            </div>
                        </div>
			
						<div class="form-group">
                            <div class="col-sm-12">
                              <input type="text" name="fde" id="fde" class="form-control" placeholder="FDE:" value="<?php echo $service_det_edit[0]->FDE ;  ?>">
                            </div>
                        </div>
			
						<div class="form-group">
                            <div class="col-sm-12">
                              <input type="text" name="office_land_no" id="office_land_no" class="form-control" placeholder="Office Land No" value="<?php echo $service_det_edit[0]->office_land_no ;  ?>" >
                            </div>
                        </div>
			
						<div class="form-group">
                            <div class="col-sm-12">
                              <input type="text" name="office_mail_id" id="office_mail_id" class="form-control" placeholder="Office Mail Id"  value="<?php echo $service_det_edit[0]->office_mail_id ;  ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                             <input type="button" name="submit" value="Submit" onclick="add_service_center();">
                            </div>
                        </div>

		</form>

<?php 
} 
function list_service_center_fn()
{
	global $wpdb;
	$support_list_str = "SELECT * FROM `kut_support` where 1 "; 
	$support_list = $wpdb->get_results($support_list_str, OBJECT);
	//print_r($support_list);
?>
<h2>Service Center List</h2>
<table>
	<tr>
		<th>State</th>
		<th>City</th>
		<th>Address</th>
		<th>Incharge Name</th>
		<th>Contact No</th>
		<th>Mail Id Of Incharge</th>
		<th>FDE</th>
		<th>Office Land No</th>
		<th>Office Mail Id</th>
		<th>Action</th>
	</tr>
<?php 
	for($i=0;$i<sizeof($support_list);$i++)
	{
?>
<tr>
	<td><?php echo $support_list[$i]->state;?></td>
	<td><?php echo $support_list[$i]->city;?></td>
	<td><?php echo $support_list[$i]->address;?></td>
	<td><?php echo $support_list[$i]->incharges_names;?></td>
	<td><?php echo $support_list[$i]->contact_no;?></td>
	<td><?php echo $support_list[$i]->mail_id_incharges;?></td>
	<td><?php echo $support_list[$i]->FDE;?></td>
	<td><?php echo $support_list[$i]->office_land_no;?></td>
	<td><?php echo $support_list[$i]->office_mail_id;?></td>
	<td><a href="<?php echo admin_url();?>admin.php?page=service-center&service_id=<?php echo $support_list[$i]->id; ?>" target="_blank">Edit</a> </td>
</tr>
<?php	
	}
?>
</table>
<?php } ?>