<?php
$betashop_options  = betashop_get_global_variables();

$webLayout = betashop_get_layout();

switch($webLayout)
{
	case "layout-1":
		get_template_part('vgwc-themes/theme/layout', '1' );
	break;
	case "layout-2":
		get_template_part('vgwc-themes/theme/layout', '2' );
	break;
	case "layout-3":
		get_template_part('vgwc-themes/theme/layout', '3' );
	break;
	case "layout-4":
		get_template_part('vgwc-themes/theme/layout', '4' );
	break;
	case "layout-5":
		get_template_part('vgwc-themes/theme/layout', '5' );
	break;
	case "layout-6":
		get_template_part('vgwc-themes/theme/layout', '6' );
	break;
	default:
		get_template_part('vgwc-themes/theme/layout', '1' );
	break;
}